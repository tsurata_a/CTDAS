#!/usr/bin/env python
# ct_statevector_tools.py

"""
.. module:: statevector
.. moduleauthor:: Wouter Peters 

Revision History:
File created on 28 Jul 2010.
Adapted by super004 on 26 Jan 2017.

The module statevector implements the data structure and methods needed to work with state vectors (a set of unknown parameters to be optimized by a DA system) of different lengths, types, and configurations. Two baseclasses together form a generic framework:
    * :class:`~da.baseclasses.statevector.StateVector`
    * :class:`~da.baseclasses.statevector.EnsembleMember`

As usual, specific implementations of StateVector objects are done through inheritance form these baseclasses. An example of designing 
your own baseclass StateVector we refer to :ref:`tut_chapter5`.

.. autoclass:: da.baseclasses.statevector.StateVector 

.. autoclass:: da.baseclasses.statevector.EnsembleMember 

"""

import os
import sys
sys.path.append(os.getcwd())

import logging
import numpy as np
from da.baseclasses.statevector import StateVector, EnsembleMember
from da.tools.general import create_dirs, to_datetime
import datetime as dtm
import da.tools.io4 as io

identifier = 'CarbonTracker Statevector '
version = '0.0'

################### Begin Class CO2StateVector ###################

class CO2StateVector(StateVector):

    def __init__(self, dacycle=None):
        if dacycle != None:
            self.dacycle = dacycle
        else:
            self.dacycle = {}
    
    def setup(self, dacycle):
        """
        setup the object by specifying the dimensions. 
        There are two major requirements for each statvector that you want to build:
        
            (1) is that the statevector can map itself onto a regular grid
            (2) is that the statevector can map itself (mean+covariance) onto TransCom regions

        An example is given below.
        """

        self.dacycle = dacycle
        self.nlag = int(self.dacycle['time.nlag'])
        self.nmembers = int(self.dacycle['da.optimizer.nmembers'])
        self.nparams = int(self.dacycle.dasystem['nparameters'])
        self.obsdir = self.dacycle.dasystem['datadir']
        self.pparam = self.dacycle.dasystem['emis.pparam']
        self.covm = self.dacycle.dasystem['ff.covariance']
        self.prop = int(self.dacycle.dasystem['run.propscheme'])
        self.nobs = 0
        
        self.obs_to_assimilate = ()  # empty containter to hold observations to assimilate later on

        # These list objects hold the data for each time step of lag in the system. Note that the ensembles for each time step consist 
        # of lists of EnsembleMember objects, we define member 0 as the mean of the distribution and n=1,...,nmembers as the spread.

        self.ensemble_members = [[] for n in range(self.nlag)]
#        self.ensemble_members = list(range(self.nlag))
#
#        for n in range(self.nlag):
#            self.ensemble_members[n] = []


        # This specifies the file to read with the gridded mask at 1x1 degrees. Each gridbox holds a number that specifies the parametermember
        #  that maps onto it. From this map, a dictionary is created that allows a reverse look-up so that we can map parameters to a grid.

        ## Initialise an array with an element for each parameter to optimise
        self.gridmap = np.arange(1,self.nparams+1,1)

        # Create a dictionary for state <-> gridded map conversions

        nparams = self.gridmap.max()
        self.griddict = {}
        for r in range(1, int(nparams) + 1):
            sel = (self.gridmap.flatten() == r).nonzero()
            if len(sel[0]) > 0: 
                self.griddict[r] = sel

        logging.debug("A dictionary to map grids to states and vice versa was created")

        # Create a mask for species/unknowns

        self.make_species_mask()
        
    def get_covariance(self, date, dacycle):
        
        file=os.path.join(self.obsdir,self.covm)
        f = io.ct_read(file, 'read')
        covmatrix = f.get_variable('covariances')[:self.nparams,:self.nparams]
        f.close()
        
        return covmatrix
    
    def write_members_to_file(self, lag, outdir,endswith='.nc'):
        """ 
           :param: lag: Which lag step of the filter to write, must lie in range [1,...,nlag]
           :param: outdir: Directory where to write files
           :param: endswith: Optional label to add to the filename, default is simply .nc
           :rtype: None

           Write ensemble member information to a NetCDF file for later use. The standard output filename is 
           *parameters.DDD.nc* where *DDD* is the number of the ensemble member. Standard output file location 
           is the `dir.input` of the dacycle object. In principle the output file will have only two datasets inside 
           called `parametervalues` which is of dimensions `nparameters` and `parametermap` which is of dimensions (180,360). 
           This dataset can be read and used by a :class:`~da.baseclasses.observationoperator.ObservationOperator` object. 

           .. note:: if more, or other information is needed to complete the sampling of the ObservationOperator you
                     can simply inherit from the StateVector baseclass and overwrite this write_members_to_file function.

        """

        # These import statements caused a crash in netCDF4 on MacOSX. No problems on Jet though. Solution was
        # to do the import already at the start of the module, not just in this method.
           
        #import da.tools.io as io
        #import da.tools.io4 as io

        members = self.ensemble_members[lag]

        for mem in members:
            filename = os.path.join(outdir, 'parameters.%03d%s' % (mem.membernumber, endswith))
            ncf = io.CT_CDF(filename, method='create')
            dimparams = ncf.add_params_dim(self.nparams)

            data = mem.param_values

            savedict = io.std_savedict.copy()
            savedict['name'] = "parametervalues"
            savedict['long_name'] = "parameter_values_for_member_%d" % mem.membernumber
            savedict['units'] = "unitless"
            savedict['dims'] = dimparams 
            savedict['values'] = data
            savedict['comment'] = 'These are parameter values to use for member %d' % mem.membernumber
            ncf.add_data(savedict)

            ncf.close()

            logging.debug('Successfully wrote data from ensemble member %d to file (%s) ' % (mem.membernumber, filename))
    
    def make_new_ensemble(self, lag, covariancematrix=None):
        """ 
        :param lag: an integer indicating the time step in the lag order
        :param covariancematrix: a matrix to draw random values from
        :rtype: None
    
        Make a new ensemble, the attribute lag refers to the position in the state vector. 
        Note that lag=1 means an index of 0 in python, hence the notation lag-1 in the indexing below.
        The argument is thus referring to the lagged state vector as [1,2,3,4,5,..., nlag]

        The optional covariance object to be passed holds a matrix of dimensions [nparams, nparams] which is
        used to draw ensemblemembers from. If this argument is not passed it will ne substituted with an 
        identity matrix of the same dimensions.

        """
        dacycle = self.dacycle    
        self.seed = int(self.dacycle.dasystem['random.seed'])
        if self.seed != 0:
            np.random.seed(self.seed)
            sds = np.random.randint(1,10000,1000)
        else:
            sds = np.random.randint(1,10000,1000)
        sid = (dacycle['time.start'] - dacycle['time.fxstart']).days
        
        np.random.seed(sds[sid])
        enssds = np.random.randint(1,10000,self.nmembers)
        
        #option 1: start each cycle with the same prior values (makes several independent estimates)
        if self.prop == 1 or dacycle['time.restart']==False:
            file=os.path.join(self.obsdir,self.pparam)
            f = io.ct_read(file, 'read')
            prmval = f.get_variable('prior_values')[:self.nparams]
            f.close()
        #option 2: propagate optimized parameter values, but not the covariance matrix
        elif self.prop == 2:
            selectdate = dacycle['time.start']-dtm.timedelta(1)
            dt=selectdate.strftime('%Y%m%d')
            file=os.path.join(dacycle['dir.da_run'], 'output', selectdate.strftime('%Y%m%d'),'optimizer.%s.nc' % selectdate.strftime('%Y%m%d'))
            f = io.ct_read(file, 'read')
            prmval = f.get_variable('statevectormean_optimized')[:]
            f.close()
        elif self.prop == 3:
        #option 3: start each cycle with the parameter values and uncertainties of the previous cycle (optimized)
            selectdate = dacycle['time.start']-dtm.timedelta(1)
            dt=selectdate.strftime('%Y%m%d')
            file=os.path.join(dacycle['dir.da_run'], 'output', selectdate.strftime('%Y%m%d'),'optimizer.%s.nc' % selectdate.strftime('%Y%m%d'))
            f = io.ct_read(file, 'read')
            prmval = f.get_variable('statevectormean_optimized')[:][:self.nparams]
            devs = f.get_variable('statevectordeviations_optimized')[:]
            f.close()

            import pickle
            covariancematrix = (np.dot(devs,devs.T)/(devs.shape[1]-1))
            covariancematrix = covariancematrix[:len(covariancematrix)//self.nlag, :len(covariancematrix)//self.nlag]
            with open('covmatrix.pkl', 'wb') as tofile:
                pickle.dump(covariancematrix, tofile)
            diag_indices = np.diag_indices_from(covariancematrix)
            covariancematrix[diag_indices] = min(covariancematrix.max()*2, 1.)
            with open('covmatrix_diag.pkl', 'wb') as tofile:
                pickle.dump(covariancematrix, tofile)
    
        covariancematrix = np.array(covariancematrix)
        self.covariancematrix = covariancematrix

        dims = covariancematrix.shape[0]
        #if dims != self.nparams:
        #    logging.error("The total dimension of the covariance matrices passed (%d) does not add up to the prescribed nparams (%d), exiting..." % (dims, self.nparams))
        #    raise ValueError

        # Make a cholesky decomposition of the covariance matrix

        try:
            _, s, _ = np.linalg.svd(covariancematrix)
        except:
            s = np.linalg.svd(covariancematrix, full_matrices=1, compute_uv=0) #Cartesius fix

        dof = np.sum(s) ** 2 / sum(s ** 2)
        
        C = np.linalg.cholesky(covariancematrix)

        logging.debug('Cholesky decomposition has succeeded ')
        logging.info('Appr. degrees of freedom in covariance matrix is %s' % (int(dof)))

        # Create mean values 
        self.prmval = prmval
        newmean = np.ones(self.nparams, float) * prmval # standard value for a new time step is 1.0

        # If this is not the start of the filter, average previous two optimized steps into the mix
        if lag == self.nlag - 1 and self.nlag >= 3:
            newmean += self.ensemble_members[lag - 1][0].param_values + \
                                           self.ensemble_members[lag - 2][0].param_values 
            newmean = newmean / 3.0

        # Create the first ensemble member with a deviation of 0.0 and add to list

        newmember = EnsembleMember(0)
        newmember.param_values = newmean.flatten()  # no deviations
        self.ensemble_members[lag].append(newmember)

        # Create members 1:nmembers and add to ensemble_members list

        for member in range(1, self.nmembers):
            np.random.seed(enssds[member])
            rands = np.random.randn(self.nparams)
            # logging.debug('rands are %f, %f, %f, %f, %f'%(rands[0],rands[1],rands[2],rands[3],rands[4]))

            newmember = EnsembleMember(member)
            newmember.param_values = np.dot(C, rands) + newmean
            self.ensemble_members[lag].append(newmember)

        logging.debug('%d new ensemble members were added to the state vector # %d' % (self.nmembers, (lag + 1)))    
    

################### End Class OPSStateVector ###################

if __name__ == "__main__":
    pass

