"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters. 
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu. 

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation, 
version 3. This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. 

You should have received a copy of the GNU General Public License along with this 
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
# pipeline.py

"""
.. module:: pipeline
.. moduleauthor:: Wouter Peters 

Revision History:
File created on 06 Sep 2010.

The pipeline module holds methods that execute consecutive tasks with each of the objects of the DA system. 

"""
import logging
import os
import sys
import datetime
import copy
import pickle
from da.tools.general import advance_time
from da.analysis.ctecc.expand_fluxes import save_weekly_avg_1x1_data,  save_weekly_avg_agg_data, time_avg
from da.analysis.ctecc.expand_molefractions import write_mole_fractions
import multiprocessing
from itertools import repeat
from da.observations.obs_ctecc import ObsPackObservations
from da.observations.obs_tccon import TCCONObservations
from da.observations.obs_column_xco2 import TotalColumnObservations
import datetime as dt 
header = '\n\n    ***************************************   '
footer = '    *************************************** \n  '
def two_step_invert(dacycle,statevector,optimizer):
    
    statevector.select_parameters(keys=['co2_bio','co2_ocn','terdis','ocedis'])

    dims = (int(dacycle['time.nlag']),
            int(dacycle['da.optimizer.nmembers']),
            int(dacycle.dasystem['nparameters']),
            statevector.nobs)

    optimizer.set_algorithm('Serial')

    optimizer.setup(dims)

    optimizer.state_to_matrix(statevector)

    diagnostics_file = os.path.join(dacycle['dir.output'], 'optimizer.%s.nc' % dacycle['time.begin'].strftime('%Y%m%d'))
    
    optimizer.write_diagnostics(diagnostics_file+'_step_1','prior')

    optimizer.serial_minimum_least_squares_loc(statevector.datevec,time_window=35)

    optimizer.write_diagnostics(diagnostics_file+'_step_1', 'optimized')
    # will assign optimized state back to members and back to sateparameter objects
    optimizer.matrix_to_state(statevector)

    statevector.select_parameters(keys=['alpha'])
    dims = (int(dacycle['time.nlag']),
            int(dacycle['da.optimizer.nmembers']),
            int(dacycle.dasystem['nparameters']),
            statevector.nobs)

    optimizer.setup(dims)

    optimizer.state_to_matrix(statevector)

    # read in already assimilated observations so that
    optimizer.read_diagnostics(diagnostics_file+'_step_1','prior',obsonly=True) 

    # save second step
    optimizer.write_diagnostics(diagnostics_file,'prior')

    optimizer.serial_minimum_least_squares_loc(statevector.datevec,time_window=35,species='co2c13')

    optimizer.write_diagnostics(diagnostics_file, 'optimized')
    # will assign optimized state back to members and back to sateparameter objects
    optimizer.matrix_to_state(statevector)

    diagnostics_file = os.path.join(dacycle['dir.output'], 'optimizer.%s.nc' % dacycle['time.begin'].strftime('%Y%m%d'))


    statevector_file=os.path.join(dacycle['dir.restart'], 'savestate_%s.nc' % dacycle['time.begin'].strftime('%Y%m%d'))
    statevector.write_to_file(statevector_file, 'opt')

    dacycle['da.optimized']=True

    # if we do chunked ctsf (i.e. restart = true and enkf type is ctsf   we need to reset for the advance
    if dacycle['time.restart'] and dacycle.dasystem['ct.enkf.type']=='ctsf':
        dacycle['time.start']=dacycle['time.begin']
        dacycle['time.end']=advance_time(dacycle['time.begin'],dacycle['time.cycle'],1)
        dacycle['time.restart']=False
    return

def two_step(optimizer,statevector,dacycle, platform, dasystem, samples=None ):
    start_job(dacycle, dasystem, platform, statevector)
    statevector_file=os.path.join(dacycle['dir.output'], 'savestate_%s.nc' % dacycle['time.begin'].strftime('%Y%m%d'))
    statevector.read_from_file(statevector_file,0,'prior')

    gather(dacycle,samples)
   
    communicate_samples(samples,statevector)

    
    statevector.select_parameters(keys=['co2_bio','co2_ocn'])

    dims = (int(dacycle['time.nlag']),
            int(dacycle['da.optimizer.nmembers']),
            int(dacycle.dasystem['nparameters']),
            statevector.nobs)
    optimizer.set_algorithm('Serial')
    optimizer.setup(dims)


    optimizer.state_to_matrix(statevector)

    diagnostics_file = os.path.join(dacycle['dir.output'], 'optimizer.%s.nc' % dacycle['time.begin'].strftime('%Y%m%d'))
    
    optimizer.write_diagnostics(diagnostics_file+'_step_1','prior')

    optimizer.serial_minimum_least_squares_loc(statevector.datevec,time_window=35,species='co2')

    optimizer.write_diagnostics(diagnostics_file+'_step_1', 'optimized')
    # will assign optimized state back to members and back to sateparameter objects
    optimizer.matrix_to_state(statevector)

    statevector.select_parameters(keys='all')
    dims = (int(dacycle['time.nlag']),
            int(dacycle['da.optimizer.nmembers']),
            int(dacycle.dasystem['nparameters']),
            statevector.nobs)
    optimizer.set_algorithm('Serial')
    optimizer.setup(dims)

    optimizer.state_to_matrix(statevector)

    # read in already assimilated observations so that
    optimizer.read_diagnostics(diagnostics_file+'_step_1','prior',obsonly=True) 
    # save second step
    optimizer.write_diagnostics(diagnostics_file+'_step_2','prior')

    optimizer.serial_minimum_least_squares_loc(statevector.datevec,time_window=35,species='co2c13')

    optimizer.write_diagnostics(diagnostics_file+'_step_2', 'optimized')
    # will assign optimized state back to members and back to sateparameter objects
    optimizer.matrix_to_state(statevector)

    diagnostics_file = os.path.join(dacycle['dir.output'], 'optimizer.%s.nc' % dacycle['time.begin'].strftime('%Y%m%d'))

    statevector.write_to_file(statevector_file, 'opt')

    dacycle['da.optimized']=True
    return


def offline_pipeline(optimizer,statevector,dacycle, platform, dasystem, samples=None ):
    start_job(dacycle, dasystem, platform, statevector)
    statevector_file=os.path.join(dacycle['dir.output'], 'savestate_%s.nc' % dacycle['time.begin'].strftime('%Y%m%d'))
    statevector.read_from_file(statevector_file,0,'prior')
    if samples:
        gather(dacycle,samples)
   
        communicate_samples(samples,statevector)

        invert(dacycle, statevector, optimizer)
    else:
        diagnostics_file = os.path.join(dacycle['dir.output'], 'optimizer.%s.nc' % dacycle['time.begin'].strftime('%Y%m%d'))

        optimizer.read_diagnostics(diagnostics_file,'prior')

        optimizer.serial_minimum_least_squares_loc(statevector.datevec,time_window=35)

        optimizer.write_diagnostics(diagnostics_file, 'optimized')

        optimizer.matrix_to_state(statevector)

        statevector.write_to_file(statevector_file, 'opt')

        dacycle['da.optimized']=True
    return

def inversion_pipeline(dacycle, platform, dasystem, samples, statevector, obsoperator, optimizer, skip_advance=False):
    """  """
    # sys.path.append(os.getcwd())

    # samples = samples if isinstance(samples,list) else [samples]

    logging.info(header + "Initializing current cycle" + footer)
    
    start_job(dacycle, dasystem, platform, statevector, obsoperator)

    prepare_state(dacycle, statevector)

    if time_to_sample(dacycle):
        sample_state(dacycle, samples, statevector, obsoperator)

    if time_to_optimize(dacycle):
        invert(dacycle, statevector, optimizer)

    if not skip_advance and time_to_advance(dacycle):
      advance(dacycle, samples,  obsoperator,statevector)

    save_and_submit(dacycle, statevector)

    postprocessing(dacycle,statevector=statevector)

    logging.info("Cycle finished...exiting pipeline")
    return
def forward_pipeline(dacycle,platform,dasystem,samples,obsoperator,statevector=None,allspecies=False):
    """ This pipeline will use all species and transport the defined emission forward
    Note that this should run really after all other analysis, as it behaves a bit differently:
        1) run fwd mode. Assumes optimized state of paramaters is present in input/parameter.000.nc
        2) creates a new obspack.pickle 
        3) gathers output now in a new expand mole fractions
    """
    sys.path.append(os.getcwd())

    logging.info(header + "Initializing current cycle" + footer)

    start_job(dacycle, dasystem, platform, obsoperator=obsoperator,statevector=statevector,allspecies=allspecies,fwd=True)

    if statevector: 
        get_state(dacycle,statevector)
        advance(dacycle, samples,  obsoperator,statevector)
    else:
        advance(dacycle, samples,  obsoperator)

    postprocessing(dacycle)

    save_and_submit(dacycle, statevector)

    logging.info("Cycle finished...exiting pipeline")
    return

def output_to_station(dacycle,platform,dasystem,samples,obsoperator,allspecies=False):
    sys.path.append(os.getcwd())

    logging.info(header + "Initializing current cycle" + footer)

    start_job(dacycle, dasystem, platform, obsoperator=obsoperator,allspecies=allspecies,fwd=True)

    lag=0

    dacycle.set_sample_times(lag)

    startdate = dacycle['time.sample.start']
    enddate = dacycle['time.sample.end']
    dacycle['time.sample.window'] = lag
    dacycle['time.sample.stamp'] = "%s_%s" % (startdate.strftime("%Y%m%d%H"), enddate.strftime("%Y%m%d%H"))

    obsoperator.get_initial_data()
    obsoperator.prepare_run(samples,with_state_vector=False)

    for sample,simulated_file in zip(samples,obsoperator.simulated_file):
        sample.setup(dacycle)
        sample.add_observations()
        # sample.add_model_data_mismatch() 
        sample.add_simulations(os.path.join('tm5mp/rundir/output', '%s_output.%s.nc' % (sample.get_samples_type(),dacycle['time.sample.stamp'])))
        if sample.get_samples_type() == 'column':
            outfile = os.path.join(dacycle['dir.output'], 'column_auxiliary_%s.nc' % dacycle['time.sample.stamp'])
        elif sample.get_samples_type() == 'tccon':
            outfile = os.path.join(dacycle['dir.output'], 'tccon_auxiliary_%s.nc' % dacycle['time.sample.stamp'])
        else:
            outfile = os.path.join(dacycle['dir.output'], 'sample_auxiliary_%s.nc' % dacycle['time.sample.stamp'])
        if not advance and (dacycle['time.restart'] == False or lag == int(dacycle['time.nlag']) - 1):
            sample.write_sample_auxiliary(outfile,'prior')
        else:
            sample.write_sample_auxiliary(outfile,'optimized')
    return

def postprocessing(dacycle, statevector=None,mole_fractions=True,fluxes=True,timeavg=True):
    """ Main entry point for analysis of ctdas results """


    logging.info(header + "Starting analysis" + footer)
   
    if mole_fractions:
        logging.info(header + "Starting mole fractions" + footer)
        write_mole_fractions(dacycle)

    if statevector and fluxes:
        logging.info(header + "Starting weekly averages" + footer)
        if dacycle.dasystem['ct.enkf.type']=='ctsf' and dacycle['da.optimized']==False: return
        statevector.setup(dacycle)

        save_weekly_avg_1x1_data(dacycle, statevector)
    # save_weekly_avg_state_data(dacycle, statevector)
    # save_weekly_avg_tc_data(dacycle, statevector)
    # save_weekly_avg_ext_tc_data(dacycle)
        save_weekly_avg_agg_data(dacycle,region_aggregate='transcom')
        save_weekly_avg_agg_data(dacycle,region_aggregate='transcom_extended')
    #save_weekly_avg_agg_data(dacycle,region_aggregate='olson')
    #save_weekly_avg_agg_data(dacycle,region_aggregate='olson_extended')
    #save_weekly_avg_agg_data(dacycle,region_aggregate='country')
        if timeavg:
            logging.info(header + "Starting monthly and yearly averages" + footer)

            time_avg(dacycle,'flux1x1')
            time_avg(dacycle,'transcom')
            time_avg(dacycle,'transcom_extended')
    #time_avg(dacycle,'olson')
    #time_avg(dacycle,'olson_extended')
    #time_avg(dacycle,'country')

    logging.info(header + "Finished analysis" + footer)
    return

def archive_pipeline(dacycle, platform, dasystem):
    """ Main entry point for archiving of output from one disk/system to another """

    if not 'task.rsync' in dacycle:
        logging.info('rsync task not found, not starting automatic backup...')
        return
    else:
        logging.info('rsync task found, starting automatic backup...')

    for task in dacycle['task.rsync'].split():
        sourcedirs = dacycle['task.rsync.%s.sourcedirs'%task]
        destdir = dacycle['task.rsync.%s.destinationdir'%task]

        rsyncflags = dacycle['task.rsync.%s.flags'%task]

        # file ID and names
        jobid = dacycle['time.end'].strftime('%Y%m%d')
        targetdir = os.path.join(dacycle['dir.exec'])
        jobfile = os.path.join(targetdir, 'jb.rsync.%s.%s.jb' % (task,jobid) )
        logfile = os.path.join(targetdir, 'jb.rsync.%s.%s.log' % (task,jobid) )
        # Template and commands for job
        jobparams = {'jobname':"r.%s" % jobid, 'jobnodes': '1', 'jobtime': '1:00:00', 'joblog': logfile, 'errfile': logfile}

        if platform.ID == 'cartesius':
            jobparams['jobqueue'] = 'staging'

        template = platform.get_job_template(jobparams)
        for sourcedir in sourcedirs.split():
            execcommand = "\nrsync %s %s %s\n" % (rsyncflags, sourcedir,destdir,)
            template += execcommand

        # write and submit
        platform.write_job(jobfile, template, jobid)
        jobid = platform.submit_job(jobfile, joblog=logfile)
    return

def start_job(dacycle, dasystem, platform, statevector=None, obsoperator=None,allspecies=False,fwd=False):
    """ Set up the job specific directory structure and create an expanded rc-file """

    # logging.debug=('Passed on validation=%s ' %validation)
    dasystem.validate()
    dacycle.dasystem = dasystem
    dacycle.daplatform = platform
    dacycle.setup()
    if fwd:
        logging.info('setting up nmembers to 1' )
        dacycle['da.optimizer.nmembers']=1 # only the mean 
        dacycle.dasystem['ct.enkf.mode']='fwd'

    if allspecies:
        dacycle.dasystem['ct.params.species']='co2 co2c13'
    if statevector:
        statevector.setup(dacycle)

    if obsoperator:
        obsoperator.setup(dacycle)  # Setup Observation Operator
    return

def prepare_state(dacycle, statevector):
    """ Set up the input data for the forward model: obs and parameters/fluxes"""

    # We now have an empty statevector object that we need to populate with data. If this is a continuation from a previous cycle, we can read
    # the previous statevector values from a NetCDF file in the restart directory. If this is the first cycle, we need to populate the statevector
    # with new values for each week. After we have constructed the statevector, it will be propagated by one cycle length so it is ready to be used
    # in the current cycle

    logging.info(header + "starting prepare_state" + footer)

    if 'inversion.savestate.dir' in dacycle:
        initdir = dacycle['inversion.savestate.dir']
        method = dacycle['inversion.savestate.method'] # valid options: read_new_member and read_mean
        logging.info('Ensemble members will be initialized from optimized ensembles in %s' %initdir)
    else:
        method = 'create_new_member'

    if not dacycle['time.restart']:

        # This hole blob of code is statevector business and can be moved to the statevector
        # the statevector holds a link to dacycle anyway so no need for this piece of loggic in here
        # Fill each week from n=1 to n=nlag with a new ensemble
        for n in range(statevector.nlag):

            if method == 'create_new_member':
                date = dacycle['time.start'] + (n + 0.5) * dacycle['cyclelength']
                cov = statevector.get_covariance(date, dacycle)
                statevector.make_new_ensemble(n, cov)

            elif method == 'read_new_member' or method == 'read_mean':
                date = dacycle['time.start'] + n * dacycle['cyclelength']
                filename_new_member = os.path.join(initdir, date.strftime('%Y%m%d'), 'savestate_%s.nc' % date.strftime('%Y%m%d'))

                # Check if filename exits, else we will need to interpolate between dates
                if os.path.exists(filename_new_member):
                    if method == 'read_new_member':
                        statevector.read_ensemble_member_from_file(filename_new_member, n, qual='opt', read_lag=0)
                    elif method == 'read_mean':
                        date = dacycle['time.start'] + (n + 0.5 )* dacycle['cyclelength']
                        cov = statevector.get_covariance(date, dacycle)
                        meanstate = statevector.read_mean_from_file(filename_new_member, n, qual='opt')
                        statevector.make_new_ensemble(n, cov, meanstate)
                else:
                    if method == 'read_new_member':
                        statevector.read_ensemble_member_from_file(filename_new_member, n, date, initdir, qual='opt', read_lag=0)
                    elif method == 'read_mean':
                        meanstate = statevector.read_mean_from_file(filename_new_member, n, date, initdir, qual='opt')
                        date = dacycle['time.start'] + (n + 0.5 ) * dacycle['cyclelength']
                        cov = statevector.get_covariance(date, dacycle)
                        statevector.make_new_ensemble(n, cov, meanstate)

    elif (dacycle['time.restart']) and (dacycle.dasystem['ct.enkf.type']=='ctsf'):
        saved_sv = os.path.join(dacycle['dir.restart'], 'savestate_%s.nc' % dacycle['time.begin'].strftime('%Y%m%d'))
        try:
            statevector.read_from_file(saved_sv,0,'opt') # by default will read "opt"(imized) variables, and then propagate
        except KeyError:
            statevector.read_from_file(saved_sv,0,'prior') # if they are not pressent we need prior 
        return # here we don't want to rewrite
    else:
        # Read the statevector data from file
        #saved_sv = os.path.join(dacycle['dir.restart.current'], 'savestate.nc')
        saved_sv = os.path.join(dacycle['dir.restart'], 'savestate_%s.nc' % dacycle['da.restart.tstamp'].strftime('%Y%m%d'))
        statevector.read_from_file(saved_sv) # by default will read "opt"(imized) variables, and then propagate

        # read ensemble for new week from file, or create new ensemble member, and propagate the ensemble by one cycle to prepare for the current cycle
        if method == 'create_new_member':
            statevector.propagate(dacycle)

        elif method == 'read_new_member' or method == 'read_mean':
            date = dacycle['time.start'] + datetime.timedelta(days=(statevector.nlag-1) * int(dacycle['time.cycle']))
            filename_new_member = os.path.join(initdir, date.strftime('%Y%m%d'), 'savestate_%s.nc'%date.strftime('%Y%m%d'))
            statevector.propagate(dacycle, method, filename_new_member, date, initdir)

    # Finally, also write the statevector to a file so that we can always access the a-priori information
    current_sv = os.path.join(dacycle['dir.restart'], 'savestate_%s.nc' % dacycle['time.start'].strftime('%Y%m%d'))
    statevector.write_to_file(current_sv, 'prior')  # write prior info
    return

def sample_state(dacycle, samples, statevector, obsoperator):
    """ Sample the filter state for the inversion """

    # Before a forecast step, save all the data to a save/tmp directory so we can later recover it before the propagation step.
    # This is especially important for:
    #  (i) The transport model restart data which holds the background mole fractions. This is needed to run the model one cycle forward
    #  (ii) The random numbers (or the seed for the random number generator) so we can recreate the ensembles if needed

    #status   = dacycle.MoveSaveData(io_option='store',save_option='partial',filter=[])
    #msg     = "All restart data have been copied to the save/tmp directory for future use"    ; logging.debug(msg)
    logging.info(header + "starting sample_state" + footer)
    nlag = int(dacycle['time.nlag'])
    logging.info("Sampling model will be run over %d cycles" % nlag)

    obsoperator.get_initial_data()

    for lag in range(nlag):
        logging.info(header + ".....Ensemble Kalman Filter at lag %d" % (lag + 1))

        ############# Perform the actual sampling loop #####################

        sample_step(dacycle, samples,  obsoperator, lag,statevector=statevector)

        logging.debug("statevector now carries %d samples" % statevector.nobs)
    return

def sample_step(dacycle, samples,  obsoperator, lag, advance=False,statevector=None,with_members=False):
    """ Perform all actions needed to sample one cycle """

    # First set up the information for time start and time end of this sample
    dacycle.set_sample_times(lag)

    startdate = dacycle['time.sample.start']
    enddate = dacycle['time.sample.end']
    dacycle['time.sample.window'] = lag
    dacycle['time.sample.stamp'] = "%s_%s" % (startdate.strftime("%Y%m%d%H"), enddate.strftime("%Y%m%d%H"))

    logging.info("New simulation interval set : ")
    logging.info("                  start date : %s " % startdate.strftime('%F %H:%M'))
    logging.info("                  end   date : %s " % enddate.strftime('%F %H:%M'))
    logging.info("                  file  stamp: %s " % dacycle['time.sample.stamp'])


    # Implement something that writes the ensemble member parameter info to file, or manipulates them further into the
    # type of info needed in your transport model

    if statevector:
        statevector.write_members_to_file(lag, dacycle['dir.input'], obsoperator=obsoperator)

    for sample in samples:

        sample.setup(dacycle)

        # Read observations + perform observation selection
        obspickle_filename = os.path.join(dacycle['dir.restart'], sample.get_samples_type()+'samples_%s.pickle' % dacycle['time.sample.stamp'])

        if os.path.isfile(obspickle_filename):
            if 'da.preprocessobs' in dacycle.dasystem and dacycle['da.preprocessobs'] == True: continue
            sample = pickle.load(open(obspickle_filename, 'rb'))
            logging.info("Loaded the samples object from file: %s"%obspickle_filename) 
        else:
            sample.add_observations()
            if 'da.preprocessobs' in dacycle.dasystem and dacycle['da.preprocessobs'] == True:
                pickle.dump(sample, open(obspickle_filename, 'wb'), -1)
                logging.info("Saved the samples object to file: %s"%obspickle_filename) 
                continue

        # Add model-data mismatch to all samples, this *might* use output from the ensemble in the future??
        sample.add_model_data_mismatch(dacycle.dasystem['obs.sites.rc'])

        sampling_coords_file = os.path.join(dacycle['dir.input'], sample.get_samples_type()+'_coordinates_%s.nc' % dacycle['time.sample.stamp'])

        sample.write_sample_coords(sampling_coords_file)

        # Write filename to dacycle, and to output collection list
        dacycle['ObsOperator.inputfile.'+sample.get_samples_type()] = sampling_coords_file
        
        
        # samples[i] = sample
    
    if 'da.preprocessobs' in dacycle.dasystem and dacycle['da.preprocessobs'] == True: return
    
    # Run the observation operator
    # if no statevector object initialized just run forward
    # Later we should have the rc specify somehow to use a specific parametermap 
    if statevector:
        obsoperator.run_forecast_model(samples)
    else:
        obsoperator.run_forecast_model(samples,with_members=with_members)

    # Read forecast model samples that were written to NetCDF files by each member. Add them to the exisiting
    # Observation object for each sample loop. This data fill be written to file in the output folder for each sample cycle.

    # We retrieve all model samples from one output file written by the ObsOperator. If the ObsOperator creates
    # one file per member, some logic needs to be included to merge all files!!!

    for sample,simulated_file in zip(samples,obsoperator.simulated_file):
        if os.path.exists(dacycle['ObsOperator.inputfile.'+sample.get_samples_type()]):
            sample.add_simulations(simulated_file)
            logging.debug("Simulated values read, continuing pipeline")
            if sample.get_samples_type() == 'column':
                outfile = os.path.join(dacycle['dir.output'], 'column_auxiliary_%s.nc' % dacycle['time.sample.stamp'])
            elif sample.get_samples_type() == 'tccon':
                outfile = os.path.join(dacycle['dir.output'], 'tccon_auxiliary_%s.nc' % dacycle['time.sample.stamp'])
            else:
                outfile = os.path.join(dacycle['dir.output'], 'sample_auxiliary_%s.nc' % dacycle['time.sample.stamp'])
            #
            # Write out the prior information the first time the samples appear:
            # In the original EnKF, info of the observations would be taken from 
            # The optimizer output. which is the first time a sample appears. 
            if not advance and (dacycle['time.restart'] == False or lag == int(dacycle['time.nlag']) - 1):
                sample.write_sample_auxiliary(outfile,'prior')
            else:
                sample.write_sample_auxiliary(outfile,'optimized')
        else:
            logging.warning("No simulations added, because input file does not exist (no samples found in obspack)")

    if dacycle['time.restart'] and dacycle.dasystem['ct.enkf.type']=='ctsf' and (dacycle['time.end']==dacycle['time.finish']) and not dacycle['da.optimized']:
        # Turn of meteo sampling. We have done that, and its not going to change
        # This also prevents the comming list from harvesting
        # unneccisary info for the inversion step.

        gather(dacycle,samples)
            # for i in nsteps_all:
                # sample.datalist.extend(gatherer(i,sampletype,copy.deepcopy(dacycle)))
        # for sample,simfile in zip(samples,obsoperator.simulated_file):
            # rootname=simfile.split('.')[0]

        # we have lost all obs due to restart cycles. this is fine
            
        # lets gather them back and add them.
  
    # Now add the observations that need to be assimilated to the statevector.
    # Note that obs will only be added to the statevector if either this is the first step (restart=False), or lag==nlag
    # This is to make sure that the first step of the system uses all observations available, while the subsequent
    # steps only optimize against the data at the front (lag==nlag) of the filter. This way, each observation is used only
    # (and at least) once in the assimilation

    if statevector and not advance:
        if dacycle['time.restart'] == False or lag == int(dacycle['time.nlag']) - 1:
            # why are we copying this to the statevector? we could simply pass this on to the optimizer directly? 
            communicate_samples(samples,statevector)
        else:
            statevector.obs_to_assimilate += (None,)
    return 
def communicate_samples(samples,statevector):

    if hasattr(samples[0],'obs_to_assimilate'):
        samples_to_assimilate=[]
        for sample in samples:
            samples_to_assimilate.append(sample.obs_to_assimilate())
            statevector.nobs += samples_to_assimilate[-1].getlength()
        statevector.obs_to_assimilate += (copy.deepcopy(samples_to_assimilate),)
    else:
        statevector.obs_to_assimilate += (copy.deepcopy(samples),)

        for sample in samples:
            statevector.nobs += sample.getlength()
    logging.info('nobs = %i' %statevector.nobs)
    logging.debug("Added samples from the observation operator to the assimilated obs list in the statevector")
    return 

def gather(dacycle,samples):
    dacycle['time.sample.start']=dacycle['time.begin']
    dacycle['time.sample.end']=dacycle['time.finish']
    dacycle.dasystem['obspack.meteo.sampling']=False
    for sample in samples:
        nsteps_all = range((dacycle["time.finish"].year-dacycle["time.begin"].year)*12+(dacycle["time.finish"].month-dacycle["time.begin"].month))
        # nsteps_all = range(1)
        logging.debug('%s' % nsteps_all)
        sample.setup(dacycle)
        sampletype=sample.get_samples_type()
        # for nstep in nsteps_all :
            # sample.datalist.extend(gatherer(nstep,sampletype,copy.deepcopy(dacycle)))
        with multiprocessing.Pool(4) as pool:
            for result in pool.starmap(gatherer,zip(nsteps_all,repeat(sampletype),repeat(copy.deepcopy(dacycle)))):
                sample.datalist.extend(result)
    return
def invert(dacycle, statevector, optimizer):
    """ Perform the inverse calculation """
    logging.info(header + "starting invert" + footer)

    if statevector.nobs <= 1: #== 0:
        logging.info('List with observations to assimilate is empty, skipping invert step and continuing without statevector update...')
        return

    dims = (int(dacycle['time.nlag']),
            int(dacycle['da.optimizer.nmembers']),
            int(dacycle.dasystem['nparameters']),
            statevector.nobs)

    if 'opt.algorithm' not in dacycle.dasystem:
        logging.info("There was no minimum least squares algorithm specified in the DA System rc file (key : opt.algorithm)")
        logging.info("...using serial algorithm as default...")
        optimizer.set_algorithm('Serial')
    elif dacycle.dasystem['opt.algorithm'] == 'serial':
        logging.info("Using the serial minimum least squares algorithm to solve ENKF equations")
        optimizer.set_algorithm('Serial')
    elif dacycle.dasystem['opt.algorithm'] == 'bulk':
        logging.info("Using the bulk minimum least squares algorithm to solve ENKF equations")
        optimizer.set_algorithm('Bulk')

    optimizer.setup(dims)
    optimizer.state_to_matrix(statevector)
    if dacycle['time.restart'] and dacycle.dasystem['ct.enkf.type']=='ctsf':
        diagnostics_file = os.path.join(dacycle['dir.output'], 'optimizer.%s.nc' % dacycle['time.begin'].strftime('%Y%m%d'))
    else:
        diagnostics_file = os.path.join(dacycle['dir.output'], 'optimizer.%s.nc' % dacycle['time.start'].strftime('%Y%m%d'))

    optimizer.write_diagnostics(diagnostics_file, 'prior')
    optimizer.set_localization(dacycle['da.system.localization'])

    if optimizer.algorithm == 'Serial':
        if hasattr(optimizer,'serial_minimum_least_squares_loc'):
            # time window shouldn't be used, but for some reason 
            # assimilating stuff in smaller batches is faster
            optimizer.serial_minimum_least_squares_loc(statevector.datevec,time_window=35)
        else:
            optimizer.serial_minimum_least_squares()
    else:
        optimizer.bulk_minimum_least_squares()

    optimizer.matrix_to_state(statevector)

    optimizer.write_diagnostics(diagnostics_file, 'optimized')

    if dacycle['time.restart'] and dacycle.dasystem['ct.enkf.type']=='ctsf':
        filename = os.path.join(dacycle['dir.restart'], 'savestate_%s.nc' % dacycle['time.begin'].strftime('%Y%m%d'))
    else:
        filename = os.path.join(dacycle['dir.restart'], 'savestate_%s.nc' % dacycle['time.start'].strftime('%Y%m%d'))
        
    statevector.write_to_file(filename, 'opt')
    dacycle['da.optimized']=True

    # if we do chunked ctsf (i.e. restart = true and enkf type is ctsf   we need to reset for the advance
    if dacycle['time.restart'] and dacycle.dasystem['ct.enkf.type']=='ctsf':
        dacycle['time.start']=dacycle['time.begin']
        dacycle['time.end']=advance_time(dacycle['time.begin'],dacycle['time.cycle'],1)
        dacycle['time.restart']=False

def advance(dacycle, samples,  obsoperator, statevector=None,):
    """ Advance the filter state to the next step """
    # This is the advance of the modeled CO2 state. Optionally, routines can be added to advance the state vector (mean+covariance)

    # Then, restore model state from the start of the filter
    logging.info(header + "starting advance" + footer)
    logging.info("Sampling model will be run over 1 cycle")

    obsoperator.get_initial_data()

    if statevector:
        sample_step(dacycle, samples,  obsoperator, 0,advance=True,statevector=statevector)
    else:
        sample_step(dacycle, samples,  obsoperator, 0 ,with_members=False)

    dacycle.restart_filelist.extend(obsoperator.restart_filelist)
    dacycle.output_filelist.extend(obsoperator.output_filelist)
    logging.debug("Appended ObsOperator restart and output file lists to dacycle for collection ")
    return

def save_and_submit(dacycle, statevector):
    """ Save the model state and submit the next job """
    logging.info(header + "starting save_and_submit" + footer)

    filename = os.path.join(dacycle['dir.restart'], 'savestate_%s.nc' % dacycle['time.start'].strftime('%Y%m%d'))

    dacycle.output_filelist.append(filename)

    dacycle.finalize()
    return

def get_state(dacycle,statevector):
    if 'forward.savestate.exceptsam' in dacycle:
        sam = (dacycle['forward.savestate.exceptsam'].upper() in ["TRUE","T","YES","Y"])
    else:
        sam = False

    if 'forward.savestate.dir' in dacycle:
        fwddir = dacycle['forward.savestate.dir']
    else:
        logging.debug("No forward.savestate.dir key found in rc-file, proceeding with self-constructed prior parameters")
        fwddir = False

    if 'forward.savestate.legacy' in dacycle:
        legacy = (dacycle['forward.savestate.legacy'].upper() in ["TRUE","T","YES","Y"])
    else:
        legacy = False
        logging.debug("No forward.savestate.legacy key found in rc-file")

    if not fwddir:
        # Simply make a prior statevector using the normal method
        prepare_state(dacycle, statevector)#LU tutaj zamiast tego raczej to stworzenie nowej kowariancji i ensembli bo pozostale rzeczy sa na gorze i na doel.
    else:
        # Read prior information from another simulation into the statevector.
        # This loads the results from another assimilation experiment into the current statevector

        if sam:
            filename = os.path.join(fwddir, dacycle['time.start'].strftime('%Y%m%d'), 'savestate_%s.nc'%dacycle['time.start'].strftime('%Y%m%d'))
            #filename = os.path.join(fwddir, dacycle['time.start'].strftime('%Y%m%d'), 'savestate.nc')
            statevector.read_from_file_exceptsam(filename, 'prior')
        elif not legacy:
            filename = os.path.join(fwddir, dacycle['time.start'].strftime('%Y%m%d'), 'savestate_%s.nc'%dacycle['time.start'].strftime('%Y%m%d'))
            statevector.read_from_file(filename, 'prior')
        else:
            filename = os.path.join(fwddir, dacycle['time.start'].strftime('%Y%m%d'), 'savestate.nc')
            statevector.read_from_legacy_file(filename, 'prior')


    # We write this "prior" statevector to the restart directory, so we can later also populate it with the posterior statevector
    # Note that we could achieve the same by just copying the wanted forward savestate.nc file to the restart folder of our current
    # experiment, but then it would already contain a posterior field as well which we will try to write in save_and_submit.
    # This could cause problems. Moreover, this method allows us to read older formatted savestate.nc files (legacy) and write them into
    # the current format through the "write_to_file" method.

    savefilename = os.path.join(dacycle['dir.restart'], 'savestate_%s.nc' % dacycle['time.start'].strftime('%Y%m%d'))
    statevector.write_to_file(savefilename, 'prior')

    # Now read optimized fluxes which we will actually use to propagate through the system

    if not fwddir:
        # if there is no forward dir specified, we simply run forward with unoptimized prior fluxes in the statevector
        logging.info("Running forward with prior savestate from: %s"%savefilename)

    else:
        # Read posterior information from another simulation into the statevector.
        # This loads the results from another assimilation experiment into the current statevector

        if sam:
            statevector.read_from_file_exceptsam(filename, 'opt')
        elif not legacy:
            statevector.read_from_file(filename, 'opt')
        else:
            statevector.read_from_legacy_file(filename, 'opt')

        logging.info("Running forward with optimized savestate from: %s"%filename)

    return


def time_to_sample(dacycle):
    if dacycle.dasystem['ct.enkf.type']=='ctsf':
        if dacycle['da.optimized']==False:
            logging.debug('Sampling the next prior cycle from %s to %s' % (dacycle['time.start'],dacycle['time.end'])  )
            return True
        else:
            logging.debug('Optimized, skipping state sampling')
            return False
    else:
        return True 

def time_to_optimize(dacycle):
    if dacycle.dasystem['ct.enkf.type']=='ctsf':
        if dacycle['time.end']==dacycle['time.finish'] and not dacycle['da.optimized']:
            logging.debug('Ready to invert')
            return True
        else:
            logging.debug('Sampling prior state is not yet finished')
            return False
    else:
        return True 

def reanalysis(dacycle,platform,dasystem,statevector=None,mole_fractions=False,fluxes=True):
    ''' Loop over al cycles to regenerate output
    '''
    # Start job stuff
    start_job(dacycle, dasystem, platform)
    if dacycle.dasystem['ct.enkf.type']=='ctsf':
        dacycle['da.optimized']=True
    if mole_fractions:
        postprocessing(dacycle,statevector=statevector,mole_fractions=True,fluxes=False)

    if fluxes:
        nsteps_all = range((dacycle["time.finish"].year-dacycle["time.begin"].year)*12+(dacycle["time.finish"].month-dacycle["time.begin"].month))
    # loop until end of cycle is equal to end of assimilation period 
    # while dacycle['time.end']<=dacycle['time.finish']:
        multi=True
        if not multi:
            dacycle['time.start']=dacycle['time.begin']
            dacycle['time.end']=advance_time(dacycle['time.start'],dacycle['time.cycle'],1)
            for i in nsteps_all:
                # logging.debug("nsteps %s %s" %(nsteps,multiprocessing.current_process()))
                logging.debug("Processing %s, %s by %s " %(dacycle['time.start'],dacycle['time.end'],multiprocessing.current_process()))
                postprocessing(dacycle,statevector=statevector,mole_fractions=False,fluxes=True,timeavg=False)
                dacycle.advance_cycle_times()

        else:
            # pass
            logger_off()
            # maybe use an initializer to pass on the constant dacycle deepcopy and statevectorelements
            # https://rvprasad.medium.com/data-and-chunk-sizes-matter-when-using-multiprocessing-pool-map-in-python-5023c96875ef
            with multiprocessing.Pool(processes=4) as pool:
                pool.starmap(postprocessor,zip(nsteps_all,
                                      repeat(copy.deepcopy(dacycle)),
                                      repeat(copy.deepcopy(statevector))
                                      ), chunksize=4)
            logger_on()
        
        dacycle['time.start']=dacycle['time.begin']
        dacycle['time.end']=advance_time(dacycle['time.start'],dacycle['time.cycle'],1)
        logging.info(header + "Starting monthly and yearly averages" + footer)
        for i in nsteps_all:
            time_avg(dacycle,'flux1x1')
            time_avg(dacycle,'transcom')
            time_avg(dacycle,'transcom_extended')
            dacycle.advance_cycle_times()
    return

def postprocessor(nsteps,dacycle,statevector):
    dacycle['time.start']=dacycle['time.begin']
    dacycle['time.end']=advance_time(dacycle['time.start'],dacycle['time.cycle'],1)
    for i in range(nsteps):
        dacycle.advance_cycle_times()
    # logging.debug("nsteps %s %s" %(nsteps,multiprocessing.current_process()))
    logging.debug("Processing %s, %s by %s " %(dacycle['time.start'],dacycle['time.end'],multiprocessing.current_process()))
    postprocessing(dacycle,statevector=statevector,mole_fractions=False,fluxes=True,timeavg=False)
    return

def time_to_advance(dacycle):
# def next_step(dacycle,statevector):
    if dacycle.dasystem['ct.enkf.type']=='ctsf':
        if dacycle['da.optimized']==True:
            logging.debug('Sampling the next posterior cycle from %s to %s' % (dacycle['time.start'],dacycle['time.end'])  )
            return True
        else:
            logging.debug('Not optimized, skipping advance')
            return False
    else:
        return True



def gatherer(nsteps,sample_type,dacycle):
    if sample_type=='flask':
        sample=ObsPackObservations()
    elif sample_type == 'column':
        sample=TotalColumnObservations()
    elif sample_type == 'TCCON':
        sample=TCCONObservations()
    dacycle['time.start']=dacycle['time.begin']
    dacycle['time.end']=advance_time(dacycle['time.start'],dacycle['time.cycle'],1)
    for i in range(nsteps):
        dacycle.advance_cycle_times()
    # logging.debug("Gathering %s, %s " %(dacycle['time.start'],dacycle['time.end']))

    sample.setup(dacycle)
    sample.startdate=dacycle['time.start']
    sample.enddate=dacycle['time.end']
    try: 
        sample.read_sample_auxiliary() 
        return sample.datalist
    except FileNotFoundError:
        logging.info("Could not find %s" % sample.aux_file_name()) 
        logging.info("Returning empty list") 
        return []

def logger_off():
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(level=logging.CRITICAL)
    return

def logger_on():
    logging.basicConfig(level=logging.DEBUG)
    return
