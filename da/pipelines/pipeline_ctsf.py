"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
# pipeline.py

"""
.. module:: pipeline
.. moduleauthor:: Wouter Peters

Revision History:
File created on 06 Sep 2010

The pipeline module holds methods that execute consecutive tasks with each of the objects of the DA system.

"""
import logging
import os
import sys
import datetime
import copy

#from da.tools.pipeline import *
from da.pipelines.pipeline_cteco2 import *

header = '\n\n    ***************************************   '
footer = '    *************************************** \n  '


def ctsf_pipeline(dacycle, platform, dasystem, samples, statevector, fluxmodel, obsoperator, optimizer):
    sys.path.append(os.getcwd())

    logging.info(header + 'Initializing CTSF inversion cycle' + footer)

    #if type(samples) is not list: samples = [samples]
    if type(samples) is not list: samples = [samples]
    
    start_job(dacycle, dasystem, platform, statevector, samples, fluxmodel, obsoperator)

    prepare_state(dacycle, statevector)

    sample_state(dacycle, samples, statevector, fluxmodel, obsoperator)

    invert(dacycle, statevector, optimizer)

    write_optimized_state_and_fluxes(dacycle, samples, statevector, fluxmodel, obsoperator)

    logging.info('Inversion cycle finished... exiting pipeline')

def ctsf_pipeline_fwd(dacycle, platform, dasystem, samples, statevector, fluxmodel, obsoperator):
    sys.path.append(os.getcwd())

    logging.info(header + 'Initializing CTSF inversion cycle' + footer)

    if type(samples) is not list: samples = [samples]
    
    start_job(dacycle, dasystem, platform, statevector, samples, fluxmodel, obsoperator)

    prepare_state(dacycle, statevector)

    sample_state(dacycle, samples, statevector, fluxmodel, obsoperator)

    logging.info('forward run finished cycle finished... exiting pipeline')


def ctsf_forward_pipeline(dacycle, platform, dasystem, samples, statevector, fluxmodel, obsoperator):
    """ The main point of entry for the pipeline.
        For forward run to get mole fractions at independent stations, make sure to define initial
        condition statevector such that result from inverse run is used! """
    sys.path.append(os.getcwd())

    if type(samples) is not list: samples = [samples]

    logging.info(header + 'Initializing CTSF forward cycle' + footer)
    start_job(dacycle, dasystem, platform, statevector, samples, fluxmodel, obsoperator)

    if dacycle.has_key('forward.savestate.file'):
        fwdfile = dacycle['forward.savestate.file']
    else:
        logging.debug("No forward.savestate.file key found in rc-file, proceeding with self-constructed prior parameters")
        fwdfile = False

    if not fwdfile:
        # Simply make a prior statevector using the normal method
        prepare_state(dacycle, statevector)
    else:
        # Read prior information from another simulation into the statevector.
        # This loads the results from another assimilation experiment into the current statevector
        statevector.read_from_file(fwdfile, 'prior')

    # We write this "prior" statevector to the restart directory, so we can later also populate it with the posterior statevector
    # Note that we could achieve the same by just copying the wanted forward savestate.nc file to the restart folder of our current
    # experiment, but then it would already contain a posterior field as well which we will try to write in save_and_submit.
    # This could cause problems. Moreover, this method allows us to read older formatted savestate.nc files (legacy) and write them into
    # the current format through the "write_to_file" method.
    savefilename = os.path.join(dacycle['dir.restart'], 'savestate_%s.nc' % dacycle['time.start'].strftime('%Y%m%d'))
    statevector.write_to_file(savefilename, 'prior')

    # Now read optimized statevector which we will actually use to propagate through the system
    if not fwdfile:
        # if there is no forward dir specified, we simply run forward with unoptimized prior fluxes in the statevector
        logging.info("Running forward with prior savestate from: %s" %savefilename)
    else:
        # Read posterior information from another simulation into the statevector.
        # This loads the results from another assimilation experiment into the current statevector
        statevector.read_from_file(fwdfile, 'opt')
        logging.info("Running forward with optimized savestate from: %s" %fwdfile)

    # Finally, we run forward with these parameters
    advance(dacycle, samples, statevector, obsoperator, fluxmodel)

    # In save_and_submit, the posterior statevector will be added to the savestate.nc file, and it is added to the copy list.
    # This way, we have both the prior and posterior data from another run copied into this assimilation, for later analysis.
    save_and_submit(dacycle, statevector)

    logging.info("Forward cycle finished...exiting pipeline")


def save_and_submit(dacycle, statevector):
    """ Save the model state and submit the next job """
    logging.info(header + "starting save_and_submit" + footer)

    filename = os.path.join(dacycle['dir.restart'], 'savestate_%s.nc' % dacycle['time.start'].strftime('%Y%m%d'))
    statevector.write_to_file(filename, 'opt')

    dacycle.output_filelist.append(filename)
    dacycle.finalize()




def advance(dacycle, samples, statevector, obsoperator, fluxmodel):
    """ Advance the filter state to the next step """

    # This is the advance of the modeled CO2 state. Optionally, routines can be added to advance the state vector (mean+covariance)

    # Then, restore model state from the start of the filter
    logging.info(header + "starting advance" + footer)
    logging.info("Sampling model will be run over 1 cycle")

    obsoperator.get_initial_data()
    sample_step(dacycle, samples, statevector, fluxmodel, obsoperator, 0, True)

    dacycle.restart_filelist.extend(obsoperator.restart_filelist)
    dacycle.output_filelist.extend(obsoperator.output_filelist)
    logging.debug("Appended ObsOperator restart and output file lists to dacycle for collection ")

    # write sample output file
    for sample in samples:
        dacycle.output_filelist.append(dacycle['ObsOperator.inputfile.'+sample.get_samples_type()])
        logging.debug("Appended Observation filename to dacycle for collection: %s"%(dacycle['ObsOperator.inputfile.'+sample.get_samples_type()]))

        sampling_coords_file = os.path.join(dacycle['dir.input'], sample.get_samples_type()+'_coordinates_%s.nc' % dacycle['time.sample.stamp'])
        if os.path.exists(sampling_coords_file):
            if sample.get_samples_type() == 'flask':
                outfile = os.path.join(dacycle['dir.output'], 'sample_auxiliary_%s.nc' % dacycle['time.sample.stamp'])
                sample.write_sample_auxiliary(outfile)
        else: logging.warning("Sample auxiliary output not written, because input file does not exist (no samples found in obspack)")
    del sample



def forward_pipeline_0(dacycle, platform, dasystem, samples, statevector, fluxmodel, obsoperator):
    """ The main point of entry for the pipeline.
        For forward run to get mole fractions at independent stations, make sure to define initial
        condition statevector such that result from inverse run is used! """
    sys.path.append(os.getcwd())

    if type(samples) is not list: samples = [samples]

    logging.info(header + 'Initializing CTSF forward cycle' + footer)
    start_job(dacycle, dasystem, platform, statevector, samples, fluxmodel, obsoperator)

    if dacycle.has_key('forward.savestate.file'):
        fwddir = dacycle['forward.savestate.file']
    else:
        logging.debug("No forward.savestate.file key found in rc-file, proceeding with self-constructed prior parameters")
        fwddir = False

    if not fwddir:
        # Simply make a prior statevector using the normal method
        prepare_state(dacycle, statevector)
    else:
        # Read prior information from another simulation into the statevector.
        # This loads the results from another assimilation experiment into the current statevector
        statevector.read_from_file(fwddir, 'opt')

    sample_state(dacycle, samples, statevector, fluxmodel, obsoperator)

    write_optimized_state_and_fluxes(dacycle, samples, statevector, fluxmodel, obsoperator, forwardrun=True)

    logging.info('Forward run finished... exiting pipeline')



def check_setup(dacycle, platform, dasystem, samples, statevector, fluxmodel, obsoperator, optimizer):
    sys.path.append(os.getcwd())

    logging.info(header + 'Initializing check_setup cycle' + footer)
    dasystem.validate()
    dacycle.dasystem = dasystem
    dacycle.daplatform = platform
    dacycle.setup()
    statevector.setup(dacycle)

    prepare_state(dacycle, statevector)

    logging.info(header + 'Addind observations' + footer)
    lag = 0
    dacycle.set_sample_times(lag)

    startdate = dacycle['time.sample.start']
    enddate = dacycle['time.sample.end']
    dacycle['time.sample.window'] = lag
    dacycle['time.sample.stamp'] = "%s_%s" % (startdate.strftime("%Y%m%d%H"), enddate.strftime("%Y%m%d%H"))
    logging.info("New simulation interval set : ")
    logging.info("                  start date : %s " % startdate.strftime('%F %H:%M'))
    logging.info("                  end   date : %s " % enddate.strftime('%F %H:%M'))
    logging.info("                  file  stamp: %s " % dacycle['time.sample.stamp'])

    # Create observation vector for simulation interval
    for sample in samples:

        sample.setup(dacycle)
        sample.add_observations()

        # If OSSE setup: overwrite obspack observation values by previously calculated samples
        if hasattr(sample, 'osse') and sample.osse: sample.add_osse_observations(sample.osse_file)

        # Add model-data mismatch to all samples, this *might* use output from the ensemble in the future??
        sample.add_model_data_mismatch(dacycle.dasystem['obs.sites.rc'], advance)

        sampling_coords_file = os.path.join(dacycle['dir.input'], sample.get_samples_type()+'_coordinates_%s.nc' % dacycle['time.sample.stamp'])
        logging.debug('sampling_coords_file = %s' % sampling_coords_file)
        sample.write_sample_coords(sampling_coords_file)

        # Write filename to dacycle, and to output collection list
        dacycle['ObsOperator.inputfile.'+sample.get_samples_type()] = sampling_coords_file

        sampling_coords_file = os.path.join(dacycle['dir.input'], 'sample_coordinates_%s.nc' % dacycle['time.sample.stamp'])

    del sample

    dacycle.finalize()
    logging.info('check_setup cycle finished... exiting pipeline')



####################################################################################################

def analysis_pipeline_fwd(dacycle, platform, dasystem, samples, statevector):
    """ Main entry point for analysis of ctdas results """

    from da.analysis.cteco2.expand_fluxes_ctsf import save_monthly_avg_1x1_data, save_monthly_avg_tc_data, save_monthly_avg_agg_data# save_monthly_avg_state_data, save_weekly_avg_ext_tc_data
    from da.analysis.cteco2.expand_molefractions import write_mole_fractions
    from da.analysis.cteco2.summarize_obs import summarize_obs
    from da.analysis.cteco2.time_avg_fluxes import time_avg

    logging.info(header + "Starting analysis" + footer)

    dasystem.validate()
    dacycle.dasystem = dasystem
    dacycle.daplatform = platform
    dacycle.setup()
    statevector.setup(dacycle)

    logging.info(header + "Starting mole fractions" + footer)

    write_mole_fractions(dacycle)
    summarize_obs(dacycle['dir.analysis'])

    logging.info(header + "Starting weekly averages" + footer)

    save_weekly_avg_1x1_data(dacycle, statevector)
    save_weekly_avg_state_data(dacycle, statevector)
    save_weekly_avg_tc_data(dacycle, statevector)
    save_weekly_avg_ext_tc_data(dacycle)
    save_weekly_avg_agg_data(dacycle,region_aggregate='transcom')
    save_weekly_avg_agg_data(dacycle,region_aggregate='transcom_extended')
    #save_weekly_avg_agg_data(dacycle,region_aggregate='olson')
    #save_weekly_avg_agg_data(dacycle,region_aggregate='olson_extended')
    save_weekly_avg_agg_data(dacycle,region_aggregate='country')

    logging.info(header + "Starting monthly and yearly averages" + footer)

    time_avg(dacycle,'flux1x1')
    time_avg(dacycle,'transcom')
    time_avg(dacycle,'transcom_extended')
    time_avg(dacycle,'olson')
    time_avg(dacycle,'olson_extended')
    time_avg(dacycle,'country')

    logging.info(header + "Finished analysis" + footer)



def analysis_pipeline(dacycle, platform, dasystem, samples, statevector):
    """ Main entry point for analysis of ctdas results """

    logging.info(header + "Starting analysis" + footer)

    from da.analysis.cteco2.expand_fluxes_ctsf import save_monthly_avg_1x1_data, save_monthly_avg_tc_data, save_monthly_avg_agg_data #save_monthly_avg_state_data#, save_weekly_avg_ext_tc_data
    #from da.analysis.expand_molefractions import write_mole_fractions
    from da.analysis.cteco2.expand_molefractions import write_mole_fractions
    #from da.analysis.summarize_obs import summarize_obs
    from da.analysis.cteco2.summarize_obs import summarize_obs
    #from da.analysis.time_avg_fluxes import time_avg
    from da.analysis.cteco2.time_avg_fluxes import time_avg

    # logging.info(header + "Starting analysis" + footer)

    dasystem.validate()
    dacycle.dasystem = dasystem
    dacycle.daplatform = platform
    dacycle.setup()
    statevector.setup(dacycle)

    logging.info(header + "Starting mole fractions" + footer)

    write_mole_fractions(dacycle)
    summarize_obs(dacycle['dir.analysis'])

    logging.info(header + "Starting monthly averages" + footer)
    save_monthly_avg_1x1_data(dacycle, statevector)
    # save_weekly_avg_state_data(dacycle, statevector)
    save_monthly_avg_tc_data(dacycle, statevector)
    save_monthly_avg_agg_data(dacycle,region_aggregate='transcom')
    save_monthly_avg_agg_data(dacycle,region_aggregate='transcom_extended')
    # save_monthly_avg_agg_data(dacycle,region_aggregate='olson')
    # save_monthly_avg_agg_data(dacycle,region_aggregate='olson_extended')
    save_monthly_avg_agg_data(dacycle,region_aggregate='country')

    logging.info(header + "Starting monthly and yearly averages" + footer)

    time_avg(dacycle,'flux1x1',monthly=True)
    time_avg(dacycle,'transcom',monthly=True)
    time_avg(dacycle,'transcom_extended',monthly=True)
    time_avg(dacycle,'olson',monthly=True)
    time_avg(dacycle,'olson_extended',monthly=True)
    time_avg(dacycle,'country',monthly=True)

    logging.info(header + "Finished analysis" + footer)


####################################################################################################


def start_job(dacycle, dasystem, platform, statevector, samples, fluxmodel, obsoperator):
    """ Set up the job specific directory structure and create an expanded rc-file """

    dasystem.validate()
    dacycle.dasystem = dasystem
    dacycle.daplatform = platform
    dacycle.setup()
    statevector.setup(dacycle)
    fluxmodel.setup(dacycle)    # Settings for calculating flux from statistical fit
    obsoperator.setup(dacycle)  # Setup Observation Operator



def sample_state(dacycle, samples, statevector, fluxmodel, obsoperator):
    """ Sample the filter state for the inversion """

    # Before a forecast step, save all the data to a save/tmp directory so we can later recover it before the propagation step.
    # This is especially important for:
    #  (i) The transport model restart data which holds the background mole fractions. This is needed to run the model one cycle forward
    #  (ii) The random numbers (or the seed for the random number generator) so we can recreate the ensembles if needed

    logging.info(header + "starting sample_state" + footer)
    nlag = int(dacycle['time.nlag'])
    logging.info("Sampling model will be run over %d cycles" % nlag)

    obsoperator.get_initial_data()

    for lag in range(nlag): # no lag is used for CTSF!
        logging.info(header + ".....Ensemble Kalman Filter at lag %d" % (lag + 1))

        ############# Perform the actual sampling loop #####################

        sample_step(dacycle, samples, statevector, fluxmodel, obsoperator, lag)

        logging.debug("statevector now carries %d samples" % statevector.nobs)



def sample_step(dacycle, samples, statevector, fluxmodel, obsoperator, lag, advance=False):
    """ Perform all actions needed to sample one cycle """

    # First set up the information for time start and time end of this sample
    dacycle.set_sample_times(lag)

    startdate = dacycle['time.sample.start']
    enddate   = dacycle['time.sample.end']
    dacycle['time.sample.window'] = lag
    dacycle['time.sample.stamp']  = "%s_%s" % (startdate.strftime("%Y%m%d%H"), enddate.strftime("%Y%m%d%H"))

    logging.info("New simulation interval set : ")
    logging.info("                  start date : %s " % startdate.strftime('%F %H:%M'))
    logging.info("                  end   date : %s " % enddate.strftime('%F %H:%M'))
    logging.info("                  file  stamp: %s " % dacycle['time.sample.stamp'])

    # Implement something that writes the ensemble member parameter info to file, or manipulates them further into the
    # type of info needed in your transport model

    statevector.write_members_to_file(lag, dacycle['dir.input']) # parametervalues, parametermap

    for sample in samples: # loop over obs objects in samples. This is probably gonna be a single object only!
 
        sample.setup(dacycle)
        #sample.add_observations(advance) # Add observations to sample.datalist. advance input not used?
        sample.add_observations()        


        # If OSSE setup: overwrite obspack observation values by previously calculated samples
        if hasattr(sample, 'osse') and sample.osse: sample.add_osse_observations(sample.osse_file)

        # Add model-data mismatch to all samples, this *might* use output from the ensemble in the future??
        #sample.add_model_data_mismatch(dacycle.dasystem['obs.sites.rc'], advance) # add mdm to each obs in sample. File and advance input not used
        sample.add_model_data_mismatch(dacycle.dasystem['obs.sites.rc']) # add mdm to each obs in sample. File and advance input not used

        sampling_coords_file = os.path.join(dacycle['dir.input'], sample.get_samples_type()+'_coordinates_%s.nc' % dacycle['time.sample.stamp'])
        logging.debug('sampling_coords_file = %s' % sampling_coords_file)
        sample.write_sample_coords(sampling_coords_file)

        # Write filename to dacycle, and to output collection list
        dacycle['ObsOperator.inputfile.'+sample.get_samples_type()] = sampling_coords_file # save path to file that contains all information of obs in current sample object
        # sampling_coords_file = os.path.join(dacycle['dir.input'], 'sample_coordinates_%s.nc' % dacycle['time.sample.stamp'])

    del sample

    # Run the observation operator (TM5 to get atmospheric CO2 concentrations)
    obsoperator.run_forecast_model(fluxmodel, samples)

    # Read forecast model samples that were written to NetCDF files by each member. Add them to the existing
    # Observation object for each sample loop. This data fill be written to file in the output folder for each sample cycle.

    # We retrieve all model samples from one output file written by the ObsOperator. If the ObsOperator creates
    # one file per member, some logic needs to be included to merge all files!!!

    for i in range(len(samples)):
        if os.path.exists(dacycle['ObsOperator.inputfile.'+samples[i].get_samples_type()]):
            samples[i].add_simulations(obsoperator.simulated_file[i])
        else: logging.warning("No simulations added, because sample input file does not exist")
    del i

    # Now add the observations that need to be assimilated to the statevector.
    # Note that obs will only be added to the statevector if either this is the first step (restart=False), or lag==nlag
    # This is to make sure that the first step of the system uses all observations available, while the subsequent
    # steps only optimize against the data at the front (lag==nlag) of the filter. This way, each observation is used only
    # (and at least) once # in the assimilation

    if not advance:
        if dacycle['time.restart'] == False or lag == int(dacycle['time.nlag']) - 1:
            statevector.obs_to_assimilate += (copy.deepcopy(samples),) # shouldn't I add samples[i] separately?
            for sample in samples:
                statevector.nobs += sample.getlength()
            del sample
            logging.info('nobs = %i' %statevector.nobs)
            logging.debug("Added samples from the observation operator to the assimilated obs list in the statevector")

        else:
            statevector.obs_to_assimilate += (None,)



def invert(dacycle, statevector, optimizer):
    """ Perform the inverse calculation """
    logging.info(header + "starting invert" + footer)

    if statevector.nobs <= 1:
        logging.info('List with observations to assimilate is empty, skipping invert step and continuing without statevector update...')
        return

    dims   = (int(dacycle['time.nlag']),
              int(dacycle['da.optimizer.nmembers']),
              statevector.nparams,
              statevector.nobs)

    #if not dacycle.dasystem.has_key('opt.algorithm'):
    if 'opt.algorithm' not in dacycle.dasystem:
        logging.info("There was no minimum least squares algorithm specified in the DA System rc file (key : opt.algorithm)")
        logging.info("...using serial algorithm as default...")
        optimizer.set_algorithm('Serial')
    elif dacycle.dasystem['opt.algorithm'] == 'serial':
        logging.info("Using the serial minimum least squares algorithm to solve ENKF equations")
        optimizer.set_algorithm('Serial')
    elif dacycle.dasystem['opt.algorithm'] == 'bulk':
        logging.info("Using the bulk minimum least squares algorithm to solve ENKF equations")
        optimizer.set_algorithm('Bulk')

    optimizer.setup(dims)
    optimizer.state_to_matrix(statevector)

    diagnostics_file = os.path.join(dacycle['dir.output'], 'optimizer.%s.nc' % dacycle['time.start'].strftime('%Y%m%d'))

    optimizer.write_diagnostics(diagnostics_file, 'prior')
    optimizer.set_localization(dacycle['da.system.localization'])

    if optimizer.algorithm == 'Serial':
        optimizer.serial_minimum_least_squares()
    else:
        optimizer.bulk_minimum_least_squares()

    optimizer.matrix_to_state(statevector)
    optimizer.write_diagnostics(diagnostics_file, 'optimized')



def write_optimized_state_and_fluxes(dacycle, samples, statevector, fluxmodel, obsoperator, forwardrun=False):
    """ Calculate the fluxes for the optimized statevector and write to output file """

    logging.info(header + 'Starting write_optimized_state_and_fluxes' + footer)

    filename = os.path.join(dacycle['dir.restart'], 'savestate_%s.nc' % dacycle['time.start'].strftime('%Y%m%d'))
    statevector.write_to_file(filename, 'opt')

    dacycle.output_filelist.extend(obsoperator.output_filelist)
    dacycle.output_filelist.append(filename)

    statevector.write_members_to_file(0, dacycle['dir.output'])

    # Run the observation operator (TM5 to get atmospheric CO2 concentrations)
    if forwardrun:
        obsoperator.run_forecast_model(fluxmodel, samples, postprocessing=True, calc_flux_only=True)
    else:
        obsoperator.run_forecast_model(fluxmodel, samples, postprocessing=True)

    # sampling_coords_file = os.path.join(dacycle['dir.input'], 'sample_coordinates_%s.nc' % dacycle['time.sample.stamp'])
    for sample in samples:
        sampling_coords_file = os.path.join(dacycle['dir.input'], sample.get_samples_type()+'_coordinates_%s.nc' % dacycle['time.sample.stamp'])
    del sample

    if os.path.exists(sampling_coords_file):
        for sample in samples:
            if sample.get_samples_type() == 'flask':
                outfile = os.path.join(dacycle['dir.output'], 'sample_auxiliary_%s.nc' % dacycle['time.sample.stamp'])
                sample.write_sample_auxiliary(outfile)
    else: logging.warning("Sample auxiliary output not written, because input file does not exist (no samples found in obspack)")

    dacycle.output_filelist.extend(obsoperator.output_filelist)
    dacycle.finalize()
