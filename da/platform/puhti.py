#!/usr/bin/env python
# fmi.py

"""
Author : Aki 

Revision History:
File created on 15 Noc 2019.

"""

import logging
import subprocess

from da.baseclasses.platform import Platform
std_joboptions = {'jobaccount':'ch4'}

class FmiPlatform(Platform):
    def __init__(self):
        self.ID     = 'Puhti-FMI'    # the identifier gives the platform name
        self.version = ''     # the platform version used

    #def give_blocking_flag(self):
    #    """
    #    Returns a blocking flag, which is important if tm5 is submitted in a queue system. The python ctdas code is forced to wait before tm5 run is finished
    #    """
    #    return "-s"

    #def give_queue_type(self):
    #    """
    #    Return a queue type depending whether your computer system has a queue system, or whether you prefer to run in the foreground. 
    #    On most large systems using the queue is mandatory if you run a large job.
    #    """
    #    return "queue"

    def get_job_template(self, joboptions={}, block=False):
        """ 
        Returns the job template for a given computing system, and fill it with options from the dictionary provided as argument.
        The job template should return the preamble of a job that can be submitted to a queue on your platform, 
        examples of popular queuing systems are:
            - SGE
            - MOAB
            - XGrid
            -

        A list of job options can be passed through a dictionary, which are then filled in on the proper line,
        an example is for instance passing the dictionary {'account':'co2'} which will be placed 
        after the ``-A`` flag in a ``qsub`` environment.

        An extra option ``block`` has been added that allows the job template to be configured to block the current
        job until the submitted job in this template has been completed fully.
        """

        template = """#!/bin/bash"""+ \
                   """\n"""+ \
                   """#SBATCH --job-name=jobname \n"""+ \
                   """#SBATCH --partition=fmi \n"""+ \
                   """#SBATCH --account=projectaccount \n"""+ \
                   """#SBATCH --time=jobtime \n"""+ \
                   """#SBATCH -o %x.o%j \n"""+ \
                   """#SBATCH --ntasks-per-node=jobnpes \n"""+ \
                   """#SBATCH --nodes=nnodes \n"""+ \
                   """#SBATCH --mem-per-cpu=mpc \n"""+ \
                   """\n"""+ \
                   """source /fmi/projappl/project_2001147/env_tm5_rhel8.sh"""+ \
                   """\n"""
        #"""#SBATCH --mem-per-cpu=jobncpu \n"""+ \
        #"""#SBATCH --mem-per-cpu=50G \n"""+ \
        #"""#SBATCH --ntasks=jobnpes \n"""+ \
        if 'depends' in joboptions:
            template += """#$ -hold_jid depends \n"""

        # First replace from passed dictionary
        for k,v in joboptions.items():
            while k in template:
                template = template.replace(k,v)

        # Fill remaining values with std_options
        for k,v in std_joboptions.items():
            while k in template:
                template = template.replace(k,v)

        return template


        #msg1  = 'Platform initialized: %s'%self.Identifier      ; logging.info(msg1)
        #msg2  = '%s version: %s'%(self.Identifier,self.Version) ; logging.info(msg2)


    def submit_job(self, jobfile, joblog=None, block=False):
        """ This method submits a jobfile to the queue, and returns the queue ID """

        if block:
            cmd = ["sbatch", "-s", jobfile]
            logging.info("A new task will be started (%s)" % cmd)
            output = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
            logging.info(output)
            print('output', output)
            jobid = output.split()[0]             
            print('jobid', jobid)
        else:
            cmd = ["sbatch", jobfile]
            logging.info("A new task will be started (%s)" % cmd)
            output = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]  ; logging.info(output)
            jobid = output.split()[-1].decode("utf-8")

        return jobid


if __name__ == "__main__":
    pass
