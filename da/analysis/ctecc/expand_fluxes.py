"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters. 
Users are recommended to contact the developers (wouter.peters@wur.nl) to receivstope
updates of the code. See also: http://www.carbontracker.eu. 

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation, 
version 3. This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. 

You should have received a copy of the GNU General Public License along with this 
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
# expand_fluxes.py
"""
Author: Joram Hooghiem 
Revision History:
File created on 05 May 2022.
based on original version by Wouter Peters
"""

import sys
import subprocess
import os
sys.path.append('../../') 
rootdir = os.getcwd().split('da/')[0] 
analysisdir = os.path.join(rootdir, 'da/analysis/cteco2/')


from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import shutil

import logging
import numpy as np
from da.tools.general import date2num, num2date, create_dirs,advance_time
import da.tools.io4 as io
import netCDF4 as cdf

from   da.analysis.cteco2.tools_regions import globarea, state_to_grid
import da.analysis.cteco2.tools_transcom as tc 
import da.analysis.cteco2.tools_country as ct
import da.analysis.cteco2.tools_time as timetools

emissions={ 'co2'       : ['ff','fire','ocn','gppiav','reco_c','gpp_r','gpp_c','recoclim','reco_r'],
            'co2c13'    : ['ff','fire','bio','ocn','biodis','ocndis','gpp'] }
# parametermap acts on:
experiments={'classic'  :{  'co2': ['bio','ocn'] ,
                            'co2c13' : []         },
             'ctsf'     :{  'co2': ['recoclim','gppiav','ocn'] ,
                            'co2c13' : ['biodis','gpp','ocndis']         }
             }
def save_weekly_avg_1x1_data(dacycle, statevector):
    """
        Function creates a NetCDF file with output on 1x1 degree grid. It uses the flux data written by the 
        :class:`~da.baseclasses.obsoperator.ObsOperator.py`, and multiplies these with the mapped parameters and
        variance (not covariance!) from the :class:`~da.baseclasses.statevector.StateVector`.
        
           :param dacycle: a :class:`~da.tools.initexit.CycleControl` object
           :param statevector: a :class:`~da.baseclasses.statevector.StateVector`
           :rtype: None
    """
    startdate,enddate,interval,dirname,experiment,counter=init_save_avg_data(dacycle,extention='flux1x1')
    proc_data_b=startdate
    more_in_cycle=True

    # get ensemble and mean data for current lag 
    if experiment=='classic':
        ensemble_data=get_ensemble_params(dacycle,statevector,startdate)
    while more_in_cycle:
    # process data in loop: 
        proc_data_e=advance_time(proc_data_b,interval)
        ndate=date2num(proc_data_b) - date2num(datetime(2000, 1, 1)) + (proc_data_e-proc_data_b).days / 2.0
        # logging.info('Processing output emissions from %s to %s' %(proc_data_b.strftime('%Y-%m-%d %H:%M'),proc_data_e.strftime('%Y-%m-%d %H:%M'))) 
        ncf=create_flux_file(dirname,proc_data_b,ndate,interval) 
        if not ncf==-1:  
            # in case of ctsf this is will have the 

            #
            # Create dimensions and lat/lon grid
            #
            dimgrid = ncf.add_latlon_dim()
            dimensemble = ncf.add_dim('members', statevector.nmembers)
            dimdate = ncf.add_date_dim()

            next = ncf.inq_unlimlen()[0]
            
            save_data(ncf,'date',ndate,dimdate,next) 
            save_data(ncf,'cell_area',globarea().tolist(),dimgrid) 

            
            if experiment=='ctsf':
                ensemble_data=get_ctsf_nee(dacycle,statevector,counter)
            # loop over species in dacycle.dasystem.ct.params.species
            for species in dacycle.dasystem['ct.params.species'].split(' '):
                ncf_in=get_input_file(dacycle,proc_data_b,proc_data_e,species)
                for emission in emissions[species]:
                    if experiment=='ctsf' and emission in experiments[experiment][species]:
                        data=np.ones((180,360))
                    else:
                        try:
                            data=np.array(ncf_in.get_variable('flux_%s_%s_prior_mean' %(species,emission)))
                        except KeyError: 
                            data=ensemble_data['%s_%s_flux_imp' %(species,emission) ]

                    if emission in experiments[experiment][species]:
                        process_emission(ncf,data,species,emission,dimdate,dimgrid,ensemble_data=ensemble_data,dimensemble=dimensemble,next=next)
                    else:
                        process_emission(ncf,data,species,emission,dimdate,dimgrid)

                ncf_in.close()

            ncf.close()

        #update proc_data_b
        counter+=1
        proc_data_b=proc_data_e
        if proc_data_b>=enddate:
            more_in_cycle=False
    


    # logging.info("Gridded weekly average fluxes now written")
    return
####
def init_save_avg_data(dacycle,extention=''):
    startdate = dacycle['time.start'] 
    enddate   = dacycle['time.end'] 
    counter=(dacycle['time.start'].year - dacycle['time.begin'].year)*12+(dacycle['time.start'].month - dacycle['time.begin'].month)
    # count the months before the start

    # logging.debug("DA Cycle start date is %s" % startdate.strftime('%Y-%m-%d %H:%M'))
    # logging.debug("DA Cycle end   date is %s" % enddate.strftime('%Y-%m-%d %H:%M'))

    experiment=dacycle.dasystem['ct.enkf.type']
    if experiment=='ctsf': 
        interval='monthly'
    else:
        interval="weekly"
    # logging.debug("Set output to %s" % interval)

    dirname = create_dirs(os.path.join(dacycle['dir.analysis'], 'data_%s_%s' % (extention,interval)))
    return startdate,enddate,interval,dirname,experiment,counter
####
## We may wish to move dirname/startdate creation into this function if not use elsewhere
def create_flux_file(dirname,startdate,ndate,interval,basename='flux_1x1'):
    """TODO: Docstring for create_flux_file.
    Returns
    -------
    TODO

    """
    if interval=='weekly':
        saveas = os.path.join(dirname, '%s.%s.nc' % (basename,startdate.strftime('%Y-%m-%d')))
    if interval=='monthly':
        saveas = os.path.join(dirname, '%s.%s.nc' % (basename,startdate.strftime('%Y-%m')))
    ncf = io.CT_CDF(saveas, 'write')
    if not ncf.has_date(ndate):
        #
        # set title and tell GMT that we are using "pixel registration"
        #
        setattr(ncf, 'Title', 'CarbonTracker fluxes')
        setattr(ncf, 'node_offset', 1)
        return ncf
    else:
        ncf.close() 
        # logging.warning('Skipping writing of data for date %s : already present in file %s' % (startdate.strftime('%Y-%m-%d'), saveas ))
        return -1

###



def save_data(ncf,varname,data,dims,count=None):
    logging.info("Writing variable %s to file" % varname )
    savedict = ncf.standard_var(varname=varname)
    savedict['values'] = data
    savedict['dims'] = dims
    if not count==None:
        savedict['count'] = count
    ncf.add_data(savedict)
    return

def get_input_file(dacycle,start,stop,species):
    """Docstring for get_input_file.
    opens an ncfile using the io4 ct_read
    filename will be constructed from startdate en enddate TYPE? 

    Parameters
    ----------
    species : string of the species name, which
    startdate :
    enddate :
    returns : carbontracker netcdf dataset object
    """
  
 
    filename = os.path.join(dacycle['dir.output'], 'flux1x1_%s_%s_%s.nc' % (species,start.strftime('%Y%m%d%H'), stop.strftime('%Y%m%d%H')))
    ncf_in = io.ct_read(filename, 'read')
    return ncf_in

def process_emission(ncf_tar,data,species,emission,dimdate,dimgrid,ensemble_data=None,dimensemble=None,next=0,experiment='classic'):
    """TODO: Docstring for process.
    Returns
    -------
    TODO

    """
    if ensemble_data!=None:
        for key,values in ensemble_data.items():
            if not species in key.split('_'): continue 
            if not emission in key.split('_'): continue 
            if 'ensemble' in key:
                if experiment=='ctsf':
                    save_data(ncf_tar,key,values.tolist(),dimdate + dimensemble + dimgrid ,next) 
                else:
                    save_data(ncf_tar,key,(data*values).tolist(),dimdate + dimensemble + dimgrid ,next) 
            else:
                if experiment=='ctsf':
                    save_data(ncf_tar,key,values.tolist(),dimdate +  dimgrid ,next) 
                else:
                    save_data(ncf_tar,key,(data*values).tolist(),dimdate +  dimgrid ,next) 
    else:
        save_data(ncf_tar,fetchkey(species,emission),data.tolist(),dimdate +  dimgrid ,next) 
    return

def fetchkey(species,emission):
    if emission in ['ff']:
        emissionfixed='fossil'
    elif emission in ['fire']:
        emissionfixed='fires'
    elif emission in ['biodiu']:
        emissionfixed='bio'
    elif emission in ['gpp_r']:
        emissionfixed='gppiav' 
    elif emission in ['reco_c']:
        emissionfixed='recoclim' 
    elif emission in ['gpp_c']:
        emissionfixed='gppclim' 
    elif emission in ['reco_r']:
        emissionfixed='recoiav' 
    else:
        emissionfixed=emission
    key='%s_%s_flux_imp' %(species,emissionfixed) 
    return key
def get_ctsf_nee(dacycle,statevector,counter):
    # savedir = dacycle['dir.output'].replace(startdate.strftime('%Y%m%d'), priordate.strftime('%Y%m%d'))
    filename = os.path.join(dacycle['dir.output'], 'savestate_%s.nc' % dacycle['time.begin'].strftime('%Y%m%d'))
    if os.path.exists(filename):
        dict_opt =statevector.state_to_grid(monthcounter=counter,filename=filename, qual='opt')
        dict_prior = statevector.state_to_grid(monthcounter=counter,filename=filename, qual='prior')
    #return the merged dictionary 
    return dict_prior | dict_opt

def get_ensemble_params(dacycle,statevector,startdate):
    '''Obtains ensemble params from both
    optimized and state
    '''
    # prior
    dt = dacycle['cyclelength']
    for n in reversed(range(1,statevector.nlag+1)):
        priordate = startdate + n*dt - timedelta(dt.days * n)
        savedir = dacycle['dir.output'].replace(startdate.strftime('%Y%m%d'), priordate.strftime('%Y%m%d'))
        filename = os.path.join(savedir, 'savestate_%s.nc' % priordate.strftime('%Y%m%d'))
        if os.path.exists(filename):
            statevector.read_from_file(filename, qual='prior')
            _, gridensemble_prior = statevector.state_to_grid(lag=n)

            # Replace the mean statevector by all ones (assumed priors)

            gridmean_prior = statevector.vector2grid(vectordata=np.ones(statevector.nparams,))

            # logging.debug('Read prior dataset from file %s, sds %d: ' % (filename, n))
            break
    # posterior
    savedir = dacycle['dir.output']
    filename = os.path.join(savedir, 'savestate_%s.nc' % startdate.strftime('%Y%m%d'))
    statevector.read_from_file(filename, qual='opt')
    # we can now get ecoregion priop and opt values
    # 
    gridmean_opt, gridensemble_opt = statevector.state_to_grid(lag=1)

    # logging.debug('Read posterior dataset from file %s, sds %d: ' % (filename, 1))
    ensemble_data={ 'opt'   :gridmean_opt,
                'opt_ensemble'   :gridensemble_opt ,
                'prior' :gridmean_prior,
                'prior_ensemble': gridensemble_prior }
    return ensemble_data

def save_weekly_avg_agg_data(dacycle, region_aggregate='olson'):
    """
        Function creates a NetCDF file with output on TransCom regions. It uses the flux input from the 
        function `save_weekly_avg_1x1_data` to create fluxes of length `nparameters`, which are then projected
        onto TC regions using the internal methods from :class:`~da.baseclasses.statevector.StateVector`.
        
           :param dacycle: a :class:`~da.tools.initexit.CycleControl` object
           :param StateVector: a :class:`~da.baseclasses.statevector.StateVector`
           :rtype: None

        This function only read the prior fluxes from the flux_1x1.nc files created before, because we want to convolve 
        these with the parameters in the statevector. This creates posterior fluxes, and the posterior covariance for the complete
        statevector in units of mol/box/s which we then turn into TC fluxes and covariances.
    """

    startdate,enddate,interval,dirname,experiment,counter=init_save_avg_data(dacycle,extention=region_aggregate)
    if interval=='monthly':
        date_fmt='%Y-%m'
    else:
        date_fmt='%Y-%m-%d'
    proc_data_b=startdate
    more_in_cycle=True
    counter=0
    # logging.debug("Aggregating 1x1 fluxes to %s totals" % region_aggregate)
    while more_in_cycle:
    # process data in loop: 
        proc_data_e=advance_time(proc_data_b,interval)
        ndate=date2num(proc_data_b) - date2num(datetime(2000, 1, 1)) + (proc_data_e-proc_data_b).days / 2.0
        # logging.info('Processing output emissions from %s to %s' %(proc_data_b.strftime('%Y-%m-%d %H:%M'),proc_data_e.strftime('%Y-%m-%d %H:%M'))) 
        infile = os.path.join(dacycle['dir.analysis'], 'data_flux1x1_%s' % interval, 'flux_1x1.%s.nc' % proc_data_b.strftime(date_fmt))
        if not os.path.exists(infile):
            logging.error("Needed input file (%s) does not exist yet, please create file first, returning..." % infile)
            return None

        ncf=create_flux_file(dirname,proc_data_b,ndate,interval,basename='%s_fluxes' % region_aggregate) 
        if not ncf==-1:  

            dimdate = ncf.add_date_dim()
            dimidateformat = ncf.add_date_dim_format()
            dimgrid = ncf.add_latlon_dim()  # for mask
            regionmask,dimname,dimregs,xform=get_region_data(ncf,region_aggregate)

            savedict = ncf.standard_var('unknown')
            savedict['name'] = 'regionmask'
            savedict['comment'] = 'numerical mask used to aggregate 1x1 flux fields, each integer 0,...,N is one region aggregated'
            savedict['values'] = regionmask.tolist()
            savedict['units'] = '-'
            savedict['dims'] = dimgrid
            savedict['count'] = 0
            ncf.add_data(savedict)

            # Get input data from 1x1 degree flux files
            ncf_in = io.ct_read(infile, 'read')

            try:
                dates = ncf_in.variables['date'][:]
            except KeyError:
                logging.error("The variable date cannot be found in the requested input file (%s) " % infile)
                logging.error("Please make sure you create gridded fluxes before making TC fluxes ")
                raise KeyError

            try:
                index = dates.tolist().index(ndate)
            except ValueError:
                logging.error("The requested cycle date is not yet available in file %s " % infile)
                logging.error("Please make sure you create state based fluxes before making TC fluxes ")
                raise ValueError

            # First add the date for this cycle to the file, this grows the unlimited dimension
            save_data(ncf,'date',ndate, dimdate,count=index)

            # Now convert other variables that were inside the statevector file

            # get area
            area = globarea()

            vardict = ncf_in.variables
            for vname, vprop in vardict.items():
                # logging.debug('processing %s' % vname)
                if vname in ['latitude','longitude', 'date', 'idate', 'std']: continue
                elif 'ensemble' in vname:

                    data = ncf_in.get_variable(vname)[index]
             
                    dimensemble = ncf.add_dim('members', data.shape[0])

                    regiondata = []
                    for member in data:
                        aggdata = state_to_grid(member * area, regionmask, reverse=True, mapname=region_aggregate)
                        regiondata.append(aggdata)

                    regiondata = np.array(regiondata)
                    try:
                        regioncov = regiondata.transpose().dot(regiondata) / (data.shape[0] - 1) 
                    except:
                        regioncov = np.dot(regiondata.transpose(), regiondata) / (data.shape[0] - 1) # Huygens fix 

                    if xform:
                        regioncov = tc.ExtendedTCRegions(regioncov,cov=True)
                
                    savedict = ncf.standard_var(varname=vname)
                    savedict['name'] = vname.replace('ensemble','covariance') 
                    savedict['units'] = '[mol/region/s]^2'
                    savedict['dims'] = dimdate + dimregs + dimregs
                    savedict['count'] = index
                    savedict['values'] = regioncov
                    ncf.add_data(savedict)

                    unit='mol/region/s'
                    dims=dimdate + dimensemble + dimregs
                
                elif 'flux' in vname:

                    data = ncf_in.get_variable(vname)[index]
                    regiondata = state_to_grid(data * area, regionmask, reverse=True, mapname=region_aggregate)

                    dims = dimdate + dimregs
                    unit = 'mol/region/s'

                else:

                    data = ncf_in.get_variable(vname)[:]
                    regiondata = state_to_grid(data, regionmask, reverse=True, mapname=region_aggregate)
                    dims = dimdate + dimregs
                    unit = ''

                if xform:
                    regiondata = tc.ExtendedTCRegions(regiondata)
                savedict = ncf.standard_var(varname=vname)
                if not unit =='':
                    savedict['units'] = unit 
                savedict['name'] = vname
                savedict['dims'] = dims 
                savedict['count'] = index
                savedict['values'] = regiondata
                ncf.add_data(savedict)

            ncf_in.close()
            ncf.close()
            # logging.info("%s aggregated %s average fluxes now written" % (dimname,interval))
        else:
            pass
    
        proc_data_b=proc_data_e
        if proc_data_b>=enddate:
            more_in_cycle=False

    return 

def get_region_data(ncf,region_aggregate):
#
#   Select regions to aggregate to
# 
    if region_aggregate == "olson":
        regionmask = tc.olson240mask
        dimname = 'olson'
        dimregs = ncf.add_dim(dimname, regionmask.max())

        regionnames = []
        for i in range(11):
            for j in range(19):
                regionnames.append("%s_%s" % (tc.transnams[i], tc.olsonnams[j],))
        regionnames.extend(tc.oifnams)
        xform = False

        for i, name in enumerate(regionnames):
            lab = 'Aggregate_Region_%03d' % (i + 1,)
            setattr(ncf, lab, name)

    elif region_aggregate == "olson_extended":
        regionmask = tc.olson_ext_mask
        dimname = 'olson_ext'
        dimregs = ncf.add_dim(dimname, regionmask.max())
        xform = False

        for i, name in enumerate(tc.olsonextnams):
            lab = 'Aggreate_Region_%03d'%(i+1)
            setattr(ncf, lab, name)

    elif region_aggregate == "transcom":
        regionmask = tc.transcommask
        dimname = 'tc'
        dimregs = ncf.add_region_dim(type='tc')
        xform = False

    elif region_aggregate == "transcom_extended":
        regionmask = tc.transcommask
        dimname = 'tc_ext'
        dimregs = ncf.add_region_dim(type='tc_ext')
        xform = True
    
    elif region_aggregate == "amazon":
        regfile = cdf.Dataset(os.path.join(analysisdir,'amazon_mask.nc'))
        regionmask = regfile.variables['regionmask'][:]
        regfile.close()
        dimname = 'amazon'
        dimregs = ncf.add_dim(dimname, regionmask.max())
        xform = False

    elif region_aggregate == "country":

        xform = False
        countrydict = ct.get_countrydict()
        selected = ['Russia', 'Canada', 'China', 'United States', 'EU27', 'Brazil', 'Australia', 'India'] #,'G8','UNFCCC_annex1','UNFCCC_annex2']
        regionmask = np.zeros((180, 360,), 'float')

        for i, name in enumerate(selected):
            lab = 'Country_%03d' % (i + 1,)
            setattr(ncf, lab, name)

            if name == 'EU27':
                namelist = ct.EU27
            elif name == 'EU25':
                namelist = ct.EU25
            elif name == 'G8':
                namelist = ct.G8
            elif name == 'UNFCCC_annex1':
                namelist = ct.annex1
            elif name == 'UNFCCC_annex2':
                namelist = ct.annex2
            else:
                namelist = [name]

            for countryname in namelist:
                try:
                    country = countrydict[countryname]
                    regionmask.put(country.gridnr, i + 1)
                except:
                    continue

        dimname = 'country'
        dimregs = ncf.add_dim(dimname, regionmask.max())

    return regionmask,dimname,dimregs,xform

def time_avg(dacycle,avg='transcom',monthly=False):
    """ Function to create a set of averaged files in a folder, needed to make longer term means """
    
    if avg not in ['transcom','transcom_extended','olson','olson_extended','country','flux1x1']:
        raise IOError('Choice of averaging invalid')

    analysisdir = dacycle['dir.analysis']

    if not os.path.exists(analysisdir):
        raise IOError('analysis dir requested (%s) does not exist, exiting...'%analysisdir)

    if dacycle.dasystem['ct.enkf.type']=='classic':
        daily_avg(dacycle,avg)
        monthly_avg(dacycle,avg)

    # These functions require still to process if output frequence < inversion cycle length
    yearly_avg(dacycle,avg)
    longterm_avg(dacycle,avg)

    return
def new_month(dacycle):
    """ check whether we just entered a new month"""

    this_month = dacycle['time.start'].month
    prev_month = (dacycle['time.start']-dacycle['cyclelength']).month

    return (this_month != prev_month)

def new_year(dacycle):
    """ check whether we just entered a new year"""

    this_year = dacycle['time.start'].year
    prev_year = (dacycle['time.start']-dacycle['cyclelength']).year

    return (this_year != prev_year)

def daily_avg(dacycle,avg):
    """ Function to create a set of daily files in a folder, needed to make longer term means """
    
    if avg not in ['transcom','transcom_extended','olson','olson_extended','country','flux1x1']:
        raise IOError('Choice of averaging invalid')

    analysisdir = dacycle['dir.analysis']
    weekdir = os.path.join(analysisdir , 'data_%s_weekly'%avg)
    daydir = os.path.join(analysisdir , 'data_%s_daily'%avg)

    if not os.path.exists(daydir):
        print("Creating new output directory " + daydir)
        os.makedirs(daydir)

    files  = os.listdir(weekdir)
    files = [f for f in files if '-' in f and f.endswith('.nc')]

    fileinfo = {}
    for filename in files:
        date=datetime.strptime(filename.split('.')[-2],'%Y-%m-%d')
        fileinfo[filename] = date
    
    dt = dacycle['cyclelength']

    for k,v in fileinfo.items():
        cycle_file = os.path.join(weekdir,k)
        for i in range(abs(dt.days)):
            daily_file = os.path.join(daydir,'%s_fluxes.%s.nc'%(avg,(v+timedelta(days=i)).strftime('%Y-%m-%d')))
            if not os.path.lexists(daily_file):
                os.symlink(cycle_file,daily_file)
                #print daily_file,cycle_file

def monthly_avg(dacycle,avg):
    """ Function to average a set of files in a folder from daily to monthly means """
    
    if avg not in ['transcom','transcom_extended','olson','olson_extended','country','flux1x1']:
        raise IOError('Choice of averaging invalid')

    analysisdir = dacycle['dir.analysis']

    daydir = os.path.join(analysisdir , 'data_%s_daily'%avg)
    monthdir = os.path.join(analysisdir,'data_%s_monthly'%avg)

    if not os.path.exists(monthdir):
        print("Creating new output directory " + monthdir)
        os.makedirs(monthdir)


    files  = os.listdir(daydir)  # get daily files
    files = [f for f in files if '-' in f and f.endswith('.nc')]

    if len(files) < 28:
        print('No month is yet complete, skipping monthly average')
        return

    fileinfo = {}
    for filename in files:  # parse date from each of them
        date=datetime.strptime(filename.split('.')[-2],'%Y-%m-%d')
        fileinfo[filename] = date

    years = [d.year for d in list(fileinfo.values())]   # get actual years
    months = set([d.month for d in list(fileinfo.values())])  # get actual months
   
    sd = datetime(min(years),1,1)
    ed = datetime(max(years)+1,1,1)

    while sd < ed: 

        nd = sd + relativedelta(months=+1)

        ndays_in_month = (nd-sd).days
        
        avg_files = [os.path.join(daydir,k) for k,v in fileinfo.items() if v < nd and v >= sd]
       
        if len(avg_files) != ndays_in_month: # only once month complete 
            #print 'New month (%02d) is not yet complete, skipping monthly average'%(sd.month)
            pass
        else:
            targetfile = os.path.join(monthdir,'%s_fluxes.%s.nc'%(avg,sd.strftime('%Y-%m')))
            if not os.path.exists(targetfile):
                print("New month (%02d) is complete, I have %d days for the next file"%(sd.month,ndays_in_month))
                command = ['ncra','-O']+ avg_files + [targetfile]
                status = subprocess.check_call(command)
            else:
                pass

        sd = nd

def yearly_avg(dacycle,avg):
    """ Function to average a set of files in a folder from monthly to yearly means """

    if avg not in ['transcom','transcom_extended','olson','olson_extended','country','flux1x1']:
        raise IOError('Choice of averaging invalid')

    analysisdir = dacycle['dir.analysis']
    monthdir = os.path.join(analysisdir , 'data_%s_monthly'%avg )
    yeardir = os.path.join(analysisdir,'data_%s_yearly'%avg)

    if not os.path.exists(yeardir):
        print("Creating new output directory " + yeardir)
        os.makedirs(yeardir)

    files  = os.listdir(monthdir)  # get monthly files
    files = [f for f in files if '-' in f and f.endswith('.nc')]

    if not files:
        print("No full year finished yet, skipping yearly average...")
        return

    fileinfo = {}
    for filename in files:
        date=datetime.strptime(filename.split('.')[-2],'%Y-%m')
        fileinfo[filename] = date

    years = set([d.year for d in list(fileinfo.values())])

    sd = datetime(min(years),1,1)
    ed = datetime(max(years)+1,1,1)

    while sd < ed: 

        nd = sd + relativedelta(years=+1)
        
        avg_files = [os.path.join(monthdir,k) for k,v in fileinfo.items() if v < nd and v >= sd]
       
        if not len(avg_files) == 12 : 
            print("Year %04d not finished yet, skipping yearly average..."%sd.year)
        else:
            targetfile = os.path.join(yeardir,'%s_fluxes.%s.nc'%(avg,sd.strftime('%Y')))
        
            if not os.path.exists(targetfile):
                print("Year %04d is complete, I have 12 months for the next file"%sd.year)
                command = ['ncra','-O']+ avg_files + [targetfile]
                status = subprocess.check_call(command)

        sd = nd

def longterm_avg(dacycle,avg):
    """ Function to average a set of files in a folder from monthly to yearly means """

    if avg not in ['transcom','transcom_extended','olson','olson_extended','country','flux1x1']:
        raise IOError('Choice of averaging invalid')

    analysisdir = dacycle['dir.analysis']

    yeardir = os.path.join(analysisdir , 'data_%s_yearly'%avg )
    longtermdir = os.path.join(analysisdir,'data_%s_longterm'%avg)

    if not os.path.exists(longtermdir):
        print("Creating new output directory " + longtermdir)
        os.makedirs(longtermdir)

    files  = os.listdir(yeardir)
    files = [f for f in files if f.endswith('.nc')]
    if not files:
        print("No full year finished yet, skipping longterm average...")
        return

    dates = []
    for filename in files:
        date=datetime.strptime(filename.split('.')[-2],'%Y')
        dates.append( date )

    avg_files = [os.path.join(yeardir,k) for k in files]
    targetfile=os.path.join(longtermdir,'%s_fluxes.%04d-%04d.nc'%(avg,min(dates).year, max(dates).year)) 
    if not os.path.exists(targetfile):
        if len(avg_files) > 0 : 
            command = ['ncra','-O']+ avg_files + [targetfile]
            status = subprocess.check_call(command)
    return
