"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters. 
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu. 

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation, 
version 3. This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. 

You should have received a copy of the GNU General Public License along with this 
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
# expand_fluxes.py
import sys
import os
import shutil
import logging
import numpy as np
from datetime import datetime, timedelta
sys.path.append('../../')
from da.tools.general import date2num, num2date, create_dirs
import da.tools.io4 as io

"""

Author: Joram Hooghiem 
after orginal version by Wouter Peters (Wouter.Peters@wur.nl) 

"""

def write_mole_fractions(dacycle,validation=False):
    """ 
    
    Write Sample information to NetCDF files. These files are organized by site and 
    have an unlimited time axis to which data is appended each cycle.

    The needed information is obtained from the sample_auxiliary.nc files and the original input data files from ObsPack. 

    The steps are:

    (1) Create a directory to hold timeseries output files
    (2) Read the sample_auxiliary.nc file for this cycle and get a list of original files they were obtained from
    (3) For each file, copy the original data file from ObsPack (if not yet present)
    (4) Open the copied file, find the index of each observation, fill in the simulated data
    
    """
    if validation:
        dirname = create_dirs(os.path.join(dacycle['dir.analysis'], 'data_molefractions_validation'))
    else:
        dirname = create_dirs(os.path.join(dacycle['dir.analysis'], 'data_molefractions'))
    #
    # Some help variables
    #
    startdate = dacycle['time.start'] 
    enddate = dacycle['time.end'] 

    logging.info("DA Cycle start date is %s" % startdate.strftime('%Y-%m-%d %H:%M'))
    logging.info("DA Cycle end   date is %s" % enddate.strftime('%Y-%m-%d %H:%M'))

    dacycle['time.sample.stamp'] = "%s_%s" % (startdate.strftime("%Y%m%d%H"), enddate.strftime("%Y%m%d%H"),)

    # Step (1): Get the posterior sample output data file for this cycle

    infile = os.path.join(dacycle['dir.output'], 'sample_auxiliary_%s.nc' % dacycle['time.sample.stamp'])

    ncf_in = io.ct_read(infile, 'read')

    if len(ncf_in.dimensions['obs']) == 0:
        ncf_in.close()
        return 

    prior_present=False
    optimized_present=False
    if "modelsamples_prior" in ncf_in.variables.keys():
        prior_present=True
        
    if "modelsamples_optimized" in ncf_in.variables.keys():
        optimized_present=True

    if not optimized_present and not prior_present: 
        ncf_in.close()
        return
    
    infiles = ncf_in.get_variable('inputfilename')

    sitedata={}
    for orig_file in set(infiles):
        logging.debug('Gathering data for %s' % orig_file)
        sitedata[orig_file]={}
        sitedata[orig_file]['assimilated']=False
        mask=(infiles==orig_file)

        sitedata[orig_file]['obs_num']=ncf_in.get_variable('obs_num')[mask]
        sitedata[orig_file]['isordered']=all(np.diff(sitedata[orig_file]['obs_num'])==1)
        # logging.debug('isordered %s' % sitedata[orig_file]['isordered'])
        sitedata[orig_file]['obs_val']=ncf_in.get_variable('observed')[mask]
        sitedata[orig_file]['mdm']=ncf_in.get_variable('modeldatamismatch')[mask]
        sitedata[orig_file]['flag']=np.array([99]*len(sitedata[orig_file]['mdm']))

        sitedata[orig_file]['fc_hphtr']=np.array([np.nan]*len(sitedata[orig_file]['mdm']))
        # logging.debug('%s %s' % (len(sitedata[orig_file]['flag']),len(sitedata[orig_file]['mdm'])))
        if prior_present:
            sitedata[orig_file]['simulated_prior']=ncf_in.get_variable('modelsamples_prior')[mask]
        if optimized_present:
            sitedata[orig_file]['simulated_optimized']=ncf_in.get_variable('modelsamples_optimized')[mask]
        if dacycle.dasystem['obspack.meteo.sampling']:
            sitedata[orig_file]['q']   = ncf_in.get_variable('q')[mask]
            sitedata[orig_file]['p']   = ncf_in.get_variable('p')[mask]
            sitedata[orig_file]['T']   = ncf_in.get_variable('T')[mask]
            sitedata[orig_file]['blh'] = ncf_in.get_variable('blh')[mask]
            sitedata[orig_file]['u']   = ncf_in.get_variable('u')[mask]
            sitedata[orig_file]['v']   = ncf_in.get_variable('v')[mask]
    ncf_in.close()

    # In case of reanalysis on different platform, obspack-input-directory might have a different name. 
    # This is checked here, and the filenames are corrected
    # dir_from_rc = dacycle.dasystem['obspack.input.dir']
    # dir_from_output = infiles[0]
    # d1 = dir_from_rc[:dir_from_rc.find('obspacks')]
    # d2 = dir_from_output[:dir_from_output.find('obspacks')]
    # if not d1 == d2:
        # for i,original in enumerate(infiles):
            # logging.debug('replacing %s' % original )
            # infiles[i]=original.replace(d2,d1)
            # logging.debug('     with %s' % infiles[i] )

    # Step (2): Get the optimization flags and k 

    if optimized_present and not validation:
        if dacycle.dasystem['ct.enkf.type']=='ctsf':
            infile = os.path.join(dacycle['dir.output'], 'optimizer.%s.nc' % dacycle['time.begin'].strftime('%Y%m%d'))
        else:
            infile = os.path.join(dacycle['dir.output'], 'optimizer.%s.nc' % startdate.strftime('%Y%m%d'))

        ncf_fc_in = io.ct_read(infile, 'read')

        fc_sitecodes = ncf_fc_in.get_variable('sitecode')
        flags=ncf_fc_in.get_variable('flag')
        obsnums=ncf_fc_in.get_variable('obspack_num')
        if dacycle.dasystem['opt.algorithm'] == 'serial':
            hphtr= ncf_fc_in.get_variable('totalmolefractionvariance')
        elif dacycle.dasystem['opt.algorithm'] == 'bulk':
            hphtr== ncf_fc_in.get_variable('totalmolefractionvariance').diagonal()
        optimized_data={ so[0]+str(so[1]) : i for i,so in enumerate(zip(fc_sitecodes,obsnums))}
        ncf_fc_in.close()
        for site in sitedata.keys():
            # The exception if data is not assimilated, which means it is not present in the optimizer
            sc=site.split('/')[-1].split('.nc')[0]
            if sum(sc==fc_sitecodes)==0: continue
            logging.debug("Found assimilated site %s" % sc)  
            if not sitedata[site]['assimilated']:
                sitedata[site]['assimilated']=True
            for i,num in enumerate(sitedata[site]['obs_num']):
                try:
                    idx=optimized_data[sc+str(num)]
                    sitedata[site]['flag'][i]=flags[idx]
                    sitedata[site]['fc_hphtr'][i]=hphtr[idx]                 
                except KeyError:
                    continue
            # logging.debug("Found assimilated site %s with %s assimilated samples and %s optimized values" % (site,sum(mask),len(sitedata[orig_file]['simulated_optimized'])))

                        
                        
            # print(sitedata[site]['fc_hphtr'])
        # Expand the list of input files with those available from the forecast list

        # infiles_rootdir = os.path.split(infiles[0])[0]
        # infiles.extend(os.path.join(infiles_rootdir, f + '.nc') for f in fc_sitecodes)

    
    #Step (2): For each observation timeseries we now have data for, open it and fill with data

    for orig_file in set(infiles):

        if not os.path.exists(orig_file):
            logging.error("The original input file (%s) could not be found, continuing to next file..." % orig_file)
            continue

        copy_file = os.path.join(dirname, os.path.split(orig_file)[-1])
        if not os.path.exists(copy_file):
            shutil.copy(orig_file, copy_file)
            logging.debug("Copied a new original file (%s) to the analysis directory" % orig_file)

            ncf_out = io.CT_CDF(copy_file, 'write')

            # Modify the attributes of the file to reflect added data from CTDAS properly
            try:
                host=os.environ['HOSTNAME']
            except:
                host='unknown'
     

            ncf_out.Caution = '==================================================================================='
            try:
                ncf_out.History += '\nOriginal observation file modified by user %s on %s\n' % (os.environ['USER'], datetime.today().strftime('%F'),)
            except:
                ncf_out.History = '\nOriginal observation file modified by user %s on %s\n' % (os.environ['USER'], datetime.today().strftime('%F'),)
            ncf_out.CTDAS_info = 'Simulated values added from a CTDAS run by %s on %s\n' % (os.environ['USER'], datetime.today().strftime('%F'),)\
                               + '\nCTDAS was run on platform %s' % (host,)\
                               + '\nCTDAS job directory was %s' % (dacycle['dir.da_run'],)\
                               + '\nCTDAS Da System was %s' % (dacycle['da.system'],)\
                               + '\nCTDAS Da ObsOperator was %s' % (dacycle['da.obsoperator'],)
            ncf_out.CTDAS_startdate = dacycle['time.start'].strftime('%F')
            ncf_out.CTDAS_enddate = dacycle['time.finish'].strftime("%F")
            ncf_out.original_file = orig_file


            # get nobs dimension

            if 'id' in ncf_out.dimensions:
                dimidob = ncf_out.dimensions['id']
                dimid = ('id',)
            elif 'obs' in ncf_out.dimensions:
                dimidob = ncf_out.dimensions['obs']
                dimid = ('obs',)

            if dimidob.isunlimited:
                nobs = ncf_out.inq_unlimlen()
            else:
                nobs = len(dimid)


            # add nmembers dimension

            if optimized_present:
                dimmembersob = ncf_out.createDimension('nmembers', size=sitedata[orig_file]['simulated_optimized'].shape[1])
            else:
                dimmembersob = ncf_out.createDimension('nmembers', size=sitedata[orig_file]['simulated_prior'].shape[1])
            dimmembers = ('nmembers',)
            nmembers = len(dimmembers)

            # Create empty arrays for posterior samples, as well as for forecast sample statistics

            savedict = io.std_savedict.copy()
            savedict['name'] = "flag_forecast"
            savedict['long_name'] = "flag_for_obs_model in forecast"
            savedict['units'] = "None"
            savedict['dims'] = dimid
            savedict['comment'] = 'Flag (0/1/2/99) for observation value, 0 means okay, 1 means QC error, 2 means rejected, 99 means not sampled'
            ncf_out.add_variable(savedict)

            savedict = io.std_savedict.copy()
            savedict['name'] = "modeldatamismatch"
            savedict['long_name'] = "modeldatamismatch"
            savedict['units'] = "[mol mol-1]"
            savedict['dims'] = dimid
            savedict['comment'] = 'Standard deviation of mole fractions resulting from model-data mismatch'
            ncf_out.add_variable(savedict)

            if dacycle.dasystem['obspack.meteo.sampling']:
                savedict = io.std_savedict.copy()
                savedict['name'] = "q" 
                savedict['long_name'] = "Specific humidity" 
                savedict['units'] = "kg kg^-1"
                savedict['dims'] = dimid 
                savedict['comment'] = 'Output from the %s model at sample location' % dacycle['da.obsoperator']
                ncf_out.add_variable(savedict)

                savedict = io.std_savedict.copy()
                savedict['name'] = "p" 
                savedict['long_name'] = "Air pressure" 
                savedict['units'] = "Pa"
                savedict['dims'] = dimid 
                savedict['comment'] = 'Output from the %s model at sample location' % dacycle['da.obsoperator']
                ncf_out.add_variable(savedict)

                savedict = io.std_savedict.copy()
                savedict['name'] = "T" 
                savedict['long_name'] = "Air temperature" 
                savedict['units'] = "K"
                savedict['dims'] = dimid 
                savedict['comment'] = 'Output from the %s model at sample location' % dacycle['da.obsoperator']
                ncf_out.add_variable(savedict)

                savedict = io.std_savedict.copy()
                savedict['name'] = "u" 
                savedict['long_name'] = "longitudinal wind component" 
                savedict['units'] = "m s^-1"
                savedict['dims'] = dimid 
                savedict['comment'] = 'Output from the %s model at sample location' % dacycle['da.obsoperator']
                ncf_out.add_variable(savedict)
                
                savedict = io.std_savedict.copy()
                savedict['name'] = "v" 
                savedict['long_name'] = "meridional wind component" 
                savedict['units'] = "m s^-1"
                savedict['dims'] = dimid 
                savedict['comment'] = 'Output from the %s model at sample location' % dacycle['da.obsoperator']
                ncf_out.add_variable(savedict)

                savedict = io.std_savedict.copy()
                savedict['name'] = "blh" 
                savedict['long_name'] = "Boundary layer height" 
                savedict['units'] = "m"
                savedict['dims'] = dimid 
                savedict['comment'] = 'Output from the %s model at sample location' % dacycle['da.obsoperator']
                ncf_out.add_variable(savedict)

            if prior_present:
                savedict = io.std_savedict.copy()
                savedict['name'] = "modelsamplesstandarddeviation_forecast"
                savedict['long_name'] = "standard deviaton of modelsamples from forecast over all ensemble members"
                savedict['units'] = "mol mol-1"
                savedict['dims'] = dimid
                savedict['comment'] = 'std dev of simulated mole fractions based on prior state vector'
                ncf_out.add_variable(savedict)

                savedict = io.std_savedict.copy()
                savedict['name'] = "modelsamplesensemble_forecast"
                savedict['long_name'] = "modelsamples from forecast over all ensemble members"
                savedict['units'] = "mol mol-1"
                savedict['dims'] = dimid + dimmembers
                savedict['comment'] = 'ensemble of simulated mole fractions based on prior state vector'
                ncf_out.add_variable(savedict)

                savedict = io.std_savedict.copy()
                savedict['name'] = "totalmolefractionvariance_forecast"
                savedict['long_name'] = "totalmolefractionvariance of forecast"
                savedict['units'] = "[mol mol-1]^2"
                savedict['dims'] = dimid
                savedict['comment'] = 'Variance of mole fractions resulting from prior state and model-data mismatch'
                ncf_out.add_variable(savedict)

                savedict = io.std_savedict.copy()
                savedict['name'] = "modelsamplesmean_forecast"
                savedict['long_name'] = "mean modelsamples from forecast"
                savedict['units'] = "mol mol-1"
                savedict['dims'] = dimid
                savedict['comment'] = 'simulated mole fractions based on prior state vector'
                ncf_out.add_variable(savedict)

            savedict = io.std_savedict.copy()
            savedict['name'] = "modelsamplesmean"
            savedict['long_name'] = "mean modelsamples"
            savedict['units'] = "mol mol-1"
            savedict['dims'] = dimid
            savedict['comment'] = 'simulated mole fractions based on optimized state vector'
            ncf_out.add_variable(savedict)


            savedict = io.std_savedict.copy()
            savedict['name'] = "modelsamplesstandarddeviation"
            savedict['long_name'] = "standard deviaton of modelsamples over all ensemble members"
            savedict['units'] = "mol mol-1"
            savedict['dims'] = dimid
            savedict['comment'] = 'std dev of simulated mole fractions based on optimized state vector'
            ncf_out.add_variable(savedict)


            savedict = io.std_savedict.copy()
            savedict['name'] = "modelsamplesensemble"
            savedict['long_name'] = "modelsamples over all ensemble members"
            savedict['units'] = "mol mol-1"
            savedict['dims'] = dimid + dimmembers
            savedict['comment'] = 'ensemble of simulated mole fractions based on optimized state vector'
            ncf_out.add_variable(savedict)


        else:
            logging.debug("Modifying existing file (%s) in the analysis directory" % copy_file)

            ncf_out = io.CT_CDF(copy_file, 'write')


        # Get existing file obs_nums to determine match to local obs_nums

        if 'merge_num' in ncf_out.variables:
            file_obs_nums = ncf_out.get_variable('merge_num').tolist()
        elif 'obspack_num' in ncf_out.variables:
            file_obs_nums = ncf_out.get_variable('obspack_num').tolist()
        elif 'id' in ncf_out.variables:
            file_obs_nums = ncf_out.get_variable('id').tolist() # lists heve the .index property
        file_ordered=all(np.diff(file_obs_nums)==1)
        # Get all obs_nums related to this file, determine their indices in the local arrays

        if sitedata[orig_file]['isordered'] and file_ordered:
            start_index=file_obs_nums.index(sitedata[orig_file]['obs_num'][0])
            stop_index=start_index+len(sitedata[orig_file]['obs_num'])
            if optimized_present:
                if sitedata[orig_file]['assimilated']:
                    ncf_out.variables['totalmolefractionvariance_forecast'][start_index:stop_index]=sitedata[orig_file]['fc_hphtr']
                ncf_out.variables['modelsamplesmean'][start_index:stop_index]=sitedata[orig_file]['simulated_optimized'][:,0]
                ncf_out.variables['modelsamplesstandarddeviation'][start_index:stop_index]=sitedata[orig_file]['simulated_optimized'][:, 1:].std()
                ncf_out.variables['modelsamplesensemble'][start_index:stop_index]=sitedata[orig_file]['simulated_optimized'][:,:]
            if prior_present:
                ncf_out.variables['modelsamplesmean_forecast'][start_index:stop_index]=sitedata[orig_file]['simulated_prior'][:,0]
                ncf_out.variables['modelsamplesstandarddeviation_forecast'][start_index:stop_index]=sitedata[orig_file]['simulated_prior'][:,1:].std()
                ncf_out.variables['modelsamplesensemble_forecast'][start_index:stop_index]=sitedata[orig_file]['simulated_prior'][:,:]
            if dacycle.dasystem['obspack.meteo.sampling']:
                ncf_out.variables['q'][start_index:stop_index]=sitedata[orig_file]['q'][:]
                ncf_out.variables['p'][start_index:stop_index]=sitedata[orig_file]['p'][:]
                ncf_out.variables['T'][start_index:stop_index]=sitedata[orig_file]['T'][:]
                ncf_out.variables['blh'][start_index:stop_index]=sitedata[orig_file]['blh'][:]
                ncf_out.variables['u'][start_index:stop_index]=sitedata[orig_file]['u'][:]
                ncf_out.variables['v'][start_index:stop_index]=sitedata[orig_file]['v'][:]
            ncf_out.variables['modeldatamismatch'][start_index:stop_index]=sitedata[orig_file]['mdm']

            # set default flag to not assimilated, assimliated data wil get either 0/1/2 from below
            ncf_out.variables['flag_forecast'][start_index:stop_index]=sitedata[orig_file]['flag']

        # Now write optimizer specifics until we fix posterior communication between optimizer and obspack 
            # if optimized_present and not validation:

        else:
            logging.info('%s not processed as unsorted date is not implemented yet' % orig_file)
        status = ncf_out.close()
            #        selected_obs_nums = [index_and_num for infile, index_and_num in zip(infiles, enumerate(obs_num)) if infile == orig_file]
            #        ############################
            #
            #        #fon=file_obs_nums.tolist() 
            #        #don=obs_num.tolist() # data to be distributed obsnum
            #        ## create a list which contains the indices of locations where model ouptut resides
            #        ## this can be used to "slice" data from a numpy array
            #        #order_in_file = [don.index(obsnum) for _, obsnum in selected_obs_nums]
            #        #selected_obs_nums = [don.index(obsnum) for infile, index_and_num in zip(infiles, obs_num)) if infile == orig_file]
            #        #son=selected_obs_nums.tolist()
            #        #order_in_file=[]
            #        #for model_index,num in selected_obs_nums:
            #        #    order_in_file.append(son.index(num))        
            #        ############################################# 
            #        # Optimized data 1st: For each index, get the data and add to the file in the proper file index location
            #
            #        for model_index,num in selected_obs_nums:
            #
            #            # model_index = np.where((obs_num == num)&(infiles==orig_file))[0][0]
            #            # model_index = np.where(obs_num == num)[0][0]
            #            file_index = np.where(file_obs_nums == num)[0][0]
            #
            #            if optimized_present:
            #                var = ncf_out.variables['modelsamplesmean']
            #                var[file_index] = simulated_optimized[model_index, 0]
            #
            #                var = ncf_out.variables['modelsamplesstandarddeviation']
            #                var[file_index] = simulated_optimized[model_index, 1:].std()
            #
            #                var = ncf_out.variables['modelsamplesensemble']
            #                var[file_index] = simulated_optimized[model_index, :]
            ###########
            #            if prior_present:
            #                var = ncf_out.variables['modelsamplesmean_forecast']
            #                var[file_index] = simulated_prior[model_index, 0]
            #
            #                var = ncf_out.variables['modelsamplesstandarddeviation_forecast']
            #                var[file_index] = simulated_prior[model_index, 1:].std()
            #
            #                var = ncf_out.variables['modelsamplesensemble_forecast']
            #                var[file_index] = simulated_prior[model_index, :]
            #
            #            var = ncf_out.variables['modeldatamismatch']
            #            var[file_index] = np.sqrt(mdm[model_index])
            #
            #            # set default flag to not assimilated, assimliated data wil get either 0/1/2 from below
            #            var = ncf_out.variables['flag_forecast']
            #            var[file_index] = 99 
            #
            #        # Now write optimizer specifics until we fix posterior communication between optimizer and obspack 
            #
            #        if optimized_present and not validation:
            #
            #            selected_fc_obs_nums = [num for sitecode, num in zip(fc_sitecodes, fc_obs_num) if sitecode in orig_file]
            #
            #            for num in selected_fc_obs_nums:
            #
            #                model_index = np.where(fc_obs_num == num)[0][0]
            #                file_index = np.where(file_obs_nums == num)[0][0]
            #
            #                var = ncf_out.variables['totalmolefractionvariance_forecast']
            #                var[file_index] = fc_hphtr[model_index]
            #
            #                var = ncf_out.variables['flag_forecast']
            #                var[file_index] = fc_flag[model_index]
            #
            #
            #        # close the file


    return 



if __name__ == "__main__":
    import logging
    from da.tools.initexit import CycleControl
    from da.carbondioxide.dasystem import CO2DaSystem

    sys.path.append('../../')

    logging.root.setLevel(logging.DEBUG)

    dacycle = CycleControl(args={'rc':'../../ctdas-od-gfed2-glb6x4-obspack-full.rc'})
    dasystem = CO2DaSystem('../rc/carbontracker_ct09_opfnew.rc')
    dacycle.dasystem = dasystem
    dacycle.setup()
    dacycle.parse_times()



    while dacycle['time.start'] < dacycle['time.finish']:

        outfile = write_mole_fractions(dacycle)

        dacycle.advance_cycle_times()

    sys.exit(0)
