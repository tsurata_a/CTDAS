"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
# expand_fluxes.py
import sys
import os
sys.path.append('../../')
rootdir = os.getcwd().split('da/')[0]
analysisdir = os.path.join(rootdir, 'da/analysis')
from datetime import datetime, timedelta

import logging
import numpy as np
from da.tools.general import date2num, num2date
import da.tools.io4 as io
#from da.analysis.tools_regions import globarea, state_to_grid
from da.analysis.cteco2.tools_regions import globarea, state_to_grid
from da.tools.general import create_dirs
#from da.analysis.tools_country import countryinfo  # needed here
from da.analysis.cteco2.tools_country import countryinfo  # needed here
#from da.analysis.tools_transcom import transcommask, ExtendedTCRegions
from da.analysis.cteco2.tools_transcom import transcommask, ExtendedTCRegions
import netCDF4 as cdf

#import da.analysis.tools_transcom as tc
import da.analysis.cteco2.tools_transcom as tc
#import da.analysis.tools_country as ct
import da.analysis.cteco2.tools_country as ct
#import da.analysis.tools_time as timetools
import da.analysis.cteco2.tools_time as timetools



"""
Author: L. Florentie

Revision History:
File created September 2019.

"""

# ==============================================================================


def save_monthly_avg_1x1_data(dacycle, statevector):
    """
        Function creates a NetCDF file with output on 1x1 degree grid. It uses the flux data written by
        :class:`~da.ctsf.fluxmodel.calc_flux`
        Note: no lag for CTSF run
    """

    dirname = create_dirs(os.path.join(dacycle['dir.analysis'], 'data_flux1x1_monthly'))

    # Some help variables
    dectime0  = date2num(datetime(2000, 1, 1))
    dt        = dacycle['cyclelength']
    startdate = dacycle['time.start']
    enddate   = dacycle['time.end']
    nlag      = statevector.nlag

    logging.debug("DA Cycle start date is %s" % startdate.strftime('%Y-%m-%d %H:%M'))
    logging.debug("DA Cycle end   date is %s" % enddate.strftime('%Y-%m-%d %H:%M'))

    # Get dates
    file  = io.ct_read(os.path.join(dacycle['dir.input'], 'fluxmodel.000.nc'), 'read')
    dates = file.get_variable('date')

    for index, day in enumerate(dates):
        ncfdate = datetime(2000,1,1)+timedelta(days=int(day))
        logging.debug('ncfdate = %s' %ncfdate)

        # Create or open NetCDF output file
        saveas = os.path.join(dirname, 'flux_1x1.%s.nc' % ncfdate.strftime('%Y-%m'))
        ncf    = io.CT_CDF(saveas, 'write')

        # Create dimensions and lat/lon grid
        dimgrid     = ncf.add_latlon_dim()
        dimensemble = ncf.add_dim('members', statevector.nmembers)
        dimdate     = ncf.add_date_dim()

        # set title and tell GMT that we are using "pixel registration"
        setattr(ncf, 'Title', 'CarbonTracker fluxes')
        setattr(ncf, 'node_offset', 1)

        # skip dataset if already in file
        ncfdate = date2num(startdate) - dectime0 + dt.days / 2.0
        skip    = ncf.has_date(ncfdate)
        if skip:
            logging.warning('Skipping writing of data for date %s : already present in file %s' % (startdate.strftime('%Y-%m-%d'), saveas))
            continue

        # if not, process this cycle. Start by getting flux input data from CTDAS

        # prior flux
        bio_ens_prior = []
        for n in range(statevector.nmembers):
            filenamein = os.path.join(dacycle['dir.input'], 'fluxmodel.%03d.nc' %n)
            file       = io.ct_read(filenamein, 'read')
            bio_ens_prior.append(file.get_variable('NEE_grid')[index,:,:])
            if n == 0:
                date = np.array(file.get_variable('date'))
            file.close()
        del n

        logging.debug('bio_ens_prior shape = %s' %str(np.array(bio_ens_prior).shape))

        # posterior flux
        bio_ens_opt = []
        for n in range(statevector.nmembers):
            filenamein = os.path.join(dacycle['dir.output'], 'fluxmodel.%03d.nc' %n)
            file       = io.ct_read(filenamein, 'read')
            bio_ens_opt.append(file.get_variable('NEE_grid')[index])
            file.close()
        del n

        logging.debug('bio_ens_opt shape = %s' %str(np.array(bio_ens_opt).shape))

        savedict = ncf.standard_var(varname='bio_flux_prior')
        savedict['values'] = bio_ens_prior[0] #biomapped.tolist()
        savedict['dims'] = dimdate + dimgrid
        ncf.add_data(savedict)

        savedict = ncf.standard_var(varname='bio_flux_prior_ensemble')
        savedict['values'] = bio_ens_prior #biovarmapped.tolist()
        savedict['dims'] = dimdate + dimensemble + dimgrid
        ncf.add_data(savedict)

        savedict = ncf.standard_var(varname='bio_flux_opt')
        savedict['values'] = bio_ens_opt[0] #biomapped.tolist()
        savedict['dims'] = dimdate + dimgrid
        ncf.add_data(savedict)

        savedict = ncf.standard_var(varname='bio_flux_opt_ensemble')
        savedict['values'] = bio_ens_opt #biovarmapped.tolist()
        savedict['dims'] = dimdate + dimensemble + dimgrid
        ncf.add_data(savedict)

        area = globarea()
        savedict = ncf.standard_var(varname='cell_area')
        savedict['values'] = area.tolist()
        savedict['dims'] = dimgrid
        ncf.add_data(savedict)

        savedict = ncf.standard_var(varname='date')
        savedict['values'] = day #date2num(startdate) - dectime0 + dt.days / 2.0
        savedict['dims'] = dimdate
        # savedict['count'] = next
        ncf.add_data(savedict)

        sys.stdout.write('.')
        sys.stdout.flush()

        # Done, close the new NetCDF file
        ncf.close()

    logging.info("Gridded monthly average fluxes now written")
#
# #
# #   Transform flux to vector and calculate standard deviations (= save_avg_state_data)
# #
#     dirname2   = create_dirs(os.path.join(dacycle['dir.analysis'], 'data_state'))
#     area       = globarea()
#     vectorarea = statevector.grid2vector(griddata=area, method='sum')
#
# #
# # Create or open NetCDF output file
# #
#     saveas2 = os.path.join(dirname2, 'statefluxes.nc')
#     ncf     = io.CT_CDF(saveas, 'write')
#
# #
# # Create dimensions and lat/lon grid
# #
#     dimregs    = ncf.add_dim('nparameters', statevector.nparams)
#     dimmembers = ncf.add_dim('nmembers', statevector.nmembers)
#     dimdate    = ncf.add_date_dim()
# #
# # set title and tell GMT that we are using "pixel registration"
# #
#     setattr(ncf, 'Title', 'CarbonTracker fluxes')
#     setattr(ncf, 'node_offset', 1)
#
# #
# # skip dataset if already in file
# #
#     skip = ncf.has_date(startdate)
#     if skip:
#         logging.warning('Skipping writing of data for date %s : already present in file %s' % (startdate.strftime('%Y-%m-%d'), saveas))
#     else:
#         next = ncf.inq_unlimlen()[0]
#
#         bio_ens_prior_vector = []
#         bio_ens_opt_vector   = []
#         for n in range(statevector.nmembers):
#             bio_ens_prior_vector.append([statevector.grid2vector(griddata=bio_ens_prior[n][t,:,:]*area, method='sum') for t in range(bio_ens_prior[n].shape[0])])
#             bio_ens_opt_vector.append([statevector.grid2vector(griddata=bio_ens_opt[n][t,:,:]*area, method='sum') for t in range(bio_ens_opt[n].shape[0])])
#         del n
#         bio_ens_prior_vector = np.array(bio_ens_prior)
#         bio_ens_opt_vector   = np.array(bio_ens_opt)
#         logging.debug('bio_ens_prior_vector shape  =%s' %str(bio_ens_prior_vector.shape))
#
#         deviations_prior = bio_ens_prior_vector - bio_ens_prior_vector[0,:,:]
#         deviations_opt   = bio_ens_opt_vector - bio_ens_opt_vector[0,:,:]
#
#         savedict = ncf.standard_var(varname='ocn_flux_prior_ensemble' )
#         savedict['values'] = deviations_prior.tolist()
#         savedict['dims'] = dimdate + dimmembers + dimregs
#         savedict['comment'] = "This is the matrix square root, use (M x M^T)/(nmembers-1) to make covariance"
#         savedict['units'] = "mol region-1 s-1"
#         savedict['count'] = next
#         ncf.add_data(savedict)
#
#         savedict = ncf.standard_var(varname='ocn_flux_opt_ensemble' )
#         savedict['values'] = deviations_opt.tolist()
#         savedict['dims'] = dimdate + dimmembers + dimregs
#         savedict['comment'] = "This is the matrix square root, use (M x M^T)/(nmembers-1) to make covariance"
#         savedict['units'] = "mol region-1 s-1"
#         savedict['count'] = next
#         ncf.add_data(savedict)
#
#         savedict = ncf.standard_var('unknown')
#         savedict['name'] = 'ocn_flux_prior_std'
#         savedict['long_name'] = 'Ocean flux standard deviation, prior'
#         savedict['values'] = deviations_prior.std(axis=0)
#         savedict['dims'] = dimdate + dimregs
#         savedict['comment'] = "This is the standard deviation on each parameter"
#         savedict['units'] = "mol region-1 s-1"
#         savedict['count'] = next
#         ncf.add_data(savedict)
#
#         savedict = ncf.standard_var('unknown')
#         savedict['name'] = 'ocn_flux_opt_std'
#         savedict['long_name'] = 'Ocean flux standard deviation, posterior'
#         savedict['values'] = deviations_opt.std(axis=0)
#         savedict['dims'] = dimdate + dimregs
#         savedict['comment'] = "This is the standard deviation on each parameter"
#         savedict['units'] = "mol region-1 s-1"
#         savedict['count'] = next
#         ncf.add_data(savedict)
#
#         savedict = ncf.standard_var(varname='date')
#         savedict['values'] = date
#         savedict['dims'] = dimdate
#         savedict['count'] = next
#         ncf.add_data(savedict)
#
#         sys.stdout.write('.')
#         sys.stdout.flush()
#
#         ncf.close()
# #
# #   Return the full name of the NetCDF file so it can be processed by the next routine
# #
#     logging.info("Vector monthly average fluxes now written")

#
#   Return the full name of the NetCDF file so it can be processed by the next routine
#
    return saveas#, saveas2


# ==============================================================================


def save_monthly_avg_tc_data(dacycle, statevector):
    """
        Function creates a NetCDF file with output on TransCom regions. It uses the flux input from the
        function `save_monthly_avg_1x1_data`, which are then projected
        onto TC regions using the internal tcmap from :class:`~da.baseclasses.statevector.StateVector`.

           :param dacycle: a :class:`~da.tools.initexit.CycleControl` object
           :param statevector: a :class:`~da.baseclasses.statevector.StateVector`
           :rtype: None

        This function only read the prior fluxes from the flux_1x1.nc files created before, because we want to convolve
        these with the parameters in the statevector. This creates posterior fluxes, and the posterior covariance for the complete
        statevector in units of mol/box/s which we then turn into TC fluxes and covariances.
    """

#
    dirname = create_dirs(os.path.join(dacycle['dir.analysis'], 'data_tc_monthly'))
#
# Some help variables
#
    dectime0 = date2num(datetime(2000, 1, 1))
    dt = dacycle['cyclelength']
    startdate = dacycle['time.start']
    enddate = dacycle['time.end']

    logging.debug("DA Cycle start date is %s" % startdate.strftime('%Y-%m-%d %H:%M'))
    logging.debug("DA Cycle end   date is %s" % enddate.strftime('%Y-%m-%d %H:%M'))

    # Write/Create NetCDF output file
    #
    saveas = os.path.join(dirname, 'tcfluxes.nc')
    ncf = io.CT_CDF(saveas, 'write')
    dimdate = ncf.add_date_dim()
    dimidateformat = ncf.add_date_dim_format()
    dimregs = ncf.add_region_dim(type='tc')
#
# set title and tell GMT that we are using "pixel registration"
#
    setattr(ncf, 'Title', 'CarbonTracker TransCom fluxes')
    setattr(ncf, 'node_offset', 1)
    #

    skip = ncf.has_date(startdate)
    if skip:
        logging.warning('Skipping writing of data for date %s : already present in file %s' % (startdate.strftime('%Y-%m-%d'), saveas))
    else:

        # Get input data
        area = globarea()

        infile = os.path.join(dacycle['dir.analysis'], 'data_flux1x1_monthly', 'flux_1x1.%s.nc' % startdate.strftime('%Y-%m-%d'))

        if not os.path.exists(infile):
            logging.error("Needed input file (%s) does not exist yet, please create file first, returning..." % infile)
            return None

        ncf_in = io.ct_read(infile, 'read')

        # Transform data one by one
        # Get the date variable, and find index corresponding to the dacycle date
        try:
            dates = ncf_in.variables['date'][:]
        except KeyError:
            logging.error("The variable date cannot be found in the requested input file (%s) " % infile)
            logging.error("Please make sure you create gridded fluxes before making TC fluxes ")
            raise KeyError

        # First add the date for this cycle to the file, this grows the unlimited dimension
        savedict = ncf.standard_var(varname='date')
        savedict['values'] = dates
        savedict['dims'] = dimdate
        # savedict['count'] = index
        ncf.add_data(savedict)

        # Now convert other variables that were inside the flux_1x1 file

        vardict = ncf_in.variables
        for vname, vprop in vardict.iteritems():

            if vname in ['latitude','longitude', 'date', 'idate'] or 'std' in vname:
                continue

            data = ncf_in.get_variable(vname)
            logging.debug('shape data = %s' %str(data.shape))
            if 'ensemble' in vname:
                tcdata = []
                for r in range(23):
                    tcdata.append(np.sum( data * (area*np.where(statevector.tcmap==r+1,1,0))[np.newaxis,np.newaxis,:,:], axis=(2,3) ))
                del r
                tcdata = np.swapaxes(np.array(tcdata),0,2)
                logging.debug('shape tcdata = %s' %str(tcdata.shape))

                cov = []
                for d in range(len(dates)):
                    try:
                        cov.append(tcdata[:,d,:].transpose().dot(tcdata[:,d,:]) / (statevector.nmembers - 1))
                    except:
                        cov.append(np.dot(tcdata[:,d,:].transpose(), tcdata[:,d,:]) / (statevector.nmembers - 1)) # Huygens fix)
                del d

                tcdata = np.array(cov)
                logging.debug('shape tcdata = %s' %str(tcdata.shape))

                savedict = ncf.standard_var(varname=vname.replace('ensemble', 'cov'))
                savedict['units'] = '[mol/region/s]**2'
                savedict['dims'] = dimdate + dimregs + dimregs

            else:

                tcdata = []
                for r in range(23):
                    tcdata.append(np.sum( data * (area*np.where(statevector.tcmap==r+1,1,0))[np.newaxis,:,:], axis=(1,2) ))
                del r
                tcdata = np.array(tcdata)
                logging.debug('shape tcdata = %s' %str(tcdata.shape))

                savedict = ncf.standard_var(varname=vname)
                savedict['dims'] = dimdate + dimregs
                savedict['units'] = 'mol/region/s'

            savedict['values'] = tcdata
            ncf.add_data(savedict)

        ncf_in.close()
    ncf.close()

    logging.info("TransCom weekly average fluxes now written")

    return saveas


# ==============================================================================


def save_monthly_avg_agg_data(dacycle, region_aggregate='olson'):
    """
        Function creates a NetCDF file with output on TransCom regions. It uses the flux input from the
        function `save_weekly_avg_1x1_data` to create fluxes of length `nparameters`, which are then projected
        onto TC regions using the internal methods from :class:`~da.baseclasses.statevector.StateVector`.

           :param dacycle: a :class:`~da.tools.initexit.CycleControl` object
           :param StateVector: a :class:`~da.baseclasses.statevector.StateVector`
           :rtype: None

        This function only read the prior fluxes from the flux_1x1.nc files created before, because we want to convolve
        these with the parameters in the statevector. This creates posterior fluxes, and the posterior covariance for the complete
        statevector in units of mol/box/s which we then turn into TC fluxes and covariances.
    """

#
    dirname = create_dirs(os.path.join(dacycle['dir.analysis'], 'data_%s_monthly' % region_aggregate))
#
# Some help variables
#
    dectime0 = date2num(datetime(2000, 1, 1))
    dt = dacycle['cyclelength']
    startdate = dacycle['time.start']
    enddate = dacycle['time.end']
    ncfdate = date2num(startdate) - dectime0 + dt.days / 2.0

    logging.debug("DA Cycle start date is %s" % startdate.strftime('%Y-%m-%d %H:%M'))
    logging.debug("DA Cycle end   date is %s" % enddate.strftime('%Y-%m-%d %H:%M'))

    logging.debug("Aggregating 1x1 fluxes to %s totals" % region_aggregate)

    # Get dates
    file  = io.ct_read(os.path.join(dacycle['dir.input'], 'fluxmodel.000.nc'), 'read')
    dates = file.get_variable('date')

    for ncfday in dates:
        ncfdate = datetime(2000,1,1) + timedelta(days=int(ncfday))

        # Write/Create NetCDF output file
        saveas = os.path.join(dirname, '%s_fluxes.%s.nc' % (region_aggregate, ncfdate.strftime('%Y-%m')))
        ncf = io.CT_CDF(saveas, 'write')
        dimdate = ncf.add_date_dim()
        dimidateformat = ncf.add_date_dim_format()
        dimgrid = ncf.add_latlon_dim()  # for mask

        # Select regions to aggregate to
        if region_aggregate == "olson":
            regionmask = tc.olson240mask
            dimname = 'olson'
            dimregs = ncf.add_dim(dimname, regionmask.max())

            regionnames = []
            for i in range(11):
                for j in range(19):
                    regionnames.append("%s_%s" % (tc.transnams[i], tc.olsonnams[j],))
            regionnames.extend(tc.oifnams)
            xform = False

            for i, name in enumerate(regionnames):
                lab = 'Aggregate_Region_%03d' % (i + 1,)
                setattr(ncf, lab, name)

        elif region_aggregate == "olson_extended":
            regionmask = tc.olson_ext_mask
            dimname = 'olson_ext'
            dimregs = ncf.add_dim(dimname, regionmask.max())
            xform = False

            for i, name in enumerate(tc.olsonextnams):
                lab = 'Aggreate_Region_%03d'%(i+1)
                setattr(ncf, lab, name)

        elif region_aggregate == "transcom":
            regionmask = tc.transcommask
            dimname = 'tc'
            dimregs = ncf.add_region_dim(type='tc')
            xform = False

        elif region_aggregate == "transcom_extended":
            regionmask = tc.transcommask
            dimname = 'tc_ext'
            dimregs = ncf.add_region_dim(type='tc_ext')
            xform = True

        elif region_aggregate == "amazon":
            regfile = cdf.Dataset(os.path.join(analysisdir,'amazon_mask.nc'))
            regionmask = regfile.variables['regionmask'][:]
            regfile.close()
            dimname = 'amazon'
            dimregs = ncf.add_dim(dimname, regionmask.max())
            xform = False

        elif region_aggregate == "country":

            xform = False
            countrydict = ct.get_countrydict()
            selected = ['Russia', 'Canada', 'China', 'United States', 'EU27', 'Brazil', 'Australia', 'India'] #,'G8','UNFCCC_annex1','UNFCCC_annex2']
            regionmask = np.zeros((180, 360,), 'float')

            for i, name in enumerate(selected):
                lab = 'Country_%03d' % (i + 1,)
                setattr(ncf, lab, name)

                if name == 'EU27':
                    namelist = ct.EU27
                elif name == 'EU25':
                    namelist = ct.EU25
                elif name == 'G8':
                    namelist = ct.G8
                elif name == 'UNFCCC_annex1':
                    namelist = ct.annex1
                elif name == 'UNFCCC_annex2':
                    namelist = ct.annex2
                else:
                    namelist = [name]

                for countryname in namelist:
                    try:
                        country = countrydict[countryname]
                        regionmask.put(country.gridnr, i + 1)
                    except:
                        continue

            dimname = 'country'
            dimregs = ncf.add_dim(dimname, regionmask.max())


        skip = ncf.has_date(ncfdate)
        if skip:
            logging.warning('Skipping writing of data for date %s : already present in file %s' % (ncfdate.strftime('%Y-%m'), saveas))
            continue

        # set title and tell GMT that we are using "pixel registration"
        setattr(ncf, 'Title', 'CTDAS Aggregated fluxes')
        setattr(ncf, 'node_offset', 1)

        savedict = ncf.standard_var('unknown')
        savedict['name'] = 'regionmask'
        savedict['comment'] = 'numerical mask used to aggregate 1x1 flux fields, each integer 0,...,N is one region aggregated'
        savedict['values'] = regionmask.tolist()
        savedict['units'] = '-'
        savedict['dims'] = dimgrid
        savedict['count'] = 0
        ncf.add_data(savedict)

        # Get input data from 1x1 degree flux files
        area = globarea()

        infile = os.path.join(dacycle['dir.analysis'], 'data_flux1x1_monthly', 'flux_1x1.%s.nc' % ncfdate.strftime('%Y-%m'))
        if not os.path.exists(infile):
            logging.error("Needed input file (%s) does not exist yet, please create file first, returning..." % infile)
            return None

        ncf_in = io.ct_read(infile, 'read')

        # Transform data one by one
        try:
            date_in = ncf_in.variables['date'][:]
        except KeyError:
            logging.error("The variable date cannot be found in the requested input file (%s) " % infile)
            logging.error("Please make sure you create gridded fluxes before making TC fluxes ")
            raise KeyError

        if date_in != ncfday:
            logging.error("monthly flux 1x1 file %s contains different date" % infile)
            raise ValueError

        # First add the date for this cycle to the file, this grows the unlimited dimension
        savedict = ncf.standard_var(varname='date')
        savedict['values'] = ncfday
        savedict['dims'] = dimdate
        ncf.add_data(savedict)

        # Now convert other variables that were inside the statevector file
        vardict = ncf_in.variables
        for vname, vprop in vardict.items():
            if vname in ['latitude','longitude','date','idate']: continue
            elif 'std' in vname: continue
            elif 'ensemble' in vname:
                data = ncf_in.get_variable(vname)
                dimensemble = ncf.add_dim('members', data.shape[0])

                regiondata = []
                for member in data:
                    aggdata = state_to_grid(member * area, regionmask, reverse=True, mapname=region_aggregate)
                    regiondata.append(aggdata)

                regiondata = np.array(regiondata)
                try:
                    regioncov = regiondata.transpose().dot(regiondata) / (data.shape[0] - 1)
                except:
                    regioncov = np.dot(regiondata.transpose(), regiondata) / (data.shape[0] - 1) # Huygens fix

                if xform:
                    regiondata = ExtendedTCRegions(regiondata,cov=False)
                    regioncov = ExtendedTCRegions(regioncov,cov=True)

                savedict = ncf.standard_var(varname=vname)
                savedict['name'] = vname.replace('ensemble','covariance')
                savedict['units'] = '[mol/region/s]^2'
                savedict['dims'] = dimdate + dimregs + dimregs
                savedict['values'] = regioncov
                ncf.add_data(savedict)

                savedict = ncf.standard_var(varname=vname)
                savedict['name'] = vname
                savedict['units'] = 'mol/region/s'
                savedict['dims'] = dimdate + dimensemble + dimregs

            elif 'flux' in vname:
                data = ncf_in.get_variable(vname)

                regiondata = state_to_grid(data * area, regionmask, reverse=True, mapname=region_aggregate)

                if xform:
                    regiondata = ExtendedTCRegions(regiondata)

                savedict = ncf.standard_var(varname=vname)
                savedict['dims'] = dimdate + dimregs
                savedict['units'] = 'mol/region/s'

            else:
                data = ncf_in.get_variable(vname)[:]
                regiondata = state_to_grid(data, regionmask, reverse=True, mapname=region_aggregate)
                if xform:
                    regiondata = ExtendedTCRegions(regiondata)

                savedict = ncf.standard_var(varname=vname)
                savedict['dims'] = dimdate + dimregs

            savedict['values'] = regiondata
            ncf.add_data(savedict)

        ncf_in.close()
        ncf.close()

    logging.info("%s aggregated monthly average fluxes now written" % dimname)

    return saveas


# ==============================================================================


def save_monthly_avg_agg_data0(dacycle, region_aggregate='olson'):
    """
        Function creates a NetCDF file with output on TransCom regions. It uses the flux input from the
        function `save_weekly_avg_1x1_data` to create fluxes of length `nparameters`, which are then projected
        onto TC regions using the internal methods from :class:`~da.baseclasses.statevector.StateVector`.

           :param dacycle: a :class:`~da.tools.initexit.CycleControl` object
           :param StateVector: a :class:`~da.baseclasses.statevector.StateVector`
           :rtype: None

        This function only read the prior fluxes from the flux_1x1.nc files created before, because we want to convolve
        these with the parameters in the statevector. This creates posterior fluxes, and the posterior covariance for the complete
        statevector in units of mol/box/s which we then turn into TC fluxes and covariances.
    """

    # Some help variables
    dectime0 = date2num(datetime(2000, 1, 1))
    dt = dacycle['cyclelength']
    startdate = dacycle['time.start']
    enddate = dacycle['time.end']

    logging.debug("DA Cycle start date is %s" % startdate.strftime('%Y-%m-%d %H:%M'))
    logging.debug("DA Cycle end   date is %s" % enddate.strftime('%Y-%m-%d %H:%M'))
    logging.debug("Aggregating 1x1 fluxes to %s totals" % region_aggregate)

    # Get input data from 1x1 degree flux files
    area = globarea()

    infile = os.path.join(dacycle['dir.analysis'], 'data_flux1x1_monthly', 'flux_1x1.%s.nc' % ncfdate.strftime('%Y-%m'))
    if not os.path.exists(infile):
        logging.error("Needed input file (%s) does not exist yet, please create file first, returning..." % infile)
        return None

    ncf_in = io.ct_read(infile, 'read')

    # Get the date variable
    try:
        dates = ncf_in.variables['date'][:]
    except KeyError:
        logging.error("The variable date cannot be found in the requested input file (%s) " % infile)
        logging.error("Please make sure you create gridded fluxes before making TC fluxes ")
        raise KeyError

    # Create directory to store output in
    dirname = create_dirs(os.path.join(dacycle['dir.analysis'], 'data_%s_monthly' % region_aggregate))

    # Write/Create NetCDF output file
    for index, ncfdate in enumerate(dates):

        saveas = os.path.join(dirname, '%s_fluxes.%s.nc' % (region_aggregate, startdate.strftime('%Y-%m-%d')))
        ncf = io.CT_CDF(saveas, 'write')
        dimdate = ncf.add_date_dim()
        dimidateformat = ncf.add_date_dim_format()
        dimgrid = ncf.add_latlon_dim()  # for mask

        skip = ncf.has_date(ncfdate)
        if skip:
            logging.warning('Skipping writing of data for date %s : already present in file %s' % (startdate.strftime('%Y-%m-%d'), saveas))
            return

        #Select regions to aggregate to
        if region_aggregate == "olson":
            regionmask = tc.olson240mask
            dimname = 'olson'
            dimregs = ncf.add_dim(dimname, regionmask.max())

            regionnames = []
            for i in range(11):
                for j in range(19):
                    regionnames.append("%s_%s" % (tc.transnams[i], tc.olsonnams[j],))
            regionnames.extend(tc.oifnams)
            xform = False

            for i, name in enumerate(regionnames):
                lab = 'Aggregate_Region_%03d' % (i + 1,)
                setattr(ncf, lab, name)

        elif region_aggregate == "olson_extended":
            regionmask = tc.olson_ext_mask
            dimname = 'olson_ext'
            dimregs = ncf.add_dim(dimname, regionmask.max())
            xform = False

            for i, name in enumerate(tc.olsonextnams):
                lab = 'Aggreate_Region_%03d'%(i+1)
                setattr(ncf, lab, name)

        elif region_aggregate == "transcom":
            regionmask = tc.transcommask
            dimname = 'tc'
            dimregs = ncf.add_region_dim(type='tc')
            xform = False

        elif region_aggregate == "transcom_extended":
            regionmask = tc.transcommask
            dimname = 'tc_ext'
            dimregs = ncf.add_region_dim(type='tc_ext')
            xform = True

        elif region_aggregate == "amazon":
            regfile = cdf.Dataset(os.path.join(analysisdir,'amazon_mask.nc'))
            regionmask = regfile.variables['regionmask'][:]
            regfile.close()
            dimname = 'amazon'
            dimregs = ncf.add_dim(dimname, regionmask.max())
            xform = False

        elif region_aggregate == "country":

            xform = False
            countrydict = ct.get_countrydict()
            selected = ['Russia', 'Canada', 'China', 'United States', 'EU27', 'Brazil', 'Australia', 'India'] #,'G8','UNFCCC_annex1','UNFCCC_annex2']
            regionmask = np.zeros((180, 360,), 'float')

            for i, name in enumerate(selected):
                lab = 'Country_%03d' % (i + 1,)
                setattr(ncf, lab, name)

                if name == 'EU27':
                    namelist = ct.EU27
                elif name == 'EU25':
                    namelist = ct.EU25
                elif name == 'G8':
                    namelist = ct.G8
                elif name == 'UNFCCC_annex1':
                    namelist = ct.annex1
                elif name == 'UNFCCC_annex2':
                    namelist = ct.annex2
                else:
                    namelist = [name]

                for countryname in namelist:
                    try:
                        country = countrydict[countryname]
                        regionmask.put(country.gridnr, i + 1)
                    except:
                        continue

            dimname = 'country'
            dimregs = ncf.add_dim(dimname, regionmask.max())

        # set title and tell GMT that we are using "pixel registration"
        setattr(ncf, 'Title', 'CTDAS Aggregated fluxes')
        setattr(ncf, 'node_offset', 1)

        savedict = ncf.standard_var('unknown')
        savedict['name'] = 'regionmask'
        savedict['comment'] = 'numerical mask used to aggregate 1x1 flux fields, each integer 0,...,N is one region aggregated'
        savedict['values'] = regionmask.tolist()
        savedict['units'] = '-'
        savedict['dims'] = dimgrid
        savedict['count'] = 0
        ncf.add_data(savedict)

        # Transform data one by one

        # First add the date for this cycle to the file, this grows the unlimited dimension
        savedict = ncf.standard_var(varname='date')
        savedict['values'] = ncfdate
        savedict['dims'] = dimdate
        savedict['count'] = index
        ncf.add_data(savedict)

        # Now convert other variables that were inside the statevector file

        vardict = ncf_in.variables
        for vname, vprop in vardict.iteritems():
            if vname == 'latitude': continue
            elif vname == 'longitude': continue
            elif vname == 'date': continue
            elif vname == 'idate': continue
            elif 'std' in vname: continue
            elif 'ensemble' in vname:

                data = ncf_in.get_variable(vname)[index]

                dimensemble = ncf.add_dim('members', data.shape[0])

                regiondata = []
                for member in data:
                    aggdata = state_to_grid(member * area, regionmask, reverse=True, mapname=region_aggregate)
                    regiondata.append(aggdata)

                regiondata = np.array(regiondata)
                try:
                    regioncov = regiondata.transpose().dot(regiondata) / (data.shape[0] - 1)
                except:
                    regioncov = np.dot(regiondata.transpose(), regiondata) / (data.shape[0] - 1) # Huygens fix

                if xform:
                    regiondata = ExtendedTCRegions(regiondata,cov=False)
                    regioncov = ExtendedTCRegions(regioncov,cov=True)

                savedict = ncf.standard_var(varname=vname)
                savedict['name'] = vname.replace('ensemble','covariance')
                savedict['units'] = '[mol/region/s]^2'
                savedict['dims'] = dimdate + dimregs + dimregs
                savedict['count'] = index
                savedict['values'] = regioncov
                ncf.add_data(savedict)

                savedict = ncf.standard_var(varname=vname)
                savedict['name'] = vname
                savedict['units'] = 'mol/region/s'
                savedict['dims'] = dimdate + dimensemble + dimregs


            elif 'flux' in vname:

                data = ncf_in.get_variable(vname)[index]

                regiondata = state_to_grid(data * area, regionmask, reverse=True, mapname=region_aggregate)

                if xform:
                    regiondata = ExtendedTCRegions(regiondata)

                savedict = ncf.standard_var(varname=vname)
                savedict['dims'] = dimdate + dimregs
                savedict['units'] = 'mol/region/s'

            else:

                data = ncf_in.get_variable(vname)[:]
                regiondata = state_to_grid(data, regionmask, reverse=True, mapname=region_aggregate)
                if xform:
                    regiondata = ExtendedTCRegions(regiondata)

                savedict = ncf.standard_var(varname=vname)
                savedict['dims'] = dimdate + dimregs

            savedict['count'] = index
            savedict['values'] = regiondata
            ncf.add_data(savedict)

        ncf_in.close()
    ncf.close()

    logging.info("%s aggregated weekly average fluxes now written" % dimname)

    return saveas
