"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters. 
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu. 

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation, 
version 3. This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. 

You should have received a copy of the GNU General Public License along with this 
program. If not, see <http://www.gnu.org/licenses/>."""

"""
.. module:: fluxfit example
.. moduleauthor:: Joram Hooghiem 

This test example currently works on Snellius and should can be run from within the ctdas folder, else change the paths so that it finds the approriate da.<modules>

Revision History:
File created on 2022-07-18 16:40:16
"""
import da.preprocessing.FluxFit as FluxFit
import numpy as np
import time
from scipy.optimize import curve_fit

# We'd like to use a smoother (most fancy would be some low-pass filter), but this will sufice
def moving_average(data,  window_size):
    convoluted_data=np.convolve(data, np.ones(window_size)/window_size, mode='same')
    return convoluted_data

# get dictionary with regions and their data
a=FluxFit.fetch_regions('regions')
nregions=240
nparams=9

# define the function to be fitted
def sf_func(t, x0, a0, a1, a2, a3,  c0, c1, c2, c3):
    """The function that is shown above.
        t in days since start
    """
    NDAYSYEAR = 365.242 # don't skip leap years
  
    # First calculate the polynomial term
    polyterm = x0 
  
    # Calculate the harmonic term
    har = 0

    sinterm = (a0 ) * (np.cos(2 * np.pi / NDAYSYEAR * 1 * t))
    costerm = (c0 ) * (np.sin(2 * np.pi / NDAYSYEAR * 1 * t))
    har += sinterm + costerm    

    sinterm = (a1 ) * (np.cos(2 * np.pi / NDAYSYEAR * 2 * t))
    costerm = (c1 ) * (np.sin(2 * np.pi / NDAYSYEAR * 2 * t))
    har += sinterm + costerm
  
    sinterm = (a2 ) * (np.cos(2 * np.pi / NDAYSYEAR * 3 * t))
    costerm = (c2 ) * (np.sin(2 * np.pi / NDAYSYEAR * 3 * t))
    har += sinterm + costerm
  
    sinterm = (a3 ) * (np.cos(2 * np.pi / NDAYSYEAR * 4 * t))
    costerm = (c3 ) * (np.sin(2 * np.pi / NDAYSYEAR * 4 * t))
    har += sinterm + costerm
    return polyterm+har

# some directory names and definitions needed
dirname="/projects/0/ctdas/joram/input/13co2/SiB4/daily_avg/"
varname='I2b'
fname="all_isotm5_daily"
outfile="test_flux.nc"

# create empty results with all zeros
fit_results=np.zeros((nparams,nregions))
fit_cov=np.zeros((nparams,nparams,nregions))

# loop ver regions
for region in range(0,nregions):
    print("Processing data for region %d" % region )
    # only in region; nr from 1-240 so add one to Python iter-counter
    region_mask=(a['regions']==(region+1))
    region_area=a['areas'][region_mask]
    if len(region_mask.nonzero()[0])==0: continue # if no regions index specife its not worth our time....
    # Else we are getting data, core function to do this:
    # we mostly pass the above defined variables. 
    data=FluxFit.fetch_data( varname,dirname,fname,region_mask,operator=np.average,axis=1,weights=region_area) 

    # no apply the moving average:
    data_conv=moving_average(data,30)

    # Okay stupid time array, but we know that we are fitting daily data....
    time=np.linspace(1,len(data),len(data))

    # fit ecoregion data using curvefit 
    fit_results[:,region],fit_cov[:,:,region]=curve_fit(sf_func,time,data_conv)

    # plot this regions fit for inspection
    FluxFit.flux_figure(time,data,data_conv,sf_func(time,*fit_results[:,region]),region)
# finaly write the flux parameters and covariance structure to a file.
FluxFit.write_flux_data(outfile,fit_results,fit_cov,nparams,nregions)
