"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters. 
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu. 

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation, 
version 3. This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. 

You should have received a copy of the GNU General Public License along with this 
program. If not, see <http://www.gnu.org/licenses/>."""

"""
.. module:: FluxFit 
.. moduleauthor:: Joram Hooghiem 

Revision History:
File created on 2022-07-18 16:40:54 
"""
import netCDF4 as cdf
from glob import glob
import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import da.tools.io4 as io
import os
def fetch_data(varname,dirname,fname,region_mask,operator=None,**kwargs):

    """ Gather data from file and apply the operator to the data 
    """
    data=[]
    # print(    sorted(glob(dirname+'*.nc')))
    for f in sorted(glob(dirname+fname+'*.nc')):
        # print(f)     
        ncf=io.ct_read(f,'read')
        # a=Dataset(f)
        if operator:
            # probably the operater will process the file contents as specified by the user
            # for now assume that we simply want to process all values individually
            # exit()
            a=operator(ncf.get_variable(varname)[:,region_mask],**kwargs)
        else:
            a=ncf.get_variable(varname)[:,region_mask]
        for i in a:
            data.append(i)
        ncf.close()
    return np.array(data )
# grid to region time and space averaged functions:

def g2r_ts_avg(data,weights_time=None,weights_area=None):
    ''' assumes time axis
    '''
    # Data is a ravelled selection fo 360/180 data
    # 
    average=np.average(data,axis=0,weights=weights_time) # reduces allong first axis, presumably time
    averaged=np.average(average,weights=weights_area)
    # average*=weights # apply weights of same order 

    return average

def fetch_regions(regions):
    '''
    Fetches a region dictionary with keys: "regions" and "areas"
    the regions is a 180 by 360 map with region numbers
    areas of each grid cell. 
    '''

    mapfile='da/analysis/cteco2/regions.nc'
    ncf          = io.ct_read(mapfile, 'read')
    region_dict={}
    region_dict['regions']=ncf.get_variable(regions)
    region_dict['areas']=ncf.get_variable('grid_cell_area')
    ncf.close()
    # data = 
    # regions = data.variables['regions'][:]
    # areas = data.variables['grid_cell_area'][:] # m2
    return region_dict

def get_y_lim(ydata):
    large=np.max(ydata)
    small=np.min(ydata)
    if large<0:
        ymax=0.99*large
    else:
        ymax=1.01*large
    if small<0:
        ymin=1.01*small
    else:
        ymin=0.99*small
    return (ymin,ymax)
def rmse(diff):
    return np.sqrt(np.mean(diff**2))
def flux_figure(xdata,ydata,ydata_conv,ydata_model,region,outfile=''):
    xlabs=['time']*4
    ylabs=['flux','residuals','flux','residuals']
    xlims=[(0,10*365),(0,10*365),(0,365),(0,365)]
    # first diff will dtermine limits
    diff=ydata_model-ydata
    
    ylims=[get_y_lim(ydata),get_y_lim(diff)]*2
    fig, axar = plt.subplots(2,2)
    # fig,axar=scantools.plot_init(2,2,xlabs,ylabs,xlims,ylims)
    axar=axar.ravel()
    axar[0].plot(xdata,ydata,color='k',label='input')
    axar[0].plot(xdata,ydata_conv,color='r',label='convoluted')
    axar[0].plot(xdata,ydata_model, color='b',label='fit')
    axar[2].plot(xdata,ydata,color='k',label='input')
    axar[2].plot(xdata,ydata_conv,color='r',label='convoluted')
    axar[2].plot(xdata,ydata_model, color='b',label='fit')

    axar[1].plot(xdata,diff,color='g',label='fit-data %.3e' % (rmse(diff)))
    axar[3].plot(xdata,diff,color='g',label='fit-data %.3e' % (rmse(diff)))

    diff=ydata_model-ydata_conv
    axar[1].plot(xdata,diff,color='y',label='fit-conv %.3e' % (rmse(diff)))
    axar[3].plot(xdata,diff,color='y',label='fit-conv %.3e' % (rmse(diff)))

    for ax,xlim,ylim,xlab,ylab in zip(axar,xlims,ylims,xlabs,ylabs):
        ax.set_xlim(*xlim)
        ax.set_ylim(*ylim)
        ax.set_xlabel(xlab)
        ax.set_ylabel(ylab)
        ax.legend()
    fig.tight_layout()
    fig.savefig('fluxplot_region%s_%s.png' % (region,outfile))
    plt.close()
    return
def write_flux_data(outputfile,fit_results,fit_cov,nparams,nregions):
    if os.path.exists(outputfile):
        # delete
        data = os.remove(outputfile)
        print('Removed existing file '+outputfile)
    
    # make a new file change to nice io4 later
    data = cdf.Dataset(outputfile, 'w', format='NETCDF4')
    data.createDimension('parameters', nparams)
    data.createDimension('regions', nregions)
    # data.createDimension('latitude', 180)
    # data.createDimension('longitude', 360)
    # data.createDimension('time', len(time_months))
    # data.createDimension('months',12)
    
    # tm = data.createVariable('time','f8',('time'))
    # tm.unit = 'days since 2000-1-1'
    # data.variables['time'][:] = time_months
    
    fp = data.createVariable('fluxmodel_parameters','f8', ('parameters','regions'))
    # fp.comment = 'model = x0 + x1*t + x2*t^2 + sum_1^'+str(nhar)+'((x3+x4*t)*cos(2*pi*i*t) + (x5+x6*t)*sin(2*pi*i*t)) with t=years since 1 Jan.2000'
    fp.unit = 'mol/m2/s'
    data.variables['fluxmodel_parameters'][:] = fit_results
    
    fc = data.createVariable('parameter_covariance','f8',('parameters','parameters','regions'))
    # fc.comment = 'Covariance matrices for the fit parameters'
    fc.unit = 'mol/m2/s'
    data.variables['parameter_covariance'][:] = fit_cov
    data.close()
    
    print('Data written to ' + outputfile)
    return
