!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!20! ctsf	        		     						!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! OUTPUT SETTINGS

writeMemberFluxes 	: True


!! INPUT FILES

regionsfile             : ${datadir}/regions.nc

! fit.anomaly.inputfile   : /projects/0/ctdas/joram/input/ctdas/ctsf/anomalies/nirv_anomaly_v0.1.nc
fit.anomaly.inputfile   : /projects/0/ctdas/joram/input/anomalies/t2m.anom.detrend_2000-2020_daily.nc
fit.anomaly.name        : __xarray_dataarray_variable__
! fit.anomaly.name        : nirv_ano_region

!fit.anomaly.inputfile  : /mnt/beegfs/user/gkoren/ctsf/opt_input/anomalies/temp_anomaly_ei_1979-2018.nc
!fit.anomaly.name       : T_ano_region

!resp.anomaly.inputfile : /mnt/beegfs/user/gkoren/ctsf/opt_input/anomalies/temp_anomaly_ei_1979-2018.nc
!resp.anomaly.name      : T_ano_region
!resp.mean.inputfile    : ${resp.anomaly.inputfile}
!resp.mean.name         : T_sc_region


! OBSERVATIONS - FLUXES
 
! number of observation stations, and fraction of stations of which data is taken per month
obs.restrict_obs_to_ecoregion : 134
obs.nsamples.locations        : all
obs.nsamples.fraction         : 1

!obs.input.dir                : /projects/0/ctdas/liesbeth/ctdas/GCP2020/
!obs.input.file               : gcp2020_v2_sibcasa_flux1x1_monthly.nc
!obs.input.var                : bio_flux_opt

obs.input.dir                 : /projects/0/ctdas/liesbeth/data/
obs.input.file                : CTE2018_monthly_v2.nc
obs.input.var                 : bio_flux_opt

mdm.read_from_file            : True
obs.relative_mdm              : 0.0
obs.mdm                       : 5.0e-8
mdm.min                       : 1.0e-10
mdm.input.file                : /projects/0/ctdas/joram/input/ctdas/ctsf/flux_fit_params_cov_gcp20.nc
mdm.input.var                 : mdm_rmse
mdm.input.rmse                : mdm_rmse

! ASSIMILATION SETTINGS

nharmonics      : 4
npolyterms	: 1
add.anomalies   : False
ngamma          : 12
add.respiration : False
time.reference  : 2000-01-01 00:00:00


! STATE VECTOR INITIALIZATION

! If unit_initialization = True: construct initial fit with parameters below, if False: provide file from which to read parameters
fit.unit_initialization	: False

params.init.file        : /projects/0/ctdas/joram/input/ctdas/ctsf/test_flux.nc
! params.init.file        : /projects/0/ctdas/joram/input/ctdas/ctsf/flux_fit_params_cov_gcp20.nc
params.init.vec         : True
params.init.name	: fluxmodel_parameters

params.init.gamma.read  : False
params.init.gamma.file  : ${params.init.file}
params.init.gamma.name  : gamma_NIRv_mean
params.init.gamma	: 0

! k, R0 and S0 are only used for TER parametrization
params.init.k           : 0.07
params.init.R0          : 1
params.init.S0          : 0

! Unit initialization settings:
! Baseline fit: p0 + p1*t + p2*t^2 + (a0 + a1*t)*sin(2*pi/T*(t + phase) + pi/2)
! or:           p0 + p1*t + p2*t^2 + (a0 + a1*t)*cos(2*pi/T*t) + (b0 + b1*t)*sin(2*pi/T*t)
! Higher order harmonics (and cos amplitude) are initialized with zero amplitude. Unit of t is days.
polynomial              : 0, 0, 0 ! p0, p1, p2

amplitude               : 0, 0 ! a0, a1
! If a0 = None: amplitude is calculated as polynomial function of latitude [deg N]
! coefficients (with increasing power of latitude):
amplitude_polyfit       : -7.09317193E-3, -5.99721142E-3, -1.87871237E-4,  2.70138119E-5, -1.01483929E-8, -7.45656932E-9, 4.55054553E-11
phase                   : 3.68882695, 10.8251453, 16.0732567, -72.8409655

! PRIOR COVARIANCES

! either 'default' or value(s)
fit.read_cov_matrices   : True
fit.cov_matrices_file   : ${params.init.file}
! fit.cov_matrices_file   : /projects/0/ctdas/joram/input/ctdas/ctsf/flux_fit_params_cov_gcp20.nc
fit.cov_matrices_name   : parameter_covariance
fit.cov_scaling         : 1

fit.anomaly.read.std    : False
fit.anomaly.std.file    : ${fit.cov_matrices_file}
fit.anomaly.std.name    : gamma_NIRv_std

bio.cov.base_fit        : default
!bio.cov.base_fit        : default,1E-12,1E-12,default,1E-12,default,1E-12,default,1E-12,default,1E-12,default,1E-12,default,1E-12,default,1E-12,default,1E-12
bio.cov.gamma           : 0.5E-6 !typicaly values: 0.5 for T, 1E4 for SIF, 1E2 for NIRv
bio.cov.R0              : 1E0
bio.cov.S0              : 1E0

fit.resolution		: month
! <12|365>
nlambda		: 12
