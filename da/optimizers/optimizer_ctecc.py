"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters. 
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu. 

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation, 
version 3. This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. 

You should have received a copy of the GNU General Public License along with this 
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
# optimizer.py

"""
.. module:: optimizer
.. moduleauthor:: Wouter Peters 

Revision History:
File created on 28 Jul 2010.

"""

import logging
import numpy as np
import da.tools.io4 as io
import pyenkf
from netCDF4 import chartostring
import threadpoolctl
from da.optimizers.optimizer_baseclass import Optimizer as OptimizerBase
import datetime as dt 
identifier = 'Optimizer baseclass'
version = '0.0'

################### Begin Class Optimizer ###################

class Optimizer( OptimizerBase ):
    """
        This creates an instance of an optimization object. It handles the minimum least squares optimization
        of the state vector given a set of sample objects. Two routines will be implemented: one where the optimization
        is sequential and one where it is the equivalent matrix solution. The choice can be made based on considerations of speed
        and efficiency.
    """

    def create_matrices(self,order='F'):
        """ Create Matrix space needed in optimization routine """

        # mean state  [X]
        self.x = np.zeros((self.nlag * self.nparams,), float, order=order)
        # deviations from mean state  [X']
        self.X_prime = np.zeros((self.nlag * self.nparams, self.nmembers,), float, order=order)
        # mean state, transported to observation space [ H(X) ]
        self.Hx = np.zeros((self.nobs,), float, order=order)
        # deviations from mean state, transported to observation space [ H(X') ]
        self.HX_prime = np.zeros((self.nobs, self.nmembers), float, order=order)
        # print(self.HX_prime.flags['F_CONTIGUOUS'])
        # observations
        self.obs = np.zeros((self.nobs,), float, order=order)
        # observation ids
        self.obs_ids = np.zeros((self.nobs,), float, order=order)
        # covariance of observations
        # Total covariance of fluxes and obs in units of obs [H P H^t + R]
        if self.algorithm == 'Serial':
            self.R = np.zeros((self.nobs,), float, order=order)
            self.HPHR = np.zeros((self.nobs,), float, order=order)
        else:
            self.R = np.zeros((self.nobs, self.nobs,), float, order=order)
            self.HPHR = np.zeros((self.nobs, self.nobs,), float, order=order)
        # localization of obs
        self.may_localize = np.zeros(self.nobs, bool, order=order)

        # rejection of obs
        self.may_reject = np.zeros(self.nobs, bool, order=order)

        # flags of obs
        self.flags = np.zeros(self.nobs, int, order=order)

        # species type
        self.species = np.zeros(self.nobs, str, order=order)
        # species type
        self.sitecode = np.zeros(self.nobs, str, order=order)
        # rejection_threshold
        self.rejection_threshold = np.zeros(self.nobs, float, order=order)

        # species mask
        self.speciesmask = {}

        # Kalman Gain matrix
        # self.KG = np.zeros((self.nlag * self.nparams, self.nobs,), float, order=order)
        self.KG = np.zeros((self.nlag * self.nparams,), float, order=order)

    def read_diagnostics(self,filename,qual,nlag=1,order='F',obsonly='False'):
        with io.CT_CDF(filename, method='read') as f:
            logging.debug('Opening file for optimizer (%s)' % filename)
            self.nobs=len(f["modelsamplesmean_%s" % qual][:])
            self.nmembers=len(f[ "modelsamplesdeviations_%s" % qual][:][0])
            self.obs_datetime=np.array([dt.datetime(date[0],date[1],date[2]) for date in np.array(f['obs_dates'][:],dtype=int)],order=order)
            if not obsonly: 

                self.nparams=len(f[ "statevectormean_%s" % qual][:])
                self.create_matrices()

                if "param_dates" in f.variables.keys():
                    self.statevector_domain_info=np.array([dt.datetime(date[0],date[1],date[2]) if date[0]!= 1 else dt.datetime(11,date[1],date[2]) for date in np.array(f['param_dates'][:],dtype=int)],order=order)

                self.nlag=nlag

                self.x[:]          = np.array(f[ "statevectormean_%s" % qual][:],order=order)[:]
                self.X_prime[:,:]  = np.array(f[ "statevectordeviations_%s" % qual][:],order=order)[:]
                self.algorithm = 'Serial'

            self.sitecode      = np.array(chartostring(f[ "sitecode"][:]),order=order)
            self.Hx[:]         = np.array(f[ "modelsamplesmean_%s" % qual][:],order=order)[:]
            self.HX_prime[:,:] = np.array(f[ "modelsamplesdeviations_%s" % qual][:],order=order)[:]
            self.obs[:]        = np.array(f[ "observed"][:]               ,order=order)[:]
            self.obs_ids[:]    = np.array(f[ "obspack_num"][:]           ,order=order)[:]
            self.R[:]          = np.array(f[ "modeldatamismatchvariance"][:],order=order)[:]
            if "flag" in f.variables.keys():
                self.flags[:]          = np.array(f[ "flag"][:],order=order)[:]
            if "totalmolefractionvariance" in f.variables.keys():
                self.HPHR[:]       = np.array(f[ "totalmolefractionvariance"][:],order=order)[:]
        return

    def serial_minimum_least_squares_loc(self,dates,time_window=150,species=None): 
        # import matplotlib.pyplot as plt
        dates=np.array([date.date() for date in dates],order='F' )
        obs_dates=np.array([date.date() for date in self.obs_datetime],order='F' )
        param_dates=np.array([date.date() for date in self.statevector_domain_info] ,order='F')
        xprior=self.x.copy()
        # mlo=self.sitecode=='co2_mlo'
        if species is not None:
            species_mask=np.array([species+'_' in site for site in self.sitecode])
            logging.info('%s' % species_mask)
            logging.info('%s' % self.sitecode)
        # logging.debug("selected %s from %s" %(sum(isco2),len(isco2)))
        rejected=np.zeros((len(obs_dates),),int,order='F')
        for date in dates:
            # year,month,day = (date.year,date.month,date.day)
            # obs_mask=(obs_dates>(date-dt.timedelta(days=time_window)))&(obs_dates<(date+dt.timedelta(days=time_window)))
            assimilate=np.array([False]*len(obs_dates),order='F') 
            if species is not None:
                assimilate[(obs_dates==date)&species_mask]=True
            else: 
                assimilate[(obs_dates==date)]=True
            if sum(assimilate)==0: continue
            # logging.info("selected %s" %  self.sitecode[(obs_dates==date)&species_mask])
            #     cyclic_date=dt.datetime(11,date.month+1,1).date()
            #     cyclic_date_start=cyclic_date-dt.timedelta(days=time_window+1)
            # else:
            #     cyclic_date=dt.datetime(11,date.month,date.day).date()
            #     cyclic_date_start=cyclic_date-dt.timedelta(days=time_window)
            # cyclic_mask=((param_dates>cyclic_date_start)&(param_dates<=cyclic_date))
            # # here we add the part that is added from previous year
            # # ie param date with 01-12 (dd-mm) should be allowed also when year is 
            # if cyclic_date_start.year==10:
            #     cyclic_date_start=cyclic_date_start.replace(year=11)
            #     cyclic_mask=((param_dates>cyclic_date_start)&(param_dates<cyclic_date_start.replace(year=12)))|cyclic_mask
            # params_mask=((param_dates>(date-dt.timedelta(days=time_window)))&(param_dates<=date))|cyclic_mask

            # logging.info("cyclic date %s seleceted %s" % (cyclic_date,sum(params_mask[:365])))
            logging.info("assimilating samples for day %s" % (date))
            # logging.info("updating in total %s samples in window with %s days" % (sum(obs_mask),time_window))
            # logging.info("updating %s elements within %s days" % (sum(params_mask),time_window))

            # obs                =np.array(self.obs[obs_mask], order='F')
            # may_reject         =np.array(self.may_reject[obs_mask], order='F')
            # rejection_threshold=np.array(self.rejection_threshold[obs_mask], order='F')
            # Hx                 =np.array(self.Hx[obs_mask], order='F')
            # HX_prime           =np.array(self.HX_prime[obs_mask], order='F')
            # X_prime            =np.array(self.X_prime[params_mask], order='F')
            # R                  =np.array(self.R[obs_mask], order='F')
            # HPHR               =np.array(self.HPHR[obs_mask], order='F')
            # x                  =np.array(self.x[params_mask], order='F')

                
            pyenkf.enkf_core.enksrf(self.obs, self.may_reject, self.rejection_threshold, self.Hx, self.HX_prime, self.X_prime, self.R, self.HPHR, self.x, rejected, assimilate)



            # self.Hx[obs_mask]                      =  Hx
            # self.HX_prime[obs_mask]                =  HX_prime
            # self.X_prime[params_mask]              =  X_prime
            # self.HPHR[obs_mask]                    =  HPHR
            # self.x[params_mask]                    =  x

        self.flags[(rejected==1)]=2

            # plt.plot(dates[:365],xprior[:365], label='prior')
            # plt.plot(dates[:365],self.x[:365],label='After assimilating %s' % date)
            # plt.ylim(-1,1)
            # plt.legend()
            # plt.savefig('/home/hooghiem/figures/baseinv/state_%s_window%s.png' % (date,time_window))
            # plt.close()
        return
    def serial_minimum_least_squares(self):
        """ Make minimum least squares solution by looping over obs"""

        # Calculate prior value cost function (observation part)
        res_prior = np.abs(self.obs-self.Hx)
        select    = (res_prior < 1E15).nonzero()[0]
        J_prior   = res_prior.take(select,axis=0)**2/self.R.take(select,axis=0)
        res_prior = np.mean(res_prior)

        rejected=np.zeros((self.nobs,),int,order='F')

        # with threadpoolctl.threadpool_limits(32):
        pyenkf.enkf_core.enksrf(self.obs,
                                self.may_reject,
                                self.rejection_threshold,
                                self.Hx,
                                self.HX_prime,
                                self.X_prime,
                                self.R,
                                self.HPHR,
                                self.x,
                                rejected)

        self.flags[(rejected==1)]=2

        res_post = np.abs(self.obs-self.Hx)
        select   = (res_post < 1E15).nonzero()[0]
        J_post   = res_post.take(select,axis=0)**2/self.R.take(select,axis=0)
        res_post = np.mean(res_post)

        logging.info('Observation part cost function: prior = %s, posterior = %s' % (np.mean(J_prior), np.mean(J_post)))
        logging.info('Mean residual: prior = %s, posterior = %s' % (res_prior, res_post))

################### End Class Optimizer ###################



if __name__ == "__main__":
    pass
