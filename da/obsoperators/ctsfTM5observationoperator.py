# """CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
# Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
# updates of the code. See also: http://www.carbontracker.eu.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# version 3. This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see <http://www.gnu.org/licenses/>."""
#
# """
# Author : peters
#
# Revision History:
# File created on 09 Feb 2009.
# Major modifications to go to a class-based approach, July 2010.
#
# This module holds specific functions needed to use the TM5 model within the data assimilation shell. It uses the information
# from the DA system in combination with the generic tm5.rc files.
#
# The TM5 model is now controlled by a python subprocess. This subprocess consists of an MPI wrapper (written in C) that spawns
# a large number ( N= nmembers) of TM5 model instances under mpirun, and waits for them all to finish.
#
# The design of the system assumes that the tm5.x (executable) was pre-compiled with the normal TM5 tools, and is residing in a
# directory specified by the ${RUNDIR} of a tm5 rc-file. This tm5 rc-file name is taken from the data assimilation rc-file. Thus,
# this python shell does *not* compile the TM5 model for you!
#
# """
#
# from __future__ import division
# #!/usr/bin/env python
# # tm5_tools.py

"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
# model.py

import logging
import os
import sys
import shutil
import subprocess
import glob
import numpy as np
import datetime as dt
import da.tools.io4 as io
import da.tools.rc as rc
#from string import join
from da.tools.general import create_dirs, to_datetime, get_dpm
#from da.tm5.observationoperator import TM5ObservationOperator
from da.obsoperators.observationoperator_tm5_cteco2 import TM5ObservationOperator

sys.path.append(os.getcwd())
sys.path.append("../../")

identifier = 'ctsfTM5ObsOperator'
version = '1.0'
mpi_shell_filename = 'ctsf_tm5_mpi_wrapper'
mpi_shell_location = 'da/bin/'


################### Begin Class TM5 ###################



class ctsfTM5ObsOperator(TM5ObservationOperator):
    """ This class holds methods and variables that are needed to run the TM5 model. It is initiated with as only argument a TM5 rc-file
        location. This rc-file will be used to figure out the settings for the run.

        *** This method of running TM5 assumes that a pre-compiled tm5.exe is present, and it will be run from time.start to time.final ***

        These settings can be modified later. To run a model version, simply compile the model using an existing TM5 rc-file, then
        open python, and type:

           []> tm=TM5('/Users/peters/Modeling/TM5/tutorial.rc')
           []> tm.write_rc()
           []> tm.WriteRunRc()
           []> tm.run()

        To use this class inside a data assimilation cycle, a stand-alone method "setup()" is included which modifies the TM5
        settings according to an external dictionary of values to overwrite, and then runs the TM5 model.

    """

    def __init__(self, filename):
        """ The instance of an TMObservationOperator is application dependent """

        self.ID               = identifier    # the identifier gives the model name
        self.version          = version       # the model version used
        self.restart_filelist = []
        self.output_filelist  = []

        self.outputdir        = None          # Needed for opening the samples.nc files created

        self.load_rc(filename)                # load the specified rc-file
        self.validate_rc()                    # validate the contents

        logging.info('Observation Operator initialized: %s (%s)' % (self.ID, self.version))



    def run_forecast_model(self,fluxmodel,samples,postprocessing=False,calc_flux_only=False):

        # set timerange for flux calculation
        startdate = self.dacycle['time.sample.start']
        enddate   = self.dacycle['time.sample.end']
        logging.info('Calculating fluxes from %s to %s' % (startdate, enddate))

        datevec = self.calc_datevec_monthly(startdate, enddate)
        timevec = []
        for date in datevec:
            timevec.append( (date-fluxmodel.refdate).days )
        del date
        self.timevec = np.array(timevec)
        logging.debug('Timevec = %s' % timevec)

        # calculate and write fluxmap for ensemble
        fluxmodel.calc_flux(self.timevec,postprocessing=postprocessing)

        if not calc_flux_only:
            # TM5 run...
            self.prepare_run(samples,postprocessing)
            self.validate_input(samples)                                            # perform checks
            self.run()                                                              # calculate the NEE fit
            self.save_data(samples)                                                 # save samples to netCDF file



    def calc_datevec_monthly(self, startdate, enddate):
        """Creates time vector with monthly interval ranging from startdate to enddate."""

        datevec = []
        ndays   = (enddate-startdate).days

        # use actual date for forward run with small (weekly) cycle length
        if ndays <= 30:
            # if startdate.month != enddate.month:
            #     datevec.append(dt.datetime(startdate.year,startdate.month,int(round((startdate.day+get_dpm(startdate))/2.0))))
            #     datevec.append(dt.datetime(enddate.year,enddate.month,int(round(enddate.day/2.0))))
            # else:
            datevec.append(startdate + dt.timedelta(days=int(round(ndays/2)-1)))
            # if startdate.month != (startdate + dt.timedelta(days=round(ndays/2)-1)).month:
            #     datevec.append(startdate)
            # datevec.append(startdate + dt.timedelta(days=round(ndays/2)-1))
            return datevec

        else:
            date = dt.datetime(startdate.year,startdate.month,15)
            while date <= enddate:
                datevec.append(date)
                if date.month < 12:
                    date = dt.datetime(date.year,date.month+1,15)
                else:
                    date = dt.datetime(date.year+1,1,15)

            logging.debug('Datevec = %s' % datevec)
            return datevec



    def prepare_run(self,samples,postprocessing=False):
        """
        Prepare a forward model TM5 run, this consists of:

          - reading the working copy TM5 rc-file,
          - validating it,
          - modifying the values,
          - Removing the existing tm5.ok file if present

        """

        # Write a modified TM5 model rc-file in which run/break times are defined by our da system
        new_items = {
                    'submit.options': self.dacycle.daplatform.give_blocking_flag(),
                    self.timestartkey: self.dacycle['time.sample.start'],
                    self.timefinalkey: self.dacycle['time.sample.end'],
                    'jobstep.timerange.start': self.dacycle['time.sample.start'],
                    'jobstep.timerange.end': self.dacycle['time.sample.end'],
                    'jobstep.length': 'inf',
                    'ct.params.input.dir': self.dacycle['dir.input'],
                    'ct.params.input.file': os.path.join(self.dacycle['dir.input'], 'fluxmodel')
                    }

        # add sample type specific settings to rc-file
        for sample in samples:
            new_items['output.'+sample.get_samples_type()]           = 'True'
            new_items['output.'+sample.get_samples_type()+'.infile'] = self.dacycle['ObsOperator.inputfile.'+sample.get_samples_type()]
        del sample

        if postprocessing:
            new_items['ct.params.input.dir']  = self.dacycle['dir.output']
            new_items['ct.params.input.file'] = os.path.join(self.dacycle['dir.output'], 'fluxmodel')

        if self.dacycle['transition']:
            new_items[self.istartkey] = self.transitionvalue
            logging.debug('Resetting TM5 to perform transition of od meteo from 25 to 34 levels')
        elif self.dacycle['time.restart']:  # If this is a restart from a previous cycle, the TM5 model should do a restart
            new_items[self.istartkey] = self.restartvalue
            logging.debug('Resetting TM5 to perform restart')
        else:
            #if not self.dacycle.has_key('da.obsoperator.restartfileinfirstcycle'):
            if 'da.obsoperator.restartfileinfirstcycle' not in self.dacycle:
                new_items[self.istartkey] = self.coldstartvalue  # if not, start TM5 'cold'
                logging.debug('Resetting TM5 to perform cold start')
            else:
                new_items[self.istartkey] = self.restartvalue  # If restart file is specified, start TM5 with initial restartfile
                logging.debug('Resetting TM5 to start with restart file: %s'%self.dacycle['da.obsoperator.restartfileinfirstcycle'])

        if self.dacycle['time.sample.window'] != 0:  # If this is a restart from a previous time step within the filter lag, the TM5 model should do a restart
            new_items[self.istartkey] = self.restartvalue
            logging.debug('Resetting TM5 to perform restart')

        # If neither one is true, simply take the istart value from the tm5.rc file that was read
        self.modify_rc(new_items)
        self.write_rc(self.rc_filename)

        # For each sample type, define the name of the file that will contain the modeled output of each observation
        self.simulated_file = [None] * len(samples)
        for i in range(len(samples)):
            self.simulated_file[i] = os.path.join(self.outputdir, '%s_output.%s.nc' % (samples[i].get_samples_type(),self.dacycle['time.sample.stamp']))
        del i



    def validate_input(self, samples):
        """
        Make sure that parameter files are written to the TM5 inputdir, and that observation lists are present
        """

        datadir = self.tm_settings['ct.params.input.dir']

        if not os.path.exists(datadir):
            msg = "The specified input directory for the TM5 model to read from does not exist (%s), exiting..." % datadir
            logging.error(msg)
            raise (IOError, msg)

        datafiles = os.listdir(datadir)

        # For every sample type, check if input file is present
        for sample in samples:
            obsfile = self.dacycle['ObsOperator.inputfile.'+sample.get_samples_type()]
            if not os.path.exists(obsfile) and len(sample.datalist) > 0:
                msg = "The specified obs input file for the TM5 model to read from does not exist (%s), exiting..." % obsfile
                logging.error(msg)
                if not self.dacycle.has_key('forward.savestate.dir'):
                    raise (IOError, msg)
        del sample

        for n in range(int(self.dacycle['da.optimizer.nmembers'])):
            paramfile = 'fluxmodel.%03d.nc' % n
            if paramfile not in datafiles:
                msg = "The specified parameter input file for the TM5 model to read from does not exist (%s), exiting..." % paramfile
                logging.error(msg)
                raise (IOError, msg)

        # Next, make sure there is an actual model version compiled and ready to execute

        targetdir = os.path.join(self.tm_settings[self.rundirkey])

        if self.rcfiletype == 'pycasso':
            self.tm5_exec = os.path.join(targetdir, self.tm_settings['my.basename'] + '.x')
        else:
            self.tm5_exec = os.path.join(targetdir, 'tm5.x')

        if not os.path.exists(self.tm5_exec):
            logging.error("Required TM5 executable was not found %s" % self.tm5_exec)
            logging.error("Please compile the model with the specified rc-file and the regular TM5 scripts first")
            raise IOError



################### End Class TM5 ###################


if __name__ == "__main__":
    pass
