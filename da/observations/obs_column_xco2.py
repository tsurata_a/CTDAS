"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
# obs.py

"""
Author : liesbeth

Revision History:
File created June 2019.

RC file keys: 
    obs.column.ncfile
    obs.column.input.dir
    obs.column.selection.variables
    obs.column.selection.criteria
    obs.column.extra.variables
    obs.rejection.threshold
    obs.column.mdm.calculation
    obs.column.mdm.weight
    obs.column.mdm.scaling.function
    obs.column.mdm.scaling.peak
    obs.column.mdm.scaling.mean
    obs.column.mdm.scaling.std
    sample.in.ctdas
    obs.column.localization.function
    obs.column.localization.length
"""
import os
import sys
import logging

import datetime as dtm
import numpy as np
#from string import strip
from numpy import array, logical_and, sqrt
sys.path.append(os.getcwd())
sys.path.append('../../')

identifier = 'CarbonTracker total column-averaged mole fractions'
version = '0.0'

from da.observations.obs_baseclass import Observations,TotalColumnSample
import da.tools.io4 as io
import da.tools.rc as rc


################### Begin Class TotalColumnSample ###################



################### End Class TotalColumnSample ###################


################### Begin Class TotalColumnObservations ###################

class TotalColumnObservations(Observations):
    """ 
    An object that holds data + methods and attributes needed to manipulate column samples
    """

    def setup(self, dacycle):

        self.enkf_mode = dacycle.dasystem['ct.enkf.mode']
        self.startdate = dacycle['time.sample.start']
        self.enddate   = dacycle['time.sample.end']

        # Path to the input data (daily files) and dummy filenames
        sat_files = dacycle.dasystem['obs.column.ncfile'].split(',')
        sat_dirs  = dacycle.dasystem['obs.column.input.dir'].split(',')

        self.sat_dirs    = []
        self.sat_files   = []
        for i, sat_dir in enumerate(sat_dirs):
            if not os.path.exists(sat_dir.strip()):
                msg = f"Could not find the required satellite input directory ({sat_dir})"
                logging.error(msg)
                raise IOError(msg)
            self.sat_dirs.append(sat_dirs[i].strip())
            self.sat_files.append(sat_files[i].strip())

        # Get observation selection criteria (if present):
        if 'obs.column.selection.variables' in dacycle.dasystem.keys() and 'obs.column.selection.criteria' in dacycle.dasystem.keys():
            self.selection_vars     = dacycle.dasystem['obs.column.selection.variables'].split(',')
            self.selection_criteria = dacycle.dasystem['obs.column.selection.criteria'].split(',')
            logging.debug(f"Data selection criteria found: {self.selection_vars}, {self.selection_criteria}")
        else:
            self.selection_vars     = []
            self.selection_criteria = []
            logging.info('No data observation selection criteria found, using all observations in file.')

        # Get names of additional variables required for sampling that should be read from sounding files
        # Currently implemented options are: 'surface_pressure', 'pressure_weight', 'SigmaB'
        # MAYBE IMPLEMENT ALSO OPTION TO ENTER SATELLITE, AND BASED ON THAT SET DEFAULT ADDITIONAL VARIABLES?
        # if 'obs.column.extra.variables' in dacycle.dasystem.keys():
        #     self.additional_variables = map(str.strip, dacycle.dasystem['obs.column.extra.variables'].split(','))
        #     logging.debug('Additional sounding variables that will be read from file: %s' %self.additional_variables)
        # else:
        self.additional_variables = []

        # Model data mismatch approach
        self.mdm_calculation = dacycle.dasystem.get('obs.column.mdm.calculation')
        if self.mdm_calculation in ['parametrization','read_from_file','none']:
            logging.info(f"Model data mismatch approach = {self.mdm_calculation}")
        else:
            logging.warning('No valid model data mismatch method found. Valid options are \'parametrization\', \'read_from_file\' or \'none\'. ' + \
                            'Using a constant estimate for the model uncertainty of 1ppm everywhere.')
        self.mdm_weight       = float(dacycle.dasystem['obs.column.mdm.weight'])

        # Scaling function for mdm to tweak relative weight obs w.r.t. surface obs: none, gaussian (gauss) or inverse gaussian (inv_gauss)
        if 'obs.column.mdm.scaling.function' in dacycle.dasystem.keys():
            self.mdm_scaling_fnc  = dacycle.dasystem.get('obs.column.mdm.scaling.function').strip()
            if self.mdm_scaling_fnc in ['gauss', 'inv_gauss']:
                self.mdm_scaling_peak = float(dacycle.dasystem.get('obs.column.mdm.scaling.peak'))
                self.mdm_scaling_mean = float(dacycle.dasystem.get('obs.column.mdm.scaling.mean'))
                self.mdm_scaling_std  = float(dacycle.dasystem.get('obs.column.mdm.scaling.std'))
                logging.info(f"MDM scaling parameters: {str(self.mdm_scaling_peak)}, {str(self.mdm_scaling_mean)}, {str(self.mdm_scaling_std)}")
        else:
            self.mdm_scaling_fnc = 'none'
        logging.info(f"MDM uniform weight = {str(self.mdm_weight)}, MDM scaling function = {self.mdm_scaling_fnc}")

        self.datalist = []

        # Switch to indicate whether simulated column samples are read from obsOperator output,
        # or whether the sampling is done within CTDAS (in obsOperator class)
        self.sample_in_ctdas = dacycle.dasystem['sample.in.ctdas'] if 'sample.in.ctdas' in dacycle.dasystem.keys() else False
        logging.debug(f"sample.in.ctdas = {self.sample_in_ctdas}")

        # localization
        if dacycle.dasystem.get('obs.column.localization'):
            self.loc_L = int(dacycle.dasystem.get('obs.column.localization.length'))
            logging.debug(f"Using localization length of {self.loc_L} km for satellite observations")
        else:
            self.loc_L = 0

        
        self.rejection_threshold = dacycle.dasystem['obs.rejection.threshold']



    def get_samples_type(self):
        return 'column'



    def add_observations(self, advance=False):
        """ Reading of total column observations, and selection of observations that will be sampled and assimilated.

        """

        # Read observations from daily input files
        for i, sat_dir in enumerate(self.sat_dirs):

            logging.info(f"Reading observations from {self.sat_dirs[i]} {self.sat_files[i]}")

            infile0 = os.path.join(self.sat_dirs[i], self.sat_files[i])
            ndays = 0

            while self.startdate+dtm.timedelta(days=ndays) <= self.enddate:

                infile = infile0.replace("<YYYYMMDD>",(self.startdate+dtm.timedelta(days=ndays)).strftime("%Y%m%d"))

                if os.path.exists(infile):

                    logging.info(f"Reading observations for {(self.startdate+dtm.timedelta(days=ndays)).strftime('%Y%m%d')}")
                    len_init = len(self.datalist)

                    # get index of observations that satisfy selection criteria (based on variable names and values in system rc file, if present)
                    ncf = io.ct_read(infile, 'read')
                    if self.selection_vars:
                        selvars = []
                        for j in self.selection_vars:
                            selvars.append(ncf.get_variable(j.strip()))
                        del j
                        criteria = []
                        for j in range(len(self.selection_vars)):
                            criteria.append(eval('selvars[j]'+self.selection_criteria[j]))
                        del j
                        #criteria  = [eval('selvars[i]'+self.selection_criteria[i]) for i in range(len(self.selection_vars))]
                        subselect = np.logical_and.reduce(criteria).nonzero()[0]
                    else:
                        subselect = np.arange(ncf.get_variable('sounding_id').size)

                    # retrieval attributes
                    code      = ncf.get_attribute('retrieval_id')
                    level_def = ncf.get_attribute('level_def')

                    # only read good quality observations
                    ids           = ncf.get_variable('sounding_id').take(subselect, axis=0)
                    lats          = ncf.get_variable('latitude').take(subselect, axis=0)
                    lons          = ncf.get_variable('longitude').take(subselect, axis=0)
                    obs           = ncf.get_variable('obs').take(subselect, axis=0)
                    unc           = ncf.get_variable('uncertainty').take(subselect, axis=0)
                    dates         = ncf.get_variable('date').take(subselect, axis=0)
                    dates         = array([dtm.datetime(*d) for d in dates])
                    av_kernel     = ncf.get_variable('averaging_kernel').take(subselect, axis=0)
                    prior_profile = ncf.get_variable('prior_profile').take(subselect, axis=0)
                    pressure      = ncf.get_variable('pressure_levels').take(subselect, axis=0)

                    if 'prior' in ncf.variables:
                        prior = ncf.get_variable('prior').take(subselect, axis=0)
                        self.additional_variables.append('prior')
                    else: prior = [float('nan')]*len(ids)

                    if 'pressure_weight' in ncf.variables:
                        h = ncf.get_variable('pressure_weight').take(subselect, axis=0)
                        self.additional_variables.append('pressure_weight')
                    else: h = [float('nan')]*len(ids)

                    if 'surface_pressure' in ncf.variables:
                        psurf = ncf.get_variable('surface_pressure').take(subselect, axis=0)
                        self.additional_variables.append('surface_pressure')
                    else: psurf = [float('nan')]*len(ids)

                    if 'model_error' in ncf.variables:
                        moderr = ncf.get_variable('model_error').take(subselect, axis=0)
                        self.additional_variables.append('model_error')
                    else: moderr = [0]*len(ids)

                    if 'SigmaB' in ncf.variables:
                        self.sigmab = ncf.get_variable('SigmaB')
                        self.additional_variables.append('SigmaB')

                    ncf.close()

                    # Add samples to datalist
                    # Note that the mdm is initialized here equal to the measurement uncertainty. This value is used in add_model_data_mismatch to calculate the mdm including model error
                    for n in range(len(ids)):
                        # Check for every sounding if time is between start and end time (relevant for first and last days of window)
                        if self.startdate <= dates[n] <= self.enddate:
                            self.datalist.append(TotalColumnSample(idx=ids[n], codex=code, xdate=dates[n], obs=obs[n], simulated=None, lat=lats[n], lon=lons[n], mdm=unc[n], prior=prior[n], prior_profile=prior_profile[n,:], \
                                                                    av_kernel=av_kernel[n,:], pressure=pressure[n,:],level_def=level_def,psurf=psurf[n],h=h[n],moderr=moderr[n],loc_L=self.loc_L))

                    logging.debug(f"Added {(len(self.datalist)-len_init)} observations to the column data list")

                ndays += 1

        del i

        if len(self.datalist) > 0:
            logging.info(f"Column observations list now holds {len(self.datalist)} values")
        else:
            logging.info("No observations found for sampling window")



    # def gauss_correction(self, lat):
    #     scaling = 1.0 + self.mdm_scaling_peak*np.exp(-(lat-self.mdm_scaling_mean)**2/(2.0*self.mdm_scaling_std**2))
    #     return scaling
    #
    # def inv_gauss_correction(self, lat):
    #     scaling = self.mdm_scaling_peak - (self.mdm_scaling_peak-1.0)*np.exp(-(lat-self.mdm_scaling_mean)**2/(2.0*self.mdm_scaling_std**2))
    #     return scaling



    def add_model_data_mismatch(self, filename=None, advance=False):
        """ This function is empty: model data mismatch calculation is done during sampling in observation operator (TM5) to enhance computational efficiency
            (i.e. to prevent reading all soundings twice and writing large additional files)

        """
        self.rejection_threshold = int(self.rejection_threshold)

        # if mdm scaling based on Nobs per zone is selected, calculate scaling factors
        if self.mdm_scaling_fnc == 'nobs_zones':
            nobs_south, nobs_tropic, nobs_north = 0.0, 0.0, 0.0
            for obs in self.datalist:
                if -90 <= obs.lat < -30:
                    nobs_south += 1.0
                elif -30 <= obs.lat < 30:
                    nobs_tropic += 1.0
                else:
                    nobs_north += 1.0
            del obs
            nobs_scaling_south  = np.min([ np.sqrt(nobs_south / np.min([nobs_south,nobs_tropic,nobs_north])) , 5 ])
            nobs_scaling_tropic = np.min([ np.sqrt(nobs_tropic / np.min([nobs_south,nobs_tropic,nobs_north])) , 5 ])
            nobs_scaling_north  = np.min([ np.sqrt(nobs_north / np.min([nobs_south,nobs_tropic,nobs_north])) , 5 ])

            logging.info(f"mdm scaling factors based on Nobs per zone calculated: southern = {nobs_scaling_south}, tropics = {nobs_scaling_tropic}, northern = {nobs_scaling_north}")

        # At this point mdm is set to the measurement uncertainty only, added in the add_observations function.
        # Here this value is used to set the combined mdm by adding an estimate for the model uncertainty as a sum of squares.
        if len(self.datalist) <= 1: return #== 0: return
        for obs in self.datalist:
            # parametrization as used by Frederic Chevallier
            if self.mdm_calculation == 'parametrization':
                obs.mdm = self.mdm_weight * ( obs.mdm*obs.mdm + (0.8*np.exp((90.0+obs.lat)/300.0))**2 )**0.5
            elif self.mdm_calculation == 'none':
                obs.mdm = self.mdm_weight * obs.mdm
            elif self.mdm_calculation == 'read_from_file':
                obs.mdm = self.mdm_weight * (obs.mdm*obs.mdm + obs.modelunc*obs.modelunc)**0.5
            else: # assume general model uncertainty of 1 ppm (arbitrary value)
                obs.mdm = self.mdm_weight * ( obs.mdm*obs.mdm + 1.0**2 )**0.5

            # latitude-dependent scaling of mdm to account for varying number of surface observations
            # when combining column obs with flask obs
            if self.mdm_scaling_fnc == 'gauss':
                obs.mdm = obs.mdm * self.gauss_correction(obs.lat)
            elif self.mdm_scaling_fnc == 'inv_gauss':
                obs.mdm = obs.mdm * self.inv_gauss_correction(obs.lat)
            elif self.mdm_scaling_fnc == 'nobs_zones':
                if -90 <= obs.lat < -30:
                    obs.mdm = obs.mdm * nobs_scaling_south
                elif -30 <= obs.lat < 30:
                    obs.mdm = obs.mdm * nobs_scaling_tropic
                else:
                    obs.mdm = obs.mdm * nobs_scaling_north
        del obs

        meanmdm = np.average(np.array( [obs.mdm for obs in self.datalist] ))
        logging.debug(f"Mean MDM = {meanmdm}")



    def add_simulations(self, filename, silent=False):
        """ Adds observed and model simulated column values to the mole fraction objects
            This function includes the add_observations and add_model_data_mismatch functionality for the sake of computational efficiency

        """

        if self.sample_in_ctdas:
            logging.debug("CODE TO ADD SIMULATED SAMPLES TO DATALIST TO BE ADDED")

        else:
            # read simulated samples from file
            if not os.path.exists(filename):
                msg = f"Sample output filename for observations could not be found : {filename}"
                logging.error(msg)
                logging.error("Did the sampling step succeed?")
                logging.error("...exiting")
                raise IOError(msg)

            ncf       = io.ct_read(filename, method='read')
            ids       = ncf.get_variable('sounding_id')
            simulated = ncf.get_variable('column_modeled')
            ncf.close()
            logging.info(f"Successfully read data from model sample file ({filename})")

            obs_ids = self.getvalues('id').tolist()

            missing_samples = []

            # Match read simulated samples with observations in datalist
            logging.info(f"Adding {len(ids)} simulated samples to the data list...")
            for i, id in enumerate(ids):
                # Assume samples are in same order in both datalist and file with simulated samples...
                if ids[i] == obs_ids[i]:
                    self.datalist[i].simulated = simulated[i]
                # If not, find index of current sample
                elif ids[i] in obs_ids:
                    index = obs_ids.index(ids[i])
                    # Only add simulated value to datalist if sample has not been filled before. Otherwise: exiting
                    if self.datalist[index].simulated is not None:
                        msg = 'Simulated and observed samples not in same order, and duplicate sample IDs found.'
                        logging.error(msg)
                        raise IOError(msg)
                    else:
                        self.datalist[index].simulated = simulated[i]
                else:
                    logging.debug(f"added {id} to missing_samples, obs id = {obs_ids[i]}")
                    missing_samples.append(ids[i])
            del i

            if not silent and missing_samples != []:
                logging.warning(f"{len(missing_samples)} Model samples were found that did not match any ID in the observation list. Skipping them...")

            # if number of simulated samples < observations: remove observations without samples
            if len(simulated) < len(self.datalist):
                test = len(self.datalist) - len(simulated)
                logging.warning(f'{test} Observations were not sampled, removing them from datalist...')
                for index in reversed(range(len(self.datalist))):
                    if self.datalist[index].simulated is None:
                        del self.datalist[index]
                del index

            logging.debug(f"{len(ids) - len(missing_samples)} simulated values were added to the data list")



    def write_sample_coords(self, obsinputfile):
        """
            Write empty sample_coords_file if soundings are present in time interval, just such that general pipeline code does not have to be changed...
        """

        if self.sample_in_ctdas:
            return

        if len(self.datalist) <= 1: #== 0:
            logging.info("No observations found for this time period, no obs file written")
            return

        # write data required by observation operator for sampling to file
        f = io.CT_CDF(obsinputfile, method='create')
        logging.debug(f"Creating new observations file for ObservationOperator ({obsinputfile}) containing {len(self.datalist)} observation")

        dimsoundings = f.add_dim('soundings', len(self.datalist))
        dimdate      = f.add_dim('epoch_dimension', 7)
        dimchar      = f.add_dim('char', 20)
        if len(self.datalist) == 1:
            dimlevels = f.add_dim('levels', len(self.getvalues('pressure')))
            if len(self.getvalues('av_kernel')) != len(self.getvalues('pressure')):
                dimlayers = f.add_dim('layers',len(self.getvalues('av_kernel')))
                layers = True
            else: layers = False
        else:
            dimlevels = f.add_dim('levels', self.getvalues('pressure').shape[1])
            if self.getvalues('av_kernel').shape[1] != self.getvalues('pressure').shape[1]:
                dimlayers = f.add_dim('layers', self.getvalues('pressure').shape[1] - 1)
                layers = True
            else: layers = False

        savedict           = io.std_savedict.copy()
        savedict['dtype']  = "int64"
        savedict['name']   = "sounding_id"
        savedict['dims']   = dimsoundings
        savedict['values'] = self.getvalues('id').tolist()
        f.add_data(savedict)

        data = [[d.year, d.month, d.day, d.hour, d.minute, d.second, d.microsecond] for d in self.getvalues('xdate') ]
        savedict           = io.std_savedict.copy()
        savedict['dtype']  = "int"
        savedict['name']   = "date"
        savedict['dims']   = dimsoundings + dimdate
        savedict['values'] = data
        f.add_data(savedict)

        savedict           = io.std_savedict.copy()
        savedict['name']   = "latitude"
        savedict['dims']   = dimsoundings
        savedict['values'] = self.getvalues('lat').tolist()
        f.add_data(savedict)

        savedict           = io.std_savedict.copy()
        savedict['name']   = "longitude"
        savedict['dims']   = dimsoundings
        savedict['values'] = self.getvalues('lon').tolist()
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name']   = "averaging_kernel"
        if layers:
            savedict['dims'] = dimsoundings + dimlayers
        else:
            savedict['dims'] = dimsoundings + dimlevels
        savedict['values'] = self.getvalues('av_kernel').tolist()
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name']   = "prior_profile"
        if layers:
            savedict['dims'] = dimsoundings + dimlayers
        else:
            savedict['dims'] = dimsoundings + dimlevels
        savedict['values'] = self.getvalues('prior_profile').tolist()
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name']   = "pressure_levels"
        savedict['dims']   = dimsoundings + dimlevels
        savedict['values'] = self.getvalues('pressure').tolist()
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['dtype']  = "char"
        savedict['name']   = "level_def"
        savedict['dims']   = dimsoundings + dimchar
        savedict['values'] = self.getvalues('level_def').tolist()
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name']   = "observed"
        savedict['dims']   = dimsoundings
        savedict['values'] = self.getvalues('obs').tolist()
        f.add_data(savedict)

        if 'prior' in self.additional_variables:
            savedict = io.std_savedict.copy()
            savedict['name']   = "prior"
            savedict['dims']   = dimsoundings
            savedict['values'] = self.getvalues('prior').tolist()
            f.add_data(savedict)

        if 'surface_pressure' in self.additional_variables:
            savedict = io.std_savedict.copy()
            savedict['name']   = "psurf"
            savedict['dims']   = dimsoundings
            savedict['values'] = self.getvalues('psurf').tolist()
            f.add_data(savedict)

        if 'SigmaB' in self.additional_variables:
            savedict = io.std_savedict.copy()
            savedict['name']   = "SigmaB"
            savedict['dims']   = dimlevels
            savedict['values'] = self.sigmab
            f.add_data(savedict)

        if 'pressure_weight' in self.additional_variables:
            savedict = io.std_savedict.copy()
            savedict['name']   = "pressure_weight"
            if layers:
                savedict['dims'] = dimsoundings + dimlayers
            else:
                savedict['dims'] = dimsoundings + dimlevels
            savedict['values'] = self.getvalues('h').tolist()
            f.add_data(savedict)

        f.close()


    def write_sample_auxiliary(self, auxoutputfile, qual='optimized'):
        """
            Write selected information contained in the Observations object to a file.
        """
        if not os.path.exists(auxoutputfile):
            # this the first time we create it
            write_info=True
            f = io.CT_CDF(auxoutputfile, method='create')
            logging.debug(f"Creating new auxiliary file for postprocessing ({auxoutputfile})")
        else: 
            f = io.CT_CDF(auxoutputfile, method='write')
            logging.debug(f"Opening auxiliary file for postprocessing ({auxoutputfile})")
        dimid = f.add_dim('obs', len(self.datalist))

        dimid      = f.add_dim('obs', None) #len(self.datalist))
        dim200char = f.add_dim('string_of200chars', 200)
        dim10char  = f.add_dim('string_of10chars', 10)
        dimcalcomp = f.add_dim('calendar_components', 7)
        # AvdB: If this is a FWD run this is 1 (substracers of species summed into species total for
        # column calculation). If not FWD, this takes the shape of no. members in the ensemble.
        if self.enkf_mode == 'fwd':
            dimmembers = f.add_dim('members', 1)
        else:
            dimmembers = f.add_dim('members', self.getvalues('simulated').shape[1])

        if len(self.datalist) <= 1: #== 0:
            f.close()
            #return outfile

        savedict = io.std_savedict.copy()
        savedict['name']      = "obs_num"
        savedict['dtype']     = "int64"
        savedict['long_name'] = "Unique_Dataset_observation_index_number"
        savedict['units']     = ""
        savedict['dims']      = dimid
        savedict['values']    = self.getvalues('id').tolist()
        savedict['comment']   = "Unique observation identifier."
        f.add_data(savedict)

        data     = [[d.year, d.month, d.day, d.hour, d.minute, d.second, d.microsecond] for d in self.getvalues('xdate') ]
        savedict = io.std_savedict.copy()
        savedict['dtype']   = "int"
        savedict['name']    = "date_components"
        savedict['units']   = "integer components of UTC date/time"
        savedict['dims']    = dimid + dimcalcomp
        savedict['values']  = data
        savedict['comment'] = "Calendar date components as integers. Times and dates are UTC."
        savedict['order']   = "year, month, day, hour, minute, second"
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name']      = "observed"
        savedict['long_name'] = "observedvalues"
        savedict['units']     = "ppm"
        savedict['dims']      = dimid
        savedict['values']    = self.getvalues('obs').tolist()
        savedict['comment']   = 'Observations used in optimization'
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name']      = "modeldatamismatch"
        savedict['long_name'] = "modeldatamismatch"
        savedict['units']     = "[ppm]"
        savedict['dims']      = dimid
        savedict['values']    = self.getvalues('mdm').tolist()
        savedict['comment']   = 'Standard deviation of mole fractions resulting from model-data mismatch'
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name']      = f"modelsamples_{qual}" 
        savedict['long_name'] = "modelsamples for all ensemble members"
        savedict['units']     = "ppm"
        savedict['dims']      = dimid + dimmembers
        savedict['values']    = self.getvalues('simulated').tolist()
        savedict['comment']   = f"simulated mole fractions based on {qual} state vector"
        f.add_data(savedict)

        f.close()

        logging.debug(f"Successfully wrote data to auxiliary sample output file ({auxoutputfile})")


    def read_localization_coefficients(self, loc_file):
        if not os.path.exists(loc_file):
            msg = f"Localization filename for observations could not be found : {loc_file}"
            logging.error(msg)
            raise IOError(msg)

        ncf       = io.ct_read(loc_file, method='read')
        self.loc  = ncf.get_variable('loc_coeff')
        ncf.close()
        logging.info(f"Successfully read localization coefficients from file ({loc_file})")




################### End Class TotalColumnObservations ###################


if __name__ == "__main__":
    pass
