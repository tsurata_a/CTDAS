"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
# obs.py

"""
Author : peters 

Revision History:
File created on 28 Jul 2010.

"""
import os
import sys
import logging
import numpy as np        
import datetime as dtm
# from string import strip
from itertools import repeat, compress
import multiprocessing as mp 
from numpy import array, ndarray, mean, logical_and, sqrt
import da.tools.molefraction_to_permil as mtp
sys.path.append(os.getcwd())
sys.path.append('../../')

identifier = 'CarbonTracker CO2 and C13 mole fractions'
version = '0.0'

from da.observations.obs_baseclass import Observations
import da.tools.io4 as io
import da.tools.rc as rc
import pickle

def check_path(path):
    if not path=='None':
        if not os.path.exists(path):
            msg = 'Could not find required input file (%s) ' % path
            logging.error(msg)
            raise IOError(msg)
    return

################### Begin Class ObsPackObservations ###################

class ObsPackObservations(Observations):
    """ an object that holds data + methods and attributes needed to manipulate mole fraction values """

    def setup(self, dacycle):

        self.startdate = dacycle['time.sample.start']
        self.enddate = dacycle['time.sample.end']

        self.species=dacycle.dasystem['ct.params.species'].split(' ')
        self.enkf_mode=dacycle.dasystem['ct.enkf.mode']
        # If meteo is sampled from tm5 boolean
        self.sample_meteo=dacycle.dasystem['obspack.meteo.sampling']
        logging.debug('meteo set to %s' % self.sample_meteo) 
        # obspack_info will hold info for the entire simulation and will
        self.dacycle=dacycle
        self.obspack_info= {}
        self.data_dict   = {}

        self.fname_info=os.path.join(dacycle['dir.output'],'..',"obspack_info.pickle")
        #check if obspack file exist
        if os.path.exists(self.fname_info):
        #el
            self.obspack_info = pickle.load(open(self.fname_info, 'rb'))
            logging.info("Loaded pickled file for ObsPack")
        else:
            for species in self.species:                
                logging.info("Retrieving ObsPack settings for %s" %species)
                # Create species dict to hold info for species
                self.obspack_info[species]={}

                

                self.obspack_info[species]["obspack_dir"]=dacycle.dasystem['obspack.input.dir.'+species]
                self.obspack_info[species]["obspack_id"]=dacycle.dasystem['obspack.input.id.'+species]
                self.obspack_info[species]["sites_file"]=dacycle.dasystem['obs.sites.rc.'+species]
                check_path(self.obspack_info[species]["sites_file"])
                check_path(self.obspack_info[species]["obspack_dir"])
                logging.info("Retrieving obspack info for %s" %species)
                self.setup_observations(dacycle,self.obspack_info[species],species)
            # self.sites_file = dacycle.dasystem['obs.sites.rc']
            
            pickle.dump(self.obspack_info, open(self.fname_info, 'wb'), -1)

            logging.info("Saved ObsPack info to %s" %self.fname_info)
        # this is used for now, in the future maybe just store indices of ofsetss
        for species in self.species:                
            self.data_dict[species] = [] #test 
        # global datalist access by other  
        self.datalist = []


        # Localization settings
        self.obspack_localization = dacycle.dasystem.get('obs.column.localization')
        if self.obspack_localization:
            # get localization length keys
            import re
            keys = dacycle.dasystem.keys()
            lockeys = [k for k in keys if 'obspack.localization.length' in k]
            loctypes = [re.compile('obspack.localization.length.(.*)').findall(lock) for lock in lockeys]
            self.loc_L_types = {}
            for ltype in loctypes:
                self.loc_L_types[ltype[0]] = int(dacycle.dasystem.get('obs.obspack.localization.length.%s'%ltype[0]))
                logging.debug('Using localization length of %s km for observations of type %s' %(dacycle.dasystem.get('obs.obspack.localization.length.%s'%ltype[0]),ltype[0]))

    # can we decorate the baseclass method? 
    def getvalues_species(self, name, constructor=array,data=None):
        # data : is the self.data_dict[species] list that we want to get the data of

        if data==None:
            data=self.data_dict['co2']
        result = constructor([getattr(o, name) for o in data])
        if isinstance(result, ndarray):
            return result.squeeze()
        else:
            return result

    def get_samples_type(self):
        return 'flask'

    def add_observations(self,**kwargs):
        for species in self.species:
            logging.info("Adding %s observations" %species)
            self.add_observations_generic(self.obspack_info[species],species,**kwargs)
        return

    def add_model_data_mismatch(self,filename=None):

        # dictionary site_info dictionary to the Observations object for future use
        if not filename==None:
            logging.warning("Running in multi-species mode. The user passed sites-file with name: " ) 
            logging.warning("\t\t%s" %(filename) ) 
            logging.warning("Is ignored. I will probably find the right one anyway, see the log below." ) 
        for species in self.species:
            logging.info("Adding %s model-data-mismatch using settings from" %(species)) 
            logging.info("\t\t%s" %(self.obspack_info[species]['sites_file'])) 

            if self.obspack_info[species]["sites_file"]!='None':
                self.add_model_data_mismatch_generic(species)

        logging.info('Removed unused observations, observation list now holds %s values' % len(self.datalist))
        logging.debug("Added Model Data Mismatch to all samples ")
        return

    def setup_observations(self, dacycle, species_dict,species):
        """
        perfomrs expensive checking that can be done only once during an experiment
        """

        # logging.info("ObsPack dataset info read, proceeding with %d netcdf files for %s" %( len(ncfilelist),species))
        # Read valid sites from site_weights file
        valid_sites   = []
        # sites file is 0 in list this should be done nicer in the future
        if species_dict["sites_file"]=='None':
            return
        sites_weights = rc.read(species_dict["sites_file"])

        species_dict['rejection_threshold'] = int(sites_weights['obs.rejection.threshold'])
        species_dict['global_R_scaling']    = float(sites_weights['global.R.scaling'])
        # species_dict['n_site_categories']   = int(sites_weights['n.site.categories'])

        logging.debug('Model-data mismatch rejection threshold: %d '   % species_dict['rejection_threshold'])
        logging.warning('Model-data mismatch scaling factor     : %f ' % species_dict['global_R_scaling']   )
        # logging.debug('Model-data mismatch site categories    : %d '   % species_dict['n_site_categories']  )

        cats = [k for k in list(sites_weights.keys()) if 'site.category' in k]

        site_categories = {}
        for key in cats:
            name, error, may_localize, may_reject = sites_weights[key].split(';')
            name         = name.strip().lower()
            error        = float(error)
            may_reject   = ("TRUE" in may_reject.upper())
            may_localize = ("TRUE" in may_localize.upper())
            site_categories[name] = {'category': name, 'error': error, 'may_localize': may_localize, 'may_reject': may_reject}
        site_info = {}
        site_incalt = {}
        site_move={}
        for key, value in list(sites_weights.items()):
            if species+'_' in key: 
                sitename = key.strip()
                values=value.split()
                site_info[sitename] = site_categories[values[0].strip().lower()].copy()
                site_info[sitename]['assimilate'] = values[5].strip()

            if 'site.move' in key:
                identifier, latmove, lonmove = value.split(';')
                site_move[identifier.strip()] = (float(latmove), float(lonmove))
            if 'site.incalt' in key:
                identifier, incalt = value.split(';')
                site_incalt[identifier.strip()] = (int(incalt))

        for key, value in sites_weights.items():
            if species+'_' in key:
                valid_sites.append(key)

        logging.info("ObsPack datasets checked, found %d valid sites for %s" %( len(valid_sites),species))
        # species_dict['ncfilelist']=ncfilelist
        species_dict['valid_sites']=valid_sites
        species_dict['site_info']=site_info
        species_dict['site_incalt']=site_incalt
        species_dict['site_move']=site_move
        pool=mp.Pool(processes=int(dacycle['da.resources.ntasks']))
        for result in pool.starmap(fetch_station,zip(species_dict['valid_sites'],repeat(species_dict),repeat(species))):
            species_dict[result[0]]=result[1]
        pool.close()
        pool.join()
        for key in species_dict:
            logging.debug('found station %s' % key)
        # if ncfile not in species_dict['valid_sites']: continue
        return 

    def add_observations_generic(self,species_dict,species,checkforassimilate=False):
        """ Returns a MoleFractionList holding individual MoleFractionSample objects for all obs in a file
      
            The ObsPack mole fraction files are provided as time series per site with all dates in sequence. 
            We will loop over all site files in the ObsPackage, and subset each to our needs
            
        """

        if species_dict["sites_file"]=='None':
            return

        for ncfile in species_dict['valid_sites']:
            # when we are gathering data for the assimilation step we don't want to gather all.
            if checkforassimilate:
                if self.obspack_info[species]['site_info'][ncfile]['assimilate']=='F': continue

            ncf    = io.ct_read(species_dict[ncfile]['ncfile'], 'read')

            subselect = logical_and(species_dict[ncfile]['dates']>= self.startdate , species_dict[ncfile]['dates']<= self.enddate).nonzero()[0]

            dates = species_dict[ncfile]['dates'].take(subselect, axis=0)

            if 'merge_num' in ncf.variables:
                obspacknum = ncf.get_variable('merge_num').take(subselect)
            else:
                obspacknum = ncf.get_variable('obspack_num').take(subselect)
            # we are not using obs_id currently.
            # its also giving weird results
            if 'ccggAllData' in ncfile:
                obspackid = ncf.get_variable('id').take(subselect, axis=0)
            else:
                obspackid = ncf.get_variable('obspack_id').take(subselect, axis=0)
            # logging.debug('obspackid %s' % obspackid)
            obspackid   = [s.tostring().lower() for s in obspackid]
            obspackid   = list(map(str.strip,str(obspackid)))
            datasetname = ncfile  # use full name of dataset to propagate for clarity
            lats        = ncf.get_variable('latitude').take(subselect, axis=0)
            lons        = ncf.get_variable('longitude').take(subselect, axis=0)
            alts        = ncf.get_variable('altitude').take(subselect, axis=0)
            obs         = ncf.get_variable('value').take(subselect, axis=0)
            species     = ncf.get_attribute('dataset_parameter')
            flags       = ncf.get_variable('obs_flag').take(subselect, axis=0)
            # 2FIX AvdB: 
            try:
                assim_concerns=np.sum( ncf.get_variable('assimilation_concerns').take(subselect, axis=0),axis=1)
            except KeyError:
                # 2FIX AvdB: if assimilation concerns is not a variable in the nc file, fill it with 0s
                logging.warning(f"Variable 'assimilation_concerns' not found in {species_dict[ncfile]['ncfile'].split('/')[-1]}, setting values to 0. Please check if this is OK!")
                assim_concerns=np.zeros(len(subselect))
            ncf.close()
            mask=(assim_concerns>0)
            flags[mask]=99

            # only add observations with flag = 1
            # counter = 0
            for n,date in enumerate(dates):
                self.datalist.append(MoleFractionSample(obspacknum[n], date, datasetname, obs[n], 0.0, 0.0, 0.0, 0.0, \
                                        flags[n], alts[n], lats[n], lons[n], obspackid[n], species, 1, 0.0,species_dict[ncfile]['ncfile']))
                        # self.data_dict[species].append(MoleFractionSample(obspacknum[n], dates[n], datasetname, obs[n], 0.0, 0.0, 0.0, 0.0, \
                                        # flags[n], alts[n], lats[n], lons[n], obspackid[n], species, 1, 0.0, infile))

            # logging.debug("Added %d observations from file (%s) to the Data list" % (len(dates), ncfile))

        # logging.info("Observations %s list now holds %d values" %(species, len(self.data_dict[species])))
        logging.info("Observations list now holds %d values" % len(self.datalist))
        return 

    def write_sample_coords(self, obsinputfile):
        """ 
            Write the information needed by the observation operator to a file. Return the filename that was written for later use

        """

        f = io.CT_CDF(obsinputfile, method='create')
        logging.debug('Creating new observations file for ObservationOperator (%s)' % obsinputfile)

        dimid = f.add_dim('obs', len(self.datalist))
        dim6char = f.add_dim('string_of6chars', 6)
        dimcalcomp = f.add_dim('calendar_components', 6)

        if len(self.datalist) == 0:
            f.close()
            return 

        for species in self.species:
            for key, value in list(self.obspack_info[species]['site_move'].items()):
                msg = "Site is moved by %3.2f degrees latitude and %3.2f degrees longitude" % value 
                f.add_attribute(key, msg)

        data = self.getvalues('id')

        savedict = io.std_savedict.copy() 
        savedict['name'] = "obs_num"
        savedict['dtype'] = "int"
        savedict['long_name'] = "Unique_Dataset_observation_index_number"
        savedict['units'] = ""
        savedict['dims'] = dimid
        savedict['values'] = data.tolist()
        savedict['comment'] = "Unique index number within this dataset ranging from 0 to UNLIMITED."
        f.add_data(savedict)

        data = [[d.year, d.month, d.day, d.hour, d.minute, d.second] for d in self.getvalues('xdate') ]

        savedict = io.std_savedict.copy() 
        savedict['dtype'] = "int"
        savedict['name'] = "date_components"
        savedict['units'] = "integer components of UTC date/time"
        savedict['dims'] = dimid + dimcalcomp
        savedict['values'] = data
        savedict['missing_value'] = -9
        savedict['comment'] = "Calendar date components as integers. Times and dates are UTC." 
        savedict['order'] = "year, month, day, hour, minute, second"
        f.add_data(savedict)

        data = self.getvalues('lat')

        savedict = io.std_savedict.copy() 
        savedict['name'] = "latitude"
        savedict['units'] = "degrees_north"
        savedict['dims'] = dimid
        savedict['values'] = data.tolist()
        savedict['missing_value'] = -999.9
        f.add_data(savedict)

        data = self.getvalues('lon')

        savedict = io.std_savedict.copy() 
        savedict['name'] = "longitude"
        savedict['units'] = "degrees_east"
        savedict['dims'] = dimid
        savedict['values'] = data.tolist()
        savedict['missing_value'] = -999.9
        f.add_data(savedict)

        data = self.getvalues('height')

        savedict = io.std_savedict.copy() 
        savedict['name'] = "altitude"
        savedict['units'] = "meters_above_sea_level"
        savedict['dims'] = dimid
        savedict['values'] = data.tolist()
        savedict['missing_value'] = -999.9
        f.add_data(savedict)

        data = self.getvalues('samplingstrategy')

        savedict = io.std_savedict.copy() 
        savedict['dtype'] = "int"
        savedict['name'] = "sampling_strategy"
        savedict['units'] = "NA"
        savedict['dims'] = dimid
        savedict['values'] = data.tolist()
        savedict['missing_value'] = -9
        f.add_data(savedict)

        # data = self.getvalues('species')

        # savedict = io.std_savedict.copy() 
        # savedict['dtype'] = "char"
        # savedict['name'] = "species"
        # savedict['units'] = "species to identify the simulated data"
        # savedict['dims'] = dimid + dim6char
        # savedict['values'] = data
        # savedict['missing_value'] = '!'
        # f.add_data(savedict)

        data = self.getvalues('species')

        savedict = io.std_savedict.copy() 
        savedict['dtype'] = "char"
        savedict['name'] = "species"
        savedict['units'] = "ObsPack identifier"
        savedict['dims'] = dimid + dim6char
        savedict['values'] = data
        savedict['missing_value'] = '!'
        f.add_data(savedict)

        # data = self.getvalues('evn')

        # savedict = io.std_savedict.copy() 
        # savedict['dtype'] = "char"
        # savedict['name'] = "obs_id"
        # savedict['units'] = "ObsPack datapoint identifier"
        # savedict['dims'] = dimid + dim200char
        # savedict['values'] = data
        # savedict['missing_value'] = '!'
        # f.add_data(savedict)

        data = self.getvalues('obs')

        savedict = io.std_savedict.copy()
        savedict['name'] = "observed"
        savedict['long_name'] = "observedvalues"
        savedict['units'] = "mol mol-1"
        savedict['dims'] = dimid
        savedict['values'] = data.tolist()
        savedict['comment'] = 'Observations used in optimization'
        f.add_data(savedict)

        data = self.getvalues('mdm')

        savedict = io.std_savedict.copy()
        savedict['name'] = "modeldatamismatch"
        savedict['long_name'] = "modeldatamismatch"
        savedict['units'] = "[mol mol-1]"
        savedict['dims'] = dimid
        savedict['values'] = data.tolist()
        savedict['comment'] = 'Standard deviation of mole fractions resulting from model-data mismatch'
        f.add_data(savedict)

        f.close()

        logging.debug("Successfully wrote data to obs file")
        logging.info("Sample input file for obs operator now in place [%s]" % obsinputfile)

    def add_model_data_mismatch_generic(self,species):

        for obs in self.datalist:
            if species==obs.species:
                obs.mdm = 1000.0  # default is very high model-data-mismatch, until explicitly set by script
                if self.obspack_info[species]['site_info'][obs.code]['assimilate']=='F':
                    obs.flag=99
                # for assimilated data set obsflag
                if obs.flag == 1: # flag is taken from the gv+ datasets: 1=background/representative, 0=local.
                    obs.flag = 0
                elif obs.flag == 0:
                    obs.flag = 99 # 99 means: do-not-use
                else: 
                    obs.flag = 99

                if obs.code in self.obspack_info[species]['site_info']:
                    if self.obspack_info[species]['site_info'][obs.code]['category'] == 'aircraft':
                        nr_obs_per_day = 1
                    else:
                        nr_obs_per_day=self.obspack_info[species][obs.code]['n_obs_per_day'][obs.xdate.date()]
                        # nr_obs_per_day = len([c.code for c in self.datalist if c.code == obs.code and c.xdate.day == obs.xdate.day and c.xdate.year == obs.xdate.year and c.flag == 0])
                    obs.mdm = self.obspack_info[species]['site_info'][obs.code]['error'] * sqrt(nr_obs_per_day) * self.obspack_info[species]['global_R_scaling']
                    # logging.debug("Observation found (%s, %d), mdm category is: %0.2f, scaled with number of observations per day (%i), final mdm applied is: %0.2f." \ % (obs.code, obs.id, self.obspack_info[species]['site_info'][obs.code]['error'],nr_obs_per_day,obs.mdm)) #site_info[obs.code]['error']*sqrt(nr_obs_per_day)))
                    obs.may_localize = self.obspack_info[species]['site_info'][obs.code]['may_localize']
                    obs.may_reject = self.obspack_info[species]['site_info'][obs.code]['may_reject']
                else:
                    logging.warning("Observation NOT found (%s, %d), please check sites.rc file (%s)  !!!" % (obs.code, obs.id, filename))

                if obs.code in self.obspack_info[species]['site_move']:
                    movelat, movelon = self.obspack_info[species]['site_move'][obs.code]
                    obs.lat = obs.lat + movelat
                    obs.lon = obs.lon + movelon
                    # logging.warning("Observation location for (%s, %d), is moved by %3.2f degrees latitude and %3.2f degrees longitude" % (obs.code, obs.id, movelat, movelon))

                if obs.code in self.obspack_info[species]['site_incalt']:
                    incalt = self.obspack_info[species]['site_incalt'][obs.code]
                    obs.height = obs.height + incalt
                    # logging.warning("Observation location for (%s, %d), is moved by %3.2f meters in altitude" % (obs.code, obs.id, incalt))

                # set localization length
                if self.obspack_localization and self.obspack_info[species]['site_info'][obs.code]['category'] in self.loc_L_types.keys():
                    obs.loc_L = self.loc_L_types[self.obspack_info[species]['site_info'][obs.code]['category']]
        # Do a loggin summary 
        return

    def read_sample_auxiliary(self,auxoutputfile=None,qual='prior',checkforassimilate=True):
        '''
        '''
        if not auxoutputfile:
            auxoutputfile=self.aux_file_name()
        self.add_observations(checkforassimilate=checkforassimilate)
        self.add_model_data_mismatch()
        with  io.ct_read(auxoutputfile, 'read') as ncf:
            obs=ncf.get_variable("modelsamples_%s" % qual)
            fromfile=list(ncf.get_variable("inputfilename"))
            obsnum=list(map(int,ncf.get_variable("obs_num")))
        obs_index={ obs.fromfile+str(obs.id):i for i,obs in enumerate(self.datalist)}
        sim_obs_index={ sim_file_obsnum[0]+str(sim_file_obsnum[1]): i for i,sim_file_obsnum in enumerate(zip(fromfile,obsnum)) }

        for key,index in obs_index.items():
            self.datalist[index].simulated=obs[sim_obs_index[key]]
        return
    def write_sample_auxiliary(self, auxoutputfile,qual='optimized'):
        """ 
            Write selected information contained in the Observation object to a file. 

        """

        write_info=False
        if not os.path.exists(auxoutputfile):
            # this the first time we create it
            write_info=True
            f = io.CT_CDF(auxoutputfile, method='create')
            logging.debug('Creating new auxiliary file for postprocessing (%s)' % auxoutputfile)

        else: 
            f = io.CT_CDF(auxoutputfile, method='write')
            logging.debug('Opening auxiliary file for postprocessing (%s)' % auxoutputfile)
        dimid = f.add_dim('obs', len(self.datalist))
        dim1000char = f.add_dim('string_of1000chars', 1000)
        dimcalcomp = f.add_dim('calendar_components', 6)

        if len(self.datalist) == 0:
            f.close()
            return 

        if write_info:
            # first time, add all we know
            logging.debug('writing info')
            for species in self.species:
                for key, value in list(self.obspack_info[species]['site_move'].items()):
                    msg = "Site is moved by %3.2f degrees latitude and %3.2f degrees longitude" % value 
                    f.add_attribute(key, msg)

            data = self.getvalues('id')

            savedict = io.std_savedict.copy() 
            savedict['name'] = "obs_num"
            savedict['dtype'] = "int"
            savedict['long_name'] = "Unique_Dataset_observation_index_number"
            savedict['units'] = ""
            savedict['dims'] = dimid
            savedict['values'] = data.tolist()
            savedict['comment'] = "Unique index number within this dataset ranging from 0 to UNLIMITED."
            f.add_data(savedict)

            data = [[d.year, d.month, d.day, d.hour, d.minute, d.second] for d in self.getvalues('xdate')]

            savedict = io.std_savedict.copy() 
            savedict['dtype'] = "int"
            savedict['name'] = "date_components"
            savedict['units'] = "integer components of UTC date/time"
            savedict['dims'] = dimid + dimcalcomp
            savedict['values'] = data
            savedict['missing_value'] = -9
            savedict['comment'] = "Calendar date components as integers. Times and dates are UTC." 
            savedict['order'] = "year, month, day, hour, minute, second"
            f.add_data(savedict)

            data = self.getvalues('obs')

            savedict = io.std_savedict.copy()
            savedict['name'] = "observed"
            savedict['long_name'] = "observedvalues"
            savedict['units'] = "mol mol-1"
            savedict['dims'] = dimid
            savedict['values'] = data.tolist()
            savedict['comment'] = 'Observations used in optimization'
            f.add_data(savedict)

            data = self.getvalues('mdm')

            savedict = io.std_savedict.copy()
            savedict['name'] = "modeldatamismatch"
            savedict['long_name'] = "modeldatamismatch"
            savedict['units'] = "[mol mol-1]"
            savedict['dims'] = dimid
            savedict['values'] = data.tolist()
            savedict['comment'] = 'Standard deviation of mole fractions resulting from model-data mismatch'
            f.add_data(savedict)

            data = self.getvalues('fromfile') 

            savedict = io.std_savedict.copy()
            savedict['name'] = "inputfilename"
            savedict['long_name'] = "name of file where original obs data was taken from"
            savedict['dtype'] = "char"
            savedict['dims'] = dimid + dim1000char
            savedict['values'] = data
            savedict['missing_value'] = '!'
            f.add_data(savedict)
            
            if self.sample_meteo:
                data = self.getvalues('q')

                savedict = io.std_savedict.copy()
                savedict['name'] = "q" 
                savedict['long_name'] = "Specific humidity" 
                savedict['units'] = "kg kg^-1"
                savedict['dims'] = dimid 
                savedict['values'] = data.tolist()
                f.add_data(savedict)

                data = self.getvalues('p')

                savedict = io.std_savedict.copy()
                savedict['name'] = "p" 
                savedict['long_name'] = "Air pressure" 
                savedict['units'] = "Pa"
                savedict['dims'] = dimid 
                savedict['values'] = data.tolist()
                f.add_data(savedict)

                data = self.getvalues('T')

                savedict = io.std_savedict.copy()
                savedict['name'] = "T" 
                savedict['long_name'] = "Air temperature" 
                savedict['units'] = "K"
                savedict['dims'] = dimid 
                savedict['values'] = data.tolist()
                f.add_data(savedict)

                data = self.getvalues('u')

                savedict = io.std_savedict.copy()
                savedict['name'] = "u" 
                savedict['long_name'] = "longitudinal wind component" 
                savedict['units'] = "m s^-1"
                savedict['dims'] = dimid 
                savedict['values'] = data.tolist()
                f.add_data(savedict)
                
                data = self.getvalues('v')

                savedict = io.std_savedict.copy()
                savedict['name'] = "v" 
                savedict['long_name'] = "meridional wind component" 
                savedict['units'] = "m s^-1"
                savedict['dims'] = dimid 
                savedict['values'] = data.tolist()
                f.add_data(savedict)

                data = self.getvalues('blh')

                savedict = io.std_savedict.copy()
                savedict['name'] = "blh" 
                savedict['long_name'] = "Boundary layer height" 
                savedict['units'] = "m"
                savedict['dims'] = dimid 
                savedict['values'] = data.tolist()
                f.add_data(savedict)

        data = self.getvalues('simulated') 

        try:
            dimmembers = f.add_dim('members', data.shape[1])
        except IndexError:
            dimmembers = f.add_dim('members', 1)

        savedict = io.std_savedict.copy()
        savedict['long_name'] = "modelsamples for all ensemble members"
        savedict['units'] = "mol mol-1"
        savedict['dims'] = dimid + dimmembers
        savedict['values'] = data.tolist()
        savedict['name'] = "modelsamples_%s" % qual
        savedict['comment'] = 'simulated mole fractions based on %s state vector' % qual
        f.add_data(savedict)


        f.close()

        logging.debug("Successfully wrote data to auxiliary sample output file (%s)" % auxoutputfile)

        #return outfile

    def add_simulations(self, filename, silent=False):
        """ Adds model simulated values to the mixing ratio objects """

        if not os.path.exists(filename):
            msg = "Sample output filename for observations could not be found : %s" % filename 
            logging.error(msg)
            logging.error("Did the sampling step succeed?")
            logging.error("...exiting")
            raise IOError(msg)
        # Seperate function for fetching data 
        ncf = io.ct_read(filename, method='read')
        sim_obs_num = ncf.get_variable('obs_num')
        sim_obs_species = ncf.get_variable('species')
        simulated = ncf.get_variable('flask')
        tracer_names = np.array([f.strip() for f in ncf.get_variable('tracer_names')])
        if self.sample_meteo:
            pressure = ncf.get_variable('pressure')
            humidity = ncf.get_variable('q')
            temperature = ncf.get_variable('temperature')
            wind_u= ncf.get_variable('u')
            wind_v = ncf.get_variable('v')
            blh = ncf.get_variable('blh')
        ncf.close()

        sim_obs_num = list(map(int, sim_obs_num))
        sim_obs_species = list(sim_obs_species)

        logging.info("Successfully read data from model sample file (%s)" % filename)
        # simulated = mtp.molefraction_to_permil(filename,simulated)
        # dim_ens = int(len(simulated[0,:])/2)
        # simulated = simulated[:,dim_ens:]
        obspack_species = self.getvalues('species')
        obspack_obs_num = self.getvalues('id')
        # obs_uniq_id is a tuple of (num(type=integer),species(type=string)) wich is unique if multiple obspacks are loaded. 
        # example (1,'co2')
        # obs_uniq_id = list(zip(obspack_obs_num, obspack_species))

        ####
        obs_index={ obs.species+str(obs.id):i for i,obs in enumerate(self.datalist)}
        sim_obs_index={ sim_file_obsnum[0]+str(sim_file_obsnum[1]): i for i,sim_file_obsnum in enumerate(zip(sim_obs_species,sim_obs_num)) }

        # obs_uniq_id = list(zip(obspack_obs_num, obspack_species))
        ###

        for key,index in obs_index.items():
            # when reading the observations
            # from multiple files in ctsf
            # it may be that there are missing keys
            simulated_index=sim_obs_index[key]
            # Proces the simulated stuff we need 
            if self.enkf_mode=='fwd':
                self.datalist[index].simulated = simulated[simulated_index] # take ehm all, also of species is c13.
            else:
                if obspack_species[index]== 'co2c13':
                    self.datalist[index].simulated= mtp.molefraction_to_permil(simulated[simulated_index], tracer_names, 'co2', 'co2c13')
                else:
                    self.datalist[index].simulated= simulated[simulated_index][(tracer_names ==obspack_species[index])]
                    # logging.debug("Add_simulations %d, %d, %s, %s" %(index, obs_num, mean(array(val)),obspack_species[index]))
                # logging.debug("Added simulations for %s with obs_num %s and a total of  %s tracers" %(obspack_species[index],obs_num,len(self.datalist[index].simulated)))
            if self.sample_meteo:
                self.datalist[index].u  =wind_u[simulated_index]
                self.datalist[index].v  =wind_v[simulated_index]
                self.datalist[index].blh=blh[simulated_index]
                self.datalist[index].q  =humidity[simulated_index]
                self.datalist[index].p  =pressure[simulated_index]
                self.datalist[index].T  =temperature[simulated_index]

        # start over a loop to process each simulations
        # we distinguish between inv and fwd mode
        # in case of a fwd mode, we need the species emissions
        # it is going to be species list order times the ensemble members
        # for i,obs_num__obs_species in enumerate(zip(sim_obs_num, sim_obs_species)): 
        #     obs_num, obs_species= obs_num__obs_species 
        #     if obs_num in obspack_obs_num:# and obs_specie == 'co2c13':
        #         # Handle the exception where an observation is not in the datalist but
        #         # it is in the simulated file. In this case simply skip
        #         # As we are assuming that we would like to add only those in the datalist
        #         try:
        #             index = obs_index[obs_species+str(obs_num)]
        #         except KeyError: continue
        #         # logging.debug("Found %s for %s at index %s" % (obs_num, obs_species, index ))
        #         if self.enkf_mode=='fwd':
        #             self.datalist[index].simulated = simulated[i] # take ehm all, also of species is c13.
        #         else:
        #             if obspack_species[index]== 'co2c13':
        #                 self.datalist[index].simulated= mtp.molefraction_to_permil(simulated[i], tracer_names, 'co2', 'co2c13')
        #             else:
        #                 self.datalist[index].simulated= simulated[i][(tracer_names ==obspack_species[index])]
        #             # logging.debug("Add_simulations %d, %d, %s, %s" %(index, obs_num, mean(array(val)),obspack_species[index]))
        #         # logging.debug("Added simulations for %s with obs_num %s and a total of  %s tracers" %(obspack_species[index],obs_num,len(self.datalist[index].simulated)))
        #         if self.sample_meteo:
        #             self.datalist[index].u  =wind_u[i]
        #             self.datalist[index].v  =wind_v[i]
        #             self.datalist[index].blh=blh[i]
        #             self.datalist[index].q  =humidity[i]
        #             self.datalist[index].p  =pressure[i]
        #             self.datalist[index].T  =temperature[i]
        #     else:     
        #         missing_samples.append(obs_num)
        # if not silent and missing_samples != []:
        #     logging.warning('Model samples were found that did not match any ID in the observation list. Skipping them...')
        # logging.debug("Added %d simulated values to the Data list" % (len(sim_obs_num) - len(missing_samples)))
        return
    
    def obs_to_assimilate(self,):
        """TODO: Docstring for obs_to_assimilate.
        :returns: TODO

        """
        selected=Observations()
        assimilate=(self.getvalues('flag')==0)
        selected.rejection_threshold=3 # it so far is always three
        selected.datalist=list(compress(self.datalist,assimilate))
        logging.info("Passing on %s out of %s observations for assimilation." % (len(selected.datalist),len(self.datalist)) )
        return selected

    def aux_file_name(self):
        stamp = "%s_%s" % (self.dacycle['time.start'].strftime("%Y%m%d%H"),self.dacycle['time.end'].strftime("%Y%m%d%H"))
        return os.path.join(self.dacycle['dir.output'], 'sample_auxiliary_%s.nc' % stamp )


################### End Class ObsPackObservations ###################



################### Begin Class MoleFractionSample ###################

class MoleFractionSample(object):
    """
        Holds the data that defines a mole fraction Sample in the data assimilation framework. Sor far, this includes all
        attributes listed below in the __init__ method. One can additionally make more types of data, or make new
        objects for specific projects.

    """

    def __init__(self, idx, xdate, code='XXX', obs=0.0, simulated=0.0, resid=0.0, hphr=0.0, mdm=0.0, flag=0, height=0.0, lat= -999., lon= -999.,
                 evn='0000', species='co2', samplingstrategy=1, sdev=0.0, fromfile='none.nc', loc_L=0,u=None,v=None,blh=None,q=None,p=None,T=None):
        self.code = code.strip()                 # dataset identifier, i.e., co2_lef_tower_insitu_1_99
        self.xdate = xdate                       # Date of obs
        self.obs = obs                           # Value observed
        self.simulated = simulated               # Value simulated by model
        self.resid = resid                       # Mole fraction residuals
        self.hphr = hphr                         # Mole fraction prior uncertainty from fluxes and (HPH) and model data mismatch (R)
        self.mdm = mdm                           # Model data mismatch
        self.may_localize = True                 # Whether sample may be localized in optimizer
        self.loc_L = loc_L                       # localization length for observation
        self.may_reject = True                   # Whether sample may be rejected if outside threshold
        self.flag = flag                         # Flag
        self.height = height                     # Sample height in masl
        self.lat = lat                           # Sample lat
        self.lon = lon                           # Sample lon
        self.id = idx                            # Obspack ID within distrution (integer), e.g., 82536
        self.evn = evn                           # Obspack Number within distrution (string), e.g., obspack_co2_1_PROTOTYPE_v0.9.2_2012-07-26_99_82536
        self.sdev = sdev                         # standard deviation of ensemble
        self.masl = True                         # Sample is in Meters Above Sea Level
        self.mag = not self.masl                 # Sample is in Meters Above Ground
        self.species = species.strip()           # Species string, e.g. co2 or co2c13 
        self.samplingstrategy = samplingstrategy # 
        self.fromfile = fromfile                 # netcdf filename inside ObsPack distribution, to write back later
        self.u   = u                             # Longitudinal wind component (eastward )
        self.v   = v                             # Meridional wind component (norhtward ) 
        self.blh = blh                           # boundary layer thickness 
        self.q   = p                             # Water content kg
        self.p   = p                             # air pressure 
        self.T   = T                             # air temperature 

################### End Class MoleFractionSample ###################

def fetch_station(site,species_dict,species):
    result={}
    logging.debug('%s processed by %s' % (site,mp.current_process()))
    
    result['ncfile']=os.path.join(species_dict["obspack_dir"], 'data', 'nc', site + '.nc')    
    ncf    = io.ct_read(result['ncfile'], 'read')
    # get integer date components
    idates = ncf.get_variable('time_components')
    flags       = ncf.get_variable('obs_flag')
    if species=='co2':
        try:
            result['assimilation_concerns']=np.sum( ncf.get_variable('assimilation_concerns'),axis=1)
            # assimilation_concerns =assimilation_concerns ,axis=1)
        except KeyError:
        # 2FIX AvdB: if assimilation concerns is not a variable in the nc file, fill it with 0s
            logging.warning(f"Variable 'assimilation_concerns' not found in {result['ncfile'].split('/')[-1]}, setting values to 0. Please check if this is OK!")
            result['assimilation_concerns']=np.zeros(ncf.dimensions['obs'].size)
    ncf.close()
    # compute datetime objects
    dates  = array([dtm.datetime(*d) for d in idates])
    # only take dates for a computaton on the amount of obs per day
    cripled_date=np.array([date.date() for date in dates])
    # dictionary for fast access
    n_obs_per_day={}


    for date in set(cripled_date):
        mask=(cripled_date==date)
        if species=='co2':
            mask=(result['assimilation_concerns'][mask]==0)&(flags[mask]==1)
            n_obs_per_day[date]=len(mask[mask])
        else: 
            n_obs_per_day[date]=len(cripled_date[mask])
        # logging.debug('found %s observations on %s' % (n_obs_per_day[date], date))
    result['dates']=dates
    result['n_obs_per_day']=n_obs_per_day
    return (site,result)

if __name__ == "__main__":

    from da.tools.initexit import CycleControl
    from da.ct.dasystem import CtDaSystem 

    sys.path.append('../../')

    logging.root.setLevel(logging.DEBUG)

    DaCycle = CycleControl(args={'rc':'../../ctdas-ei-gfed2-bcb-6x4-gridded-test.rc'})
    #DaCycle.Initialize()
    DaCycle.ParseTimes()

    DaSystem = CtDaSystem('../rc/carbontracker_geocarbon_gridded.rc')
    #DaSystem.Initialize()

    DaCycle.DaSystem = DaSystem

    obs = ObsPackObservations()
    obs.DaCycle = DaCycle

    while DaCycle['time.start'] < DaCycle['time.finish']:

        DaCycle['time.sample.start'] = DaCycle['time.start']
        DaCycle['time.sample.end'] = DaCycle['time.end']

        obs.Initialize()
        #obs.Validate()
        obs.add_observations()
        obs.add_model_data_mismatch()

        print(obs.getvalues('obs'))
        print(obs.getvalues('mdm'))

        DaCycle.AdvanceCycleTimes()



