"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
# obs.py

"""
Author : peters

Revision History:
File created on 28 Jul 2010.

"""
import pickle
from glob import glob
import da.tools.rc as rc
import da.tools.io4 as io
from da.observations.obs_baseclass import Observations
import os
import sys
import logging
import numpy as np
import datetime as dtm
from itertools import repeat, compress
import multiprocessing as mp
from numpy import array, ndarray, mean, logical_and, sqrt
import da.tools.molefraction_to_permil as mtp
sys.path.append(os.getcwd())
sys.path.append('../../')

identifier = 'CarbonTracker/TCCON XCO2 columns'
version = '0.0'



################### Begin Class TCCONObservations ###################


class TCCONObservations(Observations):
    """
    A class very similar to ObsPackObservations, but samples columns or profiles instead of
    molefractions at height z.


    pipeline gatherer calls first .setup() and later (if possible) .read_sample_auxiliary()
    """

    def setup(self, dacycle):
        """
        Setup de TCCON observations object.
        Beside cycle start/end time and species info, setup() adds the TCCON rcfile path to the
        object and a flag whether to co-sample basic meteorological variables.
        """

        # Cycle info
        self.startdate = dacycle['time.sample.start']
        self.enddate = dacycle['time.sample.end']

        # Species and run-mode info
        self.species = dacycle.dasystem['ct.params.species'].split(' ')
        self.enkf_mode = dacycle.dasystem['ct.enkf.mode']

        # Sample meteo variables?
        self.sample_meteo = dacycle.dasystem['tccon.meteo.sampling']
        logging.debug('meteo set to %s' % self.sample_meteo)

        # tccon_info object will hold info for the entire simulation
        self.dacycle = dacycle
        self.tccon_info = {}
        self.data_dict = {}

        self.fname_info = os.path.join(
            dacycle['dir.output'], '..', "tccon_info.pickle")
        # Check if the file exists allready
        if os.path.exists(self.fname_info):
            self.tccon_info = pickle.load(open(self.fname_info, 'rb'))
            logging.info("Loaded pickled file for TCCON")
        else:
            # Continue only for CO2 or CO tracers
            for species in self.species:
                if species in ['co2', 'co']:
                        logging.info(f"Retrieving TCCON settings for {species}")
                        # Create species dict to hold info for species
                        self.tccon_info[species] = {}
                        self.tccon_info[species]['tccon_dir'] = dacycle.dasystem['tccon.input.dir.'+species]
                        self.tccon_info[species]['tccon_stations_rc'] = dacycle.dasystem['tccon.stations.rc.'+species]
                        check_path(self.tccon_info[species]['tccon_stations_rc'])
                        check_path(self.tccon_info[species]['tccon_dir'])
                        logging.info(f"Retrieving tccon info for {species}")
                        self.setup_observations(dacycle, self.tccon_info[species], species)
                else:
                    print("ERROR. TCCON code only works for CO2 and CO tracers.")

            pickle.dump(self.tccon_info, open(self.fname_info, 'wb'), -1)
            logging.info(f"Saved TCCON info to {self.fname_info}")


    def setup_observations(self, dacycle, species_dict, species):
        """
        Performs expensive checking that can be done only once during an experiment.
        """

        # logging.info("TCCON station info to read, proceeding with %d netcdf files for %s" %( len(ncfilelist),species))

        # Read stationlist file to get stations
        tccon_stations = []
        if species_dict["tccon_stations_rc"] == 'None':
            return
        else:
            for key, value in rc.read(species_dict["tccon_stations_rc"]).items():
                tccon_stations.append(key)
            species_dict['tccon_stations'] = tccon_stations
            logging.info(f"AvdB Found {len(species_dict['tccon_stations'])} stations: {species_dict['tccon_stations']}")

        # For each station, add station specific information (lat, lon, times, ...) to species_dict
        pool = mp.Pool(processes=int(dacycle['da.resources.ntasks']))
        for result in pool.starmap(fetch_station_info, zip(species_dict['tccon_stations'], repeat(species_dict), repeat(species))):
            species_dict[result[0]]=result[1]
        pool.close()
        pool.join()

        logging.info(f"AvdB species_dict keys: {species_dict.keys()}")

        return

    def add_observations(self, **kwargs):
        """
        """
        logging.info("AvdB | Starting: add_observations")
        for species in self.species:
            # Only continue for CO2 and CO tracers!
            if species in ['co2', 'co']:
                logging.info(f"Adding {species} observations")
                species_to_int_dict = {'co2': 1, 'co': 2}
                species_int = species_to_int_dict[species]
                self.add_observations_generic(self.tccon_info[species], species, species_int, **kwargs)
        return

    def add_observations_generic(self, species_dict, species, species_int, checkforassimilate=False):
        """
        Called by add_observations()
        Here MoleFractionSample objects are created.
        """
        logging.info("AvdB | Starting: add_observations_generic")

        if species_dict["tccon_stations_rc"]=='None':
            return


        for station in species_dict['tccon_stations']:
            logging.info(f"AvdB | Processing station {station}")

            # Get absolute nc filepath
            ncf = io.ct_read(species_dict[station]['ncfile'], 'read')

            # Get data, only keep data points within the current cycle
            subselect = logical_and(species_dict[station]['dates'] >= self.startdate, species_dict[station]['dates'] <= self.enddate).nonzero()[0]
            dates = species_dict[station]['dates'].take(subselect, axis=0)
            logging.info(f"AvdB | Station {station} has {len(dates)} observations in current cycle")

            # 1d variables (time)
            fromfile             = species_dict[station]['ncfile'].split('/')[-1]
            obs_idx              = np.arange(len(self.datalist), len(self.datalist) + len(dates))
            latitude             = ncf.get_variable('lat').take(subselect, axis=0)
            longitude            = ncf.get_variable('long').take(subselect, axis=0)
            station_altitude     = ncf.get_variable('zobs').take(subselect, axis=0)
            xobs                 = ncf.get_variable(f'x{species}').take(subselect, axis=0)
            xobs_err             = ncf.get_variable(f'x{species}_error').take(subselect, axis=0)
            dim_ak_alt           = ncf.get_variable('ak_altitude')
            dim_prior_alt        = ncf.get_variable('prior_altitude')
            prior_xspecies       = ncf.get_variable(f'prior_x{species}').take(subselect, axis=0)
            # 2d variables (time, altitude)
            prior_profile_species= ncf.get_variable(f'prior_{species}').take(subselect, axis=0)
            prior_pressure       = ncf.get_variable('prior_pressure').take(subselect, axis=0)
            ak_xspecies          = ncf.get_variable(f'ak_x{species}').take(subselect, axis=0)
            integration_operator = ncf.get_variable('integration_operator').take(subselect, axis=0)

            ## Check: all dim_ak_alt and dim_prior_alt?
            if (dim_ak_alt == dim_prior_alt).all():
                dim_alt = dim_ak_alt
            else:
                logging.error("AvdB | Altitude levels [km] of TCCON prior and TCCON AK are not equal!")

            ## Unit conversions:
            # Convert from atm to pa
            prior_pressure = prior_pressure * 101325

            # Close netcdf
            ncf.close()

            for n, date in enumerate(dates):
                self.datalist.append(TotalColumnSample(species, species_int, station_id=station[0:2], idx=obs_idx[n], xdate=date,
                                                       lat=latitude[n], lon=longitude[n],
                                                       height=station_altitude[n], xobs=xobs[n], xobs_err=xobs_err[n], dim_alt=dim_alt[:], prior_xspecies=prior_xspecies[n],
                                                       prior_profile_species=prior_profile_species[n,:], prior_pressure=prior_pressure[n,:], ak_xspecies=ak_xspecies[n,:],
                                                       integration_operator=integration_operator[n,:], fromfile=fromfile))


        logging.info(f"Adding TCCON observations for {species} to the observation list. It now holds {len(self.datalist)} values")
        return

    def getvalues_species(self, name, constructor=array, data=None):
        """
        """
        logging.info("AvdB | Starting: getvalues_species")

    def get_samples_type(self):
        """
        """
        logging.info("AvdB | Starting: get_samples_type")
        return 'tccon'

    def add_model_data_mismatch(self, filename=None):
        """
        EMPTY FOR NOW
        """
        logging.info("AvdB | Starting: add_model_data_mismatch - SKIPPING FOR NOW...")


    def write_sample_coords(self, obsinputfile):
        """
        Write sampling coordinate information to a file, required by the obsoperator. Return
        filename for later use.
        """
        logging.info("AvdB | Starting: write_sample_coords")

        f = io.CT_CDF(obsinputfile, method='create')
        logging.debug('Creating new observations file for ObservationOperator (%s)' % obsinputfile)

        dimid = f.add_dim('obs', len(self.datalist))
        dimalt = f.add_dim('lev', len(self.getvalues('dim_alt')[0,:]))
        dim6char = f.add_dim('string_of6chars', 6)
        dimcalcomp = f.add_dim('calendar_components', 6)

        if len(self.datalist) == 0:
            f.close()
            return

        data = self.getvalues('id')
        savedict = io.std_savedict.copy()
        savedict['name'] = "obs_num"
        savedict['dtype'] = "int"
        savedict['long_name'] = "Unique_Dataset_observation_index_number"
        savedict['units'] = ""
        savedict['dims'] = dimid
        savedict['values'] = data.tolist()
        savedict['comment'] = "Unique index number within this dataset ranging from 0 to UNLIMITED."
        f.add_data(savedict)

        data = self.getvalues('dim_alt')[0] # same values stored for each sample, so take only the first
        savedict = io.std_savedict.copy()
        savedict['name'] = "altitude"
        savedict['long_name'] = "tccon altitude profile"
        savedict['units'] = "km"
        savedict['dims'] = dimalt
        savedict['values'] = data.tolist()
        savedict['comment'] = "TCCON altitude levels, these are the same for all priors and ak's"
        f.add_data(savedict)

        data = [[d.year, d.month, d.day, d.hour, d.minute, d.second] for d in self.getvalues('xdate') ]
        savedict = io.std_savedict.copy()
        savedict['name'] = "date_components"
        savedict['units'] = "integer components of UTC date/time"
        savedict['dims'] = dimid + dimcalcomp
        savedict['values'] = data
        savedict['missing_value'] = -9
        savedict['comment'] = "Calendar date components as integers. Times and dates are UTC."
        savedict['order'] = "year, month, day, hour, minute, second"
        f.add_data(savedict)

        data = self.getvalues('lat')
        savedict = io.std_savedict.copy()
        savedict['name'] = "latitude"
        savedict['units'] = "degrees north"
        savedict['dims'] = dimid
        savedict['values'] = data.tolist()
        savedict['missing_value'] = -999.9
        f.add_data(savedict)

        data = self.getvalues('lon')
        savedict = io.std_savedict.copy()
        savedict['name'] = "longitude"
        savedict['units'] = "degrees east"
        savedict['dims'] = dimid
        savedict['values'] = data.tolist()
        savedict['missing_value'] = -999.9
        f.add_data(savedict)

        data = self.getvalues('species')
        savedict = io.std_savedict.copy()
        savedict['dtype'] = "char"
        savedict['name'] = "species"
        savedict['units'] = "-"
        savedict['dims'] = dimid + dim6char
        savedict['values'] = data
        savedict['missing_value'] = ""
        f.add_data(savedict)

        # AvdB: TEMPORARY - added to dodge .nc char var reading issue (compiler-specific).
        data = self.getvalues('species_int')
        savedict = io.std_savedict.copy()
        savedict['dtype'] = "int"
        savedict['name'] = "species_int"
        savedict['units'] = "-"
        savedict['dims'] = dimid
        savedict['values'] = data
        savedict['missing_value'] = ""
        f.add_data(savedict)

        data = self.getvalues('station_id')
        savedict = io.std_savedict.copy()
        savedict['dtype'] = "char"
        savedict['name'] = "station_id"
        savedict['units'] = "-"
        savedict['dims'] = dimid + dim6char
        savedict['values'] = data
        savedict['missing_value'] = ""
        f.add_data(savedict)

        data = self.getvalues(f'prior_xspecies')
        savedict = io.std_savedict.copy()
        savedict['name'] = "prior_xspecies"
        savedict['long_name'] = f"column average dry atmosphere mole fraction of gas"
        savedict['units'] = "co2 in ppm, co in ppb"
        savedict['dims'] = dimid
        savedict['values'] = data.tolist()
        savedict['comment'] = f"0.2095*Xgas/column_o2"
        f.add_data(savedict)

        data = self.getvalues(f'ak_xspecies')
        savedict = io.std_savedict.copy()
        savedict['name'] = "ak_xspecies"
        savedict['long_name'] = f"species column averaging kernel"
        savedict['units'] = "1"
        savedict['dims'] = dimid + dimalt
        savedict['values'] = data.tolist()
        f.add_data(savedict)

        data = self.getvalues(f'prior_profile_species')
        savedict = io.std_savedict.copy()
        savedict['name'] = "prior_profile_species"
        savedict['long_name'] = f"wet atmosphere molefraction of gas"
        savedict['units'] = "co2 in ppm, co in ppb"
        savedict['dims'] = dimid + dimalt
        savedict['values'] = data.tolist()
        savedict['comment'] = f"a priori profile of species"
        savedict['note'] = "Prior VMRs are given in wet mole fractions. To convert to dry mole fractions, you must calculate H2O_dry = H2O_wet/(1 - H2O_wet) and then gas_dry = gas_wet * (1 + H2O_dry), where H2O_wet is the prior_h2o variable. (Be sure to convert the H2O and gas priors to compatible units.)"
        f.add_data(savedict)

        data = self.getvalues(f'integration_operator')
        savedict = io.std_savedict.copy()
        savedict['name'] = "integration_operator"
        savedict['long_name'] = f"integration operator"
        savedict['units'] = "1"
        savedict['dims'] = dimid + dimalt
        savedict['values'] = data.tolist()
        savedict['comment'] = "A vector that, when the dot product is taken with a wet mole fraction profile, applies the TCCON column-average integration. This does NOT include the averaging kernel, those must be applied in addition to this vector. The relates_to attribute indicates which Xgas variable to use this operator for when trying to compare against."
        f.add_data(savedict)

        data = self.getvalues(f'prior_pressure')
        savedict = io.std_savedict.copy()
        savedict['name'] = "prior_pressure"
        savedict['long_name'] = f"prior pressure profile"
        savedict['units'] = "Pa"
        savedict['dims'] = dimid + dimalt
        savedict['values'] = data.tolist()
        f.add_data(savedict)

        f.close()

        logging.debug("Successfully wrote data to obs file")
        logging.info(f"Sample input file for obs operator now in place [{obsinputfile}]")

    def add_simulations(self, filename, silent=False):
        """ Adds observed and model simulated_xspecies column values to the mole fraction objects
            This function includes the add_observations and add_model_data_mismatch functionality for the sake of computational efficiency

        """
        logging.info("AvdB | Starting: add_simulations")

        if not os.path.exists(filename):
            msg = f"Sample output filename for observations could not be found : {filename}"
            logging.error(msg)
            logging.error("Did the sampling step succeed?")
            logging.error("...exiting")
            raise IOError(msg)

        ncf = io.ct_read(filename, method='read')
        ids = ncf.get_variable('obs_num')
        mapmat = ncf.get_variable('mapmat')
        simulated_pressure_profile = ncf.get_variable('tm5_pressure_profile')
        simulated_profile_h2o = ncf.get_variable('tm5_profile_h2o')
        simulated_wet_profile_species = ncf.get_variable('tm5_wet_profile_species')
        # Convert to ppm
        simulated_xspecies = ncf.get_variable('tm5_xspecies')
        ncf.close()
        logging.info("Successfully read data from model sample file (%s)" % filename)

        obs_ids = self.getvalues('id').tolist()
        ids = list(map(int, ids))

        missing_samples = []

        # Add , simulated_pressure_profile, simulated_profile_h2o, simulated_wet_profile_species to the code below
        for idx, v1, v2, v3, v4, v5 in zip(ids, mapmat, simulated_pressure_profile, simulated_profile_h2o, simulated_wet_profile_species, simulated_xspecies):
            if idx in obs_ids:
                index = obs_ids.index(idx)
                self.datalist[index].mapmat = v1
                self.datalist[index].simulated_pressure_profile = v2
                self.datalist[index].simulated_profile_h2o = v3
                self.datalist[index].simulated_wet_profile_species = v4
                self.datalist[index].simulated_xspecies = v5
            else:
                missing_samples.append(idx)

        if not silent and missing_samples != []:
            logging.warning('Model samples were found that did not match any ID in the observation list. Skipping them...')
            #msg = '%s'%missing_samples ; logging.warning(msg)

        logging.debug("Added %d simulated_xspecies values to the Data list" % (len(ids) - len(missing_samples)))

    def write_sample_auxiliary(self, auxoutputfile, qual=None):
        """
        Write selected information contained in the Observations object to a file.
        """

        f = io.CT_CDF(auxoutputfile, method='create')
        logging.debug(f'Creating new auxiliary sample output file for postprocessing ({auxoutputfile})')

        dimid = f.add_dim('obs', len(self.datalist))
        dimalt = f.add_dim('lev', len(self.getvalues('dim_alt')[0,:]))
        dimalttm5 = f.add_dim('levtm5', len(self.getvalues('simulated_pressure_profile')[0,:]))
        dim6char = f.add_dim('string_of6chars', 6)
        dim1000char = f.add_dim('string_of1000chars', 1000)
        dimcalcomp = f.add_dim('calendar_components', 6)

        if self.enkf_mode == 'fwd':
            dimmembers = f.add_dim('members', 1)
        else:
            dimmembers = f.add_dim('members', self.getvalues('simulated_xspecies').shape[1])

        if len(self.datalist) == 0:
            f.close()
            #return outfile

        # for key, value in list(self.site_move.items()):
        #     msg = "Site is moved by %3.2f degrees latitude and %3.2f degrees longitude" % value
        #     f.add_attribute(key, msg)

        savedict = io.std_savedict.copy()
        savedict['name'] = "obs_num"
        savedict['dtype'] = "int"
        savedict['long_name'] = "Unique_Dataset_observation_index_number"
        savedict['units'] = ""
        savedict['dims'] = dimid
        savedict['values'] = self.getvalues('id').tolist()
        savedict['comment'] = "Unique index number within this dataset ranging from 0 to UNLIMITED."
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name'] = "altitude"
        savedict['long_name'] = "TCCON altitude profile"
        savedict['units'] = "km"
        savedict['dims'] = dimalt
        savedict['values'] = self.getvalues('dim_alt')[0].tolist() # same values stored for each sample, so take only the first
        savedict['comment'] = "TCCON altitude levels, these are the same for all priors and ak's"
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['dtype'] = "int"
        savedict['name'] = "date_components"
        savedict['units'] = "integer components of UTC date/time"
        savedict['dims'] = dimid + dimcalcomp
        savedict['values'] = [[d.year, d.month, d.day, d.hour, d.minute, d.second] for d in self.getvalues('xdate')]
        savedict['missing_value'] = -9
        savedict['comment'] = "Calendar date components as integers. Times and dates are UTC."
        savedict['order'] = "year, month, day, hour, minute, second"
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['dtype'] = "char"
        savedict['name'] = "species"
        savedict['units'] = "-"
        savedict['dims'] = dimid + dim6char
        savedict['values'] = self.getvalues('species')
        savedict['missing_value'] = ""
        f.add_data(savedict)

##########
        savedict = io.std_savedict.copy()
        savedict['name'] = "ak_xspecies"
        savedict['long_name'] = f"species column averaging kernel"
        savedict['units'] = "1"
        savedict['dims'] = dimid + dimalt
        savedict['values'] = self.getvalues(f'ak_xspecies').tolist()
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name'] = "prior_profile_species"
        savedict['long_name'] = f"wet atmosphere molefraction of gas"
        savedict['units'] = "co2 in ppm, co in ppb"
        savedict['dims'] = dimid + dimalt
        savedict['values'] = self.getvalues(f'prior_profile_species').tolist()
        savedict['comment'] = f"a priori profile of species"
        savedict['note'] = "Prior VMRs are given in wet mole fractions. To convert to dry mole fractions, you must calculate H2O_dry = H2O_wet/(1 - H2O_wet) and then gas_dry = gas_wet * (1 + H2O_dry), where H2O_wet is the prior_h2o variable. (Be sure to convert the H2O and gas priors to compatible units.)"
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name'] = "integration_operator"
        savedict['long_name'] = f"integration operator"
        savedict['units'] = "1"
        savedict['dims'] = dimid + dimalt
        savedict['values'] = self.getvalues(f'integration_operator').tolist()
        savedict['comment'] = "A vector that, when the dot product is taken with a wet mole fraction profile, applies the TCCON column-average integration. This does NOT include the averaging kernel, those must be applied in addition to this vector. The relates_to attribute indicates which Xgas variable to use this operator for when trying to compare against."
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name'] = "prior_pressure"
        savedict['long_name'] = f"prior pressure profile"
        savedict['units'] = "Pa"
        savedict['dims'] = dimid + dimalt
        savedict['values'] = self.getvalues(f'prior_pressure').tolist()
        f.add_data(savedict)
##########

        savedict = io.std_savedict.copy()
        savedict['name'] = "prior_xspecies"
        savedict['long_name'] = f"TCCON prior column average dry atmosphere mole fraction of gas"
        savedict['units'] = "co2 in ppm, co in ppb"
        savedict['dims'] = dimid
        savedict['values'] = self.getvalues(f'prior_xspecies').tolist()
        savedict['comment'] = f"0.2095*Xgas/column_o2"
        f.add_data(savedict)

        # savedict = io.std_savedict.copy()
        # savedict['name'] = "modeldatamismatch"
        # savedict['long_name'] = "modeldatamismatch"
        # savedict['units'] = "[mol mol-1]"
        # savedict['dims'] = dimid
        # savedict['values'] = self.getvalues('mdm').tolist()
        # savedict['comment'] = 'Standard deviation of mole fractions resulting from model-data mismatch'
        # f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name'] = "mapmat"
        savedict['long_name'] = "Mapping matix between TM5 and TCCON levels based on pressure"
        savedict['units'] = "-"
        savedict['dims'] = dimid + dimalttm5 + dimalt
        savedict['values'] = self.getvalues(f'mapmat').tolist()
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name'] = "simulated_pressure_profile"
        savedict['long_name'] = "Simulated pressure profile"
        savedict['units'] = "Pa"
        savedict['dims'] = dimid + dimalttm5
        savedict['values'] = self.getvalues(f'simulated_pressure_profile').tolist()
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name'] = "simulated_profile_h2o"
        savedict['long_name'] = "Simulated humidity profile"
        savedict['units'] = "kg/kg"
        savedict['dims'] = dimid + dimalt
        savedict['values'] = self.getvalues(f'simulated_profile_h2o').tolist()
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name'] = "simulated_wet_profile_species"
        savedict['long_name'] = f"Simulated (wet) atmosphere mole fractions profile of gas based on {qual}"
        savedict['units'] = "co2 in ppm, co in ppb"
        savedict['dims'] = dimid + dimalt + dimmembers
        savedict['values'] = self.getvalues(f'simulated_wet_profile_species')[:, :, None]
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name'] = f"simulated_xspecies_{qual}"
        savedict['long_name'] = f"Simulated column average dry atmosphere mole fraction of gas based on {qual}"
        savedict['units'] = "co2 in ppm, co in ppb"
        savedict['dims'] = dimid + dimmembers
        savedict['values'] = self.getvalues(f'simulated_xspecies').tolist()
        f.add_data(savedict)

        savedict = io.std_savedict.copy()
        savedict['name'] = "inputfilename"
        savedict['long_name'] = "name of file where original obs data was taken from"
        savedict['dtype'] = "char"
        savedict['dims'] = dimid + dim1000char
        savedict['values'] = self.getvalues('fromfile')
        savedict['missing_value'] = '!'
        f.add_data(savedict)

        f.close()

        logging.debug(f"Successfully wrote data to auxiliary sample output file ({auxoutputfile})")


    def read_sample_auxiliary(self, auxoutputfile=None, qual='prior', checkforassimilate=True):
        """
        """
        logging.info("AvdB | Starting: read_sample_auxiliary")

    def obs_to_assimilate(self,):
        """
        """
        logging.info("AvdB | Starting: obs_to_assimilate")

    def aux_file_name(self):
        """
        """
        logging.info("AvdB | Starting: aux_file_name")

################### End Class ObsPackObservations #####################
################### Begin Class MoleFractionSample ####################


class TotalColumnSample:
    """
    Class for TCCON XCO2 and XCO observations.
    """

    def __init__(self, species, species_int, station_id, idx, xdate, xobs, xobs_err, lat, lon, height, dim_alt, prior_xspecies, prior_profile_species, prior_pressure, ak_xspecies, integration_operator, fromfile):

        self.species              = species
        self.species_int          = species_int
        self.station_id           = station_id
        self.id                   = idx
        self.xdate                = xdate
        self.xobs                 = xobs
        self.xobs_err             = xobs_err
        self.lat                  = lat
        self.lon                  = lon
        self.height               = height
        self.dim_alt              = dim_alt
        self.prior_pressure       = prior_pressure
        self.prior_xspecies       = prior_xspecies
        self.prior_profile_species= prior_profile_species
        self.ak_xspecies          = ak_xspecies
        self.integration_operator = integration_operator
        self.fromfile             = fromfile

################### End Class MoleFractionSample ######################

################### Other Functions ###################################


def check_path(path):
    """
    """
    logging.info("AvdB | Starting: check_path")
    if not path=='None':
        if not os.path.exists(path):
            msg = 'Could not find required input file (%s) ' % path
            logging.error(msg)
            raise IOError(msg)
    return

def fetch_station_info(station, species_dict, species):
    """
    For a specific station, fetch the station specific information (n_obs, time, lat, lon, height)
    from (original) netcdf and return it as a dictionary.
    """

    # Throw info/debug messages
    # logging.info("AvdB | Starting: fetch_station_info")
    # logging.debug(f"{station} processed by {mp.current_process()}")

    # Create result dictionary
    result = {}

    # Get netcdf file path of the station
    result['ncfile'] = glob(f"{species_dict['tccon_dir']}/{station}*.nc")[0].replace('//','/')

    # Open, read (float dates, lat, lon), close netcdf.
    ncf = io.ct_read(result['ncfile'], 'read')
    fdates = ncf.get_variable('time')
    ncf.close()

    # Compute datetime objects and get dates
    all_dates = np.array([dtm.datetime.fromtimestamp(d) for d in fdates])
    # date_dates = np.array([date.date() for date in all_dates])

    # # Calculate number of observations per day
    # n_obs_per_day = {}
    # for date in set(date_dates):
    #     mask = (date_dates == date)
    #     n_obs_per_day[date] = len(mask[mask])
    #     # logging.debug(f"found {n_obs_per_day[date]} observations on {date}")

    # Add to result dictionary
    result['dates'] = all_dates
    # result['n_obs_per_day'] = n_obs_per_day

    return (station, result)

#######################################################################
