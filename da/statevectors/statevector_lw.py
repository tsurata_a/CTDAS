"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
import os
import sys
sys.path.append(os.getcwd())
import logging
import numpy as np
import datetime as dt
from dateutil.relativedelta import relativedelta
import da.tools.io4 as io
from da.tools.general import to_datetime,datevector
from da.statevectors.statevector_baseclass import StateVector, EnsembleMember
from netCDF4 import Dataset
from functools import partial
from da.tools.covariance import cov_gaussian  
from da.tools.StateParameter import StateParameter 
from da.ctdata import fluxload, fluxscaling
identifier = 'CarbonTracker Statistical fit'
version = '0.0'

################### Start Class CTSFStateVector ###################
class ctsfStateVector(StateVector):

    """
    This is a StateVector object for CarbonTracker. It is created for use with the SIF inversion approach,
    and contains statistical parameters and SIF sensitivity parameters per ecoregion to be assembled.
    It has a private method to make new ensemble members.
    """

    def setup(self, dacycle):
        """
        setup the object by specifying the dimensions.
        There are two major requirements for each statvector that you want to build:
            (1) is that the statevector can map itself onto a regular grid
            (2) is that the statevector can map itself (mean+covariance) onto TransCom regions
        An example is given below.
        """

        self.dacycle = dacycle

        logging.info('Calculating fluxes from %s to %s' % (self.dacycle['time.begin'],self.dacycle['time.finish']))

        self.isOptimized=False

        # Temporal settings
        self.nlag       = int(dacycle['time.nlag'])

        # calc_datevec to timetools?
        self.datevec    = np.array(datevector(self.dacycle['time.begin'],self.dacycle['time.finish'],'day'))

        # Spatial settings
        self.nlat = 180
        self.nlon = 360

        # Inversion setup
        self.nmembers   = int(dacycle['da.optimizer.nmembers'])                 # number of ensemble members
        logging.debug(self.nmembers) 

        self.nobs              = 0
        self.obs_to_assimilate = ()                                             # empty containter to hold observations to assimilate later on

        # These list objects hold the data for each time step of lag in the system. Note that the ensembles for each time step consist
        # of lists of EnsembleMember objects, we define member 0 as the mean of the distribution and n=1,...,nmembers as the spread.

        # This specifies the file to read with the gridded mask at 1x1 degrees. Each gridbox holds a number that specifies the ecoregion it belongs to.
        # From this map, a dictionary is created that allows a reverse look-up so that we can map parameters to a grid.
        # logging.debug('There are %s non-empty ecoregions: %s' %(self.nregions, self.regionnumbers))
        # logging.debug('A dictionary to map grids to states and vice versa was created')
        # logging.debug('Average latitude per (nonempty) ecoregion was calculated.')
        # logging.info("Updating with dacycle.dasystem['nparameters']=%d to be used by the optimizer" %self.nparams )
        self.parameters_init = { "co2_gpp_r"      : {    "variance"         : 0.02,
                                                     "cov_t_length"       : 6,
                                                     "cyclic"             : False,
                                                     "water"              : False,
                                                     "freq"               : 'daily',
                                                     "nmembers"           : self.nmembers,
                                                     "mean"               : 1.0,
                                                     "fluxname"           : 'sib4247gppiav',
                                                     "regions"            : 'regions',
                                                     "startdate"          : dacycle['time.begin'],
                                                     "enddate"            : dacycle['time.finish'],
                                                     "within_tcregions"   : [3,4,5,6,9,10]},# SH region, default length scale of 1000 

                                 "co2_reco_c"      : {  "variance"          : 0.001,
                                                     "cov_t_length"       : 50,
                                                     "cyclic"             : True,
                                                     "water"              : False,
                                                     "freq"               : 'daily',
                                                     "nmembers"           : self.nmembers,
                                                     "mean"               : 1.0,
                                                     "fluxname"           : 'sib4247terclim',
                                                     "regions"            : 'regions',
                                                     "startdate"          : dacycle['time.begin'],
                                                     "enddate"            : dacycle['time.finish'],
                                                     "within_tcregions"   : [3,4,5,6,9,10]},# SH region

                                 "co2_ocn"      : {"variance"             : 0.2,
                                                     "cov_t_length"       : 6,
                                                     "cyclic"             : False,
                                                     "water"              : True,
                                                     "freq"               : 'daily',
                                                     "nmembers"           : self.nmembers,
                                                     "mean"               : 1.0,
                                                     "fluxname"           : 'carboscopev2022',
                                                     "regions"            : 'regions',
                                                     "startdate"          : dacycle['time.begin'],
                                                     "enddate"            : dacycle['time.finish']}
                                                     # "length_scales"      : [2000] }


                                 # "terdis"      : {   "variance"           : 0.2,
                                 #                     "cov_t_length"       : 0.5,
                                 #                     "cyclic"             : False,
                                 #                     "water"              : False,
                                 #                     "freq"               : 'year',
                                 #                     "nmembers"           : self.nmembers,
                                 #                     "mean"               : 1.0,
                                 #                     "fluxname"           : 'sib4biodis',
                                 #                     "regions"            : 'transcom_regions',
                                 #                     "startdate"        : dacycle['time.begin'],
                                 #                     "enddate"          : dacycle['time.finish']},

                                 # "ocedis"      : {   "variance"           : 0.2,
                                 #                     "cov_t_length"       : 0.5,
                                 #                     "cyclic"             : False,
                                 #                     "water"              : True,
                                 #                     "freq"               : 'year',
                                 #                     "nmembers"           : self.nmembers,
                                 #                     "mean"               : 1.0,
                                 #                     "fluxname"           : 'sib4ocndis',
                                 #                     "regions"            : 'transcom_regions',
                                 #                     "startdate"           : dacycle['time.begin'],
                                 #                     "enddate"           : dacycle['time.finish']}
                                 }

        # logging.debug("%s" % self.nee_U)
        self.parameters={}
        for key,args in self.parameters_init.items():
            self.parameters[key]=StateParameter(**args)
            logging.debug("setup a stateparameter '%s'" % (key))
        self.dacycle.dasystem['statevector.names']=' '.join(self.parameters.keys())

        # by default select all
        self._select()
        return

    def write_to_file(self,file,qual):
        """ do whatever super does
        then also add nee dter and dgpp sets
        """
        if not len(self.loaded_keys)==len(self.parameters_init.keys()):
            #by default selects all when writing the statevector to file
            self._select()
            self._stateparameter2members()
        # file, dim of nparams ,nmembers, dimlag
        f,dimparams, dimmembers, dimlag ,filename=super().write_to_file(file,qual,return_file=True)
        f.close()
        logging.info('successfully wrote the state Vector to file (%s) ' % filename)
        return
    
    def read_from_file(self, filename,lag, qual='opt'):
        """ read initial data for the statevector from given file 
        lag: integer, currently only needed for backwards compatibility with pipelines 
        qual: string quality of the state: 'opt' or 'prior'
        """
        if not len(self.loaded_keys)==len(self.parameters_init.keys()):
            #by default selects all when reading the statevector from file
            self._select()
        lag=lag-1
        super().read_from_file(filename,qual) 
        logging.info('successfully read the statevector from file (%s) ' % filename)
        self._members2stateparameter()
        self._setdomaininfo()
        logging.info('successfully assigned members back to state ')
        return

    def _members2stateparameter(self,lag=0):
        for key in self.loaded_keys:
            self.parameters[key].members_to_state(self.ensemble_members[lag],*self.StateParameter_indices[key])
            logging.info("Returning statevector elements for %s to parameter object" % key)
        return

    def _stateparameter2members(self,lag=0):
        for key in self.loaded_keys:
            self.parameters[key].state_to_members(self.ensemble_members[lag],*self.StateParameter_indices[key])
            logging.info("Inserting statevector elements for %s into the statevector" % key)
        return

    def _setdomaininfo(self):
        for key in self.loaded_keys:
            self.parameters[key].domain_info(self.domain_info,*self.StateParameter_indices[key])
            logging.info("Inserting statevector elements for %s into the statevector" % key)
        return

    def state_to_grid(self, fluxvector=None, lag=1,monthcounter=None,filename=None,qual=None):
        """
            transforms the statevector information (mean + covariance) to a 1x1 degree grid.


            :param: fluxvector: a vector of length (nparams,) that holds the fluxes associated with each parameter in the StateVector
            :param: lag: the lag at which to evaluate the StateVector
            :rtype: a tuple of two arrays (gridmean,gridvariance) with dimensions (nfitparams,180,360)
            counter will fetch the fluxvalues for month=counter

            if the attribute `fluxvector` is not passed, the function will return the mean parameter value and its variance on a 1x1 map.

            ..note:: although we can return the variance information for each gridbox, the covariance information contained in the original ensemble is lost when mapping to 1x1 degree!
        """

        self.read_from_file(filename,lag,qual)

        currentdate=self.datevec[0]+relativedelta(months=monthcounter)
        mask=[(currentdate.month==t.month)&(currentdate.year==t.year) for t in self.datevec]
        
        dates= self.datevec[mask]
        results ={}
        
        for k,pobject in self.parameters.items():
            logging.debug('using %s to calculated %s %s' % (k,qual,pobject.fluxname))
            results.update(pobject.prior2post(dates,qual))
        # Get the mean flux:
        results_mean = { key.replace('_ensemble','') : results[key][0] for key in results.keys() if '_ensemble' in key  }
        return results | results_mean

    def write_members_to_file(self, lag, outdir, endswith='.nc', member=None,obsoperator=None):
        """
           :param: lag: which lag step of the filter to write, must lie in range [1,...,nlag]
           :param: outdir: directory where to write files
           :param: endswith: optional label to add to the filename, default is simply .nc
           :rtype: none

           write ensemble member information to a NetCDF file for later use. The standard output filename is
           *parameters.ddd.nc* where *ddd* is the number of the ensemble member. Standard output file location
           is the `dir.input` of the dacycle object. In principle the output file will have only two datasets inside
           called `parametervalues` which is of dimensions `nparameters` and `parametermap` which is of dimensions (180,360).
           this dataset can be read and used by a :class:`~da.baseclasses.observationoperator.ObservationOperator` object.

           .. note:: if more, or other information is needed to complete the sampling of the ObservationOperator you
                     can simply inherit from the StateVector baseclass and overwrite this write_members_to_file function.

        """

        # these import statements caused a crash in netCDF4 on MacOSX. No problems on Jet though. Solution was
        # to do the import already at the start of the module, not just in this method.

        lag=lag-1
        if member is None:
            members = self.ensemble_members[lag]
        else:
            members = [self.ensemble_members[lag][member]]

        logging.debug('write_members_to_file: dir = %s' %outdir)

        # make parallel?
        mask=[(self.dacycle['time.start']<=dt)&(dt<self.dacycle['time.end']) for dt in self.datevec]
        # logging.debug('in this cycle we will do %s' % self.datevec[mask])
        for member in members:

            filename     = os.path.join(outdir, 'parameters.%03d%s' % (member.membernumber, endswith))
            ncf          = io.CT_CDF(filename, method='create')
            dimt         = ncf.add_dim('time',len(self.datevec[mask]))
            dimgrid      = ncf.add_latlon_dim()

            idx=member.membernumber
            for k,pobject in self.parameters.items():
                grid_vals=pobject.vector_to_4d(idx,self.datevec[mask])
                savedict              = io.std_savedict.copy()
                savedict['name']      = k 
                savedict['long_name'] = ('Statevector gridded elements for flux %s NEE values on lat-lon grid for ensemble member %s' % (k, idx))
                savedict['dims']      = dimt + dimgrid
                savedict['values']    = grid_vals.tolist()
                savedict['units']     = 'unitless'
                ncf.add_data(savedict)

            ncf.close()

            logging.debug('successfully wrote data from ensemble member %d to file (%s) ' % (member.membernumber, filename))
        # exit()
        return


    def get_covariance(self,date,dacycle):
        """
        make a new ensemble from specified matrices, the attribute lag refers to the position in the state vector.
            note that lag=1 means an index of 0 in python, hence the notation lag-1 in the indexing below.
            the argument is thus referring to the lagged state vector as [1,2,3,4,5,..., nlag]
        """
        for k,pobject in self.parameters.items():
            pobject.compute_cov()
    
        return

    def ensemble_init(self, lag=0,cov=None):
        """
        :param lag: an integer indicating the time step in the lag order
        :param covariancematrix: a list of matrices specifying the covariance distribution to draw from
        :rtype: none

        make a new ensemble, the attribute lag refers to the position in the state vector.
        note that lag=1 means an index of 0 in python, hence the notation lag-1 in the indexing below.
        the argument is thus referring to the lagged state vector as [1,2,3,4,5,..., nlag]

        the covariance list object to be passed holds a list of matrices with a total number of dimensions [nparams, nparams], which is
        used to draw ensemblemembers from. each draw is done on a matrix from the list, to make the computational burden smaller when
        the statevector nparams becomes very large.

        """
        self.ensemble_members = list([[]]*self.nlag)
        for member in range(self.nmembers):
            new_member = EnsembleMember(member)
            # dev_matrix is initialized at zero; we add all zeros to the first member
            new_member.param_values=np.zeros((self.nparams),float,order='F')
            # logging.info("%s" % self.nparams)
            self.ensemble_members[lag].append(new_member)
        return

    def make_new_ensemble(self, lag,cov):
        """
        :param lag: an integer indicating the time step in the lag order
        :param covariancematrix: a list of matrices specifying the covariance distribution to draw from
        :rtype: none

        make a new ensemble, the attribute lag refers to the position in the state vector.
        note that lag=1 means an index of 0 in python, hence the notation lag-1 in the indexing below.
        the argument is thus referring to the lagged state vector as [1,2,3,4,5,..., nlag]

        the covariance list object to be passed holds a list of matrices with a total number of dimensions [nparams, nparams], which is
        used to draw ensemblemembers from. each draw is done on a matrix from the list, to make the computational burden smaller when
        the statevector nparams becomes very large.

        """
        # correct index
        lag=lag-1
        # self.ensemble_init(lag=lag)
        dof=0
        for k,pobject in self.parameters.items():
            pobject.make_new_ensemble()
            pobject.create_mean_statevector()
            dof+=pobject.dof
        
        # at this time the statevector doesn't contain elements. Fill it up with the default loaded_keys():
        self._stateparameter2members(lag)
        # and set the domain_info
        self._setdomaininfo()

        logging.info('%d new ensemble members were added to the state vector # %d' % (self.nmembers, (lag + 1)))
        logging.info('statevector contains %s elements with %s degrees of freedom' % (self.nparams,dof))
        return

    def propagate(self, dacycle):
        """
        :rtype: None

        propagate the parameter values in the stateVector to the next cycle. This means a shift by one cycle
        step for all states that will
        be optimized once more, and the creation of a new ensemble for the time step that just
        comes in for the first time (step=nlag).
        in the future, this routine can incorporate a formal propagation of the statevector.

        """
        # in lw not really progpagation
        return
    def select_parameters(self,keys):
        """
        User method to select state parameters into the statevector. Will reshape the vector.
        """
        self._select(keys)
        self._stateparameter2members()
        self._setdomaininfo()
        return

    def _select(self,keys=None):
        # Select from different parameter objects the different elements listed in keys
        if keys==None or keys=='all':
            keys=list(self.parameters_init.keys())
        self.loaded_keys=keys
        # set the indices and the load the 
        self._gather_indices()
        self.ensemble_init()
        return

    def _gather_indices(self):
        nparams_per_key=[self.parameters[key].nparams for key in self.loaded_keys ] 
        indexes=np.append([0],np.cumsum(nparams_per_key))
        self.StateParameter_indices={}
        for i,k in enumerate(self.loaded_keys):
            self.StateParameter_indices[k]=(indexes[i],indexes[i+1])
        self.nparams=np.sum(nparams_per_key)
        self.domain_info=[None]*self.nparams
        self.dacycle.dasystem['nparameters']=self.nparams
        return

################### end class ctsfstatevector ###################

if __name__ == "__main__":
    pass

