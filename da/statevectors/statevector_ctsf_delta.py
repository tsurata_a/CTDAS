"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python

from __future__ import division

import os
import sys
sys.path.append(os.getcwd())
import logging
import numpy as np
import datetime as dt
from dateutil.relativedelta import relativedelta
import da.tools.io4 as io
from da.tools.general import to_datetime
from da.statevectors.statevector_baseclass import StateVector, EnsembleMember
from netCDF4 import Dataset
from functools import partial
from da.tools.covariance import cov_gaussian  
identifier = 'CarbonTracker Statistical fit'
version = '0.0'

################### Start Class CTSFStateVector ###################
class ctsfStateVector(StateVector):

    """
    This is a StateVector object for CarbonTracker. It is created for use with the SIF inversion approach,
    and contains statistical parameters and SIF sensitivity parameters per ecoregion to be assembled.
    It has a private method to make new ensemble members.
    """

    def setup(self, dacycle):
        """
        setup the object by specifying the dimensions.
        There are two major requirements for each statvector that you want to build:
            (1) is that the statevector can map itself onto a regular grid
            (2) is that the statevector can map itself (mean+covariance) onto TransCom regions
        An example is given below.
        """

        self.dacycle = dacycle

        # JJDH used to be sample start 
        # Yet, in ctsf inversions their is no difference
        startdate = self.dacycle['time.start']
        enddate   = self.dacycle['time.end']

        logging.info('Calculating fluxes from %s to %s' % (startdate, enddate))

        self.isOptimized=False

        # Temporal settings
        self.nlag       = int(dacycle['time.nlag'])
        self.refdate    = to_datetime(dacycle.dasystem['time.reference'])
         
        self.datevec    = np.array(self.calc_datevec(startdate, enddate,'day'))
        self.timevec = np.array([(date-self.refdate).days for date in self.datevec])
        logging.info('Timevec = %s' % self.timevec)


        # Spatial settings
        self.nlat = 180
        self.nlon = 360

        # Inversion setup
        self.nmembers   = int(dacycle['da.optimizer.nmembers'])                 # number of ensemble members
        
        # Fluxfit settings
        # self.unit_initialization = dacycle.dasystem['fit.unit_initialization']  # if True: initial fit will be constructed based on latitude, else: read from file
        writememberfluxes        = self.dacycle.dasystem['writeMemberFluxes']   # write gridded fluxes for every member? [bool]

        logging.info('Read parameters for flux calculation')
        # fit settings
        self.nharmonics = int(dacycle.dasystem['nharmonics'])               # number of harmonical terms in baseline fit
        self.npolyterms = int(dacycle.dasystem['npolyterms'])

        logging.debug('Using {} harmonic parameters'.format(self.nharmonics))
        logging.debug('Using {} polynomial terms'.format(self.npolyterms))
        self.nbasefitparams = self.npolyterms + self.nharmonics*2 
        logging.debug('Resulting in {} parameters ({}+ 2 * {} )'.format(self.nbasefitparams, self.npolyterms, self.nharmonics))

        self.nobs              = 0
        self.obs_to_assimilate = ()                                             # empty containter to hold observations to assimilate later on


        # else: Read fit parameters from file
        self.initparamsfile     = dacycle.dasystem['params.init.file']
            # self.read_initparamsvec = dacycle.dasystem['params.init.vec']       # True: directly read statevector, False: extract statevector from gridded field
        self.initparamsname     = dacycle.dasystem['params.init.name']
        self.nfitparams = self.nbasefitparams

        # anomaly term
        self.ngamma = 365 
        self.anofile = dacycle.dasystem['fit.anomaly.inputfile']
        self.anoname = dacycle.dasystem['fit.anomaly.name']                 # name of anomaly in inputfile
        self.gamma0 = float(dacycle.dasystem['params.init.gamma']) #.split(',')  # enter single value, or comma-separated list of ngamma (default: one for each month)

        # statevector settings 
        self.nlambda=365
        self.ngamma_alpha=365
        self.nlambda_terdis=365

        # These list objects hold the data for each time step of lag in the system. Note that the ensembles for each time step consist
        # of lists of EnsembleMember objects, we define member 0 as the mean of the distribution and n=1,...,nmembers as the spread.
        self.ensemble_members = list([[]]*self.nlag)

        # This specifies the file to read with the gridded mask at 1x1 degrees. Each gridbox holds a number that specifies the ecoregion it belongs to.
        # From this map, a dictionary is created that allows a reverse look-up so that we can map parameters to a grid.
        mapfile      = os.path.join(dacycle.dasystem['regionsfile'])
        ncf          = io.ct_read(mapfile, 'read')
        self.gridmap = ncf.get_variable('regions')                              # read ecoregions map (nlat x nlon)
        self.tcmap   = ncf.get_variable('transcom_regions')                     # read transcom regions map (nlat x nlon)
        olsonmap     = ncf.get_variable('land_ecosystems')
        ncf.close()

        logging.debug("A TransCom  map on 1x1 degree was read from file %s" % dacycle.dasystem['regionsfile'])
        logging.debug("An ecoregion parameter map on 1x1 degree was read from file %s" % dacycle.dasystem['regionsfile'])

        # Create a dictionary for state <-> gridded map conversions (keys = regionnumbers)
        # And calculate mean latitude per ecoregion (only if self.amplitude[0] = None)
        regnums       = []
        self.griddict = {}
        lat_regions   = []

        for i in range(209):
            sel = np.where(self.gridmap == i+1)
            if len(sel[0]) > 0 and np.mean(olsonmap[sel] < 19):
                regnums.append(i+1)
                self.griddict[i+1]    = sel

        self.regionnumbers = np.asarray(regnums)
        self.nregions      = len(self.regionnumbers)
        self.i_ln=0
        self.i_gn=self.nlambda 
        self.i_ga=self.nlambda + self.ngamma 
        self.i_ld=self.nlambda + self.ngamma + self.ngamma_alpha 


        self.nparams_per_region=self.nlambda + self.ngamma + self.ngamma_alpha + self.nlambda_terdis
        self.nparams       = self.nregions * self.nparams_per_region                  # number of elements in statevector

        logging.debug('There are %s non-empty ecoregions: %s' %(self.nregions, self.regionnumbers))
        # logging.debug('A dictionary to map grids to states and vice versa was created')
        logging.debug('Average latitude per (nonempty) ecoregion was calculated.')
        logging.info("Updating with dacycle.dasystem['nparameters']=%d to be used by the optimizer" %self.nparams )

        dacycle.dasystem['nparameters']=self.nparams
        
        self.nee_U=np.zeros((self.nregions),float)
        self.get_nee_unc()

        # Create a mask for species/unknowns
        self.make_species_mask()
        # Validate the requested input files
        self.validate_input()

        return

    def write_to_file(self,file,qual):
        """ do whatever super does
        then also add nee dter and dgpp sets
        """
        # file, dim of nparams ,nmembers, dimlag
        logging.debug('Starting write to file')
        f,dimparams, dimmembers, dimlag ,filename=super().write_to_file(file,qual,return_file=True)
        logging.debug('Continuing write to file')
        f.close()
        logging.info('Successfully wrote the State Vector to file (%s) ' % filename)
        return
    
    def read_from_file(self, filename, qual='opt'):
        """ Read initial data for the statevector from given files
        """
        super().read_from_file(filename,qual) 
        f = io.ct_read(filename, 'read')
        f.close()
        logging.info('Successfully read the fluxmodel data from file (%s) ' % filename)

    def state_to_grid(self, fluxvector=None, lag=1,monthcounter=None,filename=None,qual=None):
        # used to write statevector in grid format for coupling with TM5 (where actual fluxes are calculated)
        # implemented such that variables with dimensions nfitparams x nlat x nlon are created

        """
            Transforms the StateVector information (mean + covariance) to a 1x1 degree grid.


            :param: fluxvector: a vector of length (nparams,) that holds the fluxes associated with each parameter in the StateVector
            :param: lag: the lag at which to evaluate the StateVector
            :rtype: a tuple of two arrays (gridmean,gridvariance) with dimensions (nfitparams,180,360)
            counter will fetch the fluxvalues for month=counter

            If the attribute `fluxvector` is not passed, the function will return the mean parameter value and its variance on a 1x1 map.

            ..note:: Although we can return the variance information for each gridbox, the covariance information contained in the original ensemble is lost when mapping to 1x1 degree!
        """

        if fluxvector == None:
            fluxvector = np.ones(self.nparams)
        else:
            logging.error("ctsfStateVector cannot handle state_to_grid with fluxvector input")
            raise ValueError

        if filename==None:
            ensemble     = self.ensemble_members[lag - 1]
            ensemblemean = ensemble[0].param_values

            # First transform the mean
            gridmean = self.vector2grid(vectordata=ensemblemean * fluxvector)

            # And now the covariance, first create covariance matrix (!), and then multiply
            deviations = np.array([mem.param_values * fluxvector - ensemblemean for mem in ensemble])
            ensemble   = np.array([self.vector2grid(mem) for mem in deviations])
            return (gridmean,ensemble)

        else:
            f = io.ct_read(filename, 'read')
            mean=f.get_variable('statevectormean_'+ qual)[lag-1,:] 
            ensemble=f.get_variable('statevectorensemble_'+ qual)[lag-1,:,:] 

            f.close()

            currentdate=self.datevec[0]+relativedelta(months=monthcounter)
            mask=[(currentdate.month==t.month)&(currentdate.year==t.year) for t in self.datevec]
            logging.debug("averaging for month %s with %d days for %s" % (currentdate,sum(mask),qual))

            # nee_grid      = np.zeros((self.nmembers,self.nlat,self.nlon,),float)
            dnee_grid      = np.zeros((sum(mask),self.nmembers,self.nlat,self.nlon,),float)
            ano_grid       = np.zeros((sum(mask),self.nmembers,self.nlat,self.nlon,),float)
            terdis_grid    = np.zeros((sum(mask),self.nmembers,self.nlat,self.nlon,),float)
            nee13_grid     = np.zeros((sum(mask),self.nmembers,self.nlat,self.nlon,),float)

            gpp_grid     = np.zeros((sum(mask),self.nlat,self.nlon,),float)
            gpp13_grid     = np.zeros((sum(mask),self.nmembers,self.nlat,self.nlon,),float)

            ano_alpha_grid =np.zeros((sum(mask),self.nmembers,self.nlat,self.nlon,),float)            
            terdis_grid_scalar=np.zeros((sum(mask),self.nmembers,self.nlat,self.nlon,),float)            
            
            ave=partial(np.average,axis=0)

            time=self.datevec[mask]
            fnames=['/projects/0/ctdas/joram/input/13co2/SiB4/isotm5_%04d-%02d-%02d.nc' % (d.year,d.month,d.day) for d in time]
            R_atm=(-8.0/1000+1)*0.01123720

            for nr in range(0,self.nmembers):
                dnee_grid[:,nr,:,:]=self.components_to_grid((mean+ensemble[nr,:]).reshape((self.nregions, self.nparams_per_region))[:,:self.i_gn],time,region_multiplier=self.nee_U)
                ano_grid[:,nr,:,:]=self.components_to_grid((mean+ensemble[nr,:]).reshape((self.nregions, self.nparams_per_region))[:,self.i_gn:self.i_ga],time,grid_multiplier=self.ano[mask])
                ano_alpha_grid[:,nr,:,:]=self.components_to_grid((mean+ensemble[nr,:]).reshape((self.nregions, self.nparams_per_region))[:,self.i_ga:self.i_ld],time,grid_multiplier=self.ano[mask])
                terdis_grid_scalar[:,nr,:,:]=self.components_to_grid((mean+ensemble[nr,:]).reshape((self.nregions, self.nparams_per_region))[:,self.i_ld:],time)

            for i,fname in enumerate(fnames):

                ncf=Dataset(fname) 
                sib4_gpp   = np.array(ncf['Fgpp_SiB4_nofilter'][:])
                sib4_gpp[sib4_gpp==-1e34 ]  =0.0

                gpp_grid[i,:,:]=ave(sib4_gpp)

                sib4_nee   = np.array(ncf['I2b'][:])
                sib4_nee[sib4_nee==-1e34 ]  =0.0

                sib4_alpha = np.array(ncf['I3'][:])
                sib4_alpha [sib4_alpha ==-1e34 ]  =0.0

                sib4_Freco = np.array(ncf['I4'][:])
                sib4_Freco[sib4_Freco ==-1e34 ]  =0.0

                sib4_rreco = np.array(ncf['I5'][:])
                sib4_rreco[sib4_rreco==-1e34 ]  =0.0
                ncf.close()


                for nr in range(0,self.nmembers):
                    # add alpha to member.
                    r_mem=R_atm*(sib4_alpha + ano_alpha_grid[i,nr,:,:])/(1+R_atm*(sib4_alpha + ano_alpha_grid[i,nr,:,:]))
                    # compute nee 
                    nee_mem = sib4_nee + dnee_grid[i,nr,:,:] + ano_grid[i,nr,:,:]
                    gpp13_mem = sib4_gpp * r_mem 

                    # reduce 3 hourly to daily after converting to 13c flux
                    nee13_grid[i,nr,:,:] = ave(nee_mem*r_mem)
                    gpp13_grid[i,nr,:,:] = ave(gpp13_mem)
                    terdis_grid[i,nr,:,:]    = ave(terdis_grid_scalar[i,nr,:,:] * sib4_Freco * ( sib4_rreco - r_mem ) )  


            # reduce to montly average
            dnee_grid      = ave(dnee_grid      )
            ano_grid       = ave(ano_grid       )
            terdis_grid    = ave(terdis_grid    )
            nee13_grid     = ave(nee13_grid     )
            gpp_grid       = ave(gpp_grid     )
            gpp13_grid     = ave(gpp13_grid     )

            gridmean        = dnee_grid[0]
            ensemble        = np.array([nee - gridmean for nee in dnee_grid])

            gridmean_ano    = ano_grid[0]
            ensemble_ano    = np.array([ano-gridmean_ano for ano in ano_grid])

            gridmean_13nee  = nee13_grid[0]
            ensemble_13nee  = np.array([ano-gridmean_13nee for ano in nee13_grid])

            gridmean_terdis = terdis_grid[0]
            ensemble_terdis = np.array([ano-gridmean_terdis for ano in terdis_grid])

            gridmean_gpp = gpp_grid

            gridmean_gpp13 = gpp13_grid[0]
            ensemble_gpp13= np.array([ano- gridmean_gpp13 for ano in gpp13_grid ])


            results ={ 'co2_gpp_flux_imp'        : gridmean_gpp,
                       'co2_bio_flux_%s' %  qual                  : gridmean,
                       'co2_bio_flux_%s_ensemble' % qual          : ensemble ,
                       'co2_bioano_flux_%s'   % qual             : gridmean_ano,
                       'co2_bioano_flux_%s_ensemble' % qual      : ensemble_ano ,
                       'co2c13_bio_flux_%s' % qual                : gridmean_13nee,
                       'co2c13_bio_flux_%s_ensemble'% qual        : ensemble_13nee,
                       'co2c13_gpp_flux_%s'    % qual          : gridmean_gpp13,
                       'co2c13_gpp_flux_%s_ensemble'% qual     : ensemble_gpp13,
                       'co2c13_biodis_flux_%s'    % qual          : gridmean_terdis,
                       'co2c13_biodis_flux_%s_ensemble'% qual     : ensemble_terdis}

            return results 


    def vector2grid(self, vectordata=None,nparams="Default"):
        """
            Map vector elements to a map or vice cersa

           :param vectordata: a vector dataset to use in case `reverse = False`. This dataset is mapped onto a 1x1 grid and must be of length `nparams`
           :rtype: ndarray: an array of size (nfitparams,360,180)

           This method makes use of a dictionary that links every parameter number [1,...,nparams] to a series of gridindices. These
           indices specify a location on a 360x180 array, stretched into a vector using `array.flat`. There are multiple ways of calling
           this method::

               griddedarray = self.vector2grid(vectordata=param_values) # simply puts the param_values onto a (180,360,) array

           .. note:: This method uses a DaSystem object that must be initialzied with a proper parameter map. See :class:`~da.baseclasses.dasystem` for details
        """

        if nparams=="Default":
            nparams=self.nparams_per_region
        if not (nparams==1): 
            result = np.zeros((nparams,self.gridmap.shape[0], self.gridmap.shape[1]), float)
        else:
            result = np.zeros((self.gridmap.shape[0], self.gridmap.shape[1]), float)

        for k,data in enumerate(vectordata):
            # logging.debug('k is %s' %k)
            # works only if nparam>1 
            if not (nparams==1):
                r = int(np.floor(k / nparams))
                i = k % nparams
                regnum = self.regionnumbers[r]
                if regnum in self.griddict:
                # logging.debug(regnum)
                    result[i,self.griddict[regnum][0],self.griddict[regnum][1]] = data 
            else:
                regnum = self.regionnumbers[k]
                if regnum in self.griddict:
                    result[self.griddict[regnum][0],self.griddict[regnum][1]] = data 


        return result

    def get_covariance(self, date, dacycle):
        """
        Make a new ensemble from specified matrices, the attribute lag refers to the position in the state vector.
            Note that lag=1 means an index of 0 in python, hence the notation lag-1 in the indexing below.
            The argument is thus referring to the lagged state vector as [1,2,3,4,5,..., nlag]
        """

        # read uncertainties for anomaly and convert to variance
        unc_gamma = float(dacycle.dasystem['bio.cov.gamma'])**2
        unc_alpha = (0.0006/5)**2   # ivelde 0.0008 variatien in alpha (0.0006). max dT = 10 Kelvin. assume outlier, devide by 5 
        unc_terdis = 0.64   # we first try scalar. 
        # loop over regions and insert
        # covariance as we go. each month will be scaled with an ecoregion dependent scalar.
        # with mean 1 and 80 percent uncertainty so the sigma**2 = 0.64. this is yet somewhat artifficial
        # in the future we may want to add a weak correlation between ecoregions 
        cov_mat_list=[]
        for i,r in enumerate(self.regionnumbers):
            cov_mat=np.zeros((self.nparams_per_region,self.nparams_per_region),float)
            # gaussian window with 12 days, cyclic
            cov_mat[          : self.i_gn,          : self.i_gn]=cov_gaussian(365,20,0.64)
            cov_mat[self.i_gn : self.i_ga,self.i_gn : self.i_ga]=cov_gaussian(365,5,unc_gamma)
            cov_mat[self.i_ga : self.i_ld,self.i_ga : self.i_ld]=cov_gaussian(365,5,unc_alpha)
            cov_mat[self.i_ld : ,self.i_ld          : ]         =cov_gaussian(365,50,unc_terdis)
            logging.debug('diagonal is %s' % np.diag(cov_mat))
            parnrs = range( i * self.nparams_per_region,(i+1)* self.nparams_per_region)
            cov_mat_list.append([cov_mat,parnrs])
        return cov_mat_list

    def make_new_ensemble(self, lag, covariancematrixlist=[None]):
        """
        :param lag: an integer indicating the time step in the lag order
        :param covariancematrix: a list of matrices specifying the covariance distribution to draw from
        :rtype: None

        Make a new ensemble, the attribute lag refers to the position in the state vector.
        Note that lag=1 means an index of 0 in python, hence the notation lag-1 in the indexing below.
        The argument is thus referring to the lagged state vector as [1,2,3,4,5,..., nlag]

        The covariance list object to be passed holds a list of matrices with a total number of dimensions [nparams, nparams], which is
        used to draw ensemblemembers from. Each draw is done on a matrix from the list, to make the computational burden smaller when
        the StateVector nparams becomes very large.

        """

        if not isinstance(covariancematrixlist, list):
            logging.error("The covariance matrix or matrices must be passed as a list of array objects, exiting..." )
            raise ValueError

        logging.debug('len(covariancematrixlist) = %s' %len(covariancematrixlist))

        # Check dimensions of covariance matrix list, must add up to nparams
        dims = 0
        for matrix,parnr in covariancematrixlist:
            dims += matrix.shape[0]

        if dims != self.nparams:
            logging.error("The total dimension of the covariance matrices passed (%d) does not add up to the prescribed nparams (%d), exiting..." % (dims, self.nparams))
            raise ValueError

        # Loop over list of covariance matrices and their statevector parameter numbers, and create a matrix of (nparams,nmembers) with the deviations
        dof = 0.0
        dev_matrix = np.zeros((self.nparams, self.nmembers,), 'float')
        randstate = np.random.get_state()

        for matrix,parnr in covariancematrixlist:

            # Make a cholesky decomposition of the covariance matrix
            for i in range(4):
                partial_mat=matrix[i*365:i*365+365,i*365:i*365+365]
                try:
                    _, s, _ = np.linalg.svd(partial_mat)
                except:
                    s = np.linalg.svd(partial_mat, full_matrices=1, compute_uv=0) #Cartesius fix
                # logging.debug("added %s degrees of freedom" %(  np.sum(s) ** 2 / sum(s ** 2) ))
                dof += np.sum(s) ** 2 / sum(s ** 2)

                try:
                    C = np.linalg.cholesky(partial_mat)
                except (np.linalg.linalg.LinAlgError, err):
                    logging.error('Cholesky decomposition has failed ')
                    # logging.error('For a matrix with parnr %s and of dimensions: %d' % (parnr,partial_mat.shape[0]))
                    logging.debug(err)
                    raise np.linalg.linalg.LinAlgError

                # Draw nmembers instances of this distribution
                npoints = partial_mat.shape[0]

                istart = parnr[0]
                istop  = parnr[-1]+1
                
                logging.debug('right amount of elements %s' %(( istart+4*365 )==istop))
                for member in range(1, self.nmembers):
                    rands = np.random.randn(npoints)
                    deviations = np.dot(C, rands)
                    dev_matrix[istart+i*365:istart+i*365+365, member] = deviations

            logging.debug("Inserted prior covariance matrix %d elements into statevector location %d through %d"%(len(parnr),istart,istop) )

        logging.debug('Successfully constructed a deviation matrix from covariance structure')
        logging.info('Appr. degrees of freedom in full covariance matrix is %s' % (int(dof)))

        # Create mean values 
        new_mean = self.create_mean_statevector()

        # Now fill the ensemble members with the deviations we have just created
        # Create members 1:nmembers and add to ensemble_members list
        for member in range(self.nmembers):
            new_member = EnsembleMember(member)
            # dev_matrix is initialized at zero; we add all zeros to the first member
            new_member.param_values = dev_matrix[:, member ] + new_mean

            self.ensemble_members[lag].append(new_member)

            # self.calculate_scaled_flux(new_member)
        logging.debug('%d new ensemble members were added to the state vector # %d' % (self.nmembers, (lag + 1)))
        return

    def calc_datevec(self, startdate, enddate,resolution):
        """Creates time vector with monthly interval ranging from startdate to enddate."""

        datevec = []
        ndays   = (enddate-startdate).days

        if resolution=='month':
            date = dt.datetime(startdate.year,startdate.month,15)
            while date <= enddate:
                datevec.append(date)
                if date.month < 12:
                    date = dt.datetime(date.year,date.month+1,15)
                else:
                    date = dt.datetime(date.year+1,1,15)

            logging.debug('Datevec = %s' % datevec)
            return datevec

        elif resolution=='day':
            date = startdate 
            datevec=[startdate+dt.timedelta(days=day) for day in range(ndays)]
                
            logging.debug('Datevec = %s' % datevec)
        return datevec

    def get_nee_unc(self):  
        # read initial statevector from file
        f_in    = io.ct_read(self.initparamsfile,method='read')
        initmat = f_in.get_variable(self.initparamsname) # [nparameters,necoregions=240]
        f_in.close()
        params=[]
        for i,nr in enumerate(self.regionnumbers):
            i0 = i*self.nfitparams
            i1 = i0 + self.nbasefitparams
            params[i0 : i1] = initmat[:,nr-1]
        
        params=np.array(params)
        nee_data = self.evaluate_model(params.reshape((self.nregions, self.nfitparams)), self.timevec)
        self.nee_U=np.std(nee_data[:,:],axis=1)
        logging.debug("retrieved flux from %s" %self.initparamsfile)
        return

    def create_mean_statevector(self):
        """Createt initial default statevector (prior)"""
        # construct initial statevector
        new_mean=[]
        for i,nr in enumerate(self.regionnumbers):
            # add scalars for each month
            new_mean.extend([0.0]*self.nlambda )
            # initialize gamma
            new_mean.extend([self.gamma0]*self.ngamma)
            # initialize alpha
            new_mean.extend([0]*self.ngamma_alpha)
            # terdis 
            new_mean.extend([1]*self.nlambda_terdis)

        new_mean = np.array(new_mean)
        logging.debug('Created new mean statevector containing %s statevector elements' %(len(new_mean)) )
        logging.debug('     Monthly lambda=0.0')
        logging.debug('     And gamma_ano=%s ' %self.gamma0)
        return new_mean

    def write_members_to_file(self, lag, outdir, endswith='.nc', member=None,obsoperator=None):
        """
           :param: lag: Which lag step of the filter to write, must lie in range [1,...,nlag]
           :param: outdir: Directory where to write files
           :param: endswith: Optional label to add to the filename, default is simply .nc
           :rtype: None

           Write ensemble member information to a NetCDF file for later use. The standard output filename is
           *parameters.DDD.nc* where *DDD* is the number of the ensemble member. Standard output file location
           is the `dir.input` of the dacycle object. In principle the output file will have only two datasets inside
           called `parametervalues` which is of dimensions `nparameters` and `parametermap` which is of dimensions (180,360).
           This dataset can be read and used by a :class:`~da.baseclasses.observationoperator.ObservationOperator` object.

           .. note:: if more, or other information is needed to complete the sampling of the ObservationOperator you
                     can simply inherit from the StateVector baseclass and overwrite this write_members_to_file function.

        """

        # These import statements caused a crash in netCDF4 on MacOSX. No problems on Jet though. Solution was
        # to do the import already at the start of the module, not just in this method.

        if member is None:
            members = self.ensemble_members[lag]
        else:
            members = [self.ensemble_members[lag][member]]

        logging.debug('write_members_to_file: dir = %s' %outdir)

        if self.isOptimized:
            logging.info('isOptimized = True; recalculating NEE before writing to file')

        # MAKE PARALLEL?
        for mem in members:

            filename     = os.path.join(outdir, 'parameters.%03d%s' % (mem.membernumber, endswith))
            ncf          = io.CT_CDF(filename, method='create')
            dimt         = ncf.add_dim('date',len(self.timevec))
            dimda        = ncf.add_dim('datearray',3)
            dimgrid      = ncf.add_latlon_dim()
            dimparams    = ncf.add_params_dim(self.nparams)
            dimfitparams = ncf.add_dim('nfitparameters',self.nparams_per_region)

            data = mem.param_values
            savedict = io.std_savedict.copy()
            savedict['name']      = "parametervalues"
            savedict['long_name'] = "parameter_values_for_member_%d" % mem.membernumber
            savedict['units']     = "unitless"
            savedict['dims']      = dimparams
            savedict['values']    = data
            savedict['comment']   = 'These are parameter values to use for member %d' % mem.membernumber
            ncf.add_data(savedict)

            # nee, dgpp, dter = self.evaluate_model(data.reshape((self.nregions,self.nfitparams)) , self.timevec)
            # nee_gridded = self.vector2grid(vectordata=nee.ravel(),nparams=12)
            griddata = self.vector2grid(vectordata=data)

            savedict = io.std_savedict.copy()
            savedict['name']      = "parametermap"
            savedict['long_name'] = "parametermap_for_member_%d" % mem.membernumber
            savedict['units']     = "unitless"
            savedict['dims']      = dimfitparams + dimgrid
            savedict['values']    = griddata.tolist()
            savedict['comment']   = 'These are gridded parameter values to use for member %d' % mem.membernumber
            ncf.add_data(savedict)

            ####################################################################
            # add time
            savedict              = io.std_savedict.copy()
            savedict['name']      = 'date'
            savedict['long_name'] = 'date as days since 1-1-2000'
            savedict['dims']      = dimt
            savedict['units']     = 'days since 1-1-2000'
            savedict['values']    = self.timevec
            ncf.add_data(savedict)

            # add date as array
            datearray = [[(self.refdate+dt.timedelta(days=int(d))).year,(self.refdate+dt.timedelta(days=int(d))).month, (self.refdate+dt.timedelta(days=int(d))).day] for d in self.timevec]
            savedict              = io.std_savedict.copy()
            savedict['name']      = 'datearray'
            savedict['long_name'] = 'date array as [YYYY,MM,DD]'
            savedict['dims']      = dimt + dimda
            savedict['values']    = np.array(datearray)
            ncf.add_data(savedict)

            membern = mem.membernumber
            grid_vals=self.components_to_grid(mem.param_values.reshape((self.nregions, self.nparams_per_region))[:,:self.i_gn],self.datevec,region_multiplier=self.nee_U)
            # logging.debug("meen of member %s : %2.10f 'mol/m2/s'" %(membern, np.mean(dnee_grid)))
            # logging.debug("shape %s " %list(np.shape(mem.param_values.reshape((self.nregions, self.nparams_per_region))[:,:self.nlambda])))
            # logging.debug("shape %s " %(mem.param_values.reshape((self.nregions, self.nparams_per_region))[:,:self.nlambda][0,0]))
            # logging.debug("shape %s " %list(np.shape(mem.param_values.reshape((self.nregions, self.nparams_per_region))[:,self.nlambda:])))
            # logging.debug("shape %s " %(mem.param_values.reshape((self.nregions, self.nparams_per_region))[:,self.nlambda:][0,0]))
            grid_vals+=self.components_to_grid(mem.param_values.reshape((self.nregions, self.nparams_per_region))[:,self.i_gn:self.i_ga],self.datevec,grid_multiplier=self.ano)
            # logging.debug("after adding ano %s : %2.10f 'mol/m2/s'" %(membern, np.mean(dnee_grid)))
                
            # add flux
            savedict              = io.std_savedict.copy()
            savedict['name']      = 'dNEE_grid'
            savedict['long_name'] = ('Simulated NEE values on lat-lon grid for ensemble member %s' % mem.membernumber)
            savedict['dims']      = dimt + dimgrid
            savedict['values']    = grid_vals.tolist()
            savedict['units']     = 'mol/m2/s'
            ncf.add_data(savedict)

            grid_vals=self.components_to_grid(mem.param_values.reshape((self.nregions, self.nparams_per_region))[:,self.i_ga:self.i_ld],self.datevec,grid_multiplier=self.ano)
            # logging.debug("after adding ano %s : %2.10f 'mol/m2/s'" %(membern, np.mean(dnee_grid)))
                
            # add flux
            savedict              = io.std_savedict.copy()
            savedict['name']      = 'dalpha'
            savedict['long_name'] = ('estimated alpha differences on lat-lon grid for ensemble member %s' % mem.membernumber)
            savedict['dims']      = dimt + dimgrid
            savedict['values']    = grid_vals.tolist()
            savedict['units']     = 'dimensionless'
            ncf.add_data(savedict)

            grid_vals=self.components_to_grid(mem.param_values.reshape((self.nregions, self.nparams_per_region))[:,self.i_ld:],self.datevec)
            # logging.debug("after adding ano %s : %2.10f 'mol/m2/s'" %(membern, np.mean(dnee_grid)))
                
            # add flux
            savedict              = io.std_savedict.copy()
            savedict['name']      = 'terdis'
            savedict['long_name'] = ('estimated alpha differences on lat-lon grid for ensemble member %s' % mem.membernumber)
            savedict['dims']      = dimt + dimgrid
            savedict['values']    = grid_vals.tolist()
            grid_vals=self.components_to_grid(mem.param_values.reshape((self.nregions, self.nparams_per_region))[:,self.i_ga:self.i_ld],self.datevec)
            # logging.debug("after adding ano %s : %2.10f 'mol/m2/s'" %(membern, np.mean(dnee_grid)))

            savedict['units']     = 'dimensionless'
            ncf.add_data(savedict)

            logging.info('Fluxmap for member %s written to file (%s)' %(mem.membernumber, filename))
            ####################################################################

            ncf.close()

            logging.debug('Successfully wrote data from ensemble member %d to file (%s) ' % (mem.membernumber, filename))
        # exit()
        return

    def propagate(self, dacycle):
        """
        :rtype: None

        Propagate the parameter values in the StateVector to the next cycle. This means a shift by one cycle
        step for all states that will
        be optimized once more, and the creation of a new ensemble for the time step that just
        comes in for the first time (step=nlag).
        In the future, this routine can incorporate a formal propagation of the statevector.

        """

        import da.analysis.tools_time as at

        # Remove State Vector n=1 by simply "popping" it from the list and appending a new empty list at the front. This empty list will
        # hold the new ensemble for the new cycle

        self.ensemble_members.pop(0)
        self.ensemble_members.append([])

        # And now create a new time step of mean + members for n=nlag

        date = dacycle['time.start'] + at.timedelta(days=(self.nlag - 0.5) * int(dacycle['time.cycle']))

        logging.info("STATEVECTOR PROPAGATE ENDTIME = %s, date = %s" % (dacycle['time.end'],date))

        cov = self.get_covariance(date, dacycle)
        self.make_new_ensemble(self.nlag - 1, cov)

        logging.info('The state vector has been propagated by one cycle')
#
# Functions for evaluation of the fluxmodel from 
#   fitparmas
#   an anomaly

    def validate_input(self):
        """ Make sure that data needed for the FluxModel (such as anomaly inputdata) are present."""

        if not os.path.exists(self.anofile):
            msg = "Anomaly input file for CTSF inversion does not exist, exiting..."
            logging.error(msg)
            raise (IOError, msg)

        # read anomalies from file for defined timerange
        self.ano = self.get_anomalies(self.anofile, self.anoname, self.timevec)
        logging.info('Obtained anomalies for timerange.')
        self.ano=np.nan_to_num(self.ano)
        return


    def get_anomalies(self, file, name, timevec):
        """ Read anomalies from file. The returned anomaly array will contain data for all times in timevec. Fillvalue = NaN."""

        # read input data
        ano, t_ano = self.get_anodata(file,name,getTime=True)
        # get index of simulation times in anomaly input array
        t_ano_index = self.get_time_index(t_ano, timevec)
        # replace anomaly array by new array that contains anomalies for the requested simulation times
        ano = self.build_anomalies_samples(ano, t_ano_index)

        return ano

    def get_mean(self, file, name, timevec):
        """ Read mean seasonal cycle from file. The returned mean array will contain data for all times in timevec. Fillvalue = NaN."""

        # read input data
        mean, t_mean = self.get_anodata(file,name,getTime=True)

        # convert to gridded mean data for timevec
        mean_out = np.zeros((len(timevec),self.nlat,self.nlon))

        if len(mean.shape) == 2 and (mean.shape[0] == 12 or mean.shape[1] == 12):
            for nr in range(209): #max(self.gridmap)):
                mask = np.where(self.gridmap == nr+1,1,0)
                for t in range(len(timevec)):
                    month = (self.refdate + dt.timedelta(days=int(timevec[t]))).month - 1
                    if mean.shape[0] == 12:
                        mean_out[t,:,:] += mask * mean[month,nr]
                    else:
                        mean_out[t,:,:] += mask * mean[nr,month]

        elif len(mean.shape) == 3 and mean.shape[0] == 12 and mean.shape[1] == self.nlat and mean.shape[2] == self.nlon:
            for t in range(len(timevec)):
                month = (self.refdate + dt.timedelta(days=int(timevec[t]))).month - 1
                mean_out[t,:,:] = mean[month,:,:]

        elif len(mean.shape) == 3 and mean.shape[0] == len(t_mean) and mean.shape[1] == self.nlat and mean.shape[2] == self.nlon:
            # get index of simulation times in mean input array
            t_ano_index = self.get_time_index(t_mean, timevec)
            # replace anomaly array by new array that contains anomalies for the requested simulation times
            mean_out = self.build_anomalies_samples(mean, t_ano_index)

        else:
            logging.error('Array with mean temperature data for respiration parametrization has invalid size (%s). Valid options are [12,nregions], [12,%s,%s] or [t,%s,%s]' \
                          % (str(mean.shape), self.nlat, self.nlon, self.nlat, self.nlon))
            raise ValueError

        return mean_out


    def get_anodata(self,filename,varname,getTime = False):

        """
        : param filename: full path to netCDF file containing 'datatype' data on 1x1 deg grid
        : param varname: string to identify the data to be read from 'filename'.

        Method to read data for construction of NEE fit, and corresponding timerange, from netCDF file.
        Note: currently units are not checked!
        """

        ncf = io.ct_read(filename, method='read')

        anodata = np.nan_to_num(ncf.get_variable(varname))
        if getTime:
            t_anodata = ncf.get_variable('time')
            ncf.close()
            logging.info("Successfully read %s and time from file (%s)" % (varname, filename))
            return anodata, t_anodata
        else:
            logging.info("Successfully read %s from file (%s)" % (varname, filename))
            return anodata



    def get_time_index(self,t_ano, timevec):
        """
        Returns a list with for every date in self.timevec the index in the input vector 'times'.
        If no input data is present for the requested time, index = None
        """

        index = []

        for t in range(len(timevec)):
            foundindex = False
            date = self.refdate + dt.timedelta(days=int(timevec[t]))
            for tt in range(len(t_ano)):
                date_input = self.refdate + dt.timedelta(days=int(t_ano[tt]))
                if (date.year == date_input.year) and (date.month == date_input.month):
                    index.append(tt)
                    foundindex = True
                    break
            if not foundindex:
                logging.info("Missing anomaly input data for month %s" % date.strftime('%Y-%m-%d'))
                index.append(None)

        logging.debug("Obtained anomaly time indices")

        return index

    def build_anomalies_samples(self, anomaly, index):
        """ Returns a 3D array with input anomaly data for times corresponding to timevec only. Fillvalue = NaN."""

        logging.debug('anomaly shape = %s' %str(anomaly.shape))
        sampled_anomaly = np.zeros((len(index),anomaly.shape[1],anomaly.shape[2]))
        for i in range(len(index)):
            if index[i] is None:
                sampled_anomaly[i,:,:] = np.nan
            else:
                sampled_anomaly[i,:,:] = anomaly[index[i],:,:]

        logging.debug("Created array with time-sampled anomalies.")

        return sampled_anomaly

    def evaluate_model(self,statemap,timevec):

        """
        Method to calculate an NEE field consisting of regionally constant second-order polynomial and harmonical terms with linearly varying amplitude
        (to account for mean seasonal cycle), enhanced with scaled anomalies to account for spatial and interannual variability.
        """

        # check dimensions of input
        if statemap.shape[0] != self.nregions:
            logging.error("Incorrect number of statemap parameters per grid cell (%d) (must equal the number of fit parameters (%d))." % (statemap.shape[0], self.nfitparams))
            raise ValueError

        i1 = self.nbasefitparams
        nee = (self.calculate_polynomic(statemap[:, :self.npolyterms], timevec) + self.calculate_harmonics_sincos(statemap[:, self.npolyterms: i1], timevec))
        return nee.T  

    def calculate_polynomic(self, statemap, timevec):
        """Calculate the NEE due to the polynomic terms
        Input: 
            statemap (np.array of NREGIONS x NPOLYTERMS): statevector values reshaped
            timevec (np.array len(NTIMES): array with number of days after a set starttime
        Output:
            NEE: (np.array of NTIMES x NREGIONS): Calculated NEE due to the polynomic terms"""
        polynee = np.zeros((len(timevec), len(statemap)))
        for i in range(self.npolyterms):
            polynee += statemap[:, i] * (timevec[:, None] ** i)
    
        return polynee 

    def calculate_harmonics_sincos(self,statemap,timevec):
        """Calculate the NEE due to the harmonic terms
        Add cosine and sine waves as decomposition
        Input: 
            statemap (np.array of NREGIONS x NHARMONICS): statevectorvalues reshaped
            timevec (np.array len(NTIMES): array with number of days after a set starttime
        Output: 
            NEE: (np.array of NTIMES x NREGIONS): calculated NEE due to the harmonic terms"""

        days_in_year = 365.242
        har = np.zeros((len(timevec),len(statemap)), 'float')

        for i in range(self.nharmonics):
            j = i + self.nharmonics
            har += statemap[:, i] * np.cos( 2.0*np.pi / days_in_year * (i + 1) * timevec[:, None]) \
                 + statemap[:, j] * np.sin( 2.0*np.pi / days_in_year * (i + 1) * timevec[:, None])
        return har #* (1 + (statemap[:, j+1] * timevec[:, None]))

    def cyclic_to_timevec(self,vector,datevec):
        """
        transforms a vector that represents 365 dayily scalars into a nregion x ndates matrix. i.e. maps elements in time 
        vector will be statevector, can be nregions * 365 or simply 365
        assumes daily scalars. In leap year, 29 feb wil be the mean of 28 feb and 1 march.
        """
        # Take the transpose, so that the time axis is first will pick based on time
        vector=vector.T 
        # a list for easy appending
        scalar_array=[]
        # take the start year
        year=datevec[0].year
        day_in_year=(datevec[0]-dt.datetime(year,1,1)).days
        # in a leap year this offsets 1 day to much after feb 
        if ((dt.datetime(year,3,1)-dt.datetime(year,2,1)).days==29 )&(datevec[0].month>2):
            day_in_year-=1
        for date in datevec:
            # reset if we are in a new year
            if year!=date.year:
                year=date.year
                day_in_year=0
            # add one to counter
            day_in_year+=1
            if date.month==2 and date.day==29:
                day_in_year-=1 # go back to 28 and take the mean of 28 feb and 1 march
                scalar_array.append((vector[day_in_year-1]+vector[day_in_year])/2)
            else:
                scalar_array.append(vector[day_in_year-1])
        # transpose back: 
        return np.array(scalar_array).T

    def components_to_grid(self,statemap,timevec,grid_multiplier=None,region_multiplier=None,ufunc=None):
        """
        takes in a statemap, assumed to be cyclic, nregions * 365
        returns array of ndays * nlat * nlon
        each entry is lambda * nregion_multiplier * grid_multiplier belonging to that particular 
        box
        example: 
        """
        # here we need to compute scaled versions of NEE and 
        # ano_term 
        # we do so by looping over each region.
        # state will be


        # statemap of nregions * nparams -> nregions * ndates 
        scalar_array=self.cyclic_to_timevec(statemap,timevec)
        # logging.debug('scalar array%s' %scalar_array)

        # apply the region based scalar 
        if region_multiplier is not None:
            scalar_array=np.array([region_multiplier ]).T*scalar_array
        # create a gridded version of the scalar
        scalar_array_grid = self.vector2grid(vectordata=scalar_array.ravel(),nparams=len(timevec))
        if grid_multiplier is not None:
            scalar_array_grid =grid_multiplier * scalar_array_grid 
        # logging.debug('allfinite is %s' % (all(np.isfinite(self.dnee_data[member.membernumber,:,:].ravel()))))
        # if an aditional function is applied, do so here
        if ufunc is not None:
            scalar_array_grid=ufunc(scalar_array_grid)
        return scalar_array_grid

################### End Class CTSFStateVector ###################

if __name__ == "__main__":
    pass

