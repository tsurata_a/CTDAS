import numpy as np
import netCDF4 as nc
import pandas as pd

import sys
import os
import logging

from collections import OrderedDict

import da.tools.io4 as io
from da.statevectors.statevector_ctsf import ctsfStateVector, EnsembleMember # move to da/statevector
from da.tools.general import to_datetime

from threadpoolctl import threadpool_limits
# Make this smaller for tests
NPARAMS_TEST = 1000000

class CTSF_SiB_statevector(ctsfStateVector):
    
    def setup(self, dacycle):
        logging.info('Starting setup of the statvector...')
        self.dacycle = dacycle
        self.nlag = int(dacycle['time.nlag'])
        self.nmembers = int(dacycle['da.optimizer.nmembers'])
        self.make_gridmap()
        self.isOptimized = False
    
        self.refdate = to_datetime(dacycle.dasystem['time.reference'])
        self.resolution = dacycle.dasystem['fit.resolution']
        self.calculate_timevec()
        self.nparameters = len(self.latgrid_flat) # Params without time dim

        self.setup_functions()
        self.nparams = len(self.latgrid_flat)  * self.all_parameters
        self.n_all_params = len(self.latgrid_flat) * self.all_parameters# params including time variability 
        logging.info(f'Using {self.nparams} parameter values')


        self.make_choleskys()

        dacycle.dasystem['nparameters'] = self.nparams
        self.setup_members()

        self.nobs = 0
        # empty containter to hold observations to assimilate later on
        self.obs_to_assimilate = ()
        logging.info('Statevector is set-up')

    def setup_functions(self):
        """method to setup the relations that will be used. 
        They are specified in the .rc file"""
        dacycle = self.dacycle
        todo = list(map(str.strip, self.dacycle.dasystem['fit.do'].split(',')))
        self.all_parameters = 0

        todo_dict = OrderedDict()
        start_idx = 0 # Keep track of where the parameters for this function will be
        # For all parameters: save how to draw the random numbers.
        # A default r of 0 (no correlation) is applied
        # Otherwise, red noise with correlation 'r' is used
        how_to_draw_rands = []
        for i, func in enumerate(todo):
            items = {}
            # look for the items that belong to this function
            for k, v in dacycle.dasystem.items():
                if f'fit.{func}.' in k:
                    # Some error handling to convert the number to a int, float or str
                    try:
                        val = int(v)
                    except ValueError as e:
                        try: 
                            val = float(v)
                        except: 
                            val = v # means it's string
                    items[k.split('.')[-1]] = val
            todo_dict[func] = items
            todo_dict[func]['start_idx'] = start_idx
            logging.debug(f'{func} indices start at {start_idx} in the statevector')

            n = todo_dict[func]['n'] 
            assert isinstance(n, int)
            start_idx += self.nparameters * n
            # Draw random numbers with correlation through time?
            if f'fit.{func}.r' in dacycle.dasystem.keys():
                r = float(dacycle.dasystem[f'fit.{func}.r'])
            else: r = 0
            logging.debug(f'{func} has a correlation in time of {r}')
            # Store the r value and the amount of data points to correlate
            # note that dec should also correlate with jan
            how_to_draw_rands.append((r, [None] * n)) 
            self.all_parameters += n

        self.todo_dict = todo_dict
        self.how_to_draw_rands = how_to_draw_rands

    def make_choleskys(self):
        """Method to create the cholesky decompositions for each function used
        Because we draw new parameters for each new timestep, we need multiple 
        decompositions. By doing it here, we only need to do it once.
        We store them in a diagonal block here"""

        choleskys = {}
        logging.info('Making cholesky decompositions...')
        for k, v in self.todo_dict.items():
            logging.debug(f'... for {k}')
            covmatrix = self.get_covariance(v['covfile'])
            choleskys[k] = np.linalg.cholesky(covmatrix)

        # Make a block diagonal matrix of all choleskies.
        cholesky_arrs = []
        for k, v in self.todo_dict.items():
            for n in range(v['n']):
                cholesky_arrs.append(choleskys[k])
        self.C = self.block_diag(cholesky_arrs)
        logging.info('Made cholesky decompositions')

    def create_new_mean(self):
        """Create a new mean statevector, for each statevectorelement"""
        newmean = []
        if not self.isOptimized:
            # The ensemble is not yet optimised. That means that
            # The mean should be 1 for multiplicative, 0 for additive
            # Save this for all timesteps
            for k, v in self.todo_dict.items():
                for j in range(v['n']):
                    newmean.append(float('mul' in self.todo_dict[k]['method'])) 
        else:
            # The ensemble has been optimised, which means the first member is the mean
            # Note that here we return a list of arrays
            parameters = self.ensemble_members[0][0]
            for j in range(self.all_parameters):
                start, end = j * self.nparameters, (j+1) * self.nparameters
                newmean.append(parameters[start: end])
        return newmean

    def make_gridmap(self):
        # Hardcode for now
        df = pd.read_csv('/home/awoude/notebooks/CTSF_GCP/latlongrid_flat_global.csv')

        self.latgrid_flat = df['latgrid_flat'].values[:NPARAMS_TEST]
        self.longrid_flat = df['longrid_flat'].values[:NPARAMS_TEST]

        self.lats = np.linspace(-89.5, 89.5, 180)
        self.lons = np.linspace(-179.5, 179.5, 360)

    def setup_members(self):
        self.ensemble_members = list(range(self.nlag))
        for n in range(self.nlag):
            self.ensemble_members[n] = []

            
    def vector2grid(self, vectordata, nparams=None):
        """Method to project the data to a grid. Make a dimension for every parameter
        Returns a 3680x180 grid with the data on it"""
        #if nparams is None:
        #    nparams = self.nbasefitparams
        
        assert vectordata.shape[-1] == self.longrid_flat.shape[-1], \
               f'Vectordata ({vectordata.shape[-1]}) not same shape as the latgrid ({self.longrid_flat.shape[-1]})'
        
        grid = np.zeros((len(self.lats), len(self.lons)))
        for i, (lo, la) in enumerate(zip(self.longrid_flat, self.latgrid_flat)):
            lo_idx = np.argmin(abs(lo - self.lons))
            la_idx = np.argmin(abs(la - self.lats))
            grid[la_idx, lo_idx] = vectordata[i]
        
        return np.array(grid)

    @staticmethod
    def block_diag(arrs):
        """Taken from scipy linalg block_diag.
        Stack arrays of different sizes to a single block matrix"""
        shapes = np.array([a.shape for a in arrs])
        out = np.zeros(np.sum(shapes, axis=0), dtype=arrs[0].dtype)

        r, c = 0, 0
        for i, (rr, cc) in enumerate(shapes):
            out[r:r + rr, c:c + cc] = arrs[i]
            r += rr
            c += cc
        return out
    
    def get_covariance(self, cov_matrix_file):
        """Get the covariance matrix
        Returns a covariance matrix (block matrix of NPARAMS x NPARAMS)
        """
        # Hardcode matrix location for now:
        logging.info(f'Reading covariancematrix from file {cov_matrix_file}')
        with nc.Dataset(cov_matrix_file) as ds:
            covmatrix = ds['covariance'][:][:NPARAMS_TEST, :NPARAMS_TEST]
        return covmatrix

    def draw_random_numbers(self):
        """The random numbers in this statevector should correlate thuough time
        Therefore, this method draws random numbers based on the
        self.how_to_draw_rands attribute
        Note that january should correlate as much with february
        as with december, complicating matters.
        Input:
            None
        Output:
            np.array: random numbers of lenght NPARAMS"""

        allrands = np.array([])
        for r, rands in self.how_to_draw_rands:
            # Make a second random vector
            # It will be used to make it cyclical
            rands2 = [None] * len(rands)
            rands_cyclical = [None] * len(rands)
            # First, draw a white noise number
            # note: self.draw_white_noise exepcts a tuple!
            for i in range(len(rands)):
                if r == 0:
                    r = 1e-3 # Very small r, so no correlation.
                    # If r == 0: no correlation (default)
                rands[i] = self.draw_red_noise(r, rands[i -1 ])
                # The second random vector will be 'backward' in time
                # So it should be based on the first draw of the forward in time
                # only for the first timestep.
                if i == 0:
                    z = rands[i]
                else:
                    z = rands2[i -1 ]
                rands2[i] = self.draw_red_noise(r, z)
            # Flip the random numbers, to make the mback in time        
            rands2 = rands2[::-1]
            # For all pair of forward-backward numbers, 'average' them, based on
            # the distance to their beginpoint
            for i, (r1, r2) in enumerate(zip(rands, rands2)):
                R1 = r ** i
                R2 = r ** abs(i - (len(rands) - 1))
                # This equation is similar to the red-noise draw
                rands_cyclical[i] = (1 - np.sqrt(1 - R1)) * r1 + (1 - np.sqrt(1 - R2)) * r2
            # Now that we have random numbers, we can append them to a list of all random numbers
            allrands = np.append(allrands, rands_cyclical)
        return allrands

    def draw_red_noise(self, r, z=None):
        """Draw red noise parameters
        Input: 
            r: float: red noise decay factor
            z: np.array: previous timestep (red) noise
        Output:
            red_noise: np.array of shape equal to shape(z) with red noise"""
        
        if z is None:
            oldnoise = self.draw_white_noise((self.nparameters,))
        else:
            oldnoise = z
        newnoise = self.draw_white_noise(oldnoise.shape) # New white noise
        return r * oldnoise + np.sqrt(1 - r**2) * newnoise
        
    @staticmethod
    def draw_white_noise(shape):
        """Draw white noise parameters
        Input: 
            shape: tuple of int: shape to draw noise for
        Output: 
            np.array of shape <shape>: white noise"""
        return np.random.randn(*shape)

    def make_new_ensemble(self, calculate_dof=False):
        """make and fill a new ensemble"""
        logging.debug('Creating and filling ensemble members...')
        if calculate_dof:
            logging.info(f'Approximate DOF = {self.calc_dof(np.dot(self.C, self.C.T))}')

        newmean = self.create_new_mean()

        for i in range(0, self.nmembers):
            # Calculate red-noise deviations on mean
            with threadpool_limits(limits=1, user_api='blas'):
                if i == 0:
                    # Member 0 does not have deviations
                    rands = np.zeros(self.nparams)
                else:
                    rands = self.draw_random_numbers()

            # initialise the new member and fill with the drawn random numbers
            new_member = EnsembleMember(i)
            param_values = np.zeros_like(rands)
            for j in range(self.all_parameters):
                mean = newmean[j]
                start, end = j * self.nparameters, (j+1) * self.nparameters
                param_values[start: end] = np.dot(self.C[start: end, start: end], rands[start: end]) + mean
            new_member.param_values = param_values # Add one to have a percentage increase or decrease
            logging.debug(f'Adding {len(new_member.param_values)} new parameters to statevector')
            self.ensemble_members[0].append(new_member) # We don't have lag, so append to the 0th lag

        logging.warning('The new mean is not propagated properly!')
        logging.info(
            'Successfully constructed a deviation matrix from covariance structure')

    def get_start_end_indices(self, itemdict, t):
        """get the start and end indices within the statevector for the current timestep
        Input: 
            itemdict: dict: contains information about the current variable
            t: int: current time index
        Output:
            startindex, endindex: tuple of int: start and end indices for the current fuction and time"""

        N = itemdict['n']
        startindex = itemdict['start_idx'] + (self.nparameters * (t % N))
        endindex = startindex + self.nparameters
        return startindex, endindex

    def calculate_long_term_mean(self, param_values, itemdict, t):
        """Calculate the anomalies due to temperature
        Input:
            param_values: np.array: statevector elements
            itemdict: dict: contains information on the current variable
            t: int: current time index
        Output:
            np.array: anomaly parametermap"""
        startindex, endindex = self.get_start_end_indices(itemdict, t)
        elements = param_values[startindex: endindex]
        #logging.debug(f'Long term mean: parameters start at {startindex}: {endindex}')
        return self.vector2grid(elements)

    def calculate_temperature_anomalies(self, param_values, itemdict, t):
        """Calculate the anomalies due to temperature
        Input: 
            param_values: np.array: statevector elements
            itemdict: dict: contains information on the current variable
            t: int: current time index
        Output:
            np.array: anomaly parametermap"""
        # Check if the temperature anomalies are already read-in 
        if not hasattr(self, 'temperature_anomalies'):
            filename = itemdict['inputfile']
            varname = itemdict['varname']
            logging.debug(f'Building anomalies for temperature from file {filename}')
            self.temperature_anomalies = self.get_anomalies(filename, varname, self.timevec)
            #logging.debug(self.temperature_anomalies.shape)

        #logging.debug(f'Using temperature anomalies from index {t}')

        # we have one new set of parameters for very 'n' timesteps.
        startindex, endindex = self.get_start_end_indices(itemdict, t)
        elements = param_values[startindex: endindex]
        #logging.debug(f'Termperature anomalies: parameters start at {startindex}: {endindex}')
        return self.vector2grid(elements) * self.temperature_anomalies[t]

    def calculate_long_term_trend(self, param_values, itemdict, t):
        """Calculate scaling factors for a trend in respiration fluxes"""
        startindex, endindex = self.get_start_end_indices(itemdict, t)
        elements = param_values[startindex: endindex]
        return self.vector2grid(param_values)[None, :, :] * self.timevec[:, None, None]

    def calculate(self, tododict_item, member, n):
        """evaluate a function with parameters from counter 1 to counter 2"""
        res = eval(f'self.{tododict_item["function"]}(member.param_values, tododict_item, n)')
        return res


    def apply_param_functions(self, tododict_item, member='all'):
        """Apply the given function, over all statevectorelements for all times
        Input:
            todo_dict_item: dict:
                contains the number of factors per year
                the function that should be called
                whether the function is time-sensitive
        Output:
            np.array: gridded result of the evaluated function for each member"""
        N_ELEMENTS_PER_PARAM = len(self.latgrid_flat)
        # Initialise a counter that keeps track of what statevectorelements we are using
        if not hasattr(self, 'startcounter'):
            self.startcounter = 0
        elif self.startcounter == self.nparams:
            # Means we have had one full cycle already
            # e.g. while writing savestate or parametermaps.
            # In this case, we should start at 0 as well.
            self.startcounter = 0

        if member == 'all':
            out = np.zeros((self.nmembers, len(self.timevec), 180, 360))
            for n in range(len(self.timevec)):
                for member in self.ensemble_members[0]: # 0th lag: we don't use lag
                    out[member.membernumber, n, :, :] = self.calculate(tododict_item, member, n)
        else:
            out = np.zeros((len(self.timevec), 180, 360))
            for n in range(len(self.timevec)):
                out[n, :, :] = self.calculate(tododict_item, member, n)
        return out

    def write_to_file(self, file, qual):
        """Write the statevector values to a 'savestate' file, which can be used
        to transfer data between cycles, or for analysis.
        Input:
            file: str: path to file (full filename). Given by the dacycle
            qual: str <prior|opt>. prior or optimised statevector
        Output:
            None, a file is filled with member values
        """
        # We first do what default does
        f, dimparams, dimmembers, dimlag, filename = super(ctsfStateVector, 
                                                            self).write_to_file(file, 
                                                                                qual, 
                                                                                return_file=True)
        dimgrid = f.add_latlon_dim()
        dimtime = f.add_dim('time', len(self.timevec))
        savedict = io.std_savedict.copy()

        dimsdone = []
        for i, (param, v) in enumerate(self.todo_dict.items()):
            savedict['dims'] = dimmembers + dimtime +  dimgrid
            logging.info(savedict['dims'])
            logging.debug(f'Writing mean {param} to file')
            data = self.apply_param_functions(v)
            savedict['name'] = param + '_' + qual
            savedict['long_name'] = f'Long-term mean scaling factor on {param}'
            savedict['values'] = data
            logging.info(data.shape)
            savedict['units'] = '[-]'
            f.add_data(savedict)

        f.close()

    def get_paramter_apply_method(self, dict_item):
        """Method that checks wether a parameter is multiplicative or additive
        This way, additive, multiplicative result in the same method as 
        add and mul, respectively"""
        if 'add' in dict_item['method'].lower():
            return 'add'
        elif 'mul' in dict_item['method'].lower():
            return 'mul'
        else:
            raise ValueError(f'{dict_item["method"]} Not recognised, should by any of <mul | add>')

    def write_members_to_file(self, lag, outdir, endswith='.nc', obsoperator=None):
        """Write members to the parameters.xxx.nc files. We use the parents' parent method
        because that already does the job
        Input: 
            lag: int: lag of current cycle. We always use 0, so unused
            outdir: str: path to write the files to. Given by the dacycle
            endswith: str: suffix of written files. Defaults to .nc
            obsoperator: object: observationoperator. Unused, defaults to None
        Output: 
            None, one file per member is written to the outdir
        """
        members = self.ensemble_members[lag]

        for mem in members:
            filename = os.path.join(outdir, 'parameters.%03d%s' % (mem.membernumber, endswith))
            ncf = io.CT_CDF(filename, method='create')
            dimparams = ncf.add_params_dim(self.nparams)
            dimgrid = ncf.add_latlon_dim()
            dimtime = ncf.add_dim('time', len(self.timevec))

            data = mem.param_values

            savedict = io.std_savedict.copy()
            savedict['name'] = "parametervalues"
            savedict['long_name'] = "parameter_values_for_member_%d" % mem.membernumber
            savedict['units'] = "unitless"
            savedict['dims'] = dimparams
            savedict['values'] = data
            savedict['comment'] = 'These are parameter values to use for member %d' % mem.membernumber
            ncf.add_data(savedict)

            savedict = io.std_savedict.copy()
            savedict['dims'] = dimtime +  dimgrid
            savedict['comment'] = 'These are gridded parameter values to use for member %d' % mem.membernumber
            dimsdone = []
            parametermap = {}
            parametercount = {}
            for i, (param, v) in enumerate(self.todo_dict.items()):
                data = self.apply_param_functions(v, member=mem)
                method = self.get_paramter_apply_method(v)
                if not method in parametermap.keys():
                    # Means we are at the first parameter to check. 
                    parametermap[method] = data.copy()
                    parametercount[method] = 0
                else:
                    # Means the parametermap for this member is already created
                    parametermap[method] += data
                parametercount[method] += 1
            for method, data in parametermap.items():
                #logging.debug(method)
                #logging.debug(f'len(timevec): {len(self.timevec)}')
                #logging.debug(f'dims: {dimtime +  dimgrid}')
                savedict['dims'] = dimtime + dimgrid
                savedict['name'] =  f'parametermap_{method}'
                data /= parametercount[method] # Make average
                logging.info(f'data.shape(): {data.shape}')
                savedict['long_name'] = f'Long-term mean scaling factor ({method})'
                savedict['values'] = np.nan_to_num(data)
                savedict['units'] = '[-]'
                ncf.add_data(savedict)
    
            ncf.close()

            logging.debug('Successfully wrote data from ensemble member %d to file (%s) ' % (mem.membernumber, filename))


    @staticmethod
    def calculate_dof(covariancematrix):
        """Method that calculates the degrees of freedom
        from a given covariancematrix
        Input:
            covariancematrix: np.array: covariancematrix (NxN)
        Output:
            float: the amount of degrees of freedom"""
        logging.debug('Using eigenvalues to calculate the DOF')

        s = np.linalg.eigh(covariancematrix)[0]
        dof = np.sum(s) ** 2 / np.sum(s ** 2)
        return dof
