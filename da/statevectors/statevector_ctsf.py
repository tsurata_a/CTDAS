"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python

from __future__ import division

import os
import sys
sys.path.append(os.getcwd())

import logging
import numpy as np
import datetime as dt
from dateutil.relativedelta import relativedelta
import da.tools.io4 as io
from da.tools.general import to_datetime
#from da.baseclasses.statevector import StateVector, EnsembleMember
from da.statevectors.statevector_baseclass import StateVector, EnsembleMember
import netCDF4 as nc

identifier = 'CarbonTracker Statistical Fit Statevector'
version = '0.0'

################### Start Class CTSFStateVector ###################


class ctsfStateVector(StateVector):

    """
    This is a StateVector object for CarbonTracker. It is created for use with the SIF inversion approach,
    and contains statistical parameters and SIF sensitivity parameters per ecoregion to be assembled.
    It has a private method to make new ensemble members.
    """

    def setup(self, dacycle):
        """
        setup the object by specifying the dimensions.
        There are two major requirements for each statvector that you want to build:
            (1) is that the statevector can map itself onto a regular grid
            (2) is that the statevector can map itself (mean+covariance) onto TransCom regions
        An example is given below.
        """
        self.dacycle = dacycle

        # JJDH used to be sample start
        # Yet, in ctsf inversions their is no difference
        startdate = self.dacycle['time.start']
        enddate = self.dacycle['time.end']

        logging.info('Calculating fluxes from %s to %s' % (startdate, enddate))

        self.isOptimized = False

        self.refdate             = to_datetime(dacycle.dasystem['time.reference'])

        self.resolution = dacycle.dasystem['fit.resolution']
        self.datevec = self.calc_datevec(startdate, enddate,self.resolution)
        timevec = []
        for date in self.datevec:
            timevec.append( (date-self.refdate).days )
        self.timevec = np.array(timevec)
        logging.debug('Timevec = %s' % timevec)

        self.nlat = 180
        self.nlon = 360

        self.nlag = int(dacycle['time.nlag'])
        # number of ensemble members
        self.nmembers = int(dacycle['da.optimizer.nmembers'])

        # if True: initial fit will be constructed based on latitude, else:
        # read from file
        self.unit_initialization = dacycle.dasystem['fit.unit_initialization']
        self.refdate = to_datetime(dacycle.dasystem['time.reference'])
        # write gridded fluxes for every member? [bool]
        writememberfluxes = self.dacycle.dasystem['writeMemberFluxes']

        logging.info('Read parameters for flux calculation')
        # fit settings
        # number of harmonical terms in baseline fit
        self.nharmonics = int(dacycle.dasystem['nharmonics'])
        self.npolyterms = int(dacycle.dasystem['npolyterms'])
        self.grow_amplitude = dacycle.dasystem['grow_amplitude']
        logging.debug('Using {} harmonic parameters'.format(self.nharmonics))
        logging.debug('Using {} polynomial terms'.format(self.npolyterms))
        self.nbasefitparams = self.npolyterms + self.nharmonics * 2 + self.grow_amplitude
        logging.debug('Resulting in {} parameters ({}+ 2 * {} + {})'.format(
            self.nbasefitparams, self.npolyterms, self.nharmonics, self.grow_amplitude))

        self.nobs = 0
        # empty containter to hold observations to assimilate later on
        self.obs_to_assimilate = ()

        # initialization of fit parameters
        self.initialise_fit_params()
        self.nfitparams = self.nbasefitparams + self.ngamma# + self.nresp

        self.create_covmatrix()

        self.make_gridmap()

        self.nee_data = np.zeros((self.nmembers, 
                                  self.nregions,
                                  len(self.timevec),))

        with nc.Dataset(dacycle.dasystem['file.spatial.distribution']) as ds:
            self.spatial_nee_distribution = ds[dacycle.dasystem['var.spatial.distribution']][:]

        # These list objects hold the data for each time step of lag in the system. Note that the ensembles for each time step consist
        # of lists of EnsembleMember objects, we define member 0 as the mean of
        # the distribution and n=1,...,nmembers as the spread.
        self.ensemble_members = list(range(self.nlag))
        for n in range(self.nlag):
            self.ensemble_members[n] = []

    def initialise_fit_params(self):
        # initialization of fit parameters
        # if True: initial fit will be constructed based on latitude, else:
        # read from file
        self.unit_initialization = self.dacycle.dasystem['fit.unit_initialization']

        if self.unit_initialization:
            # construct a 'unit fit' from parameters in rc file

            # coefficients polynomial part: p0 + p1*t + p2*t^2
            polynomial = self.dacycle.dasystem['polynomial'].split(',')
            self.polynomial = np.asarray([float(i) for i in polynomial])

            # initialize amplitude of first harmonics
            amplitude = self.dacycle.dasystem['amplitude'].split(',')
            if amplitude[0] == 'None':
                self.amplitude = [None, float(amplitude[1])]
                amplitude_polyfit = self.dacycle.dasystem['amplitude_polyfit'].split(
                    ',')
                self.amplitude_polyfit = np.asarray(
                    [float(i) for i in amplitude_polyfit])
                logging.debug(
                    'Amplitude of first harmonic will be initialized as %s-th order polynomial function with coefficients (increasing order): %s' %
                    (len(
                        self.amplitude_polyfit) -
                        1,
                        self.amplitude_polyfit))
            else:
                self.amplitude = [float(amplitude[0]), float(amplitude[1])]

        if not self.unit_initialization:
            # else: Read fit parameters from file
            self.initparamsfile = self.dacycle.dasystem['params.init.file']
            # True: directly read statevector, False: extract statevector from
            # gridded field
            self.read_initparamsvec = self.dacycle.dasystem['params.init.vec']
            self.initparamsname = self.dacycle.dasystem['params.init.name']

        # anomaly term
        self.add_ano = self.dacycle.dasystem['add.anomalies']
        if self.add_ano:
            # number of sensitivity parameters per ecoregion: default = 12, 1
            # per calendar month
            self.ngamma = int(self.dacycle.dasystem['ngamma'])
            self.anofile = self.dacycle.dasystem['fit.anomaly.inputfile']
            # name of anomaly in inputfile
            self.anoname = self.dacycle.dasystem['fit.anomaly.name']
            self.read_gamma0 = self.dacycle.dasystem['params.init.gamma.read']
            if self.unit_initialization or not self.read_gamma0:
                # enter single value, or comma-separated list of ngamma
                # (default: one for each month)
                self.gamma0 = self.dacycle.dasystem['params.init.gamma'].split(',')
            else:
                self.initgammafile = self.dacycle.dasystem['params.init.gamma.file']
                self.initgammaname = self.dacycle.dasystem['params.init.gamma.name']

            self.readanostd = self.dacycle.dasystem['fit.anomaly.read.std']
            if self.readanostd:
                self.anostdfile = self.dacycle.dasystem['fit.anomaly.std.file']
                self.anostdname = self.dacycle.dasystem['fit.anomaly.std.name']
        else:
            self.ngamma = 0

    def create_covmatrix(self):
        # covariance matrix settings
        # if True: a covariance matrix will be read from file for each
        # ecoregion,
        self.read_cov_mat = self.dacycle.dasystem['fit.read_cov_matrices']
        # else, construct diag. matrix from uncertainty
        # self.cov_mat_name = dacycle.dasystem['fit.cov_matrices_name'] if
        if self.read_cov_mat:
            self.cov_mat_file = self.dacycle.dasystem['fit.cov_matrices_file']
            self.cov_mat_name = self.dacycle.dasystem['fit.cov_matrices_name']
            self.cov_scaling = float(
                self.dacycle.dasystem['fit.cov_scaling']) if self.dacycle.dasystem['fit.cov_scaling'] else 1


    def make_gridmap(self):
        # This specifies the file to read with the gridded mask at 1x1 degrees. Each gridbox holds a number that specifies the ecoregion it belongs to.
        # From this map, a dictionary is created that allows a reverse look-up
        # so that we can map parameters to a grid.
        mapfile = os.path.join(self.dacycle.dasystem['regionsfile'])
        ncf = io.ct_read(mapfile, 'read')
        # read ecoregions map (nlat x nlon)
        self.gridmap = ncf.get_variable('regions')
        # read transcom regions map (nlat x nlon)
        self.tcmap = ncf.get_variable('transcom_regions')
        olsonmap = ncf.get_variable('land_ecosystems')
        if self.unit_initialization:
            if self.amplitude[0] is None:
                areas = ncf.get_variable('grid_cell_area')
                latvec = ncf.get_variable('lat')
                latgrid = np.ones(areas.shape) * latvec[:, np.newaxis]
        ncf.close()

        logging.debug(
            "A TransCom  map on 1x1 degree was read from file %s" %
            self.dacycle.dasystem['regionsfile'])
        logging.debug(
            "An ecoregion parameter map on 1x1 degree was read from file %s" %
            self.dacycle.dasystem['regionsfile'])

        # Create a dictionary for state <-> gridded map conversions (keys = regionnumbers)
        # And calculate mean latitude per ecoregion (only if self.amplitude[0]
        # = None)
        regnums = []
        self.griddict = {}
        lat_regions = []

        for i in range(209):
            sel = np.where(self.gridmap == i + 1)
            if len(sel[0]) > 0 and np.mean(olsonmap[sel] < 19):
                regnums.append(i + 1)
                self.griddict[i + 1] = sel
                if (self.unit_initialization) and (self.amplitude[0] is None):
                    lat_regions.append(np.average(latgrid[sel],
                                                  weights=areas[sel]))

        if self.unit_initialization:
            if self.amplitude[0] is None and len(lat_regions) != len(regnums):
                logging.error(
                    'Inconsistent number of elements in lists with regional latitudes (%s) and with regionnumbers (%s)' %
                    (len(lat_regions), len(regnums)))
                raise IOError
            elif self.amplitude[0] is not None and len(lat_regions) != 0:
                logging.error(
                    'average latitudes per ecoregion calculated, but amplitudes will not be initialized with latitude function...')
                raise IOError

        self.regionnumbers = np.asarray(regnums)
        self.nregions = len(self.regionnumbers)
        # number of elements in statevector
        self.nparams = self.nregions * self.nfitparams
        if self.unit_initialization:
            if self.amplitude[0] is None:
                # average latitude per ecoregion
                self.lat_regions = np.asarray(lat_regions)

        logging.debug('There are %s non-empty ecoregions: %s' % (self.nregions, self.regionnumbers))
        logging.info("Updating with dacycle.dasystem['nparameters']=%d to be used by the optimizer" %
                     self.nparams)
        self.dacycle.dasystem['nparameters'] = self.nparams

        # Create a matrix for state <-> TransCom conversions
        self.tcmatrix = np.zeros((self.nparams, 23), 'float')

        for r in range(self.nregions):
            er = self.regionnumbers[r]
            sel = (self.gridmap.flat == er).nonzero()
            if len(sel[0]) < 1:
                continue
            else:
                n_tc = set(self.tcmap.flatten().take(sel[0]))
                if len(n_tc) > 1:
                    logging.error(
                        "Parametergroup %d seems to map to multiple TransCom regions (%s), I do not know how to handle this" %
                        (er, n_tc))
                    raise ValueError
                self.tcmatrix[r * self.nfitparams:(r + 1) * self.nfitparams, int(n_tc.pop()) - 1] = 1.0

        logging.debug(
            "A matrix to map states to TransCom regions and vice versa was created")
        self.nee_data = np.zeros((self.nmembers, 
                                  self.nregions,
                                  len(self.timevec)))

        # Create a mask for species/unknowns
        self.make_species_mask()
        # Validate the requested input files
        self.validate_input()

        return

    def write_to_file(self, file, qual):
        """ do whatever super does
        then also add nee dter and dgpp sets
        """
        # file, dim of nparams ,nmembers, dimlag
        f, dimparams, dimmembers, dimlag, filename = super().write_to_file(file,
                                                                           qual, return_file=True)
        dimt = f.add_dim('date', len(self.timevec))
        dimgrid = f.add_latlon_dim()
        if not qual == 'opt':
            savedict = io.std_savedict.copy()
            savedict['name'] = 'date'
            savedict['long_name'] = 'date as days since 1-1-2000'
            savedict['dims'] = dimt
            savedict['units'] = 'days since 1-1-2000'
            savedict['values'] = self.timevec
            f.add_data(savedict)
        # note we have no lag here

        dimregions = f.add_dim('regions', self.nregions)
        savedict = io.std_savedict.copy()
        savedict['name'] = 'NEE_' + qual
        savedict['long_name'] = ('Simulated NEE values per ecoregion')
        savedict['dims'] = dimmembers + dimregions + dimt
        savedict['values'] = self.nee_data.tolist()
        savedict['units'] = 'mol/m2/s'
        f.add_data(savedict)

        f.close()

        logging.info(
            'Successfully wrote the State Vector to file (%s) ' %
            filename)
        return

    def read_from_file(self, filename, qual='opt'):
        """ Read initial data for the statevector from given files
        """
        super().read_from_file(filename, qual)
        f = io.ct_read(filename, 'read')
        self.nee_data = f.get_variable('NEE_' + qual)
        f.close()
        logging.info(
            'Successfully read the fluxmodel data from file (%s) ' %
            filename)

    def state_to_grid(
            self,
            fluxvector=None,
            lag=1,
            counter=None,
            filename=None,
            qual=None):
        # used to write statevector in grid format for coupling with TM5 (where actual fluxes are calculated)
        # implemented such that variables with dimensions nfitparams x nlat x
        # nlon are created
        """
            Transforms the StateVector information (mean + covariance) to a 1x1 degree grid.


            :param: fluxvector: a vector of length (nparams,) that holds the fluxes associated with each parameter in the StateVector
            :param: lag: the lag at which to evaluate the StateVector
            :rtype: a tuple of two arrays (gridmean,gridvariance) with dimensions (nfitparams,180,360)
            counter will fetch the fluxvalues for month=counter

            If the attribute `fluxvector` is not passed, the function will return the mean parameter value and its variance on a 1x1 map.

            ..note:: Although we can return the variance information for each gridbox, the covariance information contained in the original ensemble is lost when mapping to 1x1 degree!
        """

        if fluxvector is None:
            fluxvector = np.ones(self.nparams)
        else:
            logging.error(
                "ctsfStateVector cannot handle state_to_grid with fluxvector input")
            raise ValueError

        if filename is None:
            ensemble = self.ensemble_members[lag - 1]
            ensemblemean = ensemble[0].param_values

            # First transform the mean
            gridmean = self.vector2grid(vectordata=ensemblemean * fluxvector)

            # And now the covariance, first create covariance matrix (!), and then multiply
            deviations = np.array([mem.param_values * fluxvector - ensemblemean for mem in ensemble])
            ensemble   = np.array([self.vector2grid(mem) for mem in deviations])
            return (gridmean,ensemble)

        else:
            f = io.ct_read(filename, 'read')
            if self.resolution == 'month':
                nee_data = f.get_variable('NEE_' + qual)[:,:,monthcounter]
                if self.add_ano:
                    dter_data=np.zeros((self.nmembers,self.nlat,self.nlon,),float)
                    mask=[(currentdate.month==t.month)&(currentdate.year==t.year) for t in self.datevec]
                    mean=f.get_variable('statevectormean_'+ qual)[lag-1,:] 
                    ensemble=f.get_variable('statevectorensemble_'+ qual)[lag-1,:,:] 
                    for nr in [member.membernumber for member in self.ensemble_members[lag - 1]]:
                        dter_data[nr,:,:]=self.calculate_anoterm((mean+ensemble[nr,:]).reshape(self.nregions, self.nparams_per_region),self.timevec[mask])
            elif self.resolution == 'day':
                currentdate = self.datevec[0] + relativedelta(months=monthcounter)
                mask = [(currentdate.month == t.month) & (currentdate.year == t.year) for t in self.datevec]
                logging.debug("averaging for month %s with %d days for %s" % (currentdate,sum(mask),qual))
                nee_data = np.average(f.get_variable('NEE_' + qual)[:,:,mask],axis=2)
                if self.add_ano:
                    dter_data=np.zeros((self.nmembers,self.nlat,self.nlon,),float)
                    mean=f.get_variable('statevectormean_'+ qual)[lag-1,:] 
                    ensemble=f.get_variable('statevectorensemble_'+ qual)[lag-1,:,:] 
                    for nr in [member.membernumber for member in self.ensemble_members[lag - 1]]:
                        dter_data[nr,:,:]=np.average(self.calculate_anoterm((mean+ensemble[nr,:]).reshape(self.nregions, self.nparams_per_region),self.timevec[mask]),axis=0)

            f.close()
            # Nee data on members by nregions 
            # ensemble mean ndim (regions)
            ensemblemean=nee_data[0][:]
            deviations= np.array([nee[:] - ensemblemean for nee in nee_data])
            gridmean = self.vector2grid(vectordata=ensemblemean,nparams=1)
            ensemble = np.array([self.vector2grid(vectordata=nee_data,nparams=1) for nee_data in deviations])

            if self.add_ano:
                # Create mask for the time elments of interest 
                gridmean+=dter_data[0]
                ensemble=ensemble-dter_data[:]-dter_data[0]
                gridmean_ano=dter_data[0]
                ensemble_ano=np.array([ano-gridmean_ano for ano in dter_data])
                return (gridmean,ensemble,gridmean_ano,ensemble_ano)
            else:
                return (gridmean,ensemble,np.zeros(np.shape(gridmean),float),np.zeros(np.shape(ensemble),float))


    def vector2grid(self, vectordata=None, nparams="Default"):
        """
            Map vector elements to a map or vice cersa

           :param vectordata: a vector dataset to use in case `reverse = False`. This dataset is mapped onto a 1x1 grid and must be of length `nparams`
           :rtype: ndarray: an array of size (nfitparams,360,180)

           This method makes use of a dictionary that links every parameter number [1,...,nparams] to a series of gridindices. These
           indices specify a location on a 360x180 array, stretched into a vector using `array.flat`. There are multiple ways of calling
           this method::

               griddedarray = self.vector2grid(vectordata=param_values) # simply puts the param_values onto a (180,360,) array

           .. note:: This method uses a DaSystem object that must be initialzied with a proper parameter map. See :class:`~da.baseclasses.dasystem` for details
        """

        if nparams == "Default":
            nparams = self.nfitparams
        result = np.zeros((nparams, self.gridmap.shape[0], self.gridmap.shape[1]))

        for k, data in enumerate(vectordata):
            # works only if nparam>1
            if nparams > 1:
                r = k // nparams
                i = k % nparams
                regnum = self.regionnumbers[r]
            else:
                regnum = self.regionnumbers[k]
                i = 0 # No parameter dimension
            if regnum in self.griddict:
                result[i, self.griddict[regnum][0],
                       self.griddict[regnum][1]] = data

        return np.squeeze(result) # Remove dimensions with 1 value (1 parameter only)

    def get_covariance(self, *args):
        """
        Read covariance matrix from file
        """

        # read uncertainties for NEE (or GPP and TER) vs proxy correlation
        # coefficients from rc file
        if self.add_ano:
            unc_gamma = float(self.dacycle.dasystem['bio.cov.gamma'])

        # Read covariance matrices fit parameters
        logging.info(
            'Reading covariance matrices for each ecoregion from file %s' %
            self.cov_mat_file)
        f_in = io.ct_read(self.cov_mat_file, method='read')
        cov_fit = f_in.get_variable(
            self.cov_mat_name)  # 'parameter_covariance')
        f_in.close()

        if self.add_ano and self.readanostd:
            logging.info(
                'Reading anomaly covariance matrices for each ecoregion from file %s' %
                self.anostdfile)
            anostd_in = io.ct_read(self.anostdfile, method='read')
            cov_ano = anostd_in.get_variable(self.anostdname)
            anostd_in.close()

        cov_mat_list = []
        for i, r in enumerate(self.regionnumbers):

            # base fit covariance
            regnrs = range(
                i * self.nfitparams,
                i * self.nfitparams + self.nbasefitparams)
            cov_mat_list.append([cov_fit[:, :, r - 1], regnrs])

            if self.add_ano:
                regnrs = range(
                    i * self.nfitparams + self.nbasefitparams,
                    (i + 1) * self.nfitparams)
                diagvec = np.zeros(self.ngamma)
                #diagvec = np.zeros(self.ngamma + self.nresp)
                if self.readanostd:
                    diagvec[:self.ngamma] = cov_ano[:, r - 1]**2
                    logging.debug('r = %s, diagvec = %s' %
                                  (r, diagvec[:self.ngamma]))
                else:
                    diagvec[:self.ngamma] = (unc_gamma)**2
            cov_mat_list.append([np.diag(diagvec), regnrs])

        return cov_mat_list

    def make_new_ensemble(self, lag, covariancematrixlist=[None]):
        """
        :param lag: an integer indicating the time step in the lag order
        :param covariancematrix: a list of matrices specifying the covariance distribution to draw from
        :rtype: None

        Make a new ensemble, the attribute lag refers to the position in the state vector.
        Note that lag=1 means an index of 0 in python, hence the notation lag-1 in the indexing below.
        The argument is thus referring to the lagged state vector as [1,2,3,4,5,..., nlag]

        The covariance list object to be passed holds a list of matrices with a total number of dimensions [nparams, nparams], which is
        used to draw ensemblemembers from. Each draw is done on a matrix from the list, to make the computational burden smaller when
        the StateVector nparams becomes very large.

        """

        if not isinstance(covariancematrixlist, list):
            logging.error(
                "The covariance matrix or matrices must be passed as a list of array objects, exiting...")
            raise ValueError

        logging.debug(
            'len(covariancematrixlist) = %s' %
            len(covariancematrixlist))

        # Check dimensions of covariance matrix list, must add up to nparams
        dims = 0
        for matrix, parnr in covariancematrixlist:
            dims += matrix.shape[0]

        if dims != self.nparams:
            logging.error(
                "The total dimension of the covariance matrices passed (%d) does not add up to the prescribed nparams (%d), exiting..." %
                (dims, self.nparams))
            raise ValueError

        # Loop over list of covariance matrices and their statevector parameter
        # numbers, and create a matrix of (nparams,nmembers) with the
        # deviations
        dof = 0.0
        dev_matrix = np.zeros((self.nparams, self.nmembers,))
        randstate = np.random.get_state()

        for matrix, parnr in covariancematrixlist:

            # Make a cholesky decomposition of the covariance matrix
            s = np.linalg.svd(matrix, compute_uv=False)
            dof += np.sum(s) ** 2 / sum(s ** 2)

            try:
                C = np.linalg.cholesky(matrix)
            except (np.linalg.linalg.LinAlgError, err):
                logging.error('Cholesky decomposition has failed ')
                logging.error(
                    'For a matrix with parnr %s and of dimensions: %d' %
                    (parnr, matrix.shape[0]))
                logging.debug(err)
                raise np.linalg.linalg.LinAlgError

            # Draw nmembers instances of this distribution
            npoints = matrix.shape[0]

            istart = parnr[0]
            istop = parnr[-1] + 1

            for member in range(1, self.nmembers):
                rands = np.random.randn(npoints)
                deviations = np.dot(C, rands)
                dev_matrix[istart:istop, member - 1] = deviations

            logging.debug(
                "Inserted prior covariance matrix %d elements into statevector location %d through %d" %
                (len(parnr), istart, istop))

        logging.debug(
            'Successfully constructed a deviation matrix from covariance structure')
        logging.info(
            'Appr. degrees of freedom in full covariance matrix is %s' %
            (int(dof)))

        # Create mean values
        new_mean = self.create_mean_statevector()

        # Now fill the ensemble members with the deviations we have just created
        # Create members 1:nmembers and add to ensemble_members list
        for member in range(self.nmembers):
            new_member = EnsembleMember(member)
            if member == 0:
                # Create the first ensemble member with a deviation of 0.0 and
                # add to list
                new_member.param_values = new_mean.flatten()  # no deviations
            else:
                new_member.param_values = dev_matrix[:, member - 1] + new_mean
            self.ensemble_members[lag].append(new_member)

            self.nee_data[member, :, :] = self.evaluate_model(
                new_member.param_values.reshape((self.nregions, self.nfitparams)), self.timevec)
        logging.debug(
            '%d new ensemble members were added to the state vector # %d' %
            (self.nmembers, (lag + 1)))

        return

    def calc_datevec(self, startdate, enddate, resolution):
        """Creates time vector with monthly interval ranging from startdate to enddate."""

        datevec = []
        ndays = (enddate - startdate).days

        # use actual date for forward run with small (weekly) cycle length
        if ndays <= 30:
            datevec.append(startdate + dt.timedelta(days=int(round(ndays/2)-1)))
            return datevec

        elif resolution=='month':
            date = dt.datetime(startdate.year,startdate.month,15)
            while date <= enddate:
                datevec.append(date)
                if date.month < 12:
                    date = dt.datetime(date.year, date.month + 1, 15)
                else:
                    date = dt.datetime(date.year + 1, 1, 15)

            logging.debug('Datevec = %s' % datevec)
            return datevec

        elif resolution == 'day':
            date = startdate 
            datevec = [startdate + dt.timedelta(days=day) for day in range(ndays)]
                
            logging.debug('Datevec = %s' % datevec)
            return datevec

    def create_mean_statevector(self):
        """Createt initial default statevector (prior)"""
        new_mean = np.zeros(self.nparams)


        logging.info(
            'Reading initial statevector values from file %s' %
            self.initparamsfile)

        # read initial statevector from file
        f_in = io.ct_read(self.initparamsfile, method='read')
        # [nparameters,necoregions=240]
        initmat = f_in.get_variable(self.initparamsname)
        f_in.close()

        # check dimensions of initial statevector array read from file
        # self.nregions:
        if initmat.shape[0] != self.nbasefitparams or initmat.shape[1] != 240:
            logging.error(
                'Size initial parameter vector (%s) is not consistent with number of fit parameters (%s) and ecoregions (%s)' %
                (str(
                    initmat.shape),
                    self.nbasefitparams,
                    self.nregions))
            raise IOError

        # add code to read gamma matrix from file...
        if self.add_ano and self.read_gamma0:
            f_in = io.ct_read(self.initgammafile, method='read')
            initgamma = f_in.get_variable(self.initgammaname)
            f_in.close()

        # construct initial statevector
        for i, nr in enumerate(self.regionnumbers):
            i0 = i * self.nfitparams
            i1 = i0 + self.nbasefitparams
            i2 = i1 + self.ngamma
       #     i3 = i2 + self.nresp

            # initialize base fit parameters
            new_mean[i0: i1] = initmat[:, nr - 1]
            # initialize gamma
            if self.add_ano and self.read_gamma0:
                new_mean[i1: i2] = initgamma[:, nr - 1]
            elif self.add_ano:
                new_mean[i1: i2] = self.gamma0

        return new_mean

    def write_members_to_file(
            self,
            lag,
            outdir,
            endswith='.nc',
            member=None,
            obsoperator=None):
        """
           :param: lag: Which lag step of the filter to write, must lie in range [1,...,nlag]
           :param: outdir: Directory where to write files
           :param: endswith: Optional label to add to the filename, default is simply .nc
           :rtype: None

           Write ensemble member information to a NetCDF file for later use. The standard output filename is
           *parameters.DDD.nc* where *DDD* is the number of the ensemble member. Standard output file location
           is the `dir.input` of the dacycle object. In principle the output file will have only two datasets inside
           called `parametervalues` which is of dimensions `nparameters` and `parametermap` which is of dimensions (180,360).
           This dataset can be read and used by a :class:`~da.baseclasses.observationoperator.ObservationOperator` object.

           .. note:: if more, or other information is needed to complete the sampling of the ObservationOperator you
                     can simply inherit from the StateVector baseclass and overwrite this write_members_to_file function.

        """

        # These import statements caused a crash in netCDF4 on MacOSX. No problems on Jet though. Solution was
        # to do the import already at the start of the module, not just in this
        # method.

        if member is None:
            members = self.ensemble_members[lag]
        else:
            members = [self.ensemble_members[lag][member]]

        logging.debug('write_members_to_file: dir = %s' % outdir)

        if self.isOptimized:
            logging.info(
                'isOptimized = True; recalculating NEE before writing to file')

            for member in self.ensemble_members[lag]:
                self.nee_data[member.membernumber, :, :] = self.evaluate_model(
                    member.param_values.reshape((self.nregions, self.nfitparams)), self.timevec)
               # if self.add_ano:
               #     self.dter_data[member.membernumber, :, :, :] = self.calculate_anoterm(
               #         member.param_values.reshape((self.nregions, self.nfitparams)), self.timevec)

        # MAKE PARALLEL?
        for mem in members:

            filename = os.path.join(
                outdir, 'parameters.%03d%s' %
                (mem.membernumber, endswith))
            ncf = io.CT_CDF(filename, method='create')
            dimt = ncf.add_dim('date', len(self.timevec))
            dimda = ncf.add_dim('datearray', 3)
            dimgrid = ncf.add_latlon_dim()
            dimparams = ncf.add_params_dim(self.nparams)
            dimfitparams = ncf.add_dim('nfitparameters', self.nfitparams)

            data = mem.param_values
            savedict = io.std_savedict.copy()
            savedict['name'] = "parametervalues"
            savedict['long_name'] = "parameter_values_for_member_%d" % mem.membernumber
            savedict['units'] = "unitless"
            savedict['dims'] = dimparams
            savedict['values'] = data
            savedict['comment'] = 'These are parameter values to use for member %d' % mem.membernumber
            ncf.add_data(savedict)

            griddata = self.vector2grid(vectordata=data)
            # JJDH inserting the FluxModel call here
            # then the normal tm5 obsoperater can be used
            # set timerange for flux calculation
            # calculate and write fluxmap for ensemble
            # post-processing call needs to be removed
            # self.calc_flux(self.timevec,postprocessing=postprocessing)
            # calculate flux for member assuming griddata is statemap => check this
            # at this point griddata is prior or posterior, anomaly input is validated and
            # timevec is known for this cycle
            # nee, dgpp, dter = self.evaluate_model(griddata , self.timevec)
            # logging.debug('Calculated fluxes for ensemble member %i' % mem.membernumber)

            savedict = io.std_savedict.copy()
            savedict['name'] = "parametermap"
            savedict['long_name'] = "parametermap_for_member_%d" % mem.membernumber
            savedict['units'] = "unitless"
            savedict['dims'] = dimfitparams + dimgrid
            savedict['values'] = griddata.tolist()
            savedict['comment'] = 'These are gridded parameter values to use for member %d' % mem.membernumber
            ncf.add_data(savedict)

            ###################################################################
            # add time
            savedict = io.std_savedict.copy()
            savedict['name'] = 'date'
            savedict['long_name'] = 'date as days since 1-1-2000'
            savedict['dims'] = dimt
            savedict['units'] = 'days since 1-1-2000'
            savedict['values'] = self.timevec
            ncf.add_data(savedict)

            # add date as array
            datearray = [[(self.refdate + dt.timedelta(days=int(d))).year, 
                          (self.refdate + dt.timedelta(days=int(d))).month,
                          (self.refdate + dt.timedelta(days=int(d))).day] 
                         for d in self.timevec]
            savedict = io.std_savedict.copy()
            savedict['name'] = 'datearray'
            savedict['long_name'] = 'date array as [YYYY,MM,DD]'
            savedict['dims'] = dimt + dimda
            savedict['values'] = np.array(datearray)
            ncf.add_data(savedict)

            membern = mem.membernumber
            nee_grid = self.vector2grid(
                vectordata=self.nee_data[membern].ravel(),
                nparams=len(self.timevec))
            nee_grid = nee_grid * self.spatial_nee_distribution[None, :, :]

            if self.add_ano:
                dter_data = self.calculate_anoterm(mem.param_values.reshape((self.nregions, self.nfitparams)), self.timevec)
                nee_grid += dter_data
            # add flux
            savedict = io.std_savedict.copy()
            savedict['name'] = 'NEE_grid'
            savedict['long_name'] = (
                'Simulated NEE values on lat-lon grid for ensemble member %s' %
                mem.membernumber)
            savedict['dims'] = dimt + dimgrid
            savedict['values'] = nee_grid.tolist()
            savedict['units'] = 'mol/m2/s'
            ncf.add_data(savedict)

            # add TER anomaly
            savedict = io.std_savedict.copy()
            savedict['name'] = 'dNEE_grid'
            savedict['long_name'] = (
                'Simulated TER anomaly values on lat-lon grid for ensemble member %s' %
                mem.membernumber)
            savedict['dims'] = dimt + dimgrid
            savedict['values'] = dter_data.tolist()
            savedict['units'] = 'mol/m2/s'
            ncf.add_data(savedict)

            logging.info(
                'Fluxmap for member %s written to file (%s)' %
                (mem.membernumber, filename))
            ###################################################################

            ncf.close()

            logging.debug('Successfully wrote data from ensemble member %d to file (%s) ' % (mem.membernumber, filename))
        # exit()
        return

    def propagate(self, dacycle):
        """
        :rtype: None

        Propagate the parameter values in the StateVector to the next cycle. This means a shift by one cycle
        step for all states that will
        be optimized once more, and the creation of a new ensemble for the time step that just
        comes in for the first time (step=nlag).
        In the future, this routine can incorporate a formal propagation of the statevector.

        """

        import da.analysis.tools_time as at

        # Remove State Vector n=1 by simply "popping" it from the list and appending a new empty list at the front. This empty list will
        # hold the new ensemble for the new cycle

        self.ensemble_members.pop(0)
        self.ensemble_members.append([])

        # And now create a new time step of mean + members for n=nlag

        date = dacycle['time.start'] + \
            at.timedelta(days=(self.nlag - 0.5) * int(dacycle['time.cycle']))

        logging.info(
            "STATEVECTOR PROPAGATE ENDTIME = %s, date = %s" %
            (dacycle['time.end'], date))

        cov = self.get_covariance()
        self.make_new_ensemble(self.nlag - 1, cov)

        logging.info('The state vector has been propagated by one cycle')

    def validate_input(self):
        """ Make sure that data needed for the FluxModel (such as anomaly inputdata) are present."""

        if self.add_ano and not os.path.exists(self.anofile):
            msg = "Anomaly input file for CTSF inversion does not exist, exiting..."
            logging.error(msg)
            raise (IOError, msg)

        # read anomalies from file for defined timerange
        if self.add_ano:
            self.ano = self.get_anomalies(
                self.anofile, self.anoname, self.timevec)
            logging.info('Obtained anomalies for timerange.')

        return

    def get_anomalies(self, file, name, timevec):
        """ Read anomalies from file. The returned anomaly array will contain data for all times in timevec. Fillvalue = NaN."""

        # read input data
        ano, t_ano = self.get_anodata(file, name, getTime=True)
        # get index of simulation times in anomaly input array
        t_ano_index = self.get_time_index(t_ano, timevec)
        # replace anomaly array by new array that contains anomalies for the
        # requested simulation times
        ano = self.build_anomalies_samples(ano, t_ano_index)

        return ano

    def get_anodata(self, filename, varname, getTime=False):
        """
        : param filename: full path to netCDF file containing 'datatype' data on 1x1 deg grid
        : param varname: string to identify the data to be read from 'filename'.

        Method to read data for construction of NEE fit, and corresponding timerange, from netCDF file.
        Note: currently units are not checked!
        """

        with nc.Dataset(filename) as ds:
            anodata = np.nan_to_num(ds[varname][:])
            if getTime:
                t_anodata = nc.num2date(ds['time'][:], ds['time'].units)
                logging.info("Successfully read %s and time from file (%s)" % (varname, filename))
                return anodata, t_anodata
            else:
                logging.info("Successfully read %s from file (%s)" % (varname, filename))
                return anodata

    def get_time_index(self, t_ano, timevec):
        """
        Returns a list with for every date in self.timevec the index in the input vector 'times'.
        If no input data is present for the requested time, index = None
        """

        cftimes = nc.num2date(timevec, f'days since {self.refdate:%Y-%m-%d}')
        indices = []
        for t in cftimes:
            index = np.argmin(abs(t - t_ano))
            indices.append(index)
            logging.debug(f'found index {index} corresponding to time {t_ano[index]} for {t}')

        logging.debug("Obtained anomaly time indices")

        return indices

    def build_anomalies_samples(self, anomaly, index):
        """ Returns a 3D array with input anomaly data for times corresponding to timevec only. Fillvalue = NaN."""

        logging.debug('anomaly shape = %s' % str(anomaly.shape))
        sampled_anomaly = np.zeros((len(index), anomaly.shape[1], anomaly.shape[2]))
        for i, idx in enumerate(index):
            if idx is None:
                sampled_anomaly[i, :, :] = np.nan
            else:
                sampled_anomaly[i, :, :] = anomaly[idx, :, :]

        logging.debug("Created array with time-sampled anomalies.")

        return sampled_anomaly

    def evaluate_model(self, statemap, timevec):
        """
        Method to calculate an NEE field consisting of regionally constant second-order polynomial and harmonical terms with linearly varying amplitude
        (to account for mean seasonal cycle), enhanced with scaled anomalies to account for spatial and interannual variability.
        """

        # check dimensions of input
        if statemap.shape[0] != self.nregions:
            logging.error("Incorrect number of statemap parameters per grid cell (%d) (must equal the number of fit parameters (%d))." % (statemap.shape[0], self.nfitparams))
            raise ValueError
        if self.add_ano and (
            (self.ano.shape[0] != len(timevec)) or 
            (self.ano.shape[1] != self.nlat) or 
            (self.ano.shape[2] != self.nlon)):
            logging.error("Incorrect dimensions of anomaly: %s, required: %i,%i,%i." %
                (self.ano.shape, len(timevec), self.nlat, self.nlon))
            raise ValueError

        i1 = self.nbasefitparams
        i2 = i1 + self.ngamma
        nee = (self.calculate_polynomic(statemap[:, :self.npolyterms], timevec) +
               self.calculate_harmonics_sincos(statemap[:, self.npolyterms: i1], timevec))


    def calculate_polynomic(self, statemap, timevec):
        """Calculate the NEE due to the polynomic terms
        Input:
            statemap (np.array of NREGIONS x NPOLYTERMS): statevector values reshaped
            timevec (np.array len(NTIMES): array with number of days after a set starttime
        Output:
            NEE: (np.array of NTIMES x NREGIONS): Calculated NEE due to the polynomic terms"""
        polynee = np.zeros((len(timevec), len(statemap)))
        for i in range(self.npolyterms):
            polynee += statemap[:, i] * (timevec[:, None] ** i)

        return polynee

    def calculate_harmonics_sincos(self, statemap, timevec):
        """Calculate the NEE due to the harmonic terms
        Add cosine and sine waves as decomposition
        Input:
            statemap (np.array of NREGIONS x NHARMONICS): statevectorvalues reshaped
            timevec (np.array len(NTIMES): array with number of days after a set starttime
        Output:
            NEE: (np.array of NTIMES x NREGIONS): calculated NEE due to the harmonic terms"""

        days_in_year = 365.242
        har = np.zeros((len(timevec), len(statemap)), 'float')

        for i in range(self.nharmonics):
            j = i + self.nharmonics
            har += statemap[:, i] * np.cos( 2.0*np.pi / days_in_year * (i + 1) * timevec[:, None]) \
                 + statemap[:, j] * np.sin( 2.0*np.pi / days_in_year * (i + 1) * timevec[:, None])
        if self.grow_amplitude:
            amp_growth = 1 + (statemap[:, j+1] * timevec[:, None])
        else: amp_growth = 1.
        return har * amp_growth

    def calculate_anoterm(self, statemap, timevec):
        """Calculate the NEE due to the anomalies
        Input:
            statemap (np.array of NREGIONS x NGAMMA): statevectorvalues reshaped
            timevec (np.array len(NTIMES): array with number of days after a set starttime
        Output:
            NEE: (np.array of NTIMES x NREGIONS): calculated NEE due to the anomalies of T"""
        i1 = self.nbasefitparams

        anoterm = np.zeros_like(self.ano)
        # make just over a year, so that integer rounding returns a index
        # (alwasy smaller than ngamma)
        NDAYS_PER_GAMMA = 366.1 / self.ngamma

        for i, t in enumerate(timevec):
            idx = int((t % 365) // NDAYS_PER_GAMMA)

            gridded_gamma = self.vector2grid(vectordata=statemap[:, i1 + idx],nparams=1)
            anoterm[i,:,:] = gridded_gamma * np.nan_to_num(self.ano[i,:,:])

        return anoterm


################### End Class CTSFStateVector ###################


if __name__ == "__main__":
    pass
