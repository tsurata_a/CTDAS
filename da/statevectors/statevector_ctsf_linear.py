"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python

from __future__ import division

import os
import sys
sys.path.append(os.getcwd())
from scipy import stats 
import logging
import numpy as np
import datetime as dt
from dateutil.relativedelta import relativedelta
import da.tools.io4 as io
from da.tools.general import to_datetime
#from da.baseclasses.statevector import StateVector, EnsembleMember
from da.statevectors.statevector_baseclass import StateVector, EnsembleMember
from da.tools.covariance import cov_gaussian  
identifier = 'CarbonTracker Statistical Fit Statevector '
version = '0.0'

################### Start Class CTSFStateVector ###################
class ctsfStateVector(StateVector):

    """
    This is a StateVector object for CarbonTracker. It is created for use with the SIF inversion approach,
    and contains statistical parameters and SIF sensitivity parameters per ecoregion to be assembled.
    It has a private method to make new ensemble members.
    """

    def setup(self, dacycle):
        """
        setup the object by specifying the dimensions.
        There are two major requirements for each statvector that you want to build:
            (1) is that the statevector can map itself onto a regular grid
            (2) is that the statevector can map itself (mean+covariance) onto TransCom regions
        An example is given below.
        """

        self.dacycle = dacycle

        # JJDH used to be sample start 
        # Yet, in ctsf inversions their is no difference
        startdate = self.dacycle['time.start']
        enddate   = self.dacycle['time.end']

        logging.info('Calculating fluxes from %s to %s' % (startdate, enddate))

        self.isOptimized=False

        # Temporal settings
        self.nlag       = int(dacycle['time.nlag'])
        self.refdate    = to_datetime(dacycle.dasystem['time.reference'])
        self.resolution = dacycle.dasystem['fit.resolution']
        self.datevec    = self.calc_datevec(startdate, enddate,self.resolution)
        timevec = []
        for date in self.datevec:
            timevec.append( (date-self.refdate).days )
        self.timevec = np.array(timevec)
        logging.info('Timevec = %s' % timevec)


        # Spatial settings
        self.nlat = 180
        self.nlon = 360

        # Inversion setup
        self.nmembers   = int(dacycle['da.optimizer.nmembers'])                 # number of ensemble members
        
        # Fluxfit settings
        # self.unit_initialization = dacycle.dasystem['fit.unit_initialization']  # if True: initial fit will be constructed based on latitude, else: read from file
        writememberfluxes        = self.dacycle.dasystem['writeMemberFluxes']   # write gridded fluxes for every member? [bool]

        logging.info('Read parameters for flux calculation')
        # fit settings
        self.nharmonics = int(dacycle.dasystem['nharmonics'])               # number of harmonical terms in baseline fit
        self.npolyterms = int(dacycle.dasystem['npolyterms'])

        logging.debug('Using {} harmonic parameters'.format(self.nharmonics))
        logging.debug('Using {} polynomial terms'.format(self.npolyterms))
        self.nbasefitparams = self.npolyterms + self.nharmonics*2 
        logging.debug('Resulting in {} parameters ({}+ 2 * {} )'.format(self.nbasefitparams, self.npolyterms, self.nharmonics))

        self.nobs              = 0
        self.obs_to_assimilate = ()                                             # empty containter to hold observations to assimilate later on


        # else: Read fit parameters from file
        self.initparamsfile     = dacycle.dasystem['params.init.file']
            # self.read_initparamsvec = dacycle.dasystem['params.init.vec']       # True: directly read statevector, False: extract statevector from gridded field
        self.initparamsname     = dacycle.dasystem['params.init.name']

        # anomaly term
        self.add_ano = dacycle.dasystem['add.anomalies']
        if self.add_ano:
            self.ngamma = int(dacycle.dasystem['ngamma'])                       # number of sensitivity parameters per ecoregion: default = 12, 1 per calendar month
            self.anofile = dacycle.dasystem['fit.anomaly.inputfile']
            self.anoname = dacycle.dasystem['fit.anomaly.name']                 # name of anomaly in inputfile
            self.read_gamma0 = dacycle.dasystem['params.init.gamma.read']
            if not self.read_gamma0:
                self.gamma0 = float(dacycle.dasystem['params.init.gamma']) #.split(',')  # enter single value, or comma-separated list of ngamma (default: one for each month)

            else:
                self.initgammafile = dacycle.dasystem['params.init.gamma.file']
                self.initgammaname = dacycle.dasystem['params.init.gamma.name']

            self.readanostd = dacycle.dasystem['fit.anomaly.read.std']
            if self.readanostd:
                self.anostdfile = dacycle.dasystem['fit.anomaly.std.file']
                self.anostdname = dacycle.dasystem['fit.anomaly.std.name']
        else:
            self.ngamma = 0

        self.nfitparams = self.nbasefitparams
        # statevector settings 
        self.nlambda=int(dacycle.dasystem['nlambda'])
        # covariance matrix settings
        self.read_cov_mat = dacycle.dasystem['fit.read_cov_matrices']    # if True: a covariance matrix will be read from file for each ecoregion,
                                                                         # else, construct diag. matrix from uncertainty
        # self.cov_mat_name = dacycle.dasystem['fit.cov_matrices_name'] if self.read_cov_mat else None # 'parameter_covariance'
        if self.read_cov_mat:
            self.cov_mat_file = dacycle.dasystem['fit.cov_matrices_file']
            self.cov_mat_name = dacycle.dasystem['fit.cov_matrices_name']
            self.cov_scaling  = float(dacycle.dasystem['fit.cov_scaling']) if dacycle.dasystem['fit.cov_scaling'] else 1

        # These list objects hold the data for each time step of lag in the system. Note that the ensembles for each time step consist
        # of lists of EnsembleMember objects, we define member 0 as the mean of the distribution and n=1,...,nmembers as the spread.
        self.ensemble_members = list(range(self.nlag))
        for n in range(self.nlag):
            self.ensemble_members[n] = []

        # This specifies the file to read with the gridded mask at 1x1 degrees. Each gridbox holds a number that specifies the ecoregion it belongs to.
        # From this map, a dictionary is created that allows a reverse look-up so that we can map parameters to a grid.
        mapfile      = os.path.join(dacycle.dasystem['regionsfile'])
        ncf          = io.ct_read(mapfile, 'read')
        self.gridmap = ncf.get_variable('regions')                              # read ecoregions map (nlat x nlon)
        self.tcmap   = ncf.get_variable('transcom_regions')                     # read transcom regions map (nlat x nlon)
        olsonmap     = ncf.get_variable('land_ecosystems')
        ncf.close()

        logging.debug("A TransCom  map on 1x1 degree was read from file %s" % dacycle.dasystem['regionsfile'])
        logging.debug("An ecoregion parameter map on 1x1 degree was read from file %s" % dacycle.dasystem['regionsfile'])

        # Create a dictionary for state <-> gridded map conversions (keys = regionnumbers)
        # And calculate mean latitude per ecoregion (only if self.amplitude[0] = None)
        regnums       = []
        self.griddict = {}
        lat_regions   = []

        for i in range(209):
            sel = np.where(self.gridmap == i+1)
            if len(sel[0]) > 0 and np.mean(olsonmap[sel] < 19):
                regnums.append(i+1)
                self.griddict[i+1]    = sel

        self.regionnumbers = np.asarray(regnums)
        self.nregions      = len(self.regionnumbers)
        self.nparams_per_region=self.nlambda + self.ngamma
        self.nparams       = self.nregions * self.nparams_per_region                  # number of elements in statevector

        logging.debug('There are %s non-empty ecoregions: %s' %(self.nregions, self.regionnumbers))
        logging.debug('A dictionary to map grids to states and vice versa was created')
        logging.debug('Average latitude per (nonempty) ecoregion was calculated.')

        logging.info("Updating with dacycle.dasystem['nparameters']=%d to be used by the optimizer" %self.nparams )
        dacycle.dasystem['nparameters']=self.nparams

        # Create a matrix for state <-> TransCom conversions
        # self.tcmatrix = np.zeros((self.nparams, 23), 'float')

        # for r in range(self.nregions):
        #     er = self.regionnumbers[r]
        #     sel = (self.gridmap.flat == er).nonzero()
        #     if len(sel[0]) < 1:
        #         continue
        #     else:
        #         n_tc = set(self.tcmap.flatten().take(sel[0]))
        #         if len(n_tc) > 1:
        #             logging.error("Parametergroup %d seems to map to multiple TransCom regions (%s), I do not know how to handle this" % (er, n_tc))
        #             raise ValueError
                # self.tcmatrix[r*self.nfitparams:(r+1)*self.nfitparams, int(n_tc.pop()) - 1] = 1.0

        # logging.debug("A matrix to map states to TransCom regions and vice versa was created")
        self.nee_data=np.zeros(( self.nmembers,self.nregions, len(self.timevec),),float)

        # Create a mask for species/unknowns
        self.make_species_mask()
        # Validate the requested input files
        self.validate_input()

        return

    def calculate_scaled_flux(self,member):
        # here we need to compute scaled versions of NEE and 
        # ano_term 
        # we do so by looping over each region.
        # state will be

        for i in range(self.nregions):
            # array with dimension of the timevec/datevec for this region
            i0 = i*(self.nlambda+self.ngamma)
            if self.nlambda==12:
                # case monthly
                # month will run from 1 to 12, index starts at 0 + offset.
                scalar_array=np.array([member.param_values[i0+d.month-1] for d in self.datevec])
            if self.nlambda==365:
                # case monthly
                # month will run from 1 to 12, index starts at 0 + offset.
                scalar_array=[]
                year=self.datevec[0].year
                day_in_year=0# implement ofset, for now we start jan alwasy
                for date in self.datevec:
                    # reset if we are in a new year
                    if year!=date.year:
                        
                        year=date.year
                        day_in_year=0
                    day_in_year+=1
                    if date.month==2 and date.day==29:
                        day_in_year-=1 # go back to 28 and take the mean of 28 feb and 1 march
                        scalar_array.append((member.param_values[i0+day_in_year-1]+member.param_values[i0+day_in_year])/2)
                    else:
                        scalar_array.append(member.param_values[i0+day_in_year-1])

                scalar_array=np.array(scalar_array)
                # logging.debug('scalar array%s' %scalar_array)
            U=np.std(self.nee_data[0,i,:])

            logging.debug('uncertainty %s' % self.nee_data[member.membernumber,i,:])
            self.nee_data[member.membernumber,i,:]=self.nee_data[0,i,:]+U*(1.0-scalar_array)
             
        # if self.add_ano:
            # self.dter_data[member.membernumber,:,:,:] = self.calculate_anoterm(member.param_values.reshape((self.nregions,self.nfitparams)) , self.timevec)
        logging.debug('allfinite is %s' % (all(np.isfinite(self.nee_data[member.membernumber,:,:].ravel()))))
        return

    def write_to_file(self,file,qual):
        """ do whatever super does
        then also add nee dter and dgpp sets
        """
        # file, dim of nparams ,nmembers, dimlag
        logging.debug('Starting write to file')
        f,dimparams, dimmembers, dimlag ,filename=super().write_to_file(file,qual,return_file=True)
        logging.debug('Continuing write to file')
        dimt                  = f.add_dim('date',len(self.timevec))
        dimgrid      = f.add_latlon_dim()
        if not qual=='opt':
            savedict              = io.std_savedict.copy()
            savedict['name']      = 'date'
            savedict['long_name'] = 'date as days since 1-1-2000'
            savedict['dims']      = dimt
            savedict['units']     = 'days since 1-1-2000'
            savedict['values']    = self.timevec
            f.add_data(savedict)
        # note we have no lag here 

        logging.debug('Writing NEE to file')
        dimregions            = f.add_dim('regions',self.nregions)
        savedict              = io.std_savedict.copy()
        savedict['name']      = 'NEE_'+qual
        savedict['long_name'] = ('Simulated NEE values per ecoregion' )
        savedict['dims']      = dimmembers + dimregions + dimt 
        savedict['values']    = self.nee_data.tolist()
        savedict['units']     = 'mol/m2/s'
        f.add_data(savedict)

        # if self.add_ano:
        #     logging.debug('Writing dNEE to file')
        #     savedict = io.std_savedict.copy()
        #     savedict['name'] = 'dNEE_' + qual
        #     savedict['long_name'] = ('Simulated anomaly in NEE values on grid')
        #     savedict['dims'] = dimmembers + dimt + dimgrid
        #     for member in self.ensemble_members[0]:
        #         i =member.membernumber
        #         mv=member.param_values
        #         dter_data = self.calculate_anoterm(mv.reshape((self.nregions, self.nparams_per_region)), self.timevec)
        #         savedict['count'] = i
        #         savedict['values'] = dter_data
        #         savedict['units'] = 'mol/m2/s'
        #         f.add_data(savedict, nsets=1)
        #         logging.debug('Written dNEE to file')
         
        f.close()

        logging.info('Successfully wrote the State Vector to file (%s) ' % filename)
        return
    
    def read_from_file(self, filename, qual='opt'):
        """ Read initial data for the statevector from given files
        """
        super().read_from_file(filename,qual) 
        f = io.ct_read(filename, 'read')
        self.nee_data = f.get_variable('NEE_' + qual)
        # if self.add_ano:
            # self.dter_data = f.get_variable('dNEE_' + qual)
        # self.dter_data = f.get_variable('dGPP_' + qual)
        f.close()
        logging.info('Successfully read the fluxmodel data from file (%s) ' % filename)

    def state_to_grid(self, fluxvector=None, lag=1,monthcounter=None,filename=None,qual=None):
        # used to write statevector in grid format for coupling with TM5 (where actual fluxes are calculated)
        # implemented such that variables with dimensions nfitparams x nlat x nlon are created

        """
            Transforms the StateVector information (mean + covariance) to a 1x1 degree grid.


            :param: fluxvector: a vector of length (nparams,) that holds the fluxes associated with each parameter in the StateVector
            :param: lag: the lag at which to evaluate the StateVector
            :rtype: a tuple of two arrays (gridmean,gridvariance) with dimensions (nfitparams,180,360)
            counter will fetch the fluxvalues for month=counter

            If the attribute `fluxvector` is not passed, the function will return the mean parameter value and its variance on a 1x1 map.

            ..note:: Although we can return the variance information for each gridbox, the covariance information contained in the original ensemble is lost when mapping to 1x1 degree!
        """

        if fluxvector == None:
            fluxvector = np.ones(self.nparams)
        else:
            logging.error("ctsfStateVector cannot handle state_to_grid with fluxvector input")
            raise ValueError

        if filename==None:
            ensemble     = self.ensemble_members[lag - 1]
            ensemblemean = ensemble[0].param_values

            # First transform the mean
            gridmean = self.vector2grid(vectordata=ensemblemean * fluxvector)

            # And now the covariance, first create covariance matrix (!), and then multiply
            deviations = np.array([mem.param_values * fluxvector - ensemblemean for mem in ensemble])
            ensemble   = np.array([self.vector2grid(mem) for mem in deviations])
            return (gridmean,ensemble)

        else:
            f = io.ct_read(filename, 'read')
            if self.resolution == 'month':
                nee_data = f.get_variable('NEE_' + qual)[:,:,monthcounter]
                if self.add_ano:

                    dter_data=np.zeros((self.nmembers,self.nlat,self.nlon,),float)
                    mask=[(currentdate.month==t.month)&(currentdate.year==t.year) for t in self.datevec]
                    mean=f.get_variable('statevectormean_'+ qual)[lag-1,:] 
                    ensemble=f.get_variable('statevectorensemble_'+ qual)[lag-1,:,:] 
                    for nr in [member.membernumber for member in self.ensemble_members[lag - 1]]:
                        dter_data[nr,:,:]=self.calculate_anoterm((mean+ensemble[nr,:]).reshape(self.nregions, self.nparams_per_region),self.timevec,mask)
                    # logging.debug("largest ano found in the mean %s" % np.max(dter_data[0,:,:]))
                # if self.add_ano:
                    # dter_data = f.get_variable('dNEE_' + qual)[:,monthcounter,:,:]
            elif self.resolution == 'day':
                currentdate=self.datevec[0]+relativedelta(months=monthcounter)
                mask=[(currentdate.month==t.month)&(currentdate.year==t.year) for t in self.datevec]
                logging.debug("averaging for month %s with %d days for %s" % (currentdate,sum(mask),qual))
                nee_data = np.average(f.get_variable('NEE_' + qual)[:,:,mask],axis=2)
                if self.add_ano:
                    dter_data=np.zeros((self.nmembers,self.nlat,self.nlon,),float)
                    mean=f.get_variable('statevectormean_'+ qual)[lag-1,:] 
                    ensemble=f.get_variable('statevectorensemble_'+ qual)[lag-1,:,:] 
                    for nr in [member.membernumber for member in self.ensemble_members[lag - 1]]:
                        dter_data[nr,:,:]=np.average(self.calculate_anoterm((mean+ensemble[nr,:]).reshape(self.nregions, self.nparams_per_region),self.timevec,mask),axis=0)
                    # logging.debug("largest ano found in the mean %s" % np.max(dter_data[0,:,:]))
                    # dter_data = np.average(f.get_variable('dNEE_' + qual)[:,mask,:,:],axis=1)
            
            # dter_data = f.get_variable('dGPP_' + qual)
            f.close()
            # logging.debug("Nee has dimeions %s %s " % np.shape(nee_data) )
            # Nee data on members by nregions 
            # ensemble mean ndim (regions)
            ensemblemean=nee_data[0][:]
            deviations= np.array([nee[:] - ensemblemean for nee in nee_data])
            gridmean = self.vector2grid(vectordata=ensemblemean,nparams=1)
            # if self.add_ano:
                # gridmean+=dter_data[0]
            ensemble = np.array([self.vector2grid(vectordata=nee_data,nparams=1) for nee_data in deviations])

            if self.add_ano:
                # Create mask for the time elments of interest 
                gridmean+=dter_data[0]
                ensemble=ensemble-dter_data[:]-dter_data[0]
                gridmean_ano=dter_data[0]
                ensemble_ano=np.array([ano-gridmean_ano for ano in dter_data])
                return (gridmean,ensemble,gridmean_ano,ensemble_ano)
            else:
                return (gridmean,ensemble,np.zeros(np.shape(gridmean),float),np.zeros(np.shape(ensemble),float))


    def vector2grid(self, vectordata=None,nparams="Default"):
        """
            Map vector elements to a map or vice cersa

           :param vectordata: a vector dataset to use in case `reverse = False`. This dataset is mapped onto a 1x1 grid and must be of length `nparams`
           :rtype: ndarray: an array of size (nfitparams,360,180)

           This method makes use of a dictionary that links every parameter number [1,...,nparams] to a series of gridindices. These
           indices specify a location on a 360x180 array, stretched into a vector using `array.flat`. There are multiple ways of calling
           this method::

               griddedarray = self.vector2grid(vectordata=param_values) # simply puts the param_values onto a (180,360,) array

           .. note:: This method uses a DaSystem object that must be initialzied with a proper parameter map. See :class:`~da.baseclasses.dasystem` for details
        """

        if nparams=="Default":
            nparams=self.nparams_per_region
        if not (nparams==1): 
            result = np.zeros((nparams,self.gridmap.shape[0], self.gridmap.shape[1]), float)
        else:
            result = np.zeros((self.gridmap.shape[0], self.gridmap.shape[1]), float)
        # result = np.zeros((self.gridmap.shape[0],self.gridmap.shape[1],nparams), float)
        # for k,data in enumerate(vectordata.reshape((self.nregions,self.nfitparams))):
        #     regnum = self.regionnumbers[k]
        #     if regnum in self.griddict:
        #         logging.debug(regnum)
        #         logging.debug(self.griddict[regnum][0])
        #         logging.debug(self.griddict[regnum][1])
        #         result[self.griddict[regnum][0],self.griddict[regnum][1],:] = data[:]
        # # exit()
        for k,data in enumerate(vectordata):
            # logging.debug('k is %s' %k)
            # works only if nparam>1 
            if not (nparams==1):
                r = int(np.floor(k / nparams))
                i = k % nparams
                regnum = self.regionnumbers[r]
                if regnum in self.griddict:
                # logging.debug(regnum)
                    result[i,self.griddict[regnum][0],self.griddict[regnum][1]] = data 
            else:
                regnum = self.regionnumbers[k]
                if regnum in self.griddict:
                    result[self.griddict[regnum][0],self.griddict[regnum][1]] = data 


        # wy? this is local, python has garbage colletion and iteraters are local:


        return result

    def get_covariance(self, date, dacycle):
        """
        Make a new ensemble from specified matrices, the attribute lag refers to the position in the state vector.
            Note that lag=1 means an index of 0 in python, hence the notation lag-1 in the indexing below.
            The argument is thus referring to the lagged state vector as [1,2,3,4,5,..., nlag]
        """

        # read uncertainties for anomaly and convert to variance
        if self.add_ano:
            unc_gamma = float(dacycle.dasystem['bio.cov.gamma'])**2
        # loop over regions and insert
        # covariance as we go. each month will be scaled with an ecoregion dependent scalar.
        # with mean 1 and 80 percent uncertainty so the sigma**2 = 0.64. this is yet somewhat artifficial
        # in the future we may want to add a weak correlation between ecoregions 
        cov_mat_list=[]
        for i,r in enumerate(self.regionnumbers):
            cov_mat=np.zeros((self.nparams_per_region,self.nparams_per_region),float)
            cov_mat[:self.nlambda,:self.nlambda]=self.get_lambda_cov()
            if self.add_ano:
                cov_mat[self.nlambda:,self.nlambda:]=np.diag([unc_gamma]*self.ngamma)
            parnrs = range( i * self.nparams_per_region,(i+1)* self.nparams_per_region)
            cov_mat_list.append([cov_mat,parnrs])
        return cov_mat_list
    def get_lambda_cov(self):
        """TODO: Docstring for get_lambda_cov.
        :returns: TODO

        """

        if self.nlambda==12:
            cov_mat_lambda=np.diag([0.64]*self.nlambda)
        if self.nlambda==365:
            cov_mat_lambda=cov_gaussian(365,12,0.64)
        return cov_mat_lambda

    def make_new_ensemble(self, lag, covariancematrixlist=[None]):
        """
        :param lag: an integer indicating the time step in the lag order
        :param covariancematrix: a list of matrices specifying the covariance distribution to draw from
        :rtype: None

        Make a new ensemble, the attribute lag refers to the position in the state vector.
        Note that lag=1 means an index of 0 in python, hence the notation lag-1 in the indexing below.
        The argument is thus referring to the lagged state vector as [1,2,3,4,5,..., nlag]

        The covariance list object to be passed holds a list of matrices with a total number of dimensions [nparams, nparams], which is
        used to draw ensemblemembers from. Each draw is done on a matrix from the list, to make the computational burden smaller when
        the StateVector nparams becomes very large.

        """

        if not isinstance(covariancematrixlist, list):
            logging.error("The covariance matrix or matrices must be passed as a list of array objects, exiting..." )
            raise ValueError

        logging.debug('len(covariancematrixlist) = %s' %len(covariancematrixlist))

        # Check dimensions of covariance matrix list, must add up to nparams
        dims = 0
        for matrix,parnr in covariancematrixlist:
            dims += matrix.shape[0]

        if dims != self.nparams:
            logging.error("The total dimension of the covariance matrices passed (%d) does not add up to the prescribed nparams (%d), exiting..." % (dims, self.nparams))
            raise ValueError

        # Loop over list of covariance matrices and their statevector parameter numbers, and create a matrix of (nparams,nmembers) with the deviations
        dof = 0.0
        dev_matrix = np.zeros((self.nparams, self.nmembers,), 'float')
        randstate = np.random.get_state()

        for matrix,parnr in covariancematrixlist:

            # Make a cholesky decomposition of the covariance matrix
            try:
                _, s, _ = np.linalg.svd(matrix)
            except:
                s = np.linalg.svd(matrix, full_matrices=1, compute_uv=0) #Cartesius fix
            dof += np.sum(s) ** 2 / sum(s ** 2)

            try:
                C = np.linalg.cholesky(matrix)
            except (np.linalg.linalg.LinAlgError, err):
                logging.error('Cholesky decomposition has failed ')
                logging.error('For a matrix with parnr %s and of dimensions: %d' % (parnr,matrix.shape[0]))
                logging.debug(err)
                raise np.linalg.linalg.LinAlgError

            # Draw nmembers instances of this distribution
            npoints = matrix.shape[0]

            istart = parnr[0]
            istop  = parnr[-1]+1

            for member in range(1, self.nmembers):
                rands = np.random.randn(npoints)
                deviations = np.dot(C, rands)
                dev_matrix[istart:istop, member] = deviations

            logging.debug("Inserted prior covariance matrix %d elements into statevector location %d through %d"%(len(parnr),istart,istop) )

        logging.debug('Successfully constructed a deviation matrix from covariance structure')
        logging.info('Appr. degrees of freedom in full covariance matrix is %s' % (int(dof)))

        # Create mean values 
        new_mean = self.create_mean_statevector()

        # Now fill the ensemble members with the deviations we have just created
        # Create members 1:nmembers and add to ensemble_members list
        for member in range(self.nmembers):
            new_member = EnsembleMember(member)
            # dev_matrix is initialized at zero; we add all zeros to the first member
            new_member.param_values = dev_matrix[:, member ] + new_mean

            self.ensemble_members[lag].append(new_member)

            self.calculate_scaled_flux(new_member)
        logging.debug('%d new ensemble members were added to the state vector # %d' % (self.nmembers, (lag + 1)))
        
        return

    def calc_datevec(self, startdate, enddate,resolution):
        """Creates time vector with monthly interval ranging from startdate to enddate."""

        datevec = []
        ndays   = (enddate-startdate).days

        # use actual date for forward run with small (weekly) cycle length
        if ndays <= 30:
            datevec.append(startdate + dt.timedelta(days=int(round(ndays/2)-1)))
            return datevec

        elif resolution=='month':
            date = dt.datetime(startdate.year,startdate.month,15)
            while date <= enddate:
                datevec.append(date)
                if date.month < 12:
                    date = dt.datetime(date.year,date.month+1,15)
                else:
                    date = dt.datetime(date.year+1,1,15)

            logging.debug('Datevec = %s' % datevec)
            return datevec

        elif resolution=='day':
            date = startdate 
            datevec=[startdate+dt.timedelta(days=day) for day in range(ndays)]
                
            logging.debug('Datevec = %s' % datevec)
            return datevec

    def get_mean_flux(self):  
        # read initial statevector from file
        f_in    = io.ct_read(self.initparamsfile,method='read')
        initmat = f_in.get_variable(self.initparamsname) # [nparameters,necoregions=240]
        f_in.close()
        params=[]
        for i,nr in enumerate(self.regionnumbers):
            i0 = i*self.nfitparams
            i1 = i0 + self.nbasefitparams
            # i2 = i1 + self.ngamma
            # i3 = i2 + self.nresp
            # initialize base fit parameters
            params[i0 : i1] = initmat[:,nr-1]
            # if self.add_ano:
                # params[i1 : i2] = [self.gamma0 for i in range(i1,i2)]
        
        params=np.array(params)
        self.nee_data[0, :, :] = self.evaluate_model(params.reshape((self.nregions, self.nfitparams)), self.timevec)
        for i,dat in enumerate(self.nee_data[0]):
            logging.debug('nee from region %s : %s ' % (self.regionnumbers[i],dat))
        # if self.add_ano:
            # self.dter_data[0,:,:,:] = self.calculate_anoterm(params.reshape((self.nregions,self.nfitparams)) , self.timevec)
        logging.debug("retrieved flux from %s" %self.initparamsfile)
        return

    def create_mean_statevector(self):
        """Createt initial default statevector (prior)"""
        # construct initial statevector
        new_mean=[]
        for i,nr in enumerate(self.regionnumbers):
            # add scalars for each month
            new_mean.extend([1.0]*self.nlambda )
            # initialize gamma
            if self.add_ano:
                new_mean.extend([self.gamma0]*self.ngamma)

        new_mean = np.array(new_mean)
        logging.debug('Created new mean statevector containing %s statevector elements' %(len(new_mean)) )
        logging.debug('     Monthly lambda=1.0')
        if self.add_ano:
            logging.debug('     And gamma_ano=%s ' %self.gamma0)
        self.get_mean_flux()
        return new_mean

    def write_members_to_file(self, lag, outdir, endswith='.nc', member=None,obsoperator=None):
        """
           :param: lag: Which lag step of the filter to write, must lie in range [1,...,nlag]
           :param: outdir: Directory where to write files
           :param: endswith: Optional label to add to the filename, default is simply .nc
           :rtype: None

           Write ensemble member information to a NetCDF file for later use. The standard output filename is
           *parameters.DDD.nc* where *DDD* is the number of the ensemble member. Standard output file location
           is the `dir.input` of the dacycle object. In principle the output file will have only two datasets inside
           called `parametervalues` which is of dimensions `nparameters` and `parametermap` which is of dimensions (180,360).
           This dataset can be read and used by a :class:`~da.baseclasses.observationoperator.ObservationOperator` object.

           .. note:: if more, or other information is needed to complete the sampling of the ObservationOperator you
                     can simply inherit from the StateVector baseclass and overwrite this write_members_to_file function.

        """

        # These import statements caused a crash in netCDF4 on MacOSX. No problems on Jet though. Solution was
        # to do the import already at the start of the module, not just in this method.

        if member is None:
            members = self.ensemble_members[lag]
        else:
            members = [self.ensemble_members[lag][member]]

        logging.debug('write_members_to_file: dir = %s' %outdir)

        if self.isOptimized:
            logging.info('isOptimized = True; recalculating NEE before writing to file')

            # update the mean last, because the mean is used to scale and all scalars are compared to the prior flux
            for member in reversed(self.ensemble_members[lag]):
                self.calculate_scaled_flux(member)
                logging.debug("updated fluxes of %s" %member.membernumber)
                # if self.add_ano:
                    # self.dter_data[member.membernumber,:,:,:] = self.calculate_anoterm(member.param_values.reshape((self.nregions,self.nfitparams)) , self.timevec)

        # MAKE PARALLEL?
        for mem in members:

            filename     = os.path.join(outdir, 'parameters.%03d%s' % (mem.membernumber, endswith))
            ncf          = io.CT_CDF(filename, method='create')
            dimt         = ncf.add_dim('date',len(self.timevec))
            dimda        = ncf.add_dim('datearray',3)
            dimgrid      = ncf.add_latlon_dim()
            dimparams    = ncf.add_params_dim(self.nparams)
            dimfitparams = ncf.add_dim('nfitparameters',self.nparams_per_region)

            data = mem.param_values
            savedict = io.std_savedict.copy()
            savedict['name']      = "parametervalues"
            savedict['long_name'] = "parameter_values_for_member_%d" % mem.membernumber
            savedict['units']     = "unitless"
            savedict['dims']      = dimparams
            savedict['values']    = data
            savedict['comment']   = 'These are parameter values to use for member %d' % mem.membernumber
            ncf.add_data(savedict)

            # nee, dgpp, dter = self.evaluate_model(data.reshape((self.nregions,self.nfitparams)) , self.timevec)
            # nee_gridded = self.vector2grid(vectordata=nee.ravel(),nparams=12)
            griddata = self.vector2grid(vectordata=data)

            savedict = io.std_savedict.copy()
            savedict['name']      = "parametermap"
            savedict['long_name'] = "parametermap_for_member_%d" % mem.membernumber
            savedict['units']     = "unitless"
            savedict['dims']      = dimfitparams + dimgrid
            savedict['values']    = griddata.tolist()
            savedict['comment']   = 'These are gridded parameter values to use for member %d' % mem.membernumber
            ncf.add_data(savedict)

            ####################################################################
            # add time
            savedict              = io.std_savedict.copy()
            savedict['name']      = 'date'
            savedict['long_name'] = 'date as days since 1-1-2000'
            savedict['dims']      = dimt
            savedict['units']     = 'days since 1-1-2000'
            savedict['values']    = self.timevec
            ncf.add_data(savedict)

            # add date as array
            datearray = [[(self.refdate+dt.timedelta(days=int(d))).year,(self.refdate+dt.timedelta(days=int(d))).month, (self.refdate+dt.timedelta(days=int(d))).day] for d in self.timevec]
            savedict              = io.std_savedict.copy()
            savedict['name']      = 'datearray'
            savedict['long_name'] = 'date array as [YYYY,MM,DD]'
            savedict['dims']      = dimt + dimda
            savedict['values']    = np.array(datearray)
            ncf.add_data(savedict)

            membern = mem.membernumber
            nee_grid = self.vector2grid(vectordata=self.nee_data[membern].ravel(),nparams=len(self.timevec))

            if self.add_ano:
                dter_data = self.calculate_anoterm(mem.param_values.reshape((self.nregions, self.nparams_per_region)), self.timevec)
                nee_grid+=dter_data
            # add flux
            savedict              = io.std_savedict.copy()
            savedict['name']      = 'NEE_grid'
            savedict['long_name'] = ('Simulated NEE values on lat-lon grid for ensemble member %s' % mem.membernumber)
            savedict['dims']      = dimt + dimgrid
            savedict['values']    = nee_grid.tolist()
            savedict['units']     = 'mol/m2/s'
            ncf.add_data(savedict)

            logging.info('Fluxmap for member %s written to file (%s)' %(mem.membernumber, filename))
            ####################################################################

            ncf.close()

            logging.debug('Successfully wrote data from ensemble member %d to file (%s) ' % (mem.membernumber, filename))
        # exit()
        return

    def propagate(self, dacycle):
        """
        :rtype: None

        Propagate the parameter values in the StateVector to the next cycle. This means a shift by one cycle
        step for all states that will
        be optimized once more, and the creation of a new ensemble for the time step that just
        comes in for the first time (step=nlag).
        In the future, this routine can incorporate a formal propagation of the statevector.

        """

        import da.analysis.tools_time as at

        # Remove State Vector n=1 by simply "popping" it from the list and appending a new empty list at the front. This empty list will
        # hold the new ensemble for the new cycle

        self.ensemble_members.pop(0)
        self.ensemble_members.append([])

        # And now create a new time step of mean + members for n=nlag

        date = dacycle['time.start'] + at.timedelta(days=(self.nlag - 0.5) * int(dacycle['time.cycle']))

        logging.info("STATEVECTOR PROPAGATE ENDTIME = %s, date = %s" % (dacycle['time.end'],date))

        cov = self.get_covariance(date, dacycle)
        self.make_new_ensemble(self.nlag - 1, cov)

        logging.info('The state vector has been propagated by one cycle')

    def validate_input(self):
        """ Make sure that data needed for the FluxModel (such as anomaly inputdata) are present."""

        if self.add_ano and not os.path.exists(self.anofile):
            msg = "Anomaly input file for CTSF inversion does not exist, exiting..."
            logging.error(msg)
            raise (IOError, msg)

        # read anomalies from file for defined timerange
        if self.add_ano:
            self.ano = self.get_anomalies(self.anofile, self.anoname, self.timevec)
            logging.info('Obtained anomalies for timerange.')

        return


    def get_anomalies(self, file, name, timevec):
        """ Read anomalies from file. The returned anomaly array will contain data for all times in timevec. Fillvalue = NaN."""

        # read input data
        ano, t_ano = self.get_anodata(file,name,getTime=True)
        # get index of simulation times in anomaly input array
        t_ano_index = self.get_time_index(t_ano, timevec)
        # replace anomaly array by new array that contains anomalies for the requested simulation times
        ano = self.build_anomalies_samples(ano, t_ano_index)

        return ano

    def get_mean(self, file, name, timevec):
        """ Read mean seasonal cycle from file. The returned mean array will contain data for all times in timevec. Fillvalue = NaN."""

        # read input data
        mean, t_mean = self.get_anodata(file,name,getTime=True)

        # convert to gridded mean data for timevec
        mean_out = np.zeros((len(timevec),self.nlat,self.nlon))

        if len(mean.shape) == 2 and (mean.shape[0] == 12 or mean.shape[1] == 12):
            for nr in range(209): #max(self.gridmap)):
                mask = np.where(self.gridmap == nr+1,1,0)
                for t in range(len(timevec)):
                    month = (self.refdate + dt.timedelta(days=int(timevec[t]))).month - 1
                    if mean.shape[0] == 12:
                        mean_out[t,:,:] += mask * mean[month,nr]
                    else:
                        mean_out[t,:,:] += mask * mean[nr,month]

        elif len(mean.shape) == 3 and mean.shape[0] == 12 and mean.shape[1] == self.nlat and mean.shape[2] == self.nlon:
            for t in range(len(timevec)):
                month = (self.refdate + dt.timedelta(days=int(timevec[t]))).month - 1
                mean_out[t,:,:] = mean[month,:,:]

        elif len(mean.shape) == 3 and mean.shape[0] == len(t_mean) and mean.shape[1] == self.nlat and mean.shape[2] == self.nlon:
            # get index of simulation times in mean input array
            t_ano_index = self.get_time_index(t_mean, timevec)
            # replace anomaly array by new array that contains anomalies for the requested simulation times
            mean_out = self.build_anomalies_samples(mean, t_ano_index)

        else:
            logging.error('Array with mean temperature data for respiration parametrization has invalid size (%s). Valid options are [12,nregions], [12,%s,%s] or [t,%s,%s]' \
                          % (str(mean.shape), self.nlat, self.nlon, self.nlat, self.nlon))
            raise ValueError

        return mean_out

#
# Functions for evaluation of the fluxmodel from 
#   fitparmas
#   an anomaly

    def get_anodata(self,filename,varname,getTime = False):

        """
        : param filename: full path to netCDF file containing 'datatype' data on 1x1 deg grid
        : param varname: string to identify the data to be read from 'filename'.

        Method to read data for construction of NEE fit, and corresponding timerange, from netCDF file.
        Note: currently units are not checked!
        """

        ncf = io.ct_read(filename, method='read')

        anodata = np.nan_to_num(ncf.get_variable(varname))
        if getTime:
            t_anodata = ncf.get_variable('time')
            ncf.close()
            logging.info("Successfully read %s and time from file (%s)" % (varname, filename))
            return anodata, t_anodata
        else:
            logging.info("Successfully read %s from file (%s)" % (varname, filename))
            return anodata



    def get_time_index(self,t_ano, timevec):
        """
        Returns a list with for every date in self.timevec the index in the input vector 'times'.
        If no input data is present for the requested time, index = None
        """

        index = []

        for t in range(len(timevec)):
            foundindex = False
            date = self.refdate + dt.timedelta(days=int(timevec[t]))
            for tt in range(len(t_ano)):
                date_input = self.refdate + dt.timedelta(days=int(t_ano[tt]))
                if (date.year == date_input.year) and (date.month == date_input.month):
                    index.append(tt)
                    foundindex = True
                    break
            if not foundindex:
                logging.info("Missing anomaly input data for month %s" % date.strftime('%Y-%m-%d'))
                index.append(None)

        logging.debug("Obtained anomaly time indices")

        return index

    def build_anomalies_samples(self, anomaly, index):
        """ Returns a 3D array with input anomaly data for times corresponding to timevec only. Fillvalue = NaN."""

        logging.debug('anomaly shape = %s' %str(anomaly.shape))
        sampled_anomaly = np.zeros((len(index),anomaly.shape[1],anomaly.shape[2]))
        for i in range(len(index)):
            if index[i] is None:
                sampled_anomaly[i,:,:] = np.nan
            else:
                sampled_anomaly[i,:,:] = anomaly[index[i],:,:]

        logging.debug("Created array with time-sampled anomalies.")

        return sampled_anomaly

    def evaluate_model(self,statemap,timevec):

        """
        Method to calculate an NEE field consisting of regionally constant second-order polynomial and harmonical terms with linearly varying amplitude
        (to account for mean seasonal cycle), enhanced with scaled anomalies to account for spatial and interannual variability.
        """

        # check dimensions of input
        if statemap.shape[0] != self.nregions:
            logging.error("Incorrect number of statemap parameters per grid cell (%d) (must equal the number of fit parameters (%d))." % (statemap.shape[0], self.nfitparams))
            raise ValueError
        # if self.add_ano and ((self.ano.shape[0] != len(timevec)) or (self.ano.shape[1] != self.nlat) or (self.ano.shape[2] != self.nlon)):
        #     logging.error("Incorrect dimensions of anomaly: %s, required: %i,%i,%i." % (self.ano.shape, len(timevec), self.nlat, self.nlon))
        #     raise ValueError
        # if self.add_resp and ((self.resp_ano.shape[0] != len(timevec)) or (self.resp_ano.shape[1] != self.nlat) or (self.resp_ano.shape[2] != self.nlon)):
        #     logging.error("Incorrect dimensions of respiration anomaly: %s, required: %i,%i,%i." % (self.resp_ano.shape, len(timevec), self.nlat, self.nlon))
        #     raise ValueError

        i1 = self.nbasefitparams
        # i2 = i1 + self.ngamma
        # i3 = i2 + self.nresp
        # if i2 != self.nfitparams:
            # logging.error("Inconsistent number of fit parameters specified...")
            # raise ValueError

        nee = (self.calculate_polynomic(statemap[:, :self.npolyterms], timevec) + self.calculate_harmonics_sincos(statemap[:, self.npolyterms: i1], timevec))

#        nee = nee + dgpp + dter
        # Return the transpose to give shape(nregion,timevec)
        # convert from micromol to mol
        return nee.T  #, dgpp.T, dter.T  


    def calculate_polynomic(self, statemap, timevec):
        """Calculate the NEE due to the polynomic terms
        Input: 
            statemap (np.array of NREGIONS x NPOLYTERMS): statevector values reshaped
            timevec (np.array len(NTIMES): array with number of days after a set starttime
        Output:
            NEE: (np.array of NTIMES x NREGIONS): Calculated NEE due to the polynomic terms"""
        polynee = np.zeros((len(timevec), len(statemap)))
        for i in range(self.npolyterms):
            polynee += statemap[:, i] * (timevec[:, None] ** i)
    
        return polynee 

    def calculate_harmonics_sincos(self,statemap,timevec):
        """Calculate the NEE due to the harmonic terms
        Add cosine and sine waves as decomposition
        Input: 
            statemap (np.array of NREGIONS x NHARMONICS): statevectorvalues reshaped
            timevec (np.array len(NTIMES): array with number of days after a set starttime
        Output: 
            NEE: (np.array of NTIMES x NREGIONS): calculated NEE due to the harmonic terms"""

        days_in_year = 365.242
        har = np.zeros((len(timevec),len(statemap)), 'float')

        for i in range(self.nharmonics):
            j = i + self.nharmonics
            har += statemap[:, i] * np.cos( 2.0*np.pi / days_in_year * (i + 1) * timevec[:, None]) \
                 + statemap[:, j] * np.sin( 2.0*np.pi / days_in_year * (i + 1) * timevec[:, None])
        return har #* (1 + (statemap[:, j+1] * timevec[:, None]))

    def calculate_anoterm(self,statemap,timevec,mask=None):
        """Calculate the NEE due to the anomalies
        Input:
            statemap (np.array of NREGIONS x NGAMMA): statevectorvalues reshaped
            timevec (np.array len(NTIMES): array with number of days after a set starttime
        Output:
            NEE: (np.array of NTIMES x NREGIONS): calculated NEE due to the harmonic terms"""
        i1 = self.nlambda 
        # i1 = self.nbasefitparams

        NDAYS_PER_GAMMA = 366.1 / self.ngamma # make just over a year, so that integer rounding returns a index (alwasy smaller than ngamma)
        if mask is None:
            timevec_masked=timevec
        else:
            timevec_masked=timevec[mask]
        anoterm = np.zeros((len(timevec_masked),np.shape(self.ano)[1],np.shape(self.ano)[2]))
        for i, t in enumerate(timevec_masked):
            idx = int((t % 365) // NDAYS_PER_GAMMA)
            gridded_gamma=self.vector2grid(vectordata=statemap[:, i1 + idx],nparams=1)
            if mask is not None:
                # we need to check the index in anoterm
                anoterm[i,:,:] = gridded_gamma * np.nan_to_num(self.ano[mask][i,:,:])
            else:
                anoterm[i,:,:] = gridded_gamma * np.nan_to_num(self.ano[i,:,:])
        return anoterm



    # def calculate_resp_anoterm(self,statemap,timevec):
    #     """Calculate the NEE due to the anomalies
    #     Input:
    #         statemap (np.array of NREGIONS x NGAMMA): statevectorvalues reshaped
    #         timevec (np.array len(NTIMES): array with number of days after a set starttime
    #     Output:
    #         NEE: (np.array of NTIMES x NREGIONS): calculated NEE due to the harmonic terms"""
    #     anoterm = np.zeros(self.resp_ano.shape)

    #     for t in range(len(timevec)):
    #         if self.nresp == 2:
    #             anoterm[t,:,:] = statemap[0,:,:] * (np.exp(self.k0*(self.resp_ano[t,:,:]-self.temp0)) - np.exp(self.k0*(self.resp_mean[t,:,:]-self.temp0))) \
    #                              + statemap[1,:,:] * ( (self.resp_ano[t,:,:]-self.temp0) * np.exp(self.k0*(self.resp_ano[t,:,:]-self.temp0)) \
    #                              - (self.resp_mean[t,:,:]-self.temp0) * np.exp(self.k0*(self.resp_mean[t,:,:]-self.temp0)) )
    #         elif self.nresp == 13:
    #             month = (self.refdate + dt.timedelta(days=int(timevec[t]))).month - 1
    #             anoterm[t,:,:] = statemap[0,:,:] * (np.exp(self.k0*(self.resp_ano[t,:,:]-self.temp0)) - np.exp(self.k0*(self.resp_mean[t,:,:]-self.temp0))) \
    #                              + statemap[1+month,:,:] * ( (self.resp_ano[t,:,:]-self.temp0) * np.exp(self.k0*(self.resp_ano[t,:,:]-self.temp0)) \
    #                              - (self.resp_mean[t,:,:]-self.temp0) * np.exp(self.k0*(self.resp_mean[t,:,:]-self.temp0)) )
    #         else:
    #             logging.error("Invalid number of respiration parameters (%s), valid options are 2 or 13" % self.nresp)
    #             raise ValueError

    #     return anoterm

################### End Class CTSFStateVector ###################


if __name__ == "__main__":
    pass

