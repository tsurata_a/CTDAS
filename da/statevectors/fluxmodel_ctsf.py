"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
# model.py

from __future__ import division

import logging
import os
import numpy as np
import datetime as dt
import da.tools.io4 as io
from da.tools.general import to_datetime
#from da.baseclasses.observationoperator import ObservationOperator
from da.obsoperators.observationoperator_baseclass import ObservationOperator

identifier = 'ctsfFluxModel'
version = '1.0'

################### Begin Class ObservationOperator ###################
class ctsfFluxModel(ObservationOperator):
    """
    This class calculates NEE using a statistical model. Nee is defined as :
    the sum of a series expansion representing the mean seasonal cycle and long term trends
    + a GPP anomaly term that is linearly related to the anomaly in a proxy
    + a TER anomaly term that is a linearized version of a Q10 temperature relationship

    Functions:
    - setup(dacycle)
    - validate_input()
    - calc_flux(time, member=None, updateano=True, postprocessing=False, write=True)
    - write_flux(flux, name, time, member, postprocessing)
    - get_anomalies(file, name, time): for reading the anomalies in GPP proxy and temperature
    - get_mean(file, name, time): for reading the mean temperatures for TER anomaly term
    - get_anodata(filename,datatype,getTime = False): called from get_anomalies and get_mean
    - get_time_index(t_ano, time): called from get_anomalies and get_mean
    - build_anomalies_samples(anomaly, index): called from get_anomalies and get_mean
    - evaluate_model(statemap,time): actual NEE calculation, returns 3D array (time x lat x lon)
    - calculate_harmonics_sincos(statemap,time): calculates the sin and cos terms that represent mean seasonal cycle (called from evaluate_model)
    - calculate_anoterm(statemap,time): calculates the GPP anomaly term (called from evaluate_model)
    - calculate_resp_anoterm(statemap,time): calculates the TER anomaly term (called from evaluate_model)

    """

    def __init__(self):
        self.ID = identifier
        self.version = version

        logging.info('FluxModel object initialized: %s' % self.ID)



    def setup(self,dacycle):
        """ Perform all steps necessary to start the observation operator through a simple Run() call """

        self.dacycle   = dacycle
        self.outputdir = dacycle['dir.output']

        self.forecast_nmembers = int(dacycle['da.optimizer.nmembers'])

        # anomaly term
        self.add_ano     = dacycle.dasystem['add.anomalies']
        if self.add_ano:
            self.ngamma  = int(dacycle.dasystem['ngamma'])                      # number of sensitivity parameters per ecoregion: default = 12, 1 per calendar month
            self.gamma0  = dacycle.dasystem['params.init.gamma'].split(',')     # enter single value, or comma-separated list of ngamma (default: one for each month)
            self.anofile = dacycle.dasystem['fit.anomaly.inputfile']
            self.anoname = dacycle.dasystem['fit.anomaly.name']                 # name of anomaly in inputfile
        else:
            self.ngamma  = 0

        # respiration parametrization
        self.add_resp   = dacycle.dasystem['add.respiration']
        if self.add_resp:
            self.nresp  = 13
            self.temp0  = 273.15
            self.k0     = float(dacycle.dasystem['params.init.k'])
            self.resp_ano_file  = dacycle.dasystem['resp.anomaly.inputfile']
            self.resp_ano_name  = dacycle.dasystem['resp.anomaly.name']         # name of anomaly (temperature) in inputfile
            self.resp_mean_file = dacycle.dasystem['resp.mean.inputfile']
            self.resp_mean_name = dacycle.dasystem['resp.mean.name']
            mapfile      = os.path.join(dacycle.dasystem['regionsfile'])
            ncf          = io.ct_read(mapfile, 'read')
            self.gridmap = ncf.get_variable('regions')
        else:
            self.nresp  = 0

        # model parameters
        self.nhar           = int(dacycle.dasystem['nharmonics'])               # number of harmonical terms in baseline fit
        self.nbasefitparams = 3 + self.nhar*4
        self.nfitparams     = self.nbasefitparams + self.ngamma + self.nresp

        self.nlat           = 180                                               # 1x1 deg grid
        self.nlon           = 360                                               # 1x1 deg grid

        self.unit_initialization = dacycle.dasystem['fit.unit_initialization']  # if True: initial fit will be constructed based on latitude, else: read from file
        self.refdate             = to_datetime(dacycle.dasystem['time.reference'])
        writememberfluxes        = self.dacycle.dasystem['writeMemberFluxes']   # write gridded fluxes for every member? [bool]

        logging.info('Read parameters for flux calculation')



    def validate_input(self):
        """ Make sure that data needed for the FluxModel (such as anomaly inputdata) are present."""

        if self.add_ano and not os.path.exists(self.anofile):
            msg = "Anomaly input file for CTSF inversion does not exist, exiting..."
            logging.error(msg)
            raise (IOError, msg)

        if self.add_resp and not os.path.exists(self.resp_ano_file):
            msg = "Respiration anomaly input file for CTSF inversion does not exist, exiting..."
            logging.error(msg)
            raise (IOError, msg)

        if self.add_resp and self.resp_mean_file is not None and not os.path.exists(self.resp_mean_file):
            msg = "Respiration mean input file for CTSF inversion does not exist, using reference temperature instead"
            self.resp_mean_file = None
            self.resp_mean_name = None



    def calc_flux(self, timevec, member=None, updateano=True, postprocessing=False, write=True):
        """ Calculate gridded flux field and write to file, for one or all ensemble members."""

        if updateano:

            # check input
            self.validate_input()

            # read anomalies from file for defined timerange
            if self.add_ano:
                self.ano = self.get_anomalies(self.anofile, self.anoname, timevec)
                logging.info('Obtained anomalies for timerange.')

            if self.add_resp:
                self.resp_ano  = self.get_anomalies(self.resp_ano_file, self.resp_ano_name, timevec)
                if self.resp_mean_name is None:
                    self.resp_mean = self.temp0 * np.ones(self.resp_ano.shape)
                else:
                    self.resp_mean = self.get_mean(self.resp_mean_file, self.resp_mean_name, timevec)
                # self.resp_ano, self.resp_mean = self.get_resp_anomalies(timevec)
                logging.info('Obtained anomalies and mean for respiration term')

        # loop over ensemble of statevectors. For each member, evaluate flux model
        for em in range(self.forecast_nmembers):

            if member is not None:
                em = member

            # read statevector, convert statevector to statevectormap...

            # read gridded statevector member
            if postprocessing:
                filename = self.dacycle['dir.output'] +'/fluxmodel.' + str(em).zfill(3) + '.nc'
            else:
                filename = self.dacycle['dir.da_run'] + '/input/fluxmodel.' + str(em).zfill(3) + '.nc'
            ncf      = io.ct_read(filename, 'read')
            statemap = ncf.get_variable('parametermap')
            ncf.close()

            # calculate flux for member
            nee, dgpp, dter = self.evaluate_model(statemap, timevec)
            logging.debug('Calculated fluxes for ensemble member %i' % em)

            # write gridded flux to file
            self.write_flux(nee, dgpp, dter, timevec, em, postprocessing)

            if member is not None:
                break

        del em

        if not (member is None or write):
            return nee



    def write_flux(self, flux, dgpp, dter, timevec, em, postprocessing):
        """Write time and calculated fluxmap to fluxmodel.<member>.nc."""

        # Create a file to hold simulated fluxes and parameters for each member
        if postprocessing:
            # create new file if necessary
            fluxfile = os.path.join(self.outputdir, 'fluxmodel.%03d.nc' % em)
            if os.path.exists(fluxfile):
                f         = io.CT_CDF(fluxfile, method='write')
            else:
                f         = io.CT_CDF(fluxfile, method='create')
                dimlatlon = f.add_latlon_dim()
        else:
            # open member file
            fluxfile = os.path.join(self.dacycle['dir.input'], 'fluxmodel.%03d.nc' % em)
            f        = io.CT_CDF(fluxfile, method='write')

        # add time
        dimt                  = f.createDimension('date',len(timevec))
        dimt                  = ('date',)
        savedict              = io.std_savedict.copy()
        savedict['name']      = 'date'
        savedict['long_name'] = 'date as days since 1-1-2000'
        savedict['dims']      = dimt
        savedict['units']     = 'days since 1-1-2000'
        savedict['values']    = timevec
        f.add_data(savedict)

        # add date as array
        datearray = [[(self.refdate+dt.timedelta(days=int(d))).year,(self.refdate+dt.timedelta(days=int(d))).month, (self.refdate+dt.timedelta(days=int(d))).day] for d in timevec]
        dimda                 = f.createDimension('datearray',3)
        dimda                 = ('datearray',)
        savedict              = io.std_savedict.copy()
        savedict['name']      = 'datearray'
        savedict['long_name'] = 'date array as [YYYY,MM,DD]'
        savedict['dims']      = dimt + dimda
        savedict['values']    = np.array(datearray)
        f.add_data(savedict)

        # add flux
        dimlatlon             = ('latitude', 'longitude',)
        savedict              = io.std_savedict.copy()
        savedict['name']      = 'NEE_grid'
        savedict['long_name'] = ('Simulated NEE values on lat-lon grid for ensemble member %s' % em )
        savedict['dims']      = dimt + dimlatlon
        savedict['values']    = flux
        savedict['units']     = 'mol/m2/s'
        f.add_data(savedict)

        # add GPP anomaly
        dimlatlon             = ('latitude', 'longitude',)
        savedict              = io.std_savedict.copy()
        savedict['name']      = 'dGPP_grid'
        savedict['long_name'] = ('Simulated GPP anomaly values on lat-lon grid for ensemble member %s' % em )
        savedict['dims']      = dimt + dimlatlon
        savedict['values']    = dgpp * 1.0E-6 # convert from micromol to mol
        savedict['units']     = 'mol/m2/s'
        f.add_data(savedict)

        # add TER anomaly
        dimlatlon             = ('latitude', 'longitude',)
        savedict              = io.std_savedict.copy()
        savedict['name']      = 'dTER_grid'
        savedict['long_name'] = ('Simulated TER anomaly values on lat-lon grid for ensemble member %s' % em )
        savedict['dims']      = dimt + dimlatlon
        savedict['values']    = dter * 1.0E-6 # convert from micromol to mol
        savedict['units']     = 'mol/m2/s'
        f.add_data(savedict)

        f.close()

        logging.info('Fluxmap for member %s written to file (%s)' %(em, fluxfile))



    def get_anomalies(self, file, name, timevec):
        """ Read anomalies from file. The returned anomaly array will contain data for all times in timevec. Fillvalue = NaN."""

        # read input data
        ano, t_ano = self.get_anodata(file,name,getTime=True)
        # get index of simulation times in anomaly input array
        t_ano_index = self.get_time_index(t_ano, timevec)
        # replace anomaly array by new array that contains anomalies for the requested simulation times
        ano = self.build_anomalies_samples(ano, t_ano_index)

        return ano



    def get_mean(self, file, name, timevec):
        """ Read mean seasonal cycle from file. The returned mean array will contain data for all times in timevec. Fillvalue = NaN."""

        # read input data
        mean, t_mean = self.get_anodata(file,name,getTime=True)

        # convert to gridded mean data for timevec
        mean_out = np.zeros((len(timevec),self.nlat,self.nlon))

        if len(mean.shape) == 2 and (mean.shape[0] == 12 or mean.shape[1] == 12):
            for nr in range(209): #max(self.gridmap)):
                mask = np.where(self.gridmap == nr+1,1,0)
                for t in range(len(timevec)):
                    month = (self.refdate + dt.timedelta(days=int(timevec[t]))).month - 1
                    if mean.shape[0] == 12:
                        mean_out[t,:,:] += mask * mean[month,nr]
                    else:
                        mean_out[t,:,:] += mask * mean[nr,month]
                del t
            del nr

        elif len(mean.shape) == 3 and mean.shape[0] == 12 and mean.shape[1] == self.nlat and mean.shape[2] == self.nlon:
            for t in range(len(timevec)):
                month = (self.refdate + dt.timedelta(days=int(timevec[t]))).month - 1
                mean_out[t,:,:] = mean[month,:,:]
            del t

        elif len(mean.shape) == 3 and mean.shape[0] == len(t_mean) and mean.shape[1] == self.nlat and mean.shape[2] == self.nlon:
            # get index of simulation times in mean input array
            t_ano_index = self.get_time_index(t_mean, timevec)
            # replace anomaly array by new array that contains anomalies for the requested simulation times
            mean_out = self.build_anomalies_samples(mean, t_ano_index)

        else:
            logging.error('Array with mean temperature data for respiration parametrization has invalid size (%s). Valid options are [12,nregions], [12,%s,%s] or [t,%s,%s]' \
                          % (str(mean.shape), self.nlat, self.nlon, self.nlat, self.nlon))
            raise ValueError

        return mean_out



    def get_anodata(self,filename,varname,getTime = False):

        """
        : param filename: full path to netCDF file containing 'datatype' data on 1x1 deg grid
        : param varname: string to identify the data to be read from 'filename'.

        Method to read data for construction of NEE fit, and corresponding timerange, from netCDF file.
        Note: currently units are not checked!
        """

        ncf = io.ct_read(filename, method='read')

        anodata = np.nan_to_num(ncf.get_variable(varname))
        if getTime:
            t_anodata = ncf.get_variable('time')
            ncf.close()
            logging.info("Successfully read %s and time from file (%s)" % (varname, filename))
            return anodata, t_anodata
        else:
            logging.info("Successfully read %s from file (%s)" % (varname, filename))
            return anodata



    def get_time_index(self,t_ano, timevec):
        """
        Returns a list with for every date in self.timevec the index in the input vector 'times'.
        If no input data is present for the requested time, index = None
        """

        index = []

        for t in range(len(timevec)):
            foundindex = False
            date = self.refdate + dt.timedelta(days=int(timevec[t]))
            for tt in range(len(t_ano)):
                date_input = self.refdate + dt.timedelta(days=int(t_ano[tt]))
                if (date.year == date_input.year) and (date.month == date_input.month):
                    index.append(tt)
                    foundindex = True
                    break
            del tt
            if not foundindex:
                logging.info("Missing anomaly input data for month %s" % date.strftime('%Y-%m-%d'))
                index.append(None)
        del t

        logging.debug("Obtained anomaly time indices")

        return index



    def build_anomalies_samples(self, anomaly, index):
        """ Returns a 3D array with input anomaly data for times corresponding to timevec only. Fillvalue = NaN."""

        logging.debug('anomaly shape = %s' %str(anomaly.shape))
        sampled_anomaly = np.zeros((len(index),anomaly.shape[1],anomaly.shape[2]))
        for i in range(len(index)):
            if index[i] is None:
                sampled_anomaly[i,:,:] = np.nan
            else:
                sampled_anomaly[i,:,:] = anomaly[index[i],:,:]

        del i
        logging.debug("Created array with time-sampled anomalies.")

        return sampled_anomaly



    def evaluate_model(self,statemap,timevec):

        """
        Method to calculate an NEE field consisting of regionally constant second-order polynomial and harmonical terms with linearly varying amplitude
        (to account for mean seasonal cycle), enhanced with scaled anomalies to account for spatial and interannual variability.
        """

        # check dimensions of input
        if statemap.shape[0] != self.nfitparams:
            logging.error("Incorrect number of statemap parameters per grid cell (%d) (must equal the number of fit parameters (%d))." % (statemap.shape[0], self.nfitparams))
            raise ValueError
        if self.add_ano and ((self.ano.shape[0] != len(timevec)) or (self.ano.shape[1] != self.nlat) or (self.ano.shape[2] != self.nlon)):
            logging.error("Incorrect dimensions of anomaly: %s, required: %i,%i,%i." % (self.ano.shape, len(timevec), self.nlat, self.nlon))
            raise ValueError
        if self.add_resp and ((self.resp_ano.shape[0] != len(timevec)) or (self.resp_ano.shape[1] != self.nlat) or (self.resp_ano.shape[2] != self.nlon)):
            logging.error("Incorrect dimensions of respiration anomaly: %s, required: %i,%i,%i." % (self.resp_ano.shape, len(timevec), self.nlat, self.nlon))
            raise ValueError

        i1 = self.nbasefitparams
        i2 = i1 + self.ngamma
        i3 = i2 + self.nresp
        if i3 != self.nfitparams:
            logging.error("Inconsistent number of fit parameters specified...")
            raise ValueError

        # calculate NEE (len(time) x nlat x nlon)
        nee = statemap[0,:,:] + statemap[1,:,:]*timevec[:,np.newaxis,np.newaxis] + statemap[2,:,:]*(timevec[:,np.newaxis,np.newaxis]**2) \
              + self.calculate_harmonics_sincos(statemap[3:i1,:,:],timevec)

        if self.add_ano:
            dgpp = self.calculate_anoterm(statemap[i1:i2,:,:],timevec)
        else: dgpp = np.zeros(nee.shape)

        if self.add_resp:
            dter = self.calculate_resp_anoterm(statemap[i2:i3,:,:],timevec)
        else: dter = np.zeros(nee.shape)

        nee = nee + dgpp + dter

        return nee*1.0e-6, dgpp*1.0e-6, dter*1.0e-6 # convert from micromol to mol



    def calculate_harmonics(self,statemap,timevec):

        """
        DO NOT USE!!!
        : param statemap: scalingfactors for the fit parameters and anomalies, dim = nhar*3 , 180 , 360
        : param baselinefit: polynomial and harmonic fit parameters, dim = nhar*3 , 180 , 360

        Method to calculate harmonical terms with linearly varying amplitude: harmonic_i = (a + b*t) * sin(2*pi*i*t + c)
        """

        days_in_year = 365.242
        har = np.zeros((len(timevec),statemap.shape[1],statemap.shape[2]), 'float')

        for i in range(self.nhar):
            har += (statemap[3*i,:,:] + statemap[3*i+1,:,:]*timevec[:,np.newaxis,np.newaxis]) \
                    * np.sin( 2.0*np.pi/days_in_year*(i+1)* (timevec[:,np.newaxis,np.newaxis] + statemap[3*i+2,:,:]) + np.pi/2 )
        del i

        return har



    def calculate_harmonics_sincos(self,statemap,timevec):
        # check if number of statevector elements equals 4*nhar
        if statemap.shape[0] != 4*self.nhar:
            logging.error('Incorrect number of statevector elements (%s) send to calculate_harmonics_sincos, which requires 4*%s elements'\
            % (statemap.shape[0],self.nhar))

        days_in_year = 365.242
        har = np.zeros((len(timevec),statemap.shape[1],statemap.shape[2]), 'float')

        for i in range(self.nhar):
            har += (statemap[4*i,:,:] + statemap[4*i+1,:,:]*timevec[:,np.newaxis,np.newaxis]) \
                    * np.cos( 2.0*np.pi/days_in_year*(i+1)* timevec[:,np.newaxis,np.newaxis]) \
                    + (statemap[4*i+2,:,:] + statemap[4*i+3,:,:]*timevec[:,np.newaxis,np.newaxis]) \
                    * np.sin( 2.0*np.pi/days_in_year*(i+1)* timevec[:,np.newaxis,np.newaxis])
        del i

        return har



    def calculate_anoterm(self,statemap,timevec):

        anoterm = np.zeros(self.ano.shape)

        for t in range(len(timevec)):
            month = (self.refdate + dt.timedelta(days=int(timevec[t]))).month - 1
            anoterm[t,:,:] = statemap[month,:,:] * np.nan_to_num(self.ano[t,:,:])
        del t

        return anoterm



    def calculate_resp_anoterm(self,statemap,timevec):

        anoterm = np.zeros(self.resp_ano.shape)

        for t in range(len(timevec)):
            if self.nresp == 2:
                anoterm[t,:,:] = statemap[0,:,:] * (np.exp(self.k0*(self.resp_ano[t,:,:]-self.temp0)) - np.exp(self.k0*(self.resp_mean[t,:,:]-self.temp0))) \
                                 + statemap[1,:,:] * ( (self.resp_ano[t,:,:]-self.temp0) * np.exp(self.k0*(self.resp_ano[t,:,:]-self.temp0)) \
                                 - (self.resp_mean[t,:,:]-self.temp0) * np.exp(self.k0*(self.resp_mean[t,:,:]-self.temp0)) )
            elif self.nresp == 13:
                month = (self.refdate + dt.timedelta(days=int(timevec[t]))).month - 1
                anoterm[t,:,:] = statemap[0,:,:] * (np.exp(self.k0*(self.resp_ano[t,:,:]-self.temp0)) - np.exp(self.k0*(self.resp_mean[t,:,:]-self.temp0))) \
                                 + statemap[1+month,:,:] * ( (self.resp_ano[t,:,:]-self.temp0) * np.exp(self.k0*(self.resp_ano[t,:,:]-self.temp0)) \
                                 - (self.resp_mean[t,:,:]-self.temp0) * np.exp(self.k0*(self.resp_mean[t,:,:]-self.temp0)) )
            else:
                logging.error("Invalid number of respiration parameters (%s), valid options are 2 or 13" % self.nresp)
                raise ValueError
        del t

        return anoterm



    def read_from_file(self,filename):
        # Read flux from output/date/fluxmodel.000.nc file, and select flux for current time based on dates
        # Only used for forward run, so no lag loop!

        f = io.ct_read(filename, 'read')
        meanflux = f.get_variable('NEE_grid') # mol/m2/s
        dates    = f.get_variable('date')     # days since 1-1-2000
        f.close()

        for n in range(self.nlag):
            if not self.ensemble_members[n] == []:
                self.ensemble_members[n] = []
                logging.warning('Existing ensemble for lag=%d was removed to make place for newly read data' % (n + 1))

            for m in range(self.nmembers):
                newmember = EnsembleMember(m)
                newmember.param_values = ensmembers[n, m, :].flatten() + meanstate[n]  # add the mean to the deviations to hold the full parameter values
                self.ensemble_members[n].append(newmember)

        logging.info('Successfully read the flux for %s from file (%s) ' % (filename))
