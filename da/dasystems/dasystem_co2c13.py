#!/usr/bin/env python
# control.py

"""
Author : peters 

Revision History:
File created on 26 Aug 2010.

"""

import logging

################### Begin Class CO2C13DaSystem ###################

from da.dasystems.dasystem_baseclass import DaSystem

class CO2C13DaSystem(DaSystem):
    """ Information on the data assimilation system used. This is normally an rc-file with settings.
    """
    def validate(self):
        """ 
        Validate the contents of the rc-file given a dictionary of required keys
        """

        needed_rc_items = ['obs.input.dir',
                           'obs.input.fname',
                           'obspack.input.id',
                           'obspack.input.dir',
                           'obspack.c13.input.id',
                           'obspack.c13.input.dir',
                           'ocn.covariance',
                           'nparameters',
                           'bio.covariance',
                           'deltaco2.prefix',
                           'regtype']


        for k, v in list(self.items()):
            if v == 'True' : 
                self[k] = True
            if v == 'False': 
                self[k] = False

        for key in needed_rc_items:
            if key not in self:
                logging.warning('Missing a required value in rc-file : %s' % key)
        logging.debug('DA System Info settings have been validated succesfully')

################### End Class CO2C13DaSystem ###################


if __name__ == "__main__":
    pass
