import datetime as dt
from glob import glob
import numpy as np 
import netCDF4 as nc
import logging
# move to platform?
# ctdas config? 
ctdas_root='/projects/0/ctdas/input/ctdas_2012/'
# ctdas_root='/mnt/beegfs/user/gkoren/ctdas_2012/'
# c13input='/mnt/beegfs/user/joram.hooghiem'
c13input='/projects/0/ctdas/joram'

carboscopev2021 = {'fileroot' : ctdas_root+'oceans/carboscope_v2021/oc_v2021_monthly_mol_s_m2_'         , 'strf' : '%Y'       , 'data_per_day' : 1 , 'key': 'co2flux_ocean'       ,'ctname' : 'co2_ocn'}
carboscopev2022 = {'fileroot' : ctdas_root+'oceans/carboscope_v2022/oc_v2022_monthly_mol_s_m2_'         , 'strf' : '%Y'       , 'data_per_day' : 1 , 'key': 'co2flux_ocean'       ,'ctname' : 'co2_ocn'}
sib4v2462022    = {'fileroot' : ctdas_root+'biosphere/SiB4v2_BioFluxes_v246/SiB4v2_BioFluxes_v246_1x1_' , 'strf' : '%Y%m'     , 'data_per_day' : 8 , 'key': 'nee'                 ,'ctname' : 'co2_bio'}
sib4gpp    = {'fileroot' : ctdas_root+'biosphere/SiB4v2_BioFluxes_v247/SiB4v2_BioFluxes_v247_1x1_' , 'strf' : '%Y%m'     , 'data_per_day' : 8 , 'key': 'gpp'                 ,'ctname' : 'co2_gpp'}
sib4reco    = {'fileroot' : ctdas_root+'biosphere/SiB4v2_BioFluxes_v247/SiB4v2_BioFluxes_v247_1x1_' , 'strf' : '%Y%m'     , 'data_per_day' : 8 , 'key': 'reco'                 ,'ctname' : 'co2_reco'}
sib4bioiav      = {'fileroot' : c13input+'/input/SiB4v2_246_split/iav_'                   , 'strf' : '%Y-%m-%d' , 'data_per_day' : 8 , 'key': 'nee_iav'             ,'ctname' : 'co2_bioiav'}
sib4biomean     = {'fileroot' : c13input+'/input/SiB4v2_246_split/daymean_'               , 'strf' : '%m-%d'    , 'data_per_day' : 1 , 'key': 'nee_daymean'         ,'ctname' : 'co2_biom'}
sib4isobio      = {'fileroot' : c13input+'/input/13co2/SiB4_v2/isotm5_'                   , 'strf' : '%Y-%m-%d' , 'data_per_day' : 8 , 'key' : 'I2b'                ,'ctname' : 'co2_bio'}
sib4biodis      = {'fileroot' : c13input+'/input/13co2/SiB4_v2/isotm5_'                   , 'strf' : '%Y-%m-%d' , 'data_per_day' : 8 , 'key' : 'biodis'             ,'ctname' : 'co2c13_biodis'}
sib4ocndis      = {'fileroot' : c13input+'/input/13co2/SiB4_v2/isotm5_'                   , 'strf' : '%Y-%m-%d' , 'data_per_day' : 8 , 'key' : 'I9'                 ,'ctname' : 'co2c13_ocndis'}
sib4isogpp         = {'fileroot' : c13input+'/input/13co2/SiB4_v2/isotm5_'                   , 'strf' : '%Y-%m-%d' , 'data_per_day' : 8 , 'key' : 'Fgpp_SiB4_nofilter' ,'ctname' : 'co2_gpp'}
sib4alpha       = {'fileroot' : c13input+'/input/13co2/SiB4_v2/isotm5_'                   , 'strf' : '%Y-%m-%d' , 'data_per_day' : 8 , 'key' : 'I3'                 ,'ctname' : 'alpha'}


sib4247terclim     = {'fileroot' : c13input+'/input/SiB4v2_247_ter_split/clim_'               , 'strf' : '%m-%d'    , 'data_per_day' : 8 , 'key': 'ter_clim'         ,'ctname' : 'co2_recoclim'}
sib4247teriav     = {'fileroot' : c13input+'/input/SiB4v2_247_ter_split/iav_'               , 'strf' : '%Y-%m-%d'    , 'data_per_day' : 8 , 'key': 'ter_iav'         ,'ctname' : 'co2_recoiav'}
sib4247gppclim     = {'fileroot' : c13input+'/input/SiB4v2_247_gpp_split/clim_'               , 'strf' : '%m-%d'    , 'data_per_day' : 8 , 'key': 'gpp_clim'         ,'ctname' : 'co2_gppclim'}
sib4247gppiav     = {'fileroot' : c13input+'/input/SiB4v2_247_gpp_split/iav_'               , 'strf' : '%Y-%m-%d'    , 'data_per_day' : 8 , 'key': 'gpp_iav'         ,'ctname' : 'co2_gppiav'}

ctefluxes={'carboscopev2021' : carboscopev2021,
         'carboscopev2022' : carboscopev2022,
            'sib4bioiav' : sib4bioiav,        
            'sib4biomean' : sib4biomean,        
'sib4v2462022': sib4v2462022,
'sib4biomean' : sib4biomean,
'sib4isobio'  : sib4isobio  ,
'sib4biodis'  : sib4biodis  ,
'sib4ocndis'  : sib4ocndis  ,
'sib4gpp'     : sib4gpp        ,
'sib4isogpp'     : sib4isogpp        ,
'sib4reco'     : sib4reco        ,
'sib4alpha'   : sib4alpha    ,
'sib4247terclim':sib4247terclim,
'sib4247teriav' :sib4247teriav ,
'sib4247gppclim':sib4247gppclim,
'sib4247gppiav' :sib4247gppiav ,
}
def fetch_data(dataname,dates):
    '''
    strf : string to pass to strftime
    '''
    logging.debug("getting %s for %s" % (dataname,len(dates)))
        # mask=(d in dates for d in [dt.datetime(dates[0].year,1,1)+dt.timedelta
    fileroot=ctefluxes[dataname]['fileroot']
    strf=ctefluxes[dataname]['strf']
    dpd=ctefluxes[dataname]['data_per_day']
    key=ctefluxes[dataname]['key']
    ctname=ctefluxes[dataname]['ctname']
    logging.debug(" searching %s for %s points per day, with name %s and %s"% ( fileroot,dpd,key,strf))
    start,stop=None,None
    reshape=False
    if strf in ['%Y']:
        start=(dates[0].timetuple().tm_yday-1)*dpd
        stop =(dates[-1].timetuple().tm_yday)*dpd
    elif strf in ['%Y%m']:
        start=(dates[0].day-1)*dpd
        stop =dates[-1].day*dpd
        if dpd>1:
            reshape=True
    #     start=(dates) 
    # else: 
    #     start=0
    #     stop=dpd
        if start>stop:
            print('index wrong')
            raise IndexError 
    data=[]
    for f in sorted(set([fileroot+d.strftime(strf)+'.nc' for d in dates])): 
        logging.debug("opening %s "% ( f))
        with nc.Dataset(f) as ncf:
            try:
                if start is not None:
                    data.append(np.array(ncf.variables[key][start:stop])) 
                else:
                    data.append(np.array(ncf.variables[key][:])) 
            except KeyError:
                print('reconfigure %s using one of these varibles:' % dataname)
                print(ncf.variables.keys())
                exit()
    data=np.squeeze(np.array(data))

    if reshape:
        before=data[4]
        data=np.reshape(data,(len(dates),dpd,180,360))
        logging.debug("%s" % all((data[0,4]==before).ravel()))
    data[data==-1e34] = 0.0
    return data,ctname

# dates=np.array([dt.datetime(2000,2,1) + dt.timedelta(i) for i in range(0,29)])
# print(dates[0])
# data=fetch_data('carboscopev2021',dates)
# print(np.shape(data))

# data=fetch_data('sib4bioiav',dates)
# print(np.shape(data))

# data=fetch_data('sib4v2462022',dates)
# print(np.shape(data))

# alpha,_=fetch_data('sib4alpha',  dates) 
# bio,_=fetch_data('sib4isobio',  dates) 
# print(np.shape(bio))
# gpp[gpp==-1e34]=0.0
# print(np.mean(np.mean(np.mean(bio*np.ones((29,180,360,))[:,np.newaxis,:,:],axis=1),axis=0)))
# with nc.Dataset('../analysis/data_flux1x1_monthly/flux_1x1.2000-02.nc') as ncf:
#     print(ncf.variables.keys())
#     nee=ncf.variables['co2_bio_flux_prior'][:,:]
# print(np.mean(nee))
# data=fetch_data('sib4gpp',dates)
# print(np.shape(data))
