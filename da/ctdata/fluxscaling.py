import numpy as np 
from da.ctdata import fluxload
# import logging
def simplescaling(parameterobject,dates,qual):
    # logging.debug("gathering...." )
    flux,ctname=fluxload.fetch_data(parameterobject.fluxname,dates) 
    # logging.debug("got %s"  % ctname)
    results={'%s_flux_%s_ensemble' % (ctname,qual) : np.zeros((parameterobject.nmembers,parameterobject.nlat,parameterobject.nlon)) } 
    for nr in range(0,parameterobject.nmembers):
        # logging.debug("computing for member %s" % nr)
        if np.shape(flux)==(len(dates),parameterobject.nlat,parameterobject.nlon):
            results['%s_flux_%s_ensemble' % (ctname,qual) ][nr,:,:]= np.mean(flux*parameterobject.vector_to_4d(nr,dates),axis=0)
        else: 
            results['%s_flux_%s_ensemble' % (ctname,qual) ][nr,:,:]= np.mean(np.mean(flux*parameterobject.vector_to_4d(nr,dates)[:,np.newaxis,:,:],axis=1),axis=0)
    return results

Ratm=(-8.0/1000+1)*0.01123720

def alphagppload(parameterobject,dates):
    gpp,_=fluxload.fetch_data('sib4gpp',  dates) 
    alpha,_=fluxload.fetch_data('sib4alpha',dates) 
    mask =parameterobject.vector_to_4d(1,dates) ==0.0
    gppshape=np.shape(gpp)
    for i in range(gppshape[0]):
        for j in range(gppshape[1]):
            gpp[i,j][mask[i]]=0.0
    return gpp,alpha

def alphagppdict(parameterobject,gpp,qual):
    results={'co2c13_gpp_flux_%s_ensemble' % (qual)  : np.zeros((parameterobject.nmembers,parameterobject.nlat,parameterobject.nlon)),
             'co2_gpp_flux_imp' :  np.zeros((parameterobject.nlat,parameterobject.nlon)) } 
    results['co2_gpp_flux_imp'][:,:]= np.mean(np.mean(gpp,axis=1),axis=0)
    return results 

def alphagppadd(parameterobject,dates,qual):
    gpp,alpha=alphagppload(parameterobject,dates)
    results=alphagppdict(parameterobject,gpp,qual)
    for nr in range(0,parameterobject.nmembers):
        scaling=parameterobject.vector_to_4d(nr,dates)
        alphaRscaled=(scaling[:,np.newaxis,:,:]+alpha)*Ratm
        results['co2c13_gpp_flux_%s_ensemble' % (qual) ][nr,:,:]= np.mean(np.mean(gpp*(alphaRscaled/(1+alphaRscaled)),axis=1),axis=0)
    return results

def alphagppmul(parameterobject,dates,qual):
    gpp,alpha=alphagppload(parameterobject,dates)
    results=alphagppdict(parameterobject,gpp,qual)
    for nr in range(0,parameterobject.nmembers):
        scaling=parameterobject.vector_to_4d(nr,dates)
        alphaRscaled=scaling[:,np.newaxis,:,:]*alpha*Ratm
        results['co2c13_gpp_flux_%s_ensemble' % (qual) ][nr,:,:]= np.mean(np.mean(gpp*(alphaRscaled/(1+alphaRscaled)),axis=1),axis=0)
    return results
