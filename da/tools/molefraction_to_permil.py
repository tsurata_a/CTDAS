"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters. 
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu. 

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation, 
version 3. This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. 

You should have received a copy of the GNU General Public License along with this 
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
# MolefractionToPermil.py

"""
Author : ivar 

Revision History:
File created on 11 May 2012.

"""
import numpy as np

R_vpdb         = 0.0112372 # 0.011112
def molefraction_to_permil(simulated,tracer_names,major,minor):
    """ Converts 13C mole fractions to permil values"""
    mask_tot=(tracer_names==major)
    mask_min=(tracer_names==minor)
    simulated[mask_min]=1000.*(((simulated[mask_min]/(simulated[mask_tot]-simulated[mask_min]))/R_vpdb)-1.)
    return simulated[mask_min]


if __name__ == "__main__":
    pass
