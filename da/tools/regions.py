import da.tools.io4 as io
import numpy as np 
import sys

def get_griddict(water=True,regions='regions', bbox=None, 
                 fname='/projects/0/ctdas/input/ctdas_2012/regions.nc'):
    '''
    Returns a dictionary with region numbers as defined by the input regions string, which should be a variable present in the regions.nc. de dictionary entry will be a latlon 
    water=True will mask land 
    water=False will mask oceans (also screened for water region)
    water='both' will mask anything
    '''

    # read ecoregions map (nlat x nlon)
    gridmap, lats, lons = get_gridmap(regionsname=regions, fname=fname)
    if not bbox is None:
        gridmap = fill_region(gridmap, lats, lons, bbox)

    # Read the file for masking water values
    ncf = io.ct_read(fname, 'read')
    if water == 'both':
        mask = ncf.get_variable('transcom_regions')<23
    elif water == True:
        mask = ncf.get_variable('ocean_regions')    
    else:
        mask = ncf.get_variable('land_ecosystems')<19
    ncf.close()
    griddict = {}
    for i in range(int(gridmap.max())+1):
        sel = np.where(gridmap == i+1)
        if len(sel[0]) > 0 and np.sum(mask[sel])>0:
            griddict[i+1] = sel

    
    return griddict

def get_transcom(griddict):
    ncf = io.ct_read('/projects/0/ctdas/input/ctdas_2012/regions.nc', 'read')
    transcom= ncf.get_variable('transcom_regions')
    names= ncf.get_variable('transcom_names')
    ncf.close()
    transomdict={}
    for key,data in griddict.items():
        lats,lons=data
        transomdict[key]=transcom[lats[0],lons[0]]
    return transomdict,names

def get_gridmap(regionsname, fname='da/analysis/cteco2/regions.nc', bbox=None):
    try:
        ncf = io.ct_read(fname, 'read')
    except FileNotFoundError:
        from importlib import resources
        with resources.path('da.analysis.cteco2','regions.nc') as path:
            ncf = io.ct_read(str(path), 'read')

    gridmap = ncf.get_variable(regionsname)
    lats = ncf['lat'][:]
    lons = ncf['lon'][:]
    ncf.close()

    if bbox is not None:
        gridmap = fill_region(gridmap, lats, lons, bbox=bbox)
    return gridmap, lats, lons

def fill_region(regions, lats, lons, bbox):
    """Fill a region with statevector elements for each (1x1) gridcell.
    Input:
        bbox: list of list: the bounding box as [[latstart, latend], [lonstart, lonend]]
              Europe would be [[33, 72], [-15, 35]] 
    Returns:
        np.array: the region data with unique numbers in the bbox.
    """

    # Make the lats and lons as meshgrid to be used for 2d indexing
    # in stead of 1d
    lons_m, lats_m = np.meshgrid(lons, lats)

    # Mask out gridcells that are currently in the bbox
    regions_nonbbox = np.where((lons_m > bbox[1][0]) & (lons_m < bbox[1][1]) & 
                                (lats_m > bbox[0][0]) & (lats_m < bbox[0][1]),
                                np.nan, regions)

    # Now, we're going to add a new 'region' per gridcell in the bbox.
    # This is the number we should start counting from:
    highest_number_region = regions.max() + 1
    # This is the amount of gridcells that need fililng in
    nregions_tofillin = regions_nonbbox[np.isnan(regions_nonbbox)].size
    # make a unique number for each gridbox to be filled in 
    region_tofillin = np.linspace(highest_number_region + 1, nregions_tofillin + highest_number_region + 1, num=int(nregions_tofillin),endpoint=False)
    # And replace the nans by these values
    regions_nonbbox[np.where((lons_m > bbox[1][0]) & (lons_m < bbox[1][1]) & 
                            (lats_m > bbox[0][0]) & (lats_m < bbox[0][1]))] = region_tofillin
    
    return regions_nonbbox

if __name__ == "__main__":
    griddict = get_griddict(water='both', regions='transcom_regions')

