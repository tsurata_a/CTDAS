"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
import datetime as dt
from collections import defaultdict
import numpy as np
from netCDF4 import Dataset
"""
Author: Joram Hooghiem
"""

class NetCDF4Defaultdict(defaultdict):
    ''' Combine the defaultdict with NetCDF4 to create a clean 
    ans pythonic way for loading netCDF4 data for data analysis. 
    Uses the netCDF4 API to acces netCDF4, within the defaultdict's default_factory.
    '''
    def __init__(self,fname,preload=None):
        """TODO: to be defined. """
        self.fname=fname         
        with Dataset(self.fname) as ncf:
            self['variables']=ncf.variables.keys()
            self['ncinfo']={key : ncf.getncattr(key) for key in ncf.ncattrs()}
            if preload is not None:
                for key in preload:
                    data=ncf.variables[key][:]
                    if data.dtype in ['float32','float64']:
                        data[data.mask]=np.nan
                    self[key]=data.data

    def __missing__(self, key):
        ret = self[key] = self.default_factory(key)
        return ret

    def __readvar__(self,nc_key):
        with Dataset(self.fname) as ncf:
            data=ncf.variables[nc_key][:]
            if data.dtype in ['float32','float64']:
                data[data.mask]=np.nan
        return data.data

    def default_factory(self,key):
        data=self.__readvar__(key)
        return data


    def dict2pandas(self,keys=None):
        import pandas as pd 
        if keys is not None:
            for key in keys:
                self[key]
        df=pd.DataFrame.from_dict(self) 
        if 'date' in df.keys():
            df.index=df['date']
        return df

class site_data(NetCDF4Defaultdict):
    """Docstring for site_data. """
    def __init__(self,fname,loadkeys=None):
        self.shortnames={ "sim"  : 'modelsamplesensemble',
                         "simf"  : 'modelsamplesensemble_forecast',
                          "time" : 'time',
                          "obs"  : 'value',
                          "mdm"  : 'modeldatamismatch',
                          "lat"  : 'latitude',
                          "lon"  : 'longitude',
                          "hphtr": 'totalmolefractionvariance_forecast'
                        }
        
        if loadkeys is not None:
            pr=loadkeys
            # pr=[self.shortnames[key] if key in self.shortnames.keys() else key for key in loadkeys ]
        else:
            pr=None
        super().__init__(fname, preload=pr)
        # if loadkeys is not None:
            # for key in loadkeys:
                # if key in self.shortnames.keys():
                    # self[key]=self.pop(self.shortnames[key]) 

        self.units={ 'co2' : 1e6,
                     'co'  : 1e9,
                     'co2c13': 1
                    }
        self.species=fname.split('/')[-1].split('_')[0]

    def default_factory(self,key):
        if key in ['res','resf']:
            data=np.array(self['sim'+key.split('res')[1]]-self['obs'])
        elif key=='date':
            data = np.array([dt.datetime(1970, 1, 1) + dt.timedelta(seconds=int(d)) for d in self['time']])
        elif key in ['obs']:
            data=self[self.shortnames[key]] * self.units[self.species]
        elif key in ['mdm']:
            data=self[self.shortnames[key]] * self.units[self.species]
        elif key in ['hphtr']:
            data=self[self.hortnames[key]] * self.units[self.species]**2
        elif key in ['sim','simf']:
            data = fwd_to_vals(self[self.shortnames[key]],self.species)
        elif key in ['sim_unc','simf_unc']:
            data=np.std(self[self.shortnames[key.split('_')[0]]][:,1:]*self.units[self.species],axis=1)
        elif key == 'chi2':
            data = self['res']**2/self['mdm']**2
        elif key == 'chi2ino':
            data = self['resf']**2/self['hphtr']
        elif key == 'chi2inof':
            data = self['resf']**2/(self['hpht']+self['mdm']**2)
        elif key == 'chi2hpht':
            data = self['resf']**2/(self['hpht'])
        elif key in self.shortnames.keys():
            data=self.__readvar__(self.shortnames[key]) 
        elif key == 'hpht':   
            data=np.zeros(np.shape(self['mdm']),float,'F')
            for i,n in enumerate(self['modelsamplesensemble_forecast']): 
                n=(n-n[0])*self.units[self.species]
                data[i] = (1/(len(n)-1)) * n.dot(n)
        else:
            data=super().default_factory(key)
        return data  

    def set_mask(self,key=None,func=None,**kwargs):
        if key is None and func is None:
            self._masked=False
        elif func is None:
            self._mask=np.isfinite(self[key])
            self._masked=True
        else:
            self._mask=func(self,**kwargs) 
            self._masked=True
        return

    def masked(self,key):
        return self[key][self._mask]

class flux_data(NetCDF4Defaultdict):
    """NetCDF4Defaultdict object with specific methods typically found
    in analysis/flux* of a ctdas analysis"""
    def __init__(self,fname):
        """TODO: to be defined. """
        super().__init__(fname)
        self.shortnames={ "dt": 'date', }

        self.conversion_factors={"PgCyr"  : 3600*24*365*12*1E-15,
                            "PgPmCyr": 3600*24*365*12*1E-15*1000/R_vpdb,
                    }

        self.texlabels={'flux_co2' : r"$F/$(PgC y$^{-1}$)",
                        'flux_co2c13' :r"$F/$(PgC \textperthousand y$^{-1}$)" }
        self.r_2_i={}

    def default_factory(self,key):
        if key in self.shortnames.keys():
            data=self.__readvar__(self.shortnames[key])
        elif key=='date':
            data=np.array([dt.datetime(2000,1,1)+dt.timedelta(d) for d in self['dt']])
        # elif not key=='variables':
            # if key in self['variables']:
                # data=self.__readvar__(key)
        elif key == 'regions':
            regions=[]
            for item in self['ncinfo'].keys():
                if 'Region_' in item:
                    regions.append(self['ncinfo'][item])
            data=np.array(regions)
        else:
            data=super().default_factory(key)
        return data  

    def r2i(self,region):
        i=0
        for i,reg in enumerate(self['regions']):
            if region==reg:
                break
        return self.r_2_i.setdefault(region,i)

    def combine(self,key,keys,func=None):
        if func is None:
            data=np.sum([self[key] for key in keys],axis=0)
        else:
            data=func([self[key] for key in keys])
        self[key]=data
        return

    def convert_unit(self,key,unit):
        return self[key]*self.conversion_factors[unit]


R_vpdb=0.01123720
d_atm =-8.0 
R_atm = (d_atm/1000+1)*R_vpdb

def fwd_to_vals(data,spec):
    if len(data[0])==1 or len(data[0])>100:
        if spec=='co2':
            return data[:,0]*1e6
        elif spec=='co2c13':
            return data[:,0] 
    mean_back=data[:,0]
    emmis_tot=data[:,1:5]-mean_back[:,None]
    mean_tot=mean_back+np.sum(emmis_tot,axis=1)
    if spec=='co2':
        return mean_tot*1e6
    elif spec=='co2c13':
        n13_atm=data[:,5]#*R_atm/(1+R_atm) # Totals
        # n12_atm=mean_back-n13_atm#*1/(1+R_atm) #Totals
        # delta_b=1000*(((n13_atm)/(n12_atm))/R_vpdb -1 )
        co2c13_em=data[:,6:12]  # includes background
        mean_c13=n13_atm+np.sum((co2c13_em-n13_atm[:,None]),axis=1)
        mean=1000*(((mean_c13)/(mean_tot-mean_c13))/R_vpdb -1 )
        # co2_em=np.zeros(np.shape(co2c13_em))
        # co2_em[:,0:4]=data[:,1:5] #includes background
        # co2_em[:,4:]+=mean_back[:,None] #includes background
        # co2_em-=co2c13_em #
        # emmis=1000*(((co2c13_em)/(co2_em))/R_vpdb -1 )
        return mean
