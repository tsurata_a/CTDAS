import netCDF4 as nc 
import numpy as np
class Anomaly(object):

    """Docstring for Anomaly. An anomaly object holds the the spatial and temporal information on 
    an gridded anomaly."""

    def __init__(self,filename,varname,resolution):
        """TODO: to be defined. """
        self.filename=filename
        self.varname=varname
        self.anomaly, self.dates=self.read_anomaly(self.filename,self.varname,True)
        return 
    
    def read_anomaly(self, filename, varname, getTime=False):
        """
        : param filename: full path to netCDF file containing 'datatype' data on 1x1 deg grid
        : param varname: string to identify the data to be read from 'filename'.

        Method to read data for construction of NEE fit, and corresponding timerange, from netCDF file.
        Note: currently units are not checked!
        """

        with nc.Dataset(filename) as ds:
            anomaly= np.nan_to_num(ds[varname][:])
            if getTime:
                dates = nc.num2date(ds['time'][:], ds['time'].units)
                return anomaly, dates
            else:
                return anomaly

    def _gettime_nearest(self, target_dates):
        """
        Returns a list with for every date in self.timevec the index in the input vector 'times'.
        If no input data is present for the requested time, index = None
        """

        indices = []
        for date in target_dates:
            index = np.argmin(abs(date - self.dates))
            indices.append(index)

        return indices

    def interpolate_anomalies(self, target_dates,method='nearest'):
        """ Returns a 3D array with input anomaly data for times corresponding to timevec only. Fillvalue = NaN."""

        sampled_anomaly = np.zeros((len(target_dates), self.anomaly.shape[1], self.anomaly.shape[2]))
        if method=='nearest':
            indices=self._gettime_nearest(target_dates)
            for sampledloc, dataloc in enumerate(indices):
                sampled_anomaly[ sampledloc, :, :] = self.anomaly[dataloc , :, :]
        elif method =='linear':
            total_seconds=[d.totalseconds() for d in self.dates-datetime.datetime(1970,1,1)]
            target_total_seconds=[d.totalseconds() for d in self.dates-datetime.datetime(1970,1,1)]
            # target_total_seconds=(target_dates-datetime.datetime(1970,1,1)).total_seconds()
            #
            pass

        self.anomaly=sampled_anomaly
        return 
    
