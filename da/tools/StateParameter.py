"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
import numpy as np
import datetime as dt
from da.tools.covariance import cov_gaussian, draw_constrained_randn,ensemble_sparse_kron,get_spatial_correlation_regions,ensemble_kron
from da.tools.regions import get_griddict,get_transcom
import logging
from da.statevectors.statevector_baseclass import StateVector, EnsembleMember
from da.tools.general import datevector
from da.ctdata.fluxscaling import simplescaling 
# import pandas as pd


class StateParameter(object):
    '''A statevector comprised of elements, and numerical/mathmatically they are the same. Yet, physically certain elements of the vector have a different meaning physically than others. 
    As an example, element with index i could be a scaling factor for the ocean flux in the Southern Ocean, and index i+1 could be a coefficient mapping a temperature anomaly to GPP. The StateParameter class aids to group statevector elements into there spatiotemporal physcial meaning. It contains the information of the initial variance, the region the belong to. Default mappings of vector to grid and vice versa are implemented here.
    Data objects : 
        nparams : number of elements in the statevector
        nmembers : nmembers number of ensemble members
        x : local statevector elements
        dev_matrix : matrix of nparams, nmemebers with devations
        dof : degrees of freedom, estimated using svd.
    Methods:

        __init__

        compute_cov : computes the covariance matrix.

    '''

    def __init__(self, variance=1, nparams_per_region=None, cov_t_length=0, cyclic=False, water=True, nmembers=150, mean=0, regions='regions', constraint=None, datetime=None, freq='D', cov_region_length=None,limit_to_transcom=True,region_covariance_matrix=None,startdate=None,enddate=None,func=simplescaling,fluxname=None,within_tcregions=None,length_scales=None): 
        ''' Initialize the StateParameter, using the components it needs to completly compute members.  
        Parameters : 
            variance : float, 
                Variance of the parameter
            nparams_per_region : int 
                number of parameters per region in the temporal direction
            cyclic : boolean
                True means that after nparams_per_region parameters, the paremter is repeated. 
                This is used when we want to have a parameter retur every year. For example,
            Water : True, Flase, 'both'
                true means apply land mask, false is apply an oceanmask, 'both' is don't apply anything.
            cov_t_length : float,
                the temporal covariance length scale, correlating the parameter in time
            nmembers : int
                the number of ensemble members needed when creating a new ensemble
            mean : float,
                Mean value of the statevecter element on initialization
            constraint : 'positive', 'negative', None
                A constraint imposed on drawing members. 'positive' means only positive members vice versa 'negative'. This may result in non-gaussian distribution.
                Consider experimenting with normality tests on the resulting deviations.
            freq: str,
                 time frequency of parameter. Accepted are pandas datetime frequencies ('D', 'M', '3M', ...)
            cov_region_length: float
                covariance lenght scale in km across regions
            limit_to_transcom: boolean, default True
                if regional covariance should be limited to within transcom regions 
            region_covariance_matrix: np.ndarray
                region matrix. should have the same dimension of the number of regions we are considering
                if sparse, a list can also be provided. Ecoregions within transcom for example
        '''
        self.variance = variance
        self.cyclic = cyclic
        self.cov_t_length = cov_t_length
        self.nmembers = nmembers
        self.mean = mean
        self.griddict = get_griddict( water=water, regions=regions, fname='/projects/0/ctdas/input/ctdas_2012/regions.nc')
        self.nregions = len(self.griddict.keys())
        self.freq = freq
        # self.regionmap=regionmap
        # Spatial settings
        self.nlat = 180
        self.nlon = 360
        # fortran memory layout for fast column computations
        self.dof = 0
        self.constraint = constraint
        self.refyear=2000
        if self.cyclic:
            self.refyear=11
            if freq=='daily':
                self.datetime = np.array([dt.datetime(self.refyear,1,1)+dt.timedelta(days=n) for n in range(365)])
            else:
                self.datetime=datevector(dt.datetime(self.refyear,1,1),dt.datetime(self.refyear,12,1),freq) 
        elif datetime is not None:
            self.datetime = datetime
        else:
            self.datetime=datevector(startdate,enddate,freq) 
        if region_covariance_matrix is not None:
            self.region_covariance_matrix=region_covariance_matrix

        secar=[(d-dt.datetime(self.refyear,1,1)).total_seconds() for d in self.datetime]
        if self.cyclic:
            secar.append( (dt.datetime(self.refyear+1,1,1)-dt.datetime(self.refyear,1,1)).total_seconds())
        self.timevec_sec=np.array(secar)
        if nparams_per_region is not None:
            self.nparams_per_region=nparams_per_region
        else:
            self.nparams_per_region = len(self.datetime)
        self.nparams = self.nparams_per_region*self.nregions
        self.x = np.zeros((self.nparams,), order='F', dtype='float')
        self.dev_matrix = np.zeros( (self.nparams, self.nmembers,), order='F', dtype='float')
        self.func=func
        self.fluxname=fluxname
        logging.info("initialized parameter for %s regions, with total of %s elements will contain %s ensemble members" % (
            self.nregions, self.nparams, self.nmembers))
        logging.info("with mean %s and variance %s" %
                     (self.mean, self.variance))
        if within_tcregions is not None:
           self.within_tcregions= within_tcregions
        if length_scales is not None:
           self.length_scales =length_scales 
            # try:
            #     datetime = pd.date_range(
            #         '2001-01-01', '2002-01-01', inclusive='left', freq='D')
            # except TypeError:  # Old pandas version?
            #     datetime = pd.date_range(
            #         '2001-01-01', '2002-01-01', closed='left', freq='D')

        # datetime = self.remove_leapdays(datetime)
        # self.datetime = datetime
        
        return

    # @staticmethod
    # def remove_leapdays(dates):
    #     """Remove leapdays from a array-like"""
    #     dates = dates[~((dates.month == 2) & (dates.day == 29))]
    #     return dates

    def compute_cov(self):
        """Compute the covariance"""
        # part 1: create temporal cov
        if self.cov_t_length == 0:
            # Means elements do not correlate in time; return only the diagonal
            self.covariance = np.diag([self.variance]*self.nparams_per_region)
        else:
            # Means elements correlate in time. For this, we calculate the covariance with the 'cov_gaussian' function 
            # This wil hold the same prior covariance for different regions considered
            self.covariance = cov_gaussian(self.nparams_per_region, self.cov_t_length, self.variance, cyclic=self.cyclic)

        _, s, _ = np.linalg.svd(self.covariance)
        self.dof_in_cov = np.sum(s) ** 2 / sum(s ** 2)        
        # part compute spatial covariance using defined lenght scale
        if hasattr(self,'within_tcregions'):
            if not hasattr(self,'length_scales'):
                self.length_scales=[1000]*len(self.within_tcregions) 
                logging.info("setting default length scale for spatial covariance accross all tc regions")
            self._compute_spatial_accross_tc()
        elif hasattr(self,'length_scales'):
            logging.info("found only the lenghts scale parameter. Assuming full correlated regions")
            self._compute_spatial()


# =======
        #     # Means elements correlate in time. For this, we calculate the covariance with the 'cov_gaussian' function
        #     self.covariance = cov_gaussian(
        #         self.nparams_per_region, self.cov_t_length, self.variance, cyclic=self.cyclic)
        return

    def create_mean_statevector(self):
        new_mean = []
        for i in range(self.nregions):
            new_mean.extend([self.mean] * self.nparams_per_region)

        # logging.debug(new_mean)
        self.x[:] = np.array(new_mean)
        # logging.debug('Created new mean statevector containing %s statevector elements' %(len(self.x)) )
        return

    def make_new_ensemble(self):

        self.dev_matrix = np.zeros((self.nparams, self.nmembers,), 'float',order='F')
        random_vectors=np.array([np.random.randn(self.nparams) for a in range(self.nmembers-1)]).T
        #randstate = np.random.get_state()
        if not hasattr(self,'region_covariance_matrix'):
            logging.info('Creating the deviations from only the temporal covariance')
            self._temporal_cov_only(random_vectors)
        elif isinstance(self.region_covariance_matrix,list):
            self._kron(random_vectors)
        # else:
            # logging.info('Creating the deviations from both regional and temporal covariance')
            # self._sparse_kron() 
        return

    def _temporal_cov_only(self,random_vectors):
        # Make a cholesky decomposition of the covariance matrix
        C = np.linalg.cholesky(self.covariance)
        for i in range(self.nregions):
            istart = i*self.nparams_per_region
            istop = (i+1) * self.nparams_per_region
            self.dev_matrix[istart:istop, 1:] = np.matmul(C,random_vectors[istart:istop, : ] )
        logging.info('added %s parameters with %s degrees of freedom to statevector' % ( self.nparams, self.dof*self.nregions))
        return

    def _kron(self,random_vectors):
        """
        creates devations by approximating the kronecker product of regional and temporal covariance. it only correlates in the spatial domain the parameters that are directly linked.
        Example: region 1 is correlated to region 2. region 2 is correlated to region 3. Then, region 1 is inderectly correlated to region 3. this should result in some "mixing" of the random numbers. 
        This computation however, this computation does not consider this indirect correlation between regions 1 and 3. as a matrix this would look like:
        [[ 1.0, 0.2, 0.0]
         [ 0.2, 1.0, 0.2]
         [ 0.0, 0.2, 1.0]] 
        """
        ensemble_kron(self.region_covariance_matrix,self.covariance,random_vectors,self.dev_matrix)
        return

    def _sparse_kron(self):
        """
        creates devations by approximating the kronecker product of regional and temporal covariance. it only correlates in the spatial domain the parameters that are directly linked.
        Example: region 1 is correlated to region 2. region 2 is correlated to region 3. Then, region 1 is inderectly correlated to region 3. this should result in some "mixing" of the random numbers. 
        This computation however, this computation does not consider this indirect correlation between regions 1 and 3. as a matrix this would look like:
        [[ 1.0, 0.2, 0.0]
         [ 0.2, 1.0, 0.2]
         [ 0.0, 0.2, 1.0]] 
        """
        ensemble_sparse_kron(self.region_covariance_matrix,self.covariance,random_vectors,self.dev_matrix)
        return

    def state_to_members(self, members, start, stop):
        '''
        Hands over the elements of this stateparameter to the corresponding 
        member object. The start stop indices are maintianed by the statevector
        '''
        for i, member in enumerate(members):
            member.param_values[start:stop] = self.x+self.dev_matrix[:, i]
        return

    def domain_info(self, domain_info, start, stop):
        '''
        Returns, for each state element in the vector, the domain information. 
        Currently only the time domain is included, i.e. datetime objects, in the future this could also be a lat lon info? even height?
        '''
        for i in range(self.nregions):
            domain_info[start+self.nparams_per_region*i:start +
                        self.nparams_per_region*(i+1)] = self.datetime
        return

    def members_to_state(self, members, start, stop):
        '''
        Receive elements of this statevector back from members. Used after optimization
        or when reading from the diagnostics file.
        '''
        for i, member in enumerate(members):
            if i == 0:
                self.x = member.param_values[start:stop]
            self.dev_matrix[:, i] = member.param_values[start:stop]-self.x
        return

    def vector2grid(self, vectordata=None, nparams="Default"):
        """
            Map vector elements to a map or vice cersa

           :param vectordata: a vector dataset to use in case `reverse = False`. This dataset is mapped onto a 1x1 grid and must be of length `nparams`
           :rtype: ndarray: an array of size (nfitparams,360,180)

           This method makes use of a dictionary that links every parameter number [1,...,nparams] to a series of gridindices. These
           indices specify a location on a 360x180 array, stretched into a vector using `array.flat`. There are multiple ways of calling
           this method::

               griddedarray = self.vector2grid(vectordata=param_values) # simply puts the param_values onto a (180,360,) array

           .. note:: This method uses a DaSystem object that must be initialzied with a proper parameter map. See :class:`~da.baseclasses.dasystem` for details
        """
        if nparams == 'Default':
            nparams = self.nparams_per_region
        result = np.zeros((nparams, 180, 360), float)
        # logging.debug("Elements in vectordata %s will be mapped to %s gridded elements" %(len(vectordata),nparams*360*180) )
        for j, (region, latlon) in enumerate(self.griddict.items()):
            for i in range(nparams):
                result[i, latlon[0], latlon[1]] = vectordata[j * nparams + i]

        return result

    # def interpolate_to_daily(self, statevector):
    #     """Interpolate a statevector to daily values"""
    #     # TODO: Now, we add the first time as last time for the interpolation.
    #     # TODO: write a full extrapolation function as well.

    #     # Here, we're going to interpolate. We have monthly/yearly/Xdaily values
    #     # Which we want to interpolate to daily.
    #     # To ease the interpolation, we make this into a df
    #     df_cyclic_month = pd.DataFrame(statevector.T)
    #     assert len(df_cyclic_month.columns) == self.nregions

    #     # Set the index as time index
    #     freq = self.freq
    #     if freq in ['Y', 'M'] and not freq.endswith('S'):
    #         freq += 'S'
    #     df_cyclic_month.index = pd.date_range(self.datetime[0],
    #                                           freq=freq,
    #                                           periods=len(df_cyclic_month))
    #     df_cyclic_month = df_cyclic_month.loc[self.remove_leapdays(
    #         df_cyclic_month.index)]
    #     if self.cyclic:
    #         # Cyclic: so we add the next value in the previous year, which is the same as the first value of this year
    #         df_cyclic_month.loc[pd.to_datetime(
    #             '2003-01')] = df_cyclic_month.iloc[0]
    #     else:
    #         last_time = self.datetime[-1].date()
    #         timedelta = last_time - self.datetime[-2].date()
    #         next_time = last_time + timedelta
    #         df_cyclic_month.loc[next_time] = df_cyclic_month.iloc[-1]
    #         df_cyclic_month.index = pd.to_datetime(df_cyclic_month.index)
    #     # Interpolate to daily.
    #     df_cyclic_day = df_cyclic_month.resample(
    #         'D').interpolate(method='quadratic')
    #     # As we have added the first day of the next year, we drop that.
    #     df_cyclic_day = df_cyclic_day.iloc[:len(df_cyclic_day)-1]
    #     logging.info(f'Interpolated from {self.freq} to daily')

    #     return df_cyclic_day.values.T

    def interpolate_state(self,state,timevec):

        refyear=dt.datetime(self.refyear,1,1)
        if self.cyclic:
            # with leap year exception: shifted to first of march
            _timevec=[dt.datetime(self.refyear,d.month,d.day) if not (d.month==2 and d.day==29) else dt.datetime(self.refyear,d.month+1,1)  for d in timevec] 
        else:
            _timevec=timevec
        target=np.array([(d-refyear).total_seconds() for d in _timevec])
        state_interpolated=[]
        for region_state in state: 
            if self.cyclic:
                temp=np.concatenate((region_state,[region_state[0]]))
            else:
                temp=region_state
            state_interpolated.append(np.interp(target, self.timevec_sec ,temp) )
        return np.array(state_interpolated)

    def cyclic_to_timevec(self, vector, datevec):
        """
        transforms a vector that represents 365 dayily scalars into a nregion x ndates matrix. i.e. maps elements in time 
        vector will be statevector, can be nregions * 365 or simply 365
        assumes daily scalars. In leap year, 29 feb wil be the mean of 28 feb and 1 march.
        """

        # Take the transpose, so that the time axis is first will pick based on time
        vector = vector.T
        # a list for easy appending
        scalar_array = []
        for date in datevec:
            day_in_year = date.timetuple().tm_yday
            # Check if leap year and Feb 29 has passed
            if date.year % 4 == 0 and date.month >= 3: 
                day_in_year -= 1 # So we subtract one day
            if date.month == 2 and date.day == 29:
                to_append = (vector[day_in_year-1] + vector[day_in_year])/2
            else:
                to_append = vector[day_in_year - 1]
            
            scalar_array.append(to_append)
        # transpose back:
        return np.array(scalar_array).T

    def prior2post(self,dates,qual):
        return self.func(self,dates,qual) 

    def vector_to_4d(self, membernumber, timevec, grid_multiplier=None, region_multiplier=None, ufunc=None):
        """
        takes in a statemap, assumed to be cyclic, nregions * 365
        returns array of ndays * nlat * nlon
        each entry is lambda * nregion_multiplier * grid_multiplier belonging to that particular 
        box
        example: 
        """
        x = self.x + self.dev_matrix[:, membernumber]
        x_reshaped = x.reshape(self.nregions, self.nparams_per_region)

        # transform to target timevec of nregions * nparams -> nregions * ndates
        # if self.cyclic:
            # scalar_array = self.cyclic_to_timevec(x_reshaped, timevec)
        # else:
            # this is somewhat overkill if the output frequency is the same as the input, and we
            # want to subselect only a part of the state. 
        scalar_array = self.interpolate_state(x_reshaped,timevec)

        # apply the region based scalar
        if region_multiplier is not None:
            scalar_array = np.array([region_multiplier]).T*scalar_array

        # create a gridded version of the scalar
        scalar_array_grid = self.vector2grid(
            vectordata=scalar_array.ravel(), nparams=len(timevec))

        # apply the gridbased multiplier (could be gridded flux for example)
        if grid_multiplier is not None:
            scalar_array_grid = grid_multiplier * scalar_array_grid

        # if an aditional function is applied, do so here
        if ufunc is not None:
            scalar_array_grid = ufunc(scalar_array_grid)
        return scalar_array_grid

    def _compute_spatial_accross_tc(self):
        # Get a dictionary linking the griddict regions to tc regions
        tc,names=get_transcom(self.griddict)
        # make a list, which is in the same order as the regios: 
        tclist=[tc[key] for key in self.griddict.keys()]

        # compute the spatial covariance only for 
        # the specified transcom regions
        covmat={}
        for tcr,L in zip(self.within_tcregions,self.length_scales): 
            tempgriddict={}
            for key in self.griddict.keys():
                if tc[key]==tcr:
                    tempgriddict[key]=self.griddict[key]
            logging.info("computing a spatial covarience structure for region %s with a lenght scale of %s km" %(names[tcr-1],L)) 
            covmat[tcr]= get_spatial_correlation_regions(tempgriddict)

        self.region_covariance_matrix=[] 
        added=[]
        for i,val in enumerate(tclist):

            if val not in self.within_tcregions:
                self.region_covariance_matrix.append(np.array([[1]]))
            elif val not in added:
                self.region_covariance_matrix.append(covmat[val])
                added.append(val)
        return
    def _compute_spatial(self):
        # Get a dictionary linking the griddict regions to tc regions
        self.region_covariance_matrix= [get_spatial_correlation_regions(self.griddict,self.length_scales[0])]
        return
################### End Class StateParameter ###################


if __name__ == "__main__":
    import logging

    logging.basicConfig(level='DEBUG',
                        format=' [%(levelname)-7s] (%(asctime)s) py-%(module)-20s : %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    # datevec = pd.date_range('2000-01-01', '2009-01-01', closed='left', freq='D')
    # datevec = pd.date_range('2000-01-01', '2009-01-01', inclusive='left', freq='D')
    from  da.tools.general import datevector
    datevec=datevector(dt.datetime(2000,1,1),dt.datetime(2001,1,1),'month') 
    # datevec = np.array([dt.datetime(y,1,1) for y in range(2000,2010+1)])
    
    # print(datevec)
    paramdict = { "cyclic"    : {"variance"           : 0.64,
                                 "nparams_per_region" : 12,
                                 "freq"               : 'month',
                                 "cov_t_length"       : 4,
                                 "cyclic"             : True,
                                 "water"              : False,
                                 "nmembers"           : 10,
                                 "mean"               : 0.0,
                                 "regions"            : 'regions'},
    
                  "noncyclic" : {"variance"           : 0.64,
                                 "nparams_per_region" : len(datevec),
                                 # "freq"               : 'M',
    #                              "nparams_per_region" : len(datevec),
    #                              "freq"               : 'D',
                                 # "nparams_per_region" : len(datevec.year.unique()),
                                 "freq"               : 'M',
                                 "cov_t_length"       : 1, #This is now in the number of elements, so if datevector is monthly and cov_t_lengt=3 it means covariance length of 3 months
                                 "cyclic"             : False,
                                 "water"              : True,
                                 "nmembers"           : 10,
                                 "mean"               : 1.0,
                                 "regions"            : 'regions',
                                 "datetime"           : datevec}}
    testpar = StateParameter(**paramdict["cyclic"])
    testpar.compute_cov()
    testpar.create_mean_statevector()
    testpar.make_new_ensemble()
    print(np.shape(testpar.vector_to_4d(3,datevec)))
    # print(testpar.datetime)
