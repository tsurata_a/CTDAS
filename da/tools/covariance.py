"""CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017 Wouter Peters.
Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
updates of the code. See also: http://www.carbontracker.eu.

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version 3. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>."""
#!/usr/bin/env python
"""
Author: Joram Hooghiem 
Revision History:
File created on 17 August 2022.

a series of functions that can create covariance matrices
"""

import numpy as np
# fix name space polution
import scipy 
from scipy import stats
import logging
import netCDF4 as nc
from da.tools.regions import get_griddict, get_gridmap, get_transcom
from da.tools.general import calc_area


def cov_gaussian(N, sigma, scalar=1, cyclic=True, spacing=None):
    """
    Returns a square NxN matrix, resembling a covariance with a gaussian window.  
    N is the number of parameters it tries to describe. it is assumed that these parameters are equally spaced. 
    The entries in the NxN matrix are assumed to be equally spaced in their dimension.
    sigma is the standard deviation used.   
    sigma*5>N/2 currently

    Todo:
        option to pass on spacing array. This allows the user the pass on the disired spacing. which can be usefull when for example distance is used, but distance between elements of the data is not uniform. 
    examples: 

    cov_gaussian(10,3,0.64,cyclic=False)
    will return a 10 by 10 matrix, with 0.64 on the diagonal. the density fals of towards the edges to zero:
[[6.40100000e-01 8.66145813e-02 2.14696082e-04 0.00000000e+00]
 [8.66145813e-02 6.40100000e-01 8.66145813e-02 2.14696082e-04]
 [2.14696082e-04 8.66145813e-02 6.40100000e-01 8.66145813e-02]
 [0.00000000e+00 2.14696082e-04 8.66145813e-02 6.40100000e-01]]

   cov_gaussian(10,3,0.64) 
    will assume that it is cyclic, i.e. the final element is correlated to the first element just as much as the second to the first. 
[[6.40100000e-01 8.66145813e-02 4.29392164e-04 8.66145813e-02]
 [8.66145813e-02 6.40100000e-01 8.66145813e-02 4.29392164e-04]
 [4.29392164e-04 8.66145813e-02 6.40100000e-01 8.66145813e-02]
 [8.66145813e-02 4.29392164e-04 8.66145813e-02 6.40100000e-01]]

    cov_gaussian(10,3,0.64,cyclic=False)

    """

    logging.debug("input dimension %s" % N)
    eval_width=int(np.min([5.0*sigma , N/2]))

        # print('5*sigma>N/2; this is not tested yet')

    
    # setup an array to evaluate window
    if spacing is None:
        spacing = np.linspace(-int(eval_width), int(eval_width), 2*int(eval_width)+1)
    # get window:
    # logging.debug("spacing %s" %spacing  )
    b = stats.norm.pdf(spacing, 0, sigma)
    # normalize so diagonal becomes 1
    b = (1/np.max(b))*b
    # setup array for first row
    if cyclic:
        y_begin = np.zeros(N, float)
        y_end = np.zeros(N, float)
    else:
        # offset so that when we roll, we obtain sigma_ij with i>j
        y_begin = np.zeros(N + int(len(b)/2)+1, float)
        y_end = np.zeros(N + int(len(b)/2)+1, float)

    # note that the middle of the gaussian is at the start:

    y_begin[-int(eval_width):] = b[:int(eval_width)]
    y_end[:int(eval_width)+1] = b[int(eval_width):]
    y = np.max([y_begin,y_end],axis=0) 
    # setup matrix
    cov_mat = np.zeros((N, N), float)
    for i in range(0, N):
        # logging.debug("at row %s" %i )
        cov_mat[i, :N] = y[:N]
        # add a tiny bit to diagonal so that the
        # matrix is a positive definite; cholesky will then succeed:
        # cov_mat[i, i] += 0.01
        # logging.debug('%s' % cov_mat_lambda[i,i])
        # roll by one, so that diagal shifts by one for next row
        y = np.roll(y, 1)
        # cov_mat_lambda=np.diag([0.64]*self.nlambda)
    # cov_mat[cov_mat<0.01]=0.0
    check_cholesky(cov_mat)
    cov_mat=cov_mat/cov_mat[0,0]
    return scalar*cov_mat

def check_cholesky(cov_mat):
    try: 
        np.linalg.cholesky(cov_mat)
    except np.linalg.LinAlgError:
        cov_mat+= np.diag([0.001]*len(cov_mat))
        check_cholesky(cov_mat)
    return
def draw_constrained_randn(mean=0, variance=1, size=1, constraint=None):
    ''' Draws size numbers of random normal points. The random points will be 
    subjected to the constraint given by the function constrain_func, which accepts one argument. this test will be performed on the value sigma*drawn_point+mean. Points will be addmitted if test succeeds
    parameters : 
        mean : float 
        sigma : float 
        size : int
        constrain_func : callabale function that accepts a number and returns a boolean.
    '''
    # Calculate the standard deviation, for use in drawing the random numbers.
    if constraint is None:
        return np.random.randn(size) 

    if constraint == 'positive':
        const = _positive

    elif constraint == 'negative':
        const = _negative

    else:
        const = _all

    result = []
    for i in range(size):
        draw = True
        while draw:
            random_number = np.random.randn() 
            # check if the resulting gaussian with normal and sigma is
            # conform our constraint
            if const(np.sqrt(variance)*random_number+mean):
                draw = False
        result.append(random_number)
    return np.array(result)

def ensemble_kron(a,B,V_randn,V_corr):
    # """Computes vectors V_corr with correlations according to a covariance matrix 
    # that is the kronecker product of A,B. A and B are symmetric positive definite matrices.
    # if A is of dimension NxN and B of dimension MxM then the resulting covariance matrix is of dimension 
    # (NxM) x (NxM). To efficiently calculate the cholesky decomposition to correlate the vector elements,
    # this algorithm only considers direct correlations of A. The loop loops over the rows in A and only
    # creates the part of len(B) of V_randn into V_corr per iterations.

    # This approach is NOT entirely accurate. better would be to find the block matrices in A.
    # However, computiationally that may be inhibbiting for fully correlated matrices A. In addition,
    # weakly correlated elements may very well be beyond the resolving power of limimted ensemble members
    # that would represent de full covariance
   

    # b is now a list with covariance matrices repressenting a sparse covariance:
    # [ b[0] 0]
    # [ 0  b[1]]
    # the order of the matrices is the e same at wicht the regions are considderd within the statevector
    # """

    # len (Vrand) is len(V_corr) -1 since we dont want to create a deviation for the mean
    C=np.linalg.cholesky(B)
    istart=0
    nregions1=0
    for mat in a:
        nregions1+=np.shape(mat)[0]
    nparams_t=len(B)
    nregions2=len(V_randn)/nparams_t
    # print(nparams_t)
    # print(nregions2, nregions1)
    istart=0

    for covmat in a:
        if len(covmat)==1:
            istop=istart+nparams_t
            V_corr[istart:istop,1:]=np.matmul(C,V_randn[istart:istop,:])
            logging.debug('inserting %s params from %s to %s' %(nparams_t,istart,istop))
            istart=istop
            # logging.debug('     with dof: %.3f ' %(compute_dof(B)))
        else:
            # create full covariance for all regions: (covst cov spatial temporal)
            logging.info("computing kronecker product")
            covst=np.kron(covmat,B) 
            # covst=scipy.sparse.kron(covmat,B,format='csc')
            logging.info("computing cholesky decomposition")
            CD=np.linalg.cholesky(covst)
            # CD=sparse_cholesky(covst)
            istop=istart+nparams_t*len(covmat)
            V_corr[istart:istop,1:]=np.matmul(CD , V_randn[istart:istop,:])
            logging.debug('inserting %s params from %s to %s' %(nparams_t*len(covmat),istart,istop))
            istart=istop
            # logging.debug('     with dof: %.3f ' %(compute_dof(covst)))
    return
def compute_dof(matrix):
    _, s, _ = np.linalg.svd(matrix)
    dof= np.sum(s) ** 2 / sum(s ** 2)        
    return dof

def sparse_cholesky(A): 
    # The input matrix A must be a sparse symmetric positive-definite.
    # obtained from 
    #             https://gist.github.com/omitakahiro/c49e5168d04438c5b20c921b928f1f5d 
    # all credit to the author
    # this returns the lower triangular matrix like np.linalg.cholesky, but unlike scipy default
    n = A.shape[0]
    LU = scipy.sparse.linalg.splu(A,diag_pivot_thresh=0,permc_spec='NATURAL') # sparse LU decomposition
    if ( LU.perm_r == np.arange(n) ).all() and ( LU.U.diagonal() > 0 ).all(): # check the matrix A is positive definite.
        return LU.L.dot( scipy.sparse.diags(LU.U.diagonal()**0.5) )
    else:
        if not ( LU.perm_r == np.arange(n) ).all():
            logging.error('pivotting happened')
        logging.error('matrix is not positive definite')
        exit()
def ensemble_sparse_kron(A,B,V_randn,V_corr):
    # create sparse full cholesky decomposition (don't store the covariance matrix itself)
    C=sparse_cholesky(scipy.sparse.kron(A,B,format='csc'))
    # don't touch the mean
    V_corr[:,1:]= (C @ V_randn)[:,:]
    return
# def ensemble_sparse_kron(A,B,V_randn,V_corr):
#     """Computes vectors V_corr with correlations according to a covariance matrix 
#     that is the kronecker product of A,B. A and B are symmetric positive definite matrices.
#     if A is of dimension NxN and B of dimension MxM then the resulting covariance matrix is of dimension 
#     (NxM) x (NxM). To efficiently calculate the cholesky decomposition to correlate the vector elements,
#     this algorithm only considers direct correlations of A. The loop loops over the rows in A and only
#     creates the part of len(B) of V_randn into V_corr per iterations.

#     This approach is NOT entirely accurate. better would be to find the block matrices in A.
#     However, computiationally that may be inhibbiting for fully correlated matrices A. In addition,
#     weakly correlated elements may very well be beyond the resolving power of limimted ensemble members
#     that would represent de full covariance
    
#     """
#     dim_B=np.shape(B)[0]
#     for i,row in enumerate(A):
#         # select only nonzero regions of element ii of A
#         mask=(row!=0)
#         logging.debug("constructing for %s with % regions" %(i,sum(mask)))
#         # construct the partial covariance matrix
#         mask_partial=[]
#         # and construct a mask for the target array so that we know how to replace the vector elements
#         mask_targetdev=[]
#         for j,masked in enumerate(mask):
#             if masked:
#                 mask_partial.extend([True]*dim_B)
#                 if j==i:
#                     mask_targetdev.extend([True]*dim_B)
#                 else:
#                     mask_targetdev.extend([False]*dim_B)
#             else:
#                 mask_partial.extend([False]*dim_B)
#         # Take make the kronecker product of the selected part of A with B
#         partial_cov=np.kron(A[:,mask][mask],B)
#         C=np.linalg.cholesky(partial_cov)
#         # Finally compute the correlated vectors, stored as columns 
#         V_corr[i*dim_B:(i+1)*dim_B]=np.matmul(C,V_randn[mask_partial,:])[mask_targetdev]
#     return

def _positive(number):
    return number >= 0


def _negative(number):
    return number <= 0


def _all(number):
    return True

def get_forest_age(bbox=[[-90, 90], [-180, 180]]):
    """Get the land use and land-sea mask given a bbox"""
    (startlat, endlat), (startlon, endlon) = bbox

    # Read in the forest age
    with nc.Dataset('./ForestAge.nc') as ds:
        lats = ds['lat'][:]
        lons = ds['lon'][:]
        
        # Clip the region
        startlat_idx, endlat_idx = np.argmin(np.abs(lats - startlat)), np.argmin(np.abs(lats - endlat))
        startlon_idx, endlon_idx = np.argmin(np.abs(lons - startlon)), np.argmin(np.abs(lons - endlon))
        endlat_idx += 1 # Including!
        endlon_idx += 1 # Including!

        # For some reason, the land-sea mask and the land-use types do not correspond. 
        # This makes sure they align.
        age = ds['forest_age'][startlat_idx: endlat_idx, startlon_idx: endlon_idx]

    return age

def get_spatial_correlation_regions(griddict, L_SCALE=1000):
    """Get the spatial (distance-based) correlation. Correlation decays exponentially with distance
    Input: 
        griddict: dict of {num: (lats, lons)}: region number and corresponding lats and lons
        L_SCALE: float-like: exponential decay lenght scale in km. Defaults to 1000km"""
    from great_circle_calculator.great_circle_calculator import distance_between_points
    # Assume we're on a globe. Potentially, we could softcode this.
    lats = np.linspace(-89.5, 89.5, 180)
    lons = np.linspace(-179.5, 179.5, 360)

    # Get the average lat and lon for each gridcell.
    # Note that the average is an approximation.
    grid_latslons = {}
    for k, (lats_g, lons_g) in griddict.items():
        grid_latslons[k] = (lats[lats_g].mean(), lons[lons_g].mean())

    # Get the distance between two griddict items
    distances = np.zeros((len(griddict), len(griddict)))
    for i, (k, (la1, lo1)) in enumerate(grid_latslons.items()):
        for j, (k, (la2, lo2)) in enumerate(grid_latslons.items()):
            # symmetric matrix
            if j<i: continue
            if i==j:
                distances[i, j] = 0.0
            else:
                res=distance_between_points((lo1, la1), (lo2, la2))
                distances[i, j] = res
                distances[j, i] = res 
                
    # Apply a lenght scale
    distance_weighing = np.exp(-1.0*distances/(L_SCALE * 1e3))
    # Everything over 4 * L_SCALE km is assumed to be 0
    distance_weighing[distances > 4 * L_SCALE * 1e3] = 0
    return distance_weighing

def get_land_use_lsm(bbox=[[-90, 90], [-180, 180]]):
    """Get the land use and land-sea mask given a bbox"""
    (startlat, endlat), (startlon, endlon) = bbox

    # Read in the land-sea mask
    with nc.Dataset('/projects/0/ctdas/awoude/CTE-SF-Europe/Data/SiB246/sib_global_200001.nc') as ds:
        lsm = np.where(ds['nee'][0] != 0., 1, np.nan)
    
    # Read in the land-use type
    with nc.Dataset('/projects/0/ctdas/input/ctdas_2012/regions.nc') as regionds:
        lats = regionds['lat'][:]
        lons = regionds['lon'][:]
        
        # Clip the region
        startlat_idx, endlat_idx = np.argmin(np.abs(lats - startlat)), np.argmin(np.abs(lats - endlat))
        startlon_idx, endlon_idx = np.argmin(np.abs(lons - startlon)), np.argmin(np.abs(lons - endlon))
        endlat_idx += 1 # Including!
        endlon_idx += 1 # Including!

        # For some reason, the land-sea mask and the land-use types do not correspond. 
        # This makes sure they align.
        lsm = lsm[startlat_idx: endlat_idx, startlon_idx: endlon_idx]
        land_use = np.ma.masked_invalid(np.ma.masked_less(regionds['land_ecosystems'][startlat_idx: endlat_idx, 
                                                                                      startlon_idx:endlon_idx].data * lsm, 0))
        land_use[(land_use.mask) & (~np.isnan(lsm.data))] = 19 # Set to water
        lsm2 = np.ma.masked_invalid(np.ma.filled(land_use * lsm, np.nan))
        lsm2 = lsm2.astype(bool)

    return lsm2, land_use

def is_same_land_use(griddict):
    """Check if two gridcells have the same land-use type and return as boolean 'correlation matrix'.
    Input:
        griddict: dict of {num: (lats, lons)}: region number and corresponding lats and lons
    Output:
        np.ndarray: 2d correlation matrix of Nregions x Nregions.
            This matrix is 1 if the two regions have the same land-use type, else 0.
    """
    lsm, land_use = get_land_use_lsm()
    # Get the land uses for all items in the griddict.
    land_use_flat = np.zeros((len(griddict)))
    for i, (k, (lat_idx, lon_idx)) in enumerate(griddict.items()):
        # We take the mode (most frequent) land-use type in the region.
        land_use_flat[i] = stats.mode(land_use[lat_idx, lon_idx])[0][0]
    
    # Here, we check if two gridcells have the same land-use type.
    land_use_flat_x, land_use_flat_y = np.meshgrid(land_use_flat, land_use_flat)
    same_land_use = land_use_flat_x == land_use_flat_y
    np.fill_diagonal(same_land_use, True)

    return same_land_use
    

def get_temperature_anomalies(bbox=[[-90, 90], [-180, 180]]):
    """Get the temperature anomalies on a 1x1 degree map"""
    (startlat, endlat), (startlon, endlon) = bbox

    with nc.Dataset('/home/awoude/t2m.anom.detrend_2000-2020.nc') as ds:
        lats = ds['lat'][:]
        lons = ds['lon'][:]
        
        startlat_idx, endlat_idx = np.argmin(np.abs(lats - startlat)), np.argmin(np.abs(lats - endlat))
        startlon_idx, endlon_idx = np.argmin(np.abs(lons - startlon)), np.argmin(np.abs(lons - endlon))
        endlat_idx += 1# Including!
        endlon_idx += 1# Including!
        t2m = ds['__xarray_dataarray_variable__'][:, startlat_idx: endlat_idx, startlon_idx:endlon_idx] 
    return t2m

def correlate_temperature(griddict):
    """Get the correlation coefficient between the temperature anomalies in different regions 
    Input:
        griddict: dict of {num: (lats, lons)}: region number and corresponding lats and lons
    Output:
        np.ndarray: 2d correlation matrix of Nregions x Nregions with the temperature correlation.
            Negative values are set to 0.
    """
    t2m = get_temperature_anomalies()
    # Assume we're on a globe. Potentially, we could softcode this.
    lats = np.linspace(-89.5, 89.5, 180)
    lons = np.linspace(-179.5, 179.5, 360)
    # Calculate the area, for giving different gridcells different weights.
    area = calc_area(lats, lons)
    # Get the temperature anomalies for each item in the griddict.
    t2m_flat = np.zeros(((len(t2m), (len(griddict)))))
    for i, (k, (lat_idx, lon_idx)) in enumerate(griddict.items()):
        a = area[lat_idx, lon_idx] # Per cell area of cells in the griddict
        t = t2m[:, lat_idx, lon_idx] # Per cell temperature anomalies of cells in the griddict
        t2m_flat[:, i] = np.average(t, weights=a, axis=1) # Calculate the weighted average.
    # Correlate the temperatures.
    corr_t2m = np.corrcoef(t2m_flat.T)
    corr_t2m[corr_t2m < 0] = 0
    return corr_t2m

def correlate_age(griddict):
    """Get the correlation coefficient between the forest age in different regions
    The correlation is based on relative distance in age.
    if cell a is 10 years old, and cell b 1 year old, the correlation is 0.1
    if cell a is 10 years old, and cell b 100 year old, the correlation is 0.1
    if cell a is 5 years old, and cell b 4 year old, the correlation is 0.8
    Hereby, we assume that the variability in younger forests is larger.
    Input:
        griddict: dict of {num: (lats, lons)}: region number and corresponding lats and lons
    Output:
        np.ndarray: 2d correlation matrix of Nregions x Nregions with the forest age correlation.
    """
    age = get_forest_age()
    # Assume we're on a globe. Potentially, we could softcode this.
    lats = np.linspace(-89.5, 89.5, 180)
    lons = np.linspace(-179.5, 179.5, 360)
    # Calculate the area, for giving different gridcells different weights.
    area = calc_area(lats, lons)
    # Get the age for each item in the griddict.
    age_flat = np.zeros(len(griddict))
    for i, (k, (lat_idx, lon_idx)) in enumerate(griddict.items()):
        a = area[lat_idx, lon_idx]
        t = age[lat_idx, lon_idx]
        age_flat[i] = np.average(t, weights=a)
    # Correlate the forest ages.
    corr_age = np.zeros((len(age_flat), len(age_flat)))
    for i, a_a in enumerate(age_flat):
        for j, a_b in enumerate(age_flat):
            corr = min(a_a, a_b) / max(a_a, a_b)
            corr_age[i, j] = corr
    corr_age = np.nan_to_num(corr_age, 0)
    np.fill_diagonal(corr_age, 1)
    return corr_age

if __name__ == '__main__':
    # print(cov_gaussian(1,0.5,1,cyclic=False))
    # bbox = [[33.5, 71.5], [-14.5, 34.5]]
    griddict = get_griddict(water=True)
    # key=list(griddict.keys())
    # print(griddict[key[0]])
    t=1000
    tcov=cov_gaussian(t,0.5,0.3,cyclic=False)
    # tc,names=get_transcom(griddict)
    spatcov=[get_spatial_correlation_regions(griddict,L_SCALE=2000)]
    # print(spatcov[0]f
    # exit()
    within=[3,4,9,11]
    # for i in within:
        # print(names[i-1])
    # tclist=[tc[key] for key in griddict.keys()]
    # spatcov=[] 
    # added=[]
    # covmat={}
    # for tcr in within: 
    #     tempgriddict={}
    #     for key in griddict.keys():
    #         if tc[key]==tcr:
    #             tempgriddict[key]=griddict[key]
        
    #     covmat[tcr]= get_spatial_correlation_regions(tempgriddict)

    # for i,val in enumerate(tclist):

    #     if val not in within:
    #         spatcov.append(np.array([[1]]))
    #     elif val not in added:
    #         spatcov.append(covmat[val])
    #         added.append(val)
    #  # print(griddict.keys())
    s=0
    for mat in spatcov:
        s+=np.shape(mat)[0]
    
    random_vectors=np.array([np.random.randn(s*t) for a in range(150)]).T
    # random_vectors2=random_vectors.copy()

    devmat=np.zeros((s*t,150) )
    # ensemble_kron(spatcov,tcov,random_vectors,devmat)
    # a=np.linalg.cholesky(tcov  )
    # b=sparse_cholesky(tcov)
    # print(a)
    # print(b)
    # print(b.toarray())
    # exit()
    devmat2=np.zeros((s*t,150) )
    ensemble_sparse_kron(spatcov[0],tcov,random_vectors,devmat2)

    print(np.allclose(devmat,devmat2)) 





    # print(devmat)
    # b=np.kron(corr_distance , tcov)
    # print(s,len(griddict.keys()))


    # print(np.array_equal(corr_distance,corr_distance.T))
    # corr_t2m = correlate_temperature(griddict)
    # corr_age = correlate_age(griddict)
    # same_land_use = is_same_land_use(griddict)
    # assert corr_distance.shape == corr_t2m.shape == corr_age.shape == same_land_use.shape
